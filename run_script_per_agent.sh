#!/bin/bash

declare -a confArray
confArray=("1" "1" "11" "11" "12" "12")

declare -a exptArray
exptArray=("1" "1" "1" "1" "1" "1")

declare -a agentArray
agentArray=("2" "2" "2" "2" "2" "2")

declare -a agIDArray
agIDArray=("0" "1" "0" "1" "0" "1")

declare -a estimateTFArray
estimateTFArray=("true")

NUMEXPTS=${#confArray[@]}

for ((i=0;i<$NUMEXPTS;i++)); do

CONF=${confArray[i]}
EXPT=${exptArray[i]}
NUMAGENTS=${agentArray[i]}
AGENT=${agIDArray[i]}
ESTIMATETF=${estimateTFArray[i]}

GAMMA="0.9"
MAXDELTA="0.1"
MAXMDPITERS="50"
MAXITERATIONS="24"
HORIZONS="6"
MAXSTAGES="100"
MAXTRIALS="30"
TEMPVAR="10"

CLSPTH="bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar"
DIRSIM="output/Simulations"
DIRCONF="$DIRSIM/Config_"$CONF"_Results"
DIREXPT="$DIRCONF/Experiment_$EXPT"
DIRDUMPS="$DIREXPT/Dumps"
DIRTEMP="$DIRDUMPS/Temp"

clear
echo "Running $NUMAGENTS-agent Wildfire Domain - Experiment $EXPT, Config $CONF, Agent $AGENT"

echo "Creating required directories.."
if [ ! -d "$DIRSIM" ]; then
  mkdir $DIRSIM
fi
if [ ! -d "$DIRCONF" ]; then
  mkdir $DIRCONF
fi
if [ ! -d "$DIREXPT" ]; then
  mkdir $DIREXPT
fi
if [ ! -d "$DIRDUMPS" ]; then
  mkdir $DIRDUMPS
fi
if [ ! -d "$DIRTEMP" ]; then
  mkdir $DIRTEMP
fi

echo "Starting nestedMDP planning..with params: $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT $AGENT"
java -cp $CLSPTH nestedMDPSolver/NestedVI_per_agent $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT $AGENT > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_NestedMDP_experiment"$EXPT"_"$MAXMDPITERS"iters_agent"$AGENT".txt

echo "Starting iPBVI planning..with params: $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT $AGENT"
java -cp $CLSPTH iPOMDPLiteSolver/iPBVI_per_agent $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT $AGENT > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_iPBVI_experiment"$EXPT"_"$HORIZONS"h-"$MAXITERATIONS"iters_agent"$AGENT".txt


done

