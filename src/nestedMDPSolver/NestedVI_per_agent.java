package nestedMDPSolver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;

import posg.POSGDomain;
import common.RandomPolicy;
import common.StateEnumerator;
import datastructures.QueueLinkedList;
import domains.wildfire.*;
import domains.wildfire.Wildfire.*;
import burlap.behavior.policy.*;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.behavior.singleagent.MDPSolver;
import burlap.behavior.singleagent.planning.Planner;
import burlap.behavior.stochasticgames.agents.SetStrategySGAgent.SetStrategyAgentFactory;
import burlap.behavior.valuefunction.*;
import burlap.behavior.valuefunction.ValueFunctionInitialization.ConstantValueFunctionInitialization;
import burlap.debugtools.*;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.*;
import burlap.oomdp.core.states.State;
import burlap.oomdp.statehashing.*;
import burlap.oomdp.stochasticgames.*;
import burlap.oomdp.stochasticgames.agentactions.*;

public class NestedVI_per_agent extends MDPSolver implements Planner, QFunction{

	public static final String WF = "wf";
	public static final String GG = "gg";
	public static final String CN = "cn";
	private static boolean finite = true;
	public enum dom {WF,GG,CN};
	protected static Set<HashableState> states = new HashSet<HashableState>();
	protected static double maxDelta;
	protected static int maxIterations;
	protected static int debugCode = 23456789;
	protected static boolean verbosemode;
	protected static boolean testingmode;
	protected static boolean debugmode = false;
	protected Map<HashableState, Double> valueFunction;
	protected ValueFunctionInitialization vinit;
	protected static JointActionModel jaModel;
	protected static JointReward jr;
	protected World w;
	protected static TerminalFunction tf;
	protected static double gamma;
	protected static HashableStateFactory hashingFactory;
	private static Map<String, SGAgentType> agentDefinitions;
	protected boolean planningStarted = false;
	protected int currentAgentIndex;
	protected String currentAgentName;
	protected Map<Integer, Policy> otherAgentsPolicyMap;
	protected int maxLevel;
	protected static int numPlayers;
	protected static Map<Integer, String> agIDtoName;
	protected static SGDomain domain;
	protected static StateEnumerator senum;
	private static boolean initNestedVI = false;
	public static boolean doreach = false;
	protected static List<Integer> agentsInWorld;

	
	
	
	@Override
	public List<QValue> getQs(State s) {
		
		String actingAgent = agIDtoName.get(this.currentAgentIndex);
		
		List<SGAgentAction> allsgActions = agentDefinitions.get(actingAgent).actions;
		List<GroundedSGAgentAction> allAgActions = SGtoGroundedSG(allsgActions,actingAgent);
		
		if(testingmode){
			dprint(actingAgent);
			printList(allAgActions);
		}
		List<QValue> qs = new ArrayList<QValue>(allAgActions.size());
		
		for(GroundedSGAgentAction ga : allAgActions){
			qs.add(this.getQ(s, ga));
		}
		if(testingmode){
			dprint("got all Q(S,a) values");
		}
		
		return qs;
	}
	
	@Override
	public QValue getQ(State s, AbstractGroundedAction ga) {
		
		List<JointAction> allJAs = JointAction.getAllJointActions(s,agentDefinitions);
		Map<GroundedSGAgentAction, List<JointAction>> gaJAMap = getGAJAMap(s, allJAs, this.currentAgentIndex);
		List<JointAction> jaListforGA = gaJAMap.get(ga);

		double sumQ = 0.;
		for(JointAction ja: jaListforGA){
			double cumOthersJAProb = 1.;
			
			for(int i:agentsInWorld){
				if(otherAgentsPolicyMap.containsKey(i)){
					String actingAgent = agIDtoName.get(i);
					State otherAgentState = Wildfire.createOtherAgentStateFromLocalState((WildfireDomain) domain,i, s);
					cumOthersJAProb *= this.otherAgentsPolicyMap.get(i).getProbOfAction(otherAgentState, ja.action(actingAgent));
				}
			}
			sumQ += cumOthersJAProb * getQValueForAgentJA(s,ja);
		}
		
		QValue q = new QValue(s, ga, sumQ);
		return q;
	}
	
	public Double getQValueForAgentJA(State s, JointAction ja) {
		
		JointActionTransitions jat = new JointActionTransitions(s, ja);
		
		double sumQ = 0.;
		if(!tf.isTerminal(s)){
			List<TransitionProbability> tps = jat.tps;
			for(int i = 0; i < tps.size(); i++){
				TransitionProbability tp = tps.get(i);
				double p = tp.p;
				HashableState sh = hashingFactory.hashState(tp.s);
				double r = jat.jrs.get(i).get(agIDtoName.get(this.currentAgentIndex));
				double vprime = this.getValue(sh);
				
				double contribution = r + gamma*vprime;
				double weightedContribution = p*contribution;
				
				sumQ += weightedContribution;				
			}			
		}
			
		return sumQ;
	}
	
	public boolean performReachabilityFrom(State initialState) {
		MyTimer rtimer = new MyTimer();
		rtimer.start();
		HashableState hashedInitialState = hashingFactory.hashState(initialState);
		if(states.contains(hashedInitialState)){
			return false;
		}
		states.add(hashedInitialState);
		LinkedList<HashableState> openQueue = new LinkedList<HashableState>();
		openQueue.add(hashedInitialState);
		System.out.print("Finding reachable states..");
		while(openQueue.size() > 0){
			
			HashableState sh = openQueue.poll();
			
			//expand
			List<JointAction> jas = JointAction.getAllJointActions(sh.s, agentDefinitions);
			for(JointAction ja : jas){
				List<TransitionProbability> tps = jaModel.transitionProbsFor(sh.s, ja);
				for(TransitionProbability tp : tps){
					HashableState shp = hashingFactory.hashState(tp.s);
					if(!states.contains(shp)){
						states.add(shp);
						openQueue.add(shp);
					}
				}
			}
			if(states.size()%1000 == 0){
				System.out.print(".");
			}
		}
		rtimer.stop();
		System.out.println("[Done] in "+rtimer.getTime()+"s; " + states.size() + " unique states found.");
		return true;
		
	}
	
	private double backupAgentValueFunction(State s) {
		List<QValue> QValues = getQs(s);
		
		if(testingmode){
			dprint("Got Qs (inside backupAgentValueFunction)");
		}
		HashableState sh = hashingFactory.hashState(s);
		double maxChange = Double.NEGATIVE_INFINITY;
		double maxQ = Double.NEGATIVE_INFINITY;
		
		double oldVal = getValue(sh);
		
		for(QValue qv : QValues){
			maxQ = Math.max(maxQ, qv.q);
		}
		double newVal = maxQ;
		
		maxChange = Math.max(maxChange, Math.abs(newVal-oldVal));
		setValue(sh, newVal);
		return maxChange;
	}
	
	@Override
	public GreedyQPolicy planFromState(State initialState) {
		
		HashableState hashedInitialState = hashingFactory.hashState(initialState);
		if(this.valueFunction.containsKey(hashedInitialState)){
			return new GreedyQPolicy(this); //already performed planning here!
		}
		
		int maxIter = 10000000;
		if(finite){
			if(!doreach) doreach = this.perform_H_StepReachabilityFrom(initialState);
			maxIter = maxIterations; 
		}else{
			if(!doreach) doreach = this.performReachabilityFrom(initialState);
			maxIter = maxIterations;
		}
		
		if(doreach){
			int iter=0;
			double maxChange;
			MyTimer timer = new MyTimer();
			do{
				timer.start();
				maxChange = Double.NEGATIVE_INFINITY;
				for(HashableState sh : states){
					if(!this.valueFunction.containsKey(sh)){
						setValue(sh, this.vinit.value(sh.s));
					}	
					double change = this.backupAgentValueFunction(sh.s);
					maxChange = Math.max(change, maxChange);
				}
				timer.stop();
				
				DPrint.cl(NestedVI_per_agent.debugCode , "Finished VI backup: "+ iter +" in: "
						+timer.getTime()+ "s w/ max change: " + maxChange);
				iter++;
			}while((maxChange >= maxDelta) && (iter < maxIter));
			DPrint.cl(NestedVI_per_agent.debugCode, "Performed " + iter + " backups..in "
					+timer.getTotalTime()+"s Avg time/backup = "+timer.getAvgTime()+"s");
		}
		
		return new GreedyQPolicy(this);
	}

	
	// TODO Main Method 
	public static void main(String[] args) {
		
		//example command line argument
		/*
		 * order of command line arguments: gamma maxDelta maxIterations conf experiment
		 * 
		 * [mkran@erebus oasys-git]$  java -cp "bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar" nestedMDPSolver/NestedVI 0.9 0.1 50 1 5 > output/Simulations/Config_1_Results/Experiment_5/RUN_2-WF_config1_NestedMDP_experiment5_50iters.txt
		 * 
		 */
		
		dom dom = NestedVI_per_agent.dom.WF;
		
		int maxLevel = 0;
		double gamma = 0;
		double maxDelta = 0;
		int maxIterations = 0;
		int conf = 0;
		int experiment = 0;
		int agent = 0;
		
		if(args.length == 6){
			gamma = Double.parseDouble(args[0]);
			maxDelta = Double.parseDouble(args[1]);
			maxIterations = Integer.parseInt(args[2]);
			conf = Integer.parseInt(args[3]);
			experiment = Integer.parseInt(args[4]);
			agent = Integer.parseInt(args[5]);
		}
		
		//Testing arguments
//		System.out.print(gamma + " " + maxDelta + " " + maxIterations + " " + conf + " " + experiment);
		
		
		
		State ms = null;
		SGDomain d = null;
		int numPlayers = 0;
		JointReward jr = null;
		TerminalFunction tf = null;
		List<Map<String, SGAgentType>> allAgentDefinitions = new ArrayList<Map<String, SGAgentType>>();
		Map<Integer, String> agIDtoName = new HashMap<Integer, String>();
		Map<Integer,Map<Integer,Policy>> agFinalPolicyMap = new HashMap<Integer,Map<Integer,Policy>>();
		
		
		MyTimer senumTimer = new MyTimer();
		
		boolean vmode = true;
		boolean dmode = false;
		boolean tmode = false;
		boolean finite = false;
		
		verbosemode = vmode;
		testingmode = tmode;
		debugmode = dmode;
		DPrint.toggleCode(debugCode,verbosemode);
		
		if(dom.equals(NestedVI_per_agent.dom.WF)){
			
			System.out.println("Generating the domain..");
			Wildfire wf = new Wildfire();
			d = (WildfireDomain) wf.generateDomain(conf);
			jr = new WildfireJointRewardFunction();
			tf = new NullTermination();
			numPlayers = WildfireParameters.numAgents;
			
			String outFileHead = "output/Simulations/Config_"+WildfireParameters.config+"_Results/Experiment_"+experiment+"/";
			
			//1. Get agent definitions
			System.out.println("Getting all agent definitions..");
			allAgentDefinitions = Wildfire.getAllAgentDefs((WildfireDomain) d);
			
			//2. Get agent id-name mapping
			System.out.println("Getting agent id-name mappings..");
			for(int n=0;n<numPlayers;n++){
				agIDtoName.put(n, Wildfire.agentName(n));
			}
			
			//3. Get initial state
			ms = Wildfire.getInitialMasterState((WildfireDomain) d);
			
//			for(int currentAgentIndex = 0; currentAgentIndex < numPlayers; currentAgentIndex++){
			int currentAgentIndex = agent;
				System.out.println("---------------------------------------------------");
				System.out.println("                  Subject agent: "+currentAgentIndex);
				System.out.println("---------------------------------------------------");
				
				//1. find neighbors of subject agent
				int[] neighbors = ((WildfireDomain) d).neighbors[currentAgentIndex];
				int numNeighbors = neighbors.length;
				
				List<Integer> agentsInWorld = new ArrayList<Integer>();
				agentsInWorld.add(currentAgentIndex);
				for(int i=0;i<numNeighbors;i++){
					agentsInWorld.add(neighbors[i]);
				}
				int numAgentsInSubjectAgentWorld = agentsInWorld.size();
				System.out.println("Number of agents in "+currentAgentIndex+"'s world: "+ numAgentsInSubjectAgentWorld);
				
				//2. set agent definitions of agents in the local neighborhood of subject agent
				System.out.println("Getting agent definitions for all agents in subject agent's world..");
				Map<String, SGAgentType> agentDefinitions = allAgentDefinitions.get(currentAgentIndex);
				
				//3. create state of subject agent from master state
				System.out.println("Getting subject agent's initial state..");
				State si = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, currentAgentIndex, ms);
				
				//4. Setting state enumerator for subject agent's world..
				System.out.println("Setting state enumerator for subject agent's world..");
				HashableStateFactory hf = new SimpleHashableStateFactory(true,false);
				((WildfireDomain) d).setStateEnumerator(new StateEnumerator(d, hf, agentDefinitions));
				
				System.out.println("Getting state enumerator for subject agent's world..");
				StateEnumerator senum = ((POSGDomain) d).getStateEnumerator();
				
				//5. Caching all enumerated states..
				senumTimer.start();
				states = new HashSet<HashableState>();
				
				System.out.println("Caching all enumerated states reachable from subject agent's initial state..");
				if(finite){
					senum.findReachableStatesAndEnumerate(si,maxIterations); //remove horizons later
				}else{
					senum.findReachableStatesAndEnumerate(si);
				}
				int nS = senum.numStatesEnumerated();
				for(int i=0;i<nS;i++){
					if(i%1000 == 0){
						System.out.print(".");
					}
					State st = senum.getStateForEnumerationId(i);
					HashableState hashedST = hf.hashState(st);
					states.add(hashedST);
				}
				senumTimer.stop();
				doreach = true;
				System.out.println("[Done] in "+senumTimer.getTime()+"s; "+states.size()+" unique states found.");
				
				Map<Integer,Policy> neighborPolicies = new HashMap<Integer,Policy>();
				
				for(int neighborAgent: neighbors){
					int neighborType = ((WildfireDomain) d).agentTypes[neighborAgent]; 
					System.out.println("Getting neighbor agent "+neighborAgent+"'s local state from subject agent "+currentAgentIndex+"'s world..");
					State s_i = Wildfire.createOtherAgentStateFromLocalState((WildfireDomain) d,neighborAgent, si);
//					System.out.println(s_i.getCompleteStateDescription());
					
					System.out.println("Planning from neighbor agent "+neighborAgent+"'s local state.."); 
					Policy nPol = NestedVI_per_agent.runVI(s_i,d,senum,agentDefinitions,agIDtoName,jr,tf,hf,
							gamma,maxDelta,maxIterations,maxLevel,agentsInWorld,neighborAgent,finite);
					
					neighborPolicies.put(neighborAgent,nPol);
					
					System.out.println("Writing neighbor agent "+neighborAgent+"'s policy to file.."); 
					//Writing policy to file
					SetStrategyAgentFactory agentFactory = new SetStrategyAgentFactory(d, nPol);
					SGAgent MDPagent = agentFactory.generateAgent();
					World w = new World(d, jr, tf, s_i);
					MDPagent.joinWorld(w, agentDefinitions.get(agIDtoName.get(neighborAgent)));
					
					String filename = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_nmdp_policy_level"+maxLevel+"_agent"+currentAgentIndex+"_neighbor"+neighborAgent+"-"+neighborType+"_iter"+maxIterations+".txt";
					
					try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)))) {
						int c=0;
						for(HashableState state: states){
							GroundedSGAgentAction mdpaction = w.getRegisteredAgents().get(0).getAction(state.s);
							List<ActionProb> aps = new ArrayList<ActionProb>();
							aps = nPol.getActionDistributionForState(state.s);
							String apsString = "";
							String actionStr = "";
							for(ActionProb ap: aps){
								if(c<1) actionStr += ap.ga.actionName()+",";
								apsString += Double.toString(ap.pSelection) +","; 
							}
							if(c<1) actionStr += "\n";
							out.println(actionStr+senum.getEnumeratedID(state)+":"+mdpaction.actionName()+":"+apsString);
							c++;
							
						}
						System.out.println("Neighbor agent "+neighborAgent+"'s policy is stored in file.."+filename); 
						
					}catch (IOException e) {
					    
					}
					
					System.out.println("The policy maps "+states.size()+" states to a distribution over "+readMDPActionNameToID(filename).size()+" actions!");
					
					
				}
				//2. Initialize nestedMDP planner
//				Policy agFinalPolicy = NestedVI.runVI(si,d,senum,agentDefinitions,agIDtoName,jr,tf,hf,
//						gamma,maxDelta,maxIterations,maxLevel,agentsInWorld,currentAgentIndex,finite);
				
				
				
//				writeActions(d,senum,mdpActions ,"output/nestedMDP_actions_agent"+currentAgentIndex+"_iter"+maxIterations+".txt");
				
				agFinalPolicyMap.put(currentAgentIndex,neighborPolicies);
				
			}
			System.out.println("Number of Agents: "+ agFinalPolicyMap.size());
			for(int i=0;i<numPlayers;i++){
				if(agFinalPolicyMap.containsKey(i)){
					System.out.println("Agent ["+i+"] has " + agFinalPolicyMap.get(i).size()+" neighbor policies..");
				}
			}
			
			
//		}
		
	}
	
	public static Map<String,Integer> readMDPActionNameToID(String filename) {
		
		Map<String,Integer> actionNameToID = new HashMap<String,Integer>();
		try {
			Scanner s = new Scanner(new File(filename));
			String actionNames = s.nextLine();
			String[] actionNamesArray = actionNames.split(",");
			for(int i=0;i<actionNamesArray.length;i++){
				actionNameToID.put(actionNamesArray[i], i);
			}
			
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return actionNameToID;
	}
	
	@SuppressWarnings("unused")
	private static void writeActions(SGDomain d, StateEnumerator senum, Map<Integer, GroundedSGAgentAction> mdpActions, String filename) {
		boolean writeToFile = true;
		if(writeToFile){
			//Write alphavector actions
			try {
				File file = new File(filename);
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				
				String content = "";//Integer.toString(alphaVectorActions .size());
				for(int i=0;i<senum.numStatesEnumerated();i++){
					content = content + Integer.toString(i) + "," + mdpActions.get(i).actionName() + "\n" ;
				}
				
				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.close();
				
				System.out.println("Done writing MDP Policy Actions..");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	

	public static Policy runVI(State s, SGDomain d, StateEnumerator senum1, Set<HashableState> hstates,
			Map<String, SGAgentType> agentDefs, Map<Integer, String> agIDtoN, 
			JointReward jr1, TerminalFunction tf1, HashableStateFactory hf, 
			double gamma1, double maxDelta1, int maxIterations1, int maxLevel, List<Integer> agentsInWorld,
			int numP, int currentAgentIndex, boolean finite1) {
		
		initNestedVI = initNestedVI(d, senum1, agentDefs, agIDtoN, jr1, tf1, hf, 
				gamma1, maxDelta1, maxIterations1, agentsInWorld, numP, finite1);
		
		states.addAll(hstates);
		System.out.println("Retrieving cached states.. "+states.size()+" unique states found.");
		doreach = true;
		
		//The following is just used to generate uniformly random policies of others for level-0 planning
		Map<Integer,Policy> otherAgentsPoliciesForLvl0Planning = new HashMap<Integer,Policy>();
		otherAgentsPoliciesForLvl0Planning = generateOthersUniformPolicy(d,s,currentAgentIndex);
		
		//The main method for computing all level policies for all agents from state
		Map<Integer,Map<Integer,Policy>> allLevelPolicyList = new HashMap<Integer,Map<Integer,Policy>>();
		allLevelPolicyList = computeAgLvlPoliciesFromState(s, currentAgentIndex, otherAgentsPoliciesForLvl0Planning, maxLevel);
		
		//Print all the level policies for all the agents for state
//				if(verbosemode){
//					printAllLevelPolicyListForState(s,d,w,allLevelPolicyList);	
//				}
		
		return allLevelPolicyList.get(maxLevel).get(currentAgentIndex);
	}

	
	public static Policy runVI(State s, SGDomain d, StateEnumerator senum1,
			Map<String, SGAgentType> agentDefs, Map<Integer, String> agIDtoN, 
			JointReward jr1, TerminalFunction tf1, HashableStateFactory hf, 
			double gamma1, double maxDelta1, int maxIterations1, int maxLevel, 
			List<Integer> agentsInWorld1, int currentAgentIndex, boolean finite1) {
		
		int numP = agentsInWorld1.size();
		initNestedVI = initNestedVI(d, senum1, agentDefs, agIDtoN, jr1, tf1, hf, 
				gamma1, maxDelta1, maxIterations1, agentsInWorld1, numP, finite1);
		
		//The following is just used to generate uniformly random policies of others for level-0 planning
		Map<Integer,Policy> otherAgentsPoliciesForLvl0Planning = new HashMap<Integer,Policy>();
		otherAgentsPoliciesForLvl0Planning = generateOthersUniformPolicy(d,s,currentAgentIndex);
		
		//The main method for computing all level policies for all agents from state
		Map<Integer,Map<Integer,Policy>> allLevelPolicyList = new HashMap<Integer,Map<Integer,Policy>>();
		allLevelPolicyList = computeAgLvlPoliciesFromState(s, currentAgentIndex, otherAgentsPoliciesForLvl0Planning, maxLevel);
		
		//Print all the level policies for all the agents for state
//				if(verbosemode){
//					printAllLevelPolicyListForState(s,d,w,allLevelPolicyList);	
//				}
		
		return allLevelPolicyList.get(maxLevel).get(currentAgentIndex);
	}
	
	private static Map<Integer, Map<Integer, Policy>> computeAgLvlPoliciesFromState(State s, 
			int subjectAgentIndex, Map<Integer, Policy> otherAgentsPoliciesForLvl0Planning, int maxLevel) {
		
		if(maxLevel==0){ 
			
			Map<Integer,Policy> agPolicyList = new HashMap<Integer,Policy>();
			Map<Integer,Map<Integer,Policy>> allLevelAgPolicyList = new HashMap<Integer,Map<Integer,Policy>>();
			
			NestedVI_per_agent nVI = new NestedVI_per_agent(new ValueFunctionInitialization.ConstantValueFunctionInitialization(0.0), 
						subjectAgentIndex, otherAgentsPoliciesForLvl0Planning, maxLevel);
			
			Policy agentPol = nVI.planFromState(s);
			Policy cAgentPol = new CachedPolicy(hashingFactory,agentPol);
			agPolicyList.put(subjectAgentIndex, cAgentPol);
			
			allLevelAgPolicyList.put(maxLevel, agPolicyList);
			return allLevelAgPolicyList;
			
		}
		
		//TODO: For max level > 0 there might be some problems below regarding "state" to plan from..fix this when needed
		
		ArrayList<Integer> agLvlList;
		HashMap<Integer, ArrayList<Integer>> agLvlMap = new HashMap<Integer, ArrayList<Integer>>();
		
		
		for(int ag:agentsInWorld){
			agLvlList = new ArrayList<Integer>();
			for(int i=0;i<=maxLevel;i++){
				if (ag == subjectAgentIndex){
					if (i==(maxLevel-1)) continue;
					agLvlList.add(i);
				}else{
					if (i==maxLevel) continue;
					agLvlList.add(i);
				}
			}
			agLvlMap.put(ag, agLvlList);
		}
		
		if(verbosemode){
			printAgLvlMap(agLvlMap);
		}
		
		Map<Integer,Map<Integer,Policy>> allLevelAgPolicyList = new HashMap<Integer,Map<Integer,Policy>>();
		
		for(int level=0; level<=maxLevel; level++){
			if (level == 0){
				Map<Integer,Policy> agPolicyList = new HashMap<Integer,Policy>();
				for(int agentID:agentsInWorld){
					if(agLvlMap.get(agentID).contains(level)){
						dprint("Computing Agent "+Integer.toString(agentID)+"'s Level-"+Integer.toString(level)+" policy:");
						NestedVI_per_agent nVI = new NestedVI_per_agent(new ValueFunctionInitialization.ConstantValueFunctionInitialization(0.0), 
								agentID, otherAgentsPoliciesForLvl0Planning, maxLevel);
						
						System.out.println("Number of other agent lvl 0 policies: "+Integer.toString(otherAgentsPoliciesForLvl0Planning.size()));
						Policy agPol = null;
//						if(agentID==subjectAgentIndex){
//							agPol = nVI.planFromState(s);
//						}else{
//							State oState = Wildfire.createOtherAgentStateFromLocalState((WildfireDomain) domain,agentID, s);
//							agPol = nVI.planFromState(oState);
//						}
						agPol = nVI.planFromState(s);
//						Policy cAgentPol = new CachedPolicy(hashingFactory,agPol);
						agPolicyList.put(agentID, agPol);
					
					}else{
						continue;
					}
				}
				allLevelAgPolicyList.put(level,agPolicyList);	
			
			}else{
				Map<Integer,Policy> agPolicyList = new HashMap<Integer,Policy>();
				for(int agentID:agentsInWorld){
					if(agLvlMap.get(agentID).contains(level)){
						dprint("Computing Agent "+Integer.toString(agentID)+"'s Level-"+Integer.toString(level)+" policy:");
						//get all other agent's policies at immediate lower level l-1
						Map<Integer,Policy> allOtherAgPolicyList = new HashMap<Integer,Policy>();
						for(Map.Entry<Integer,Policy> e:allLevelAgPolicyList.get(level-1).entrySet()){
							if(e.getKey() == agentID){
								continue;
							}
							allOtherAgPolicyList.put(e.getKey(),e.getValue());
						}
						
						NestedVI_per_agent nVI = new NestedVI_per_agent(new ValueFunctionInitialization.ConstantValueFunctionInitialization(0.0), 
								agentID, allOtherAgPolicyList, maxLevel);
						
						Policy cAgentPol = new CachedPolicy(hashingFactory,nVI.planFromState(s));
						agPolicyList.put(agentID, cAgentPol);
						
					}else{
						continue;
					}
				}
				allLevelAgPolicyList.put(level,agPolicyList);	
			}
		}
		
		return allLevelAgPolicyList;
	}

	public static Map<Integer,Policy> generateOthersUniformPolicy(SGDomain d, State s, Integer subjectAgent) {
		Map<Integer,Policy> otherAgentsPoliciesForLvl0Planning = new HashMap<Integer,Policy>();
		
		if(numPlayers > 1){
			for(int agentID:agentsInWorld){
				if(agentID == subjectAgent){
					continue;
				}
				String actingAgent = agIDtoName.get(agentID);
				
				List<SGAgentAction> allsgActions = agentDefinitions.get(actingAgent).actions;
				List<GroundedSGAgentAction> allAgActions = SGtoGroundedSG(allsgActions,actingAgent);
				
				if(debugmode){
					System.out.println("GroundedSGAgentActions for "+actingAgent+":");
					printList(SGAgentAction.getAllApplicableGroundedActionsFromActionList(s, actingAgent, d.getAgentActions()));
					System.out.println("SGAgentActions for "+actingAgent+":");
					printList3(allsgActions);
				}
				
				Policy randPolicy = new RandomPolicy(d, allAgActions);
				otherAgentsPoliciesForLvl0Planning.put(agentID,randPolicy);
			}
		}
		if(numPlayers != (otherAgentsPoliciesForLvl0Planning.size()+1)){
			dprint("The number of other agent policies -"+otherAgentsPoliciesForLvl0Planning.size()+"- doesn't makes sense");
		}
		
		return otherAgentsPoliciesForLvl0Planning;
	}
	
	
	public static void printAgentPolicyForState(State s, StateEnumerator senum, Policy agPolicy) {
		System.out.print("State ["+senum.getEnumeratedID(s)+"]  ");
		for (int a=0; a<agPolicy.getActionDistributionForState(s).size(); a++){
			System.out.print(" "+agPolicy.getActionDistributionForState(s).get(a).pSelection);
		}
		System.out.println();
	}
	
		
	public NestedVI_per_agent(ConstantValueFunctionInitialization vinit, int subjectAgentIndex, 
			Map<Integer, Policy> otherAgentsPolicies, int maxLevel2) {
		
		if(initNestedVI){
			this.valueFunction = new HashMap<HashableState, Double>();
			this.vinit = vinit;
			this.currentAgentIndex = subjectAgentIndex;
			this.currentAgentName = agIDtoName.get(this.currentAgentIndex);
			this.otherAgentsPolicyMap = otherAgentsPolicies;
			this.maxLevel = maxLevel2;
		}else{
			System.out.println("NestedVI planner not initialized..");
			System.exit(0);
		}
	}

	public static boolean initNestedVI(SGDomain d, StateEnumerator senum1, Map<String, SGAgentType> agentDefs, 
			Map<Integer, String> agIDtoN, JointReward jr1, TerminalFunction tf1, 
			HashableStateFactory hashingFactory1, double gamma1, double maxDelta1, 
			int maxIterations1, List<Integer> agentsInWorld1, int numP, boolean finite1) {
		
		domain = d;
		senum = senum1;
		agentDefinitions = agentDefs;
		agIDtoName = agIDtoN;
		jr = jr1;
		tf = tf1;
		hashingFactory = hashingFactory1;
		gamma = gamma1;
		maxDelta = maxDelta1;
		maxIterations = maxIterations1;
		agentsInWorld = agentsInWorld1;
		numPlayers = numP;
		finite = finite1;
		jaModel = d.getJointActionModel();
		
		return true;
	}
	

	@Override
	public void resetSolver() {}

	@Override
	public double value(State s) {
		Double d = this.valueFunction.get(hashingFactory.hashState(s));
		if(d == null){
			return vinit.value(s);
		}
		return d;
	}
	
	public static List<GroundedSGAgentAction> SGtoGroundedSG(
			List<SGAgentAction> allsgActions, String actingAgent) {
		List<GroundedSGAgentAction> allAgActions = new ArrayList<GroundedSGAgentAction>();
		for(int i=0;i<allsgActions.size();i++){
			allAgActions.add(allsgActions.get(i).getAssociatedGroundedAction(actingAgent));
		}
		return allAgActions;
	}
	
	public double getValue(HashableState sh){
		Double stored = this.valueFunction.get(sh);
		if(stored != null){
			return stored;
		}
		double v = 0.;
		if(!tf.isTerminal(sh.s)){
			v = this.vinit.value(sh.s);
		}
		this.valueFunction.put(sh, v);
		return v;
	}
	
	public void setValue(HashableState sh, double v){
		this.valueFunction.put(sh, v);
	}
	
	public class JointActionTransitions{
		public JointAction ja;
		public List<TransitionProbability> tps;
		public List<Map<String, Double>> jrs;
		
		/**
		 * Generates the transition information for the given state and joint aciton
		 * @param s the state in which the joint action is applied
		 * @param ja the joint action applied to the given state
		 */
		public JointActionTransitions(State s, JointAction ja){
			this.ja = ja;
			this.tps = jaModel.transitionProbsFor(s, ja);
			this.jrs = new ArrayList<Map<String,Double>>(this.tps.size());
			for(TransitionProbability tp : this.tps){
				Map<String, Double> jrr = jr.reward(s, ja, tp.s);
				this.jrs.add(jrr);
			}
		}
		
		public JointActionTransitions(State s, JointAction ja, List<TransitionProbability> tps){
			this.ja = ja;
			this.tps = tps;
			this.jrs = new ArrayList<Map<String,Double>>(tps.size());
			for(TransitionProbability tp : tps){
				Map<String, Double> jrr = jr.reward(s, ja, tp.s);
				this.jrs.add(jrr);
			}
		}	
	}
	
	public boolean perform_H_StepReachabilityFrom(State initialState) {
		MyTimer rtimer = new MyTimer();
		rtimer.start();
		HashableState hashedInitialState = hashingFactory.hashState(initialState);
		if(states.contains(hashedInitialState)){
			return false;
		}
		
		states.add(hashedInitialState);
		QueueLinkedList<HashableState> openQueue = new QueueLinkedList<HashableState>();
		openQueue.add(hashedInitialState);
		
		int h=0;
		System.out.print("Finding reachable states..");
		while(openQueue.size() > 0){
			HashableState sh = openQueue.poll();
			h++;
			List<JointAction> jas = JointAction.getAllJointActions(sh.s, agentDefinitions);
			for(JointAction ja : jas){
				List<TransitionProbability> tps = jaModel.transitionProbsFor(sh.s, ja);
				for(TransitionProbability tp : tps){
					HashableState shp = hashingFactory.hashState(tp.s);
					if(!states.contains(shp) && (h <= maxIterations)){
						states.add(shp);
						openQueue.add(shp);
					}
				}
			}
			if(states.size()%1000 == 0){
				System.out.print(".");
			}
		}
		rtimer.stop();
		System.out.println("[Done] in "+rtimer.getTime()+"s; " + states.size() + " unique states found.");
		return true;
		
	}
	
	
	public static void dprint(String toPrint){
		DPrint.cl(NestedVI_per_agent.debugCode, toPrint);
	}
	
	public void setAgentDefinitions(Map<String, SGAgentType> agentDefs){
		
		if(this.planningStarted){
			throw new RuntimeException("Cannot reset the agent definitions after planning has already started.");
		}
		
		if(agentDefinitions == null){
			return;
		}
		
		if(agentDefinitions == agentDefs){
			return ;
		}
		
		agentDefinitions = agentDefs;
		
	}
	
	public List<GroundedSGAgentAction> getAllGroundedActionsForAgentID(State s, int agentIndex){
		String actingAgent = agIDtoName.get(agentIndex);
		List<SGAgentAction> allsgActions = agentDefinitions.get(actingAgent).actions;
		List<GroundedSGAgentAction> allAgActions = SGtoGroundedSG(allsgActions,actingAgent);
		return allAgActions;
	}
	
	public Map<GroundedSGAgentAction,List<JointAction>> getGAJAMap(State s, List<JointAction> allJAs, int agentIndex){
		Map<GroundedSGAgentAction,List<JointAction>> gaJAMap = new HashMap<GroundedSGAgentAction,List<JointAction>>();
		List<GroundedSGAgentAction> allAgActions = getAllGroundedActionsForAgentID(s,agentIndex);
		
		for(GroundedSGAgentAction ga:allAgActions){
			gaJAMap.put(ga, getJAListforGA(ga,allJAs,agentIndex));
		}
		
		return gaJAMap;
	}
	
	public List<JointAction> getJAListforGA(AbstractGroundedAction ga, List<JointAction> allJAs, int agentIndex) {
		List<JointAction> jaList = new ArrayList<JointAction>();
		for(JointAction ja: allJAs){
			String actingAgent = agIDtoName.get(agentIndex);
			if(ja.action(actingAgent).action.actionName == ga.actionName()){
				jaList.add(ja);
			}
		}
		return jaList;
	}


	public static void printAgLvlMap(HashMap<Integer, ArrayList<Integer>> hmap) {
		Set<Entry<Integer, ArrayList<Integer>>> set = hmap.entrySet();
	  	Iterator<Entry<Integer, ArrayList<Integer>>> iterator = set.iterator();
	  	while(iterator.hasNext()) {
	  		@SuppressWarnings("rawtypes")
			Map.Entry mentry = (Map.Entry)iterator.next();
	  		System.out.print("Agent ID: "+ mentry.getKey() + " & Level-Policies to be computed: ");
	  		System.out.println(mentry.getValue());
  		}
	}
	
	public static HashMap<Integer, ArrayList<Integer>> makeAgLvlMap(int level, int currentAgentIndex) {
		HashMap<Integer, ArrayList<Integer>> agLvlMap = new HashMap<Integer, ArrayList<Integer>>();
		ArrayList<Integer> lvlList;
		int curPlayerLevel = level;
		int otherPlayerLevel = level-1;
		
		for(int i=0;i<numPlayers;i++){
			if(level==0) break;
			lvlList = new ArrayList<Integer>();
			for(int k=0;k<=level;k++){
				if(i==currentAgentIndex){
					if(k==otherPlayerLevel) continue;
					lvlList.add(k);
				}else{
					if(k==curPlayerLevel) continue;
					lvlList.add(k);
				}
			}
			agLvlMap.put(i, lvlList);
		}
		return agLvlMap;
	}
	
	public static void printAllLevelPolicyListForState(State s, ArrayList<ArrayList<Policy>> allLevelPolicyList, int numJointActions) {
		for (int i=0; i<allLevelPolicyList.size(); i++){
			System.out.println("Level "+i+" Policy Distributions: ");
			for (int j=0; j<allLevelPolicyList.get(i).size(); j++){
				for (int a=0; a<numJointActions; a++){
					System.out.print("  "+allLevelPolicyList.get(i).get(j).getActionDistributionForState(s).get(a).pSelection);
				}
				System.out.println();
			}
		}
		
	}
	
	public static void printAllLevelPolicyListForState(State s, SGDomain d, 
			Map<Integer, Map<Integer, Policy>> allLevelPolicyList) {
		
		int numLevels = allLevelPolicyList.size();
		
		for(int l=0; l<numLevels; l++){
			System.out.println("Level "+l+" Policy Distributions: ");
			for (Map.Entry<Integer, Policy> agEntry : allLevelPolicyList.get(l).entrySet()){	
				int ag = agEntry.getKey();
				String actingAgent = agIDtoName.get(ag);
				
				List<SGAgentAction> allsgActions = agentDefinitions.get(actingAgent).actions;
				List<GroundedSGAgentAction> allAgActions = SGtoGroundedSG(allsgActions,actingAgent);
				
				System.out.print(actingAgent+" : ");
				for (int a=0; a<allAgActions.size(); a++){
					System.out.print("  "+allLevelPolicyList.get(l).get(ag).getActionDistributionForState(s).get(a).pSelection);
				}
				System.out.println("");
			}
		}
	}
	
	public static void printMapAgentPolicy(State s, SGDomain d, 
			Map<Integer, Policy> agentPolicyMap) {
		for(Map.Entry<Integer, Policy> e:agentPolicyMap.entrySet() ){
			int ag = e.getKey();
			String actingAgent = agIDtoName.get(ag);

			List<SGAgentAction> allsgActions = agentDefinitions.get(actingAgent).actions;
			List<GroundedSGAgentAction> allAgActions = SGtoGroundedSG(allsgActions,actingAgent);
			
			System.out.print("Distribution for "+actingAgent+": ");
			for (int a=0; a<allAgActions.size(); a++){
				System.out.print("  "+agentPolicyMap.get(ag).getActionDistributionForState(s).get(a).pSelection);
			}
			System.out.println("");
		}
	}

	public static void printAgentPolicyForState(int agIndex, State s, Policy agPolicy, int numActions) {
		System.out.println("Agent "+agIndex+"'s Policy (for simulations): ");
		for (int a=0; a<numActions; a++){
			System.out.print("  "+agPolicy.getActionDistributionForState(s).get(a).pSelection);
		}
		System.out.println("");
	}
	
	public static void printList(List<GroundedSGAgentAction> allAgActions) {
		for(int i=0;i<allAgActions.size();i++){
			System.out.print("  "+ allAgActions.get(i).actionName());
		}
		System.out.println();	
	}
	
	public static void printList2(List<JointAction> allAgActions) {
		for(int i=0;i<allAgActions.size();i++){
			System.out.print("  "+ allAgActions.get(i).actionName());
		}
		System.out.println();
	}
	
	private static void printList3(List<SGAgentAction> allSGActions) {
		for(int i=0;i<allSGActions.size();i++){
			System.out.print("  "+ allSGActions.get(i).actionName);
		}
		System.out.println();
	}
	
}
