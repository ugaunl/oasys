package posg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import common.StateEnumerator;
import common.pomdp.BeliefState;
import common.pomdp.DenseBeliefVector;
import common.pomdp.EnumerableBeliefState;
import domains.wildfire.Wildfire;
import burlap.debugtools.RandomFactory;
import burlap.oomdp.core.Attribute;
import burlap.oomdp.core.ObjectClass;
import burlap.oomdp.core.TransitionProbability;
import burlap.oomdp.core.objects.MutableObjectInstance;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.MutableState;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointActionModel;

public class TabularBeliefState implements BeliefState, EnumerableBeliefState, DenseBeliefVector {
	
	public static final String BELIEFCLASSNAME = "belief";
	public static final String BELIEFATTNAME = "belief";
	
	private static POSGDomain tabularBeliefStateDomain = null;
	
	public static POSGDomain getTabularBeliefMDPDomain(){
		if(tabularBeliefStateDomain != null){
			return tabularBeliefStateDomain;
		}
		tabularBeliefStateDomain = new POSGDomain();
		Attribute att = new Attribute(tabularBeliefStateDomain, BELIEFATTNAME, Attribute.AttributeType.DOUBLEARRAY);
		ObjectClass oclass = new ObjectClass(tabularBeliefStateDomain, BELIEFCLASSNAME);
		oclass.addAttribute(att);

		return tabularBeliefStateDomain;
	}
	
	/**
	 * A state enumerator for determining the index of MDP states in the belief vector.
	 */
	protected StateEnumerator stateEnumerator;
	
	/**
	 * The belief vector, stored sparsely with a {@link java.util.Map}.
	 */
	public Map<Integer, Double> beliefValues = new HashMap<Integer, Double>();
	
	/**
	 * The POSG domain with which this belief state is associated. Contains the ObservationFunction necessary to perform
	 * belief state updates.
	 */
	protected POSGDomain domain;
	
	protected String curAgentName;
	private boolean debug = false;

	public TabularBeliefState(TabularBeliefState srcBeliefState) {
		this(srcBeliefState.domain, srcBeliefState.stateEnumerator, srcBeliefState.curAgentName);
		this.beliefValues = new HashMap<Integer, Double>(srcBeliefState.beliefValues.size());
		for(Map.Entry<Integer, Double> e : srcBeliefState.beliefValues.entrySet()){
			this.beliefValues.put(e.getKey(), e.getValue());
		}
	}
	
	public TabularBeliefState(POSGDomain domain, String curAgentName) {
		if(!domain.providesStateEnumerator()){
			throw new RuntimeException("TabularBeliefState(POSGDomain domain, String curAgentName) constructor requires that" +
					"the POSGDomain provides a StateEnumerator, but it does not. Alternatively consider using the" +
					"TabularBeliefState(PODomain domain, StateEnumerator stateEnumerator), where you can" +
					"specify your own StateEnumerator for lazy state indexing.");
		}
		this.domain = domain;
		this.stateEnumerator = domain.getStateEnumerator();
		this.curAgentName = curAgentName;
	}

	public TabularBeliefState(POSGDomain domain, StateEnumerator stateEnumerator, String curAgentName) {
		this.domain = domain;
		this.stateEnumerator = stateEnumerator;
		this.curAgentName = curAgentName;
	}
	
	@Override
	public double belief(State s) {
		int sid = this.stateEnumerator.getEnumeratedID(s);
		return this.belief(sid);
	}
	
	/**
	 * Returns the value of the belief vector for the provided index.
	 * @param stateId the index (state identification number) of the belief vector to return.
	 * @return the value of the belief vector for the provided index.
	 */
	public double belief(int stateId){

		Double b = this.beliefValues.get(stateId);
		if(b == null){
			return 0.;
		}
		return b;
	}

	/**
	 * Sets the probability mass (belief) associated with the underlying MDP state. If this object has not yet assigned
	 * a unique identifier to the provided MDP state, then it will first create one. Note that using this method
	 * will not ensure that the total probability mass across this belief state sums to 1, so other changes will have
	 * to be specified manually.
	 * @param s the underlying MDP state defined as a {@link burlap.oomdp.core.states.State}
	 * @param b the probability mass to assigned to the underlying MDP state.
	 */
	public void setBelief(State s, double b){
		int sid = this.stateEnumerator.getEnumeratedID(s);
		this.setBelief(sid, b);
	}


	/**
	 * Sets the probability mass (belief) associated with the underlying MDP state. Note that using this method
	 * will not ensure that the total probability mass across this belief state sums to 1, so other changes will have
	 * to be specified manually.
	 * @param stateId the unique numeric identifier of the underlying MDP state defined.
	 * @param b the probability mass to assigned to the underlying MDP state.
	 */
	public void setBelief(int stateId, double b){
		if(stateId < 0 || stateId > this.numStates()){
			throw new RuntimeException("Error; cannot set belief value for state id " + stateId + "; belief vector is of dimension " + this.numStates());
		}

		if(b != 0){
			this.beliefValues.put(stateId, b);
		}
		else{
			this.beliefValues.remove(stateId);
		}
	}


	/**
	 * Returns the probability that the underlying MDP will transition from state s to sp when taking action a in state s.
	 * @param s the previous MDP state defined by a {@link burlap.oomdp.core.states.State}
	 * @param ga the taken action defined by a {@link burlap.oomdp.singleagent.GroundedAction}
	 * @param sp The next MDP state observed defined by a {@link burlap.oomdp.core.states.State}.
	 * @return the probability that the underlying MDP will transition from state s to sp when taking action a in state s.
	 */
	protected double getTransitionProb(State s, JointAction ga, State sp){
		JointActionModel jaModel = this.domain.getJointActionModel();
		List<TransitionProbability> tps = jaModel.transitionProbsFor(s, ga);
		for(TransitionProbability tp : tps){
			if(tp.s.equals(sp)){
				return tp.p;
			}
		}
		return 0.;

	}
	
	/**
	 *  returns probability of an observation given previous belief state
	 */
//	public double probObservation(State observation, JointAction ja){
//		ObservationFunction of = this.domain.getObservationFunction();
//		double sum = 0.;
//		for(int i = 0; i < this.numStates(); i++){
//			State ns = this.stateForId(i);
//			double transitionSum = 0.;
//			for(Map.Entry<Integer, Double> srcStateEntry : this.beliefValues.entrySet()){
//				double srcB = srcStateEntry.getValue();
//				State srcState = this.stateEnumerator.getStateForEnumerationId(srcStateEntry.getKey());
//				double op = of.getObservationProbability(observation,srcState, ns, ja);
//				
//				double tp = this.getTransitionProb(srcState, ja, ns);
//				transitionSum += srcB * tp * op;
//			}
//			sum += transitionSum;
//		}
//		return sum;
//	}
	
	/**
	 * Computes and returns the probability of observing an observation in a given BeleifState when a specific action is taken.
	 * @param bs the previous belief state
	 * @param observation the observation that will be observed
	 * @param ja the pomdp action that would be selected in the previous belief state
	 * @return the probability of observing the given observation
	 */
	public double getProbObservation(State observation, JointAction ja){

		ObservationFunction of = this.domain.getObservationFunction();
		double sum = 0.;
		List <StateBelief> beliefs = getStatesAndBeliefsWithNonZeroProbability();
		for(StateBelief sb : beliefs){
			List<TransitionProbability> mdpTps = this.domain.getJointActionModel().transitionProbsFor(sb.s, ja);//ga.getTransitions(sb.s);
			for(TransitionProbability tp : mdpTps){
				double op = of.getObservationProbability(observation,sb.s, tp.s, ja);
				double term = sb.belief * tp.p * op;
				sum += term;
			}
		}

		return sum;

	}
	

	@Override
	public State sampleStateFromBelief() {
		List<StateBelief> sbs = this.getStatesAndBeliefsWithNonZeroProbability();
		double sum = 0.;
		double rand = RandomFactory.getMapped(0).nextDouble();
		for(StateBelief sb:sbs){
			sum += sb.belief;
			if (rand < sum){
				return sb.s;
			}
		}
		
//		double sumProb = 0.;
//		double r = RandomFactory.getMapped(0).nextDouble();
//		for(Map.Entry<Integer, Double> e : this.beliefValues.entrySet()){
//			sumProb += e.getValue();
//			if(r < sumProb){
//				return this.stateEnumerator.getStateForEnumerationId(e.getKey());
//			}
//		}

		throw new RuntimeException("Error; could not sample from belief state because the beliefs did not sum to 1; they summed to: " + sum);
	}
	

	@Override
	public BeliefState getUpdatedBeliefState(State observation, JointAction ga) {
		ObservationFunction of = this.domain.getObservationFunction();
		double [] newBeliefStateVector = new double[this.numStates()];
		
		double sum = 0.;
		int f=0;
		for(int i = 0; i < newBeliefStateVector.length; i++){
			State ns = this.stateForId(i);
			double transitionSum = 0.;
			for(Map.Entry<Integer, Double> srcStateEntry : this.beliefValues.entrySet()){
				double srcB = srcStateEntry.getValue();
//				System.out.println("srcB: "+srcB);
				State srcState = this.stateEnumerator.getStateForEnumerationId(srcStateEntry.getKey());
//				System.out.println("srcState: "+this.stateEnumerator.getEnumeratedID(srcState));
				double op = of.getObservationProbability(observation, srcState, ns, ga);
//				if(op>0) {System.out.println("op: "+op); f++;}
				double tp = getTransitionProb(srcState, ga, ns);
//				System.out.println("tp: "+tp);
				transitionSum += op * srcB * tp;
			}
			double numerator = transitionSum;
			sum += numerator;
			newBeliefStateVector[i] = numerator;

		}
//		System.out.print("FLAG = "+f);

		TabularBeliefState newBeliefState = new TabularBeliefState(this.domain, this.stateEnumerator, this.curAgentName);
		for(int i = 0; i < newBeliefStateVector.length; i++){
			double nb = newBeliefStateVector[i] / sum;
			newBeliefState.setBelief(i, nb);
		}


		return newBeliefState;
	}
	
	public BeliefState getUpdatedBeliefState(State observation, State agentSP, JointAction ga) {
		
//		System.out.println(agentID + " == "+this.curAgentName);
		ObservationFunction of = this.domain.getObservationFunction();
		double [] newBeliefStateVector = new double[this.numStates()];
		
		double sum = 0.;
		for(int i = 0; i < newBeliefStateVector.length; i++){
			State ns = this.stateForId(i);
			double transitionSum = 0.;
			for(StateBelief sb: this.getStatesAndBeliefsWithNonZeroProbability()){
				double srcB = sb.belief;
				double op = of.getObservationProbability(observation, sb.s, ns, ga);
				double tp = getTransitionProb(sb.s, ga, ns);
				transitionSum += op * srcB * tp;
			}
//			for(Map.Entry<Integer, Double> srcStateEntry : this.beliefValues.entrySet()){
//				double srcB = srcStateEntry.getValue();
//				State srcState = this.stateEnumerator.getStateForEnumerationId(srcStateEntry.getKey());
//				double op = of.getObservationProbability(observation, srcState, ns, ga);
//				double tp = getTransitionProb(srcState, ga, ns);
//				transitionSum += op * srcB * tp;
//			}
			double numerator = transitionSum;
			sum += numerator;
			newBeliefStateVector[i] = numerator;
		}
		
		TabularBeliefState newBeliefState = new TabularBeliefState(this.domain, this.stateEnumerator, this.curAgentName);
		for(int i = 0; i < newBeliefStateVector.length; i++){
			double nb = newBeliefStateVector[i] / sum;
			newBeliefState.setBelief(i, nb);
		}
		
		List<StateBelief> sbs = newBeliefState.getStatesAndBeliefsWithNonZeroProbability();
//		System.out.println("NewBeliefs (num of non zero state beliefs) : "+sbs.size());
		
//		int[] availabilities = new int[Wildfire.NUM_AVAILABLE_STATES];
//		for(int i=0;i<Wildfire.NUM_AVAILABLE_STATES;i++){
//			availabilities[i] = i;
//		}
		
		int numAvail = Wildfire.NUM_AVAILABLE_STATES;
		int numNeighbors = agentSP.getObjectsOfClass("agent").size();
		List<int[]> availList = getCombs(numAvail,numNeighbors);
//		printList(availList);
		
		TabularBeliefState finalBeliefState = new TabularBeliefState(this.domain, this.stateEnumerator, this.curAgentName);
		
		
		
		for(int[] availability:availList){
//			System.out.println(Arrays.toString(availability));
			State asp = new MutableState();
			asp = (MutableState) agentSP.copy(); 
			for(int otherAgentID=0;otherAgentID<numNeighbors;otherAgentID++){
				asp.getObjectsOfClass("agent").get(otherAgentID).setValue("available", availability[otherAgentID]);
			}
//			System.out.println(asp.getCompleteStateDescription());
			double spSum = 0.;
			for(StateBelief sp: sbs){
				
				int[] realAvailability = new int[availability.length];
				for(int otherAgentID=0;otherAgentID<numNeighbors;otherAgentID++){
//					String aName = ga.getAgentNames().get(otherAgentID);
//					if(aName.equals(this.curAgentName)){
//						continue;
//					}
					realAvailability[otherAgentID] = sp.s.getObjectsOfClass("agent").get(otherAgentID).getIntValForAttribute("available");
//					System.out.println("realAvailability["+otherAgentID+"] = "+realAvailability[otherAgentID]);
				}
				
				if(Arrays.equals(availability, realAvailability)){
					spSum += sp.belief;
				}
			}
			finalBeliefState.setBelief(asp, spSum);		
		}
		
		double totalProb = 0.;
		for(StateBelief bs: finalBeliefState.getStatesAndBeliefsWithNonZeroProbability()){
			totalProb += bs.belief;
		}
		
		if(totalProb<0.99){
			System.out.println("Invalid Total Prob: "+ totalProb);
		}
		
		return finalBeliefState;
	}
					
//					if(sp.s.getObjectsOfClass("agent").get(otherAgentID).getIntValForAttribute("available") == availability[otherAgentID]){
//						cnt++;
//						System.out.println(otherAgentID +" -- "+sp.s.getObjectsOfClass("agent").get(otherAgentID).getIntValForAttribute("available") + " == "+availability[otherAgentID] + " Cnt: "+ cnt);
//						
//					}else{
//						System.out.println(otherAgentID +" -- "+sp.s.getObjectsOfClass("agent").get(otherAgentID).getIntValForAttribute("available") + " != "+availability[otherAgentID]);
//						
//					}
//				}
//				if(cnt == numNeighbors){
//					System.out.println(cnt+" == " +numNeighbors +" spSum = "+spSum);
//					spSum += sp.belief;
//				}
//			}
			
//		}
		
		
//		for(int[] availability:availList){
//			
//			State asp = new MutableState();
//			asp = (MutableState) agentSP.copy(); 
//			asp.getObjectsOfClass("agent").get(agentID).setValue("available", availability);
//			double spSum = 0.;
//			for(StateBelief sp: newBeliefState.getStatesAndBeliefsWithNonZeroProbability()){
//				for(int otherAgentID=0;otherAgentID<ga.getAgentNames().size();otherAgentID++){
//					String aName = ga.getAgentNames().get(otherAgentID);
//					if(aName.equals(this.curAgentName)){
//						continue;
//					}
//					flag = false;
//					if(sp.s.getObjectsOfClass("agent").get(otherAgentID).getIntValForAttribute("available") == availability){
//						spSum += sp.belief;
//					}
//				}
//			}
//			finalBeliefState.setBelief(asp, spSum);
//		}
		
		
//		double totalProb = 0.;
//		for(StateBelief bs: finalBeliefState.getStatesAndBeliefsWithNonZeroProbability()){
//			totalProb += bs.belief;
//		}
//		for(Map.Entry<Integer, Double> finalStateEntry : finalBeliefState.beliefValues.entrySet()){
//			Double d = finalStateEntry.getValue();
//			if(d>0){
//				totalProb += finalStateEntry.getValue();
////				System.out.print(" "+d);
//			}
//		}
//		if(totalProb<1){
//			System.out.println("Invalid Total Prob: "+ totalProb);
//		}
//		
//		return finalBeliefState;
		
//	}
	
	private static void printList(List<int[]> availList){
		for(int[] li : availList){
			System.out.println(Arrays.toString(li));
		}
	}
	
	private static List<int[]> getCombs(int numAvail, int numNeighbors) {
		List<int[]> availList = new ArrayList<int[]>();
		int totCombs = (int) Math.pow(numAvail, numNeighbors);
//		System.out.println(numAvail+" "+numNeighbors+" "+totCombs);
		for(int i=0;i<totCombs;i++){
			String combTemp = Integer.toString(i, numAvail);
			String form = "%0"+Integer.toString(numNeighbors)+"d";
			String combFinal = String.format(form,Integer.parseInt(combTemp));
			int[] digits = new int[numNeighbors];
			for(int j=0;j<numNeighbors;j++){
				digits[j] = Character.digit(combFinal.charAt(j), 10);
			}
			availList.add(digits);
		}
		
		
		return availList;
	}
	
	public static State toggleAvailabilityForAgent(State curState, int agent){
//		System.out.println("OLD STATE:\n"+curState.getCompleteStateDescription());
		int agentAvailability = curState.getObjectsOfClass("agent").get(agent).getIntValForAttribute("available");
//		System.out.println("agentAvailability: "+agentAvailability);
		int toggle  = (agentAvailability == 1) ? 0:1;
//		System.out.println("toggle: "+toggle);
		State newState = new MutableState();
		newState = (MutableState) curState.copy();
		newState.getObjectsOfClass("agent").get(agent).setValue("available", toggle);
//		System.out.println("OLD STATE:\n"+curState.getCompleteStateDescription());
//		System.out.println("NEW STATE:\n"+newState.getCompleteStateDescription());
		
		return newState;
		
	}
	
	@Override
	public List<StateBelief> getStatesAndBeliefsWithNonZeroProbability(){
		List<StateBelief> result = new LinkedList<StateBelief>();
		for(Map.Entry<Integer, Double> e : this.beliefValues.entrySet()){
			StateBelief sb = new StateBelief(this.stateForId(e.getKey()), e.getValue());
			result.add(sb);
		}
		return result;
	}
	
	/**
	 * Returns the size of the observed underlying MDP state space.
	 * @return the size of the observed underlying MDP state space.
	 */
	public int numStates(){
		return this.stateEnumerator.numStatesEnumerated();
	}
	
	/**
	 * Returns the corresponding MDP state for the provided unique identifier.
	 * @param id the MDP state identifier
	 * @return the corresponding MDP state, defined by a {@link burlap.oomdp.core.states.State}, for the provided unique identifier.
	 */
	public State stateForId(int id){
		return this.stateEnumerator.getStateForEnumerationId(id);
	}
	
	/**
	 * Sets this belief state to the provided. Dense belief vector. If the belief vector dimensionality does not match
	 * this objects dimensionality then a runtime exception will be thrown.
	 * @param b the belief vector to set this belief state to.
	 */
	@Override
	public void setBeliefVector(double [] b){
		if(b.length != this.numStates()){
			throw new RuntimeException("Error; cannot set belief state with provided vector because dimensionality does not match." +
					"Provided vector of dimension " + b.length + " need dimension " + this.numStates());
		}

		for(int i = 0; i < b.length; i++){
			this.setBelief(i, b[i]);
		}
	}

	/**
	 * Returns this belief state as a dense (non-sparse) belief vector.
	 * @return a double array specifying this belief state as a dense (non-sparse) belief vector.
	 */
	@Override
	public double [] getBeliefVector(){
		double [] b = new double[this.numStates()];
		for(int i = 0; i < b.length; i++){
			b[i] = this.belief(i);
		}
		return b;
	}
	
	
	/**
	 * Returns the set of underlying MDP states this belief vector spans.
	 * @return the set of underlying MDP states this belief vector spans.
	 */
	public List<State> getStateSpace(){
		LinkedList<State> states = new LinkedList<State>();
		for(int i = 0; i < this.numStates(); i++){
			states.add(this.stateForId(i));
		}
		return states;
	}
	
	/**
	 * Sets this belief state to have zero probability mass for all underlying MDP states.
	 */
	public void zeroOutBeliefVector(){
		this.beliefValues.clear();
	}
	
	/**
	 * Initializes this belief state to a uniform distribution
	 */
	public void initializeBeliefsUniformly(){
		double b = 1. / (double)this.numStates();
		this.initializeAllBeliefValuesTo(b);
	}
	
	public void initializeBeliefsUniformly(List<Integer> sIDList) {
		double b = 1. / (double)sIDList.size();
		double sum=0.;
		for(int i = 0; i < this.numStates(); i++){
			if(sIDList.contains(i))
				this.setBelief(i, b);
			sum+=this.belief(i);
		}
		if(debug ){
			if(sum!=1){
				System.out.println("Beliefs dont sum to 1, they sum to "+sum);
			}else{
				printBSwithNonZeroProb(getStatesAndBeliefsWithNonZeroProbability());
			}
		}
		
		
	}
	
	
	
	private void printBSwithNonZeroProb(
			List<StateBelief> statesAndBeliefsWithNonZeroProbability) {
		for(StateBelief sb: statesAndBeliefsWithNonZeroProbability){
			System.out.println(stateEnumerator.getEnumeratedID(sb.s)+" -> "+sb.belief);
		}
		
	}

	/**
	 * Initializes the probability mass of all underlying MDP states to the specified value. Note that this will not
	 * enforce summing to one, so you will have to manually modify the value of other states if initialValue is not
	 * 1/n where n is the number of underlying MDP states.
	 * @param initialValue the probability mass to assign to each state.
	 */
	public void initializeAllBeliefValuesTo(double initialValue){
		if(initialValue == 0){
			this.zeroOutBeliefVector();
		}
		else{
			for(int i = 0; i < this.numStates(); i++){
				this.setBelief(i, initialValue);
			}
		}
	}
	
	
	
	public void beliefNorm() {
		double sum = 0.0;
		for(double d : this.beliefValues.values()){
			sum+=d;
		}
		
		for(int keyInput : this.beliefValues.keySet()){
			double tempDouble = this.beliefValues.get(keyInput);
			this.beliefValues.put(keyInput, tempDouble/sum);
		}
	}
	
	@Override
	public State copy() {
		return new TabularBeliefState(this);
	}

	@Override
	public State addObject(ObjectInstance o) {
		throw new UnsupportedOperationException("TabularBeliefState cannot have OO-MDP objects added to it.");
	}

	@Override
	public State addAllObjects(Collection<ObjectInstance> objects) {
		throw new UnsupportedOperationException("TabularBeliefState cannot have OO-MDP objects added to it.");
	}

	@Override
	public State removeObject(String oname) {
		throw new UnsupportedOperationException("TabularBeliefState cannot have OO-MDP objects removed from it.");
	}

	@Override
	public State removeObject(ObjectInstance o) {
		throw new UnsupportedOperationException("TabularBeliefState cannot have OO-MDP objects removed from it.");
	}

	@Override
	public State removeAllObjects(Collection<ObjectInstance> objects) {
		throw new UnsupportedOperationException("TabularBeliefState cannot have OO-MDP objects removed from it.");
	}

	@Override
	public State renameObject(String originalName, String newName) {
		throw new UnsupportedOperationException("TabularBeliefState cannot have OO-MDP objects renamed");
	}

	@Override
	public State renameObject(ObjectInstance o, String newName) {
		throw new UnsupportedOperationException("TabularBeliefState cannot have OO-MDP objects renamed");
	}

	@Override
	public Map<String, String> getObjectMatchingTo(State so, boolean enforceStateExactness) {
		
		Map<String, String> matching = new HashMap<String, String>(1);

		if(so instanceof TabularBeliefState){
			TabularBeliefState otb = (TabularBeliefState)so;
			if(this.beliefValues.size() == otb.beliefValues.size()) {
				boolean match = true;
				for(Map.Entry<Integer, Double> e : this.beliefValues.entrySet()) {
					if(Math.abs(otb.beliefValues.get(e.getKey()) - e.getValue()) > 1e-10){
						match = false;
						break;
					}
				}
				if(match){
					matching.put(BELIEFCLASSNAME, BELIEFCLASSNAME);
				}
			}
		}
		else {
			ObjectInstance obelief = so.getFirstObjectOfClass(BELIEFCLASSNAME);
			if(obelief != null) {
				double [] vec = obelief.getDoubleArrayValForAttribute(BELIEFATTNAME);
				if(vec.length >= this.beliefValues.size()){
					boolean match = true;
					for(int i = 0; i < vec.length; i++){
						if(Math.abs(vec[i] - this.belief(i)) > 1e-10){
							match = false;
							break;
						}
					}
					if(match){
						matching.put(BELIEFCLASSNAME, BELIEFCLASSNAME);
					}
				}
			}
		}

		return matching;
	}

	@Override
	public int numTotalObjects() {
		return 1;
	}

	@Override
	public ObjectInstance getObject(String oname) {
		if(oname.equals(BELIEFCLASSNAME)){
			ObjectInstance o = new MutableObjectInstance(getTabularBeliefMDPDomain().getObjectClass(BELIEFCLASSNAME), BELIEFCLASSNAME);
			o.setValue(BELIEFATTNAME, this.getBeliefVector());
			return o;
		}

		return null;
	}

	@Override
	public List<ObjectInstance> getAllObjects() {
		return Arrays.asList(this.getObject(BELIEFCLASSNAME));
	}

	@Override
	public List<ObjectInstance> getObjectsOfClass(String oclass) {
		if(oclass.equals(BELIEFCLASSNAME)){
			return this.getAllObjects();
		}
		return new ArrayList<ObjectInstance>();
	}

	@Override
	public ObjectInstance getFirstObjectOfClass(String oclass) {
		if(oclass.equals(BELIEFCLASSNAME)){
			return this.getObject(BELIEFCLASSNAME);
		}
		return null;
	}

	@Override
	public Set<String> getObjectClassesPresent() {
		Set <String> set = new HashSet<String>();
		set.add(BELIEFCLASSNAME);
		return set;
	}

	@Override
	public List<List<ObjectInstance>> getAllObjectsByClass() {
		return Arrays.asList(this.getAllObjects());
	}


	@Override
	public String getCompleteStateDescription() {
		return this.beliefValues.toString();
	}

	@Override
	public Map<String, List<String>> getAllUnsetAttributes() {
		return new HashMap<String, List<String>>();
	}

	@Override
	public String getCompleteStateDescriptionWithUnsetAttributesAsNull() {
		return this.getCompleteStateDescription();
	}

	@Override
	public List<List<String>> getPossibleBindingsGivenParamOrderGroups(String[] paramClasses, String[] paramOrderGroups) {
		if(paramClasses.length > 1){
			return new ArrayList<List<String>>();
		}
		if(!paramClasses[0].equals(BELIEFCLASSNAME)){
			return new ArrayList<List<String>>();
		}
		return Arrays.asList(Arrays.asList(BELIEFCLASSNAME));
	}

	@Override
	public boolean equals(Object obj) {

		if(!(obj instanceof TabularBeliefState)){
			return false;
		}

		TabularBeliefState otb = (TabularBeliefState)obj;
		if(this.beliefValues.size() == otb.beliefValues.size()) {
			for(Map.Entry<Integer, Double> e : this.beliefValues.entrySet()) {
				Double otherVal = otb.beliefValues.get(e.getKey());
				if(otherVal == null){
					return false;
				}
				if(Math.abs(otherVal - e.getValue()) > 1e-10){
					return false;
				}
			}
			return true;
		}

		return false;

	}

	

	

}
