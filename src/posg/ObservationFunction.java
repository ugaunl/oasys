package posg;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import burlap.debugtools.RandomFactory;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;


public abstract class ObservationFunction {

	protected POSGDomain domain;

	public ObservationFunction(POSGDomain domain){
		this.domain = domain;
		this.domain.setObservationFunction(this);
	}

	public abstract boolean canEnumerateObservations();

	public abstract List<State> getAllPossibleObservations();

	public abstract double getObservationProbability(State observation, State state, State nextState, JointAction ja);

	public abstract List<ObservationProbability> getObservationProbabilities(State state, State nextState, JointAction ja);

	public abstract State sampleObservation(State state, State nextState, JointAction ja);

	protected final List<ObservationProbability> getObservationProbabilitiesByEnumeration(State state, State nextState, JointAction ja){
		List<State> possibleObservations = this.getAllPossibleObservations();
		
		List<ObservationProbability> probs = new ArrayList<ObservationProbability>(possibleObservations.size());
		double sumP=0.;
		String probStr="[";
		for(State obs : possibleObservations){
			double p = this.getObservationProbability(obs, state, nextState, ja);
			probStr += " "+p;
			if(p != 0){
				sumP+=p;
				probs.add(new ObservationProbability(obs, p));
			}
		}
		if(sumP<1.){
			System.out.println("Possible Observation Size ["+this.domain.getStateEnumerator().getEnumeratedID(state)+","
					+this.domain.getStateEnumerator().getEnumeratedID(nextState)+","+ja.actionName()+"] :"+possibleObservations.size()+
					" Total Obs Prob: "+probStr+"] = "+sumP);
		}
		
		return probs;
	}

	protected final State sampleObservationByEnumeration(State state, State nextState, JointAction ja){
//		List<ObservationProbability> obProbs = this.getObservationProbabilities(state, nextState, ja);
		List<ObservationProbability> obProbs = getObservationProbabilitiesByEnumeration(state, nextState, ja);
		
		Random rand = RandomFactory.getMapped(0);
		double r = rand.nextDouble();
		double sumProb = 0.;
//		System.out.println();
		for(ObservationProbability op : obProbs){
//			System.out.print(" "+op.p);
			sumProb += op.p;
			if(r < sumProb){
				return op.observation;
			}
		}
		

		throw new RuntimeException("Could not sample observation because observation probabilities did not sum to 1; they summed to " + sumProb);
	}




	/**
	 * A class for associating a probability with an observation. The class is pair consisting of a {@link burlap.oomdp.core.states.State}
	 * for the observation and a double for the probability.
	 */
	public class ObservationProbability{

		/**
		 * The observation represented with a {@link burlap.oomdp.core.states.State} object.
		 */
		public State observation;

		/**
		 * The probability of the observation
		 */
		public double p;


		/**
		 * Initializes.
		 * @param observation the observation represented with a {@link burlap.oomdp.core.states.State} object.
		 * @param p the probability of the observation
		 */
		public ObservationProbability(State observation, double p){
			this.observation = observation;
			this.p = p;
		}
	}
	
}
