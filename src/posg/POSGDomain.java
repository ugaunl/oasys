package posg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import common.StateEnumerator;
import domains.wildfire.Wildfire;
import domains.wildfire.WildfireDomain;
import domains.wildfire.WildfireParameters;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.stochasticgames.JointActionModel;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;

public class POSGDomain extends SGDomain{
	/**
	 * The full set of actions that could be taken by any agent.
	 */
	protected Set <SGAgentAction> agentActions;

	/**
	 * A map from action names to their corresponding {@link SGAgentAction}
	 */
	protected Map <String, SGAgentAction> singleActionMap;

	/**
	 * The list of actions for each agent. 
	 */
	protected Map<String, List<SGAgentAction>> actionsPerAgent;
	

	/**
	 * The joint action model of the domain
	 */
	protected JointActionModel jam;
	
	/**
	 * The observation function
	 */
	protected ObservationFunction 	obsevationFunction;
	
	public void setObservationFunction(ObservationFunction observationFunction){
		this.obsevationFunction = observationFunction;
	}
	
	public ObservationFunction getObservationFunction(){
		return this.obsevationFunction;
	}
	
	public Map<String, List<SGAgentAction>> getActionsPerAgent(){
		return this.actionsPerAgent;
	}
	
	
	protected StateEnumerator stateEnumerator;

//	private Map<Integer, State> agentStateMap;
	
	public POSGDomain() {
		super();
		
		agentActions = new HashSet<SGAgentAction>();
		singleActionMap = new HashMap<String, SGAgentAction>();
		actionsPerAgent = new HashMap<String, List<SGAgentAction>>();
	}


	/**
	 * Sets the joint action model associated with this domain.
	 * @param jam the joint action model to associate with this domain.
	 */
	public void setJointActionModel(JointActionModel jam){
		this.jam = jam;
	}

	/**
	 * Returns the joint action model associated with this domain.
	 * @return the joint action model associated with this domain.
	 */
	public JointActionModel getJointActionModel(){
		return this.jam;
	}
	
	@Override
	public void addSGAgentAction(SGAgentAction sa){
		if(!this.singleActionMap.containsKey(sa.actionName)){
			agentActions.add(sa);
			singleActionMap.put(sa.actionName, sa);
		}
	}
	
	public void addSGAgentAction(SGAgentAction sa, String agentName){
		List<SGAgentAction> actions = actionsPerAgent.get(agentName);
		if (actions == null) {
			actions = new ArrayList<SGAgentAction>();
			actionsPerAgent.put(agentName, actions);
		}
		actions.add(sa);
	}

	
	
	@Override
	public List <SGAgentAction> getAgentActions(){
		return new ArrayList<SGAgentAction>(agentActions);
	}
	
	public List <SGAgentAction> getAgentActions(String agentName){
		return new ArrayList<SGAgentAction>(actionsPerAgent.get(agentName));
	}
	
	
	
	@Override
	public SGAgentAction getSingleAction(String name){
		return singleActionMap.get(name);
	}



	@Override
	public void addAction(Action act) {
		throw new UnsupportedOperationException("Stochastic Games domain cannot add actions designed for single agent formalisms");
	}



	@Override
	public List<Action> getActions() {
		throw new UnsupportedOperationException("Stochastic Games domain does not contain any action for single agent formalisms");
	}



	@Override
	public Action getAction(String name) {
		throw new UnsupportedOperationException("Stochastic Games domain does not contain any action for single agent formalisms");
	}



	@Override
	protected Domain newInstance() {
		return new POSGDomain();
	}
	
	public boolean providesStateEnumerator(){ return this.stateEnumerator != null; }
	
	public StateEnumerator getStateEnumerator() {
		if(this.stateEnumerator == null){
			throw new RuntimeException("This domain cannot return a StateEnumerator because one is not defined for it. " +
					"Use the providesStateEnumerator() method to check if one is provided in advance.");
		}
		return stateEnumerator;
	}
	
	public void setStateEnumerator(StateEnumerator stateEnumerator) {
		this.stateEnumerator = stateEnumerator;
	}
	
//	public void setAgentStatesFromMasterState(Map<Integer,State> agentStateMap) {
//		this.agentStateMap = agentStateMap;
//	}
//	
//	public Map<Integer, State> getAgentStatesFromMasterState(Map<Integer,State> agentStateMap) {
//		if(this.agentStateMap == null){
//			throw new RuntimeException("This domain cannot return a agentStateMap because one is not defined for it. ");
//		}
//		return this.agentStateMap;
//	}
	
	
	
}
