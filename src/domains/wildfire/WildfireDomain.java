package domains.wildfire;

import posg.POSGDomain;

public class WildfireDomain extends POSGDomain {
	/** The locations of the agents. */
	// agentLocations[i][0] is i's X coordinate, [i][1] is i's Y coordinate
	public int[][] agentLocations;
	
	/** The types of the agents. */
	// these are the power with which the agent can fight fires
	public int[] agentTypes;
	
	/** Each agent's neighborhoods. */
	// [i][j][0] is agent i's jth location's X coordinate, and the Y coordinate is [i][j][1]
	public int[][][] neighborhoods;
	
	/** Each agent's neighboring agents. */
	// [i][j] is agent i's jth neighbor's agentNum ID
	public int[][] neighbors;
	
	/** The locations of the fires. */
	// fireLocations[f][0] is f's X coordinate, [f][1] is f's Y coordinate
	public int[][] fireLocations;
	
	/** The indices of the visible fires for each agent. */
	// [i][0] is the index into {@code int[][] fireLocations} of the first fire visible by agent i, etc.
	// NOTE: fire locations are indexed within the master world first by Y coordinate, then X coordinte
	// e.g.,
	// F2 F4 F6
	// F1 F3 F5
	public int[][] visibleFireIndices;
	
	/**
	 * Constructs a new {@link WildfireDomain}.
	 */
	public WildfireDomain() {
		super();
	}
}
