package domains.wildfire;

/**
 * The {@link WildfireParameters} class provides static references to values
 * defining parameters in the Wildfire domain.
 * 
 * @author Adam Eck
 */
public class WildfireParameters {

	public static int config;
	
	/** The number of agents. */
    public static int numAgents;
    
    /** The width of the grid. */
    public static int width;
    
    /** The height of the grid. */
    public static int height;
    
    /** The probability that the suppressant level decreases after each non-NOOP action. */
    public static double dischargeProb = 0.5;
    public static double trueDischargeProb = 0.75;
    
    /** The average amount of time an requires for charging. */
    public static int chargingTime = 3;
    public static int trueChargingTime = 2;
    
	/** The size of a cell (in meters). */
    public static double CELL_SIZE = 50.0;
    
    /** The default wind direction. */
    /* Note: 0 = North, PI = SOUTH, 1/2 PI = EAST, 3/2 PI = WEST */
    public static double WIND_DIRECTION = 0.25 * Math.PI;
    
    /** The initial, random spread probability for dealing with locations 
     * outside the frontier. 
     * This breaks domains.WildfireMechanics.actionHelper when RANDOM_SPREAD_PROB = 1, 0.5;
     * */ 
    public static double RANDOM_SPREAD_PROB = 0.05;
    
	public WildfireParameters(int conf) {
		if(conf==1){
			config = 1;
			numAgents = 2;
			width = 3;
			height = 3;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else if(conf==11){
			config = 11;
			numAgents = 2;
			width = 3;
			height = 3;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else if(conf==12){
			config = 12;
			numAgents = 2;
			width = 3;
			height = 3;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else if(conf == 2){
			config = 2;
			numAgents = 3;
			width = 3;
			height = 3;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else if(conf == 21){
			config = 21;
			numAgents = 3;
			width = 3;
			height = 3;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else if(conf == 22){
			config = 22;
			numAgents = 3;
			width = 3;
			height = 3;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else if(conf == 3){
			config = 3;
			numAgents = 5;
			width = 5;
			height = 4;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else if(conf == 4){
			config = 4;
			numAgents = 5;
			width = 5;
			height = 4;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else if(conf == 5){
			config = 5;
			numAgents = 3;
			width = 5;
			height = 3;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else if(conf == 0){
			config = 0;
			numAgents = 2;
			width = 3;
			height = 3;
//			System.out.println("Generating the wildfire domain with CONFIG: " + config);
		}else{
			System.out.println("Invalid Configuration is set!!");
			System.exit(0);
		}
	}
    
    
}
