package domains.wildfire;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import burlap.debugtools.RandomFactory;
import burlap.oomdp.core.TransitionProbability;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointActionModel;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

public class WildfireMechanics extends JointActionModel {
	/** The {@link WildfireDomain}. */
	private WildfireDomain domain;
	
	/** The {@link WildfireSpreadModel}. */
	private WildfireSpreadModel wildfireModel;
	
	/**
	 * Constructs a new {@link WildfireMechanics}.
	 * 
	 * @param pWildfireDomain The {@link WildfireDomain}
	 */
	public WildfireMechanics(WildfireDomain pDomain) {
		// save the params
		domain = pDomain;
		
		// create the objects
		wildfireModel = new WildfireSpreadModel(WildfireParameters.CELL_SIZE,
				WildfireParameters.WIND_DIRECTION);
	}

	@Override
	public List<TransitionProbability> transitionProbsFor(State s,
			JointAction ja) {
		List<TransitionProbability> ret = new ArrayList<TransitionProbability>();
		
		// save each possible transition
		List<State> nextStates = generateNextStates(s, ja);
//		System.out.println("Number of next states: "+nextStates.size());
		double prob;
//		double sum = 0;
		for (State nextState : nextStates) {
			prob = computeStateTransitionProbability(s, ja, nextState);
//			sum +=prob;
			if (prob > 0.0) {
				ret.add(new TransitionProbability(nextState, prob));
			}
		}
//		if(sum < 1.){
//			System.out.println("|NextStates|: "+nextStates.size()+"["+this.domain.getStateEnumerator().getEnumeratedID(s)+","+ja.actionName()+"] : Sum = "+sum);
//			try {
//				Thread.sleep(1000);
//			} catch (Exception ex) {
//				// do nothing
//			}
//		}
		
		return ret;
	}
	
	
	@Override
	protected State actionHelper(State s, JointAction ja) {
		List<TransitionProbability> transProbs = this.transitionProbsFor(s, ja);
		
		
		// randomly sample a transition
		Random rand = RandomFactory.getMapped(0);
		double r = rand.nextDouble();
		double sumProb = 0.0;
		for(TransitionProbability trans : transProbs){
			sumProb += trans.p;
			if(r < sumProb){
				return trans.s;
			}
		}

		throw new RuntimeException("Could not sample next state because transition probabilities did not sum to 1; they summed to " + sumProb);
	}
	
	/**
	 * Computes the possible next {@link State}s from a given {@link State} 
	 * and {@link JointAction}.
	 * 
	 * @param state The current {@link State}
	 * @param ja The {@link JointAction} performed in {@code state}
	 * 
	 * @return A list of possible next {@link State}s after {@code ja} is performed in {@code state}
	 */
	public List<State> generateNextStates(State state, JointAction ja) {
		List<State> nextStates = new ArrayList<State>();
		
		// get the agent number
		List<ObjectInstance> selfAgentStates = state.getObjectsOfClass(Wildfire.CLASS_SELF_AGENT);
		int agentNum = selfAgentStates.get(0).getIntValForAttribute(Wildfire.ATT_AGENTNUM);
		
		// most fire transitions are deterministic based on actions
		List<ObjectInstance> fires = state.getObjectsOfClass(Wildfire.CLASS_FIRE);
		ObjectInstance fire;
		int intensity, powers, x, y;
		int[][] nextIntensities = new int[fires.size()][];
		for (int i = 0; i < fires.size(); i++) {
			fire = fires.get(i);
			intensity = fire.getIntValForAttribute(Wildfire.ATT_INTENSITY);
			
			if (intensity == Wildfire.NUM_FIRE_STATES - 1) {
				// this fire burned out. it's intensity doesn't change
				nextIntensities[i] = new int[1];
				nextIntensities[i][0] = intensity;
				continue;
			}
			
			// did any agent work on this location?
			x = fire.getIntValForAttribute(Wildfire.ATT_X);
			y = fire.getIntValForAttribute(Wildfire.ATT_Y);
			powers = 0;
			for (GroundedSGAgentAction action : ja.getActionList()) {
				if (action.actionName().equals("x" + x + "y" + y)) {
					// add this agent's power to the amount of fire fighting
//					System.out.println("blahh: "+action.actingAgent+" - "+Wildfire.agentNum(action.actingAgent));
					powers += ((WildfireDomain )domain).agentTypes[Wildfire.agentNum(action.actingAgent)];
				}
			}
			
			if (powers > 0) {
				// at least one agent worked on this location
				 nextIntensities[i] = new int[1];
				 nextIntensities[i][0] = Math.max(0, intensity - powers);
			} else {
				// no one worked on this location
				
				// was it already on fire?
				if (intensity == 0) {
					// this location was not on fire, but it might start on fire
					nextIntensities[i] = new int[2];
					nextIntensities[i][0] = 0;
					nextIntensities[i][1] = Math.min(2,  Wildfire.NUM_FIRE_STATES - 2);
				} else if (intensity == Wildfire.NUM_FIRE_STATES - 2) {
					// this location is not burned out yet, but it might after this step
					nextIntensities[i] = new int[2];
					nextIntensities[i][0] = intensity;
					nextIntensities[i][1] = intensity + 1;
				} else {
					// the fire will increase by 1
					nextIntensities[i] = new int[1];
					nextIntensities[i][0] = intensity + 1;
				}
			}
		}
		
		// count the number of possible fire states
		int numFireStates = 1;
		for (int i = 0; i < nextIntensities.length; i++) {
			numFireStates *= nextIntensities[i].length;
		}
		
		// count the number of possible agent states
		List<ObjectInstance> agentStates = state.getObjectsOfClass(Wildfire.CLASS_AGENT);
		int numAgentStates = agentNum == Wildfire.MASTERSTATE_AGENTNUM ? 1 : Wildfire.NUM_AVAILABLE_STATES;
		for (int i = 0; i < agentStates.size(); i++) {
			numAgentStates *= Wildfire.NUM_AVAILABLE_STATES;
		}
		int numAgents = agentNum == Wildfire.MASTERSTATE_AGENTNUM 
				? agentStates.size() 
				: agentStates.size() + 1;
		
		State nextState;
		int fireStateNum, agentStateNum, agentIndex, available;
		for (int i = 0; i < numFireStates; i++) {
			for (int j = 0; j < numAgentStates; j++) {
				// start the state
				nextState = agentNum > Wildfire.MASTERSTATE_AGENTNUM
						? Wildfire.getCleanState((WildfireDomain) domain, agentNum)
						: Wildfire.getCleanMasterState((WildfireDomain) domain);
			    nextStates.add(nextState);
						
				// assign the fire values
				fireStateNum = i;
				for (int k = 0; k < nextIntensities.length; k++) {
					if (nextIntensities[k].length == 1) {
						Wildfire.setFire(nextState, 
								k, 
								fires.get(k).getIntValForAttribute(Wildfire.ATT_X),
								fires.get(k).getIntValForAttribute(Wildfire.ATT_Y),
								nextIntensities[k][0]);
					} else {
						// assign the intensity from this fireStateNum count
						intensity  = nextIntensities[k][fireStateNum % nextIntensities[k].length];
		                fireStateNum /= nextIntensities[k].length;
		                
		                // assign the intensity
						Wildfire.setFire(nextState, 
								k, 
								fires.get(k).getIntValForAttribute(Wildfire.ATT_X),
								fires.get(k).getIntValForAttribute(Wildfire.ATT_Y),
								intensity);
					}
				}
				
				// assign the agent values
				agentStateNum = j;
				for (int k = 0; k < numAgents; k++) {
					// figure out the availability
					available  = agentStateNum % Wildfire.NUM_AVAILABLE_STATES;
	                agentStateNum /= Wildfire.NUM_AVAILABLE_STATES;
					
					agentIndex = agentNum == Wildfire.MASTERSTATE_AGENTNUM
							? k : k - 1;
					if (agentIndex == -1) {
						// this is the self agent
						Wildfire.setSelfAgent(nextState, 
								selfAgentStates.get(0).getIntValForAttribute(Wildfire.ATT_X), 
								selfAgentStates.get(0).getIntValForAttribute(Wildfire.ATT_Y),
								selfAgentStates.get(0).getIntValForAttribute(Wildfire.ATT_POWER),
								available,
								agentNum);
					} else {
						// this is another agent
						Wildfire.setOtherAgent(nextState,
								agentIndex,
								agentStates.get(agentIndex).getIntValForAttribute(Wildfire.ATT_X), 
								agentStates.get(agentIndex).getIntValForAttribute(Wildfire.ATT_Y),
								agentStates.get(agentIndex).getIntValForAttribute(Wildfire.ATT_POWER),
								available,
								agentStates.get(agentIndex).getIntValForAttribute(Wildfire.ATT_AGENTNUM));
					}
				}
			}
		}
		
		
		return nextStates;
	}
	
	/**
     * Calculates the probability of a state transition between two states.
     * 
     * @param state The current state of the environment
     * @param ja The actions taken by each agent
     * @param nextState The next state of the environment
     * 
     * @return The probability that the environment goes from {@code state}
     *      to {@code nextState} after the agents perform {@code ja}
     */
    public double computeStateTransitionProbability(State state, 
    		JointAction ja, State nextState) {
        double totalProb = computeFireTransitionProbability(state, ja, nextState);
//        System.out.println("fire prob: " + totalProb);
        if (totalProb > 0.0) {
//        	double internalProb = computeInternalTransitionProbability(state, ja, nextState);
//			System.out.println("internal prob: " + internalProb);
//        	totalProb *= internalProb;
        	
            totalProb *= computeInternalTransitionProbability(state, ja, nextState);
        } 
        
        return totalProb;
    }
    
    /**
     * Computes the probability of a transition in the state of fires in an
     * agent's neighborhood.
     * 
     * @param state The current state of the environment
     * @param ja The actions taken by each agent
     * @param nextState The next state of the environment
     *  
     * @return The probability that the fire portion of {@code state}
     *      changes to {@code nextState} after the agents perform 
     *      {@code ja}
     */
    private double computeFireTransitionProbability(State state,
    		JointAction ja, State nextState) {
    	List<ObjectInstance> fireStates = state.getObjectsOfClass(Wildfire.CLASS_FIRE);
    	List<ObjectInstance> nextFireStates = nextState.getObjectsOfClass(Wildfire.CLASS_FIRE);
    	
    	// use the probability for each location
        int current, next, power;
        double totalProb = 1.0;
        int[] location;
        int agent;
        List<GroundedSGAgentAction> actions = ja.getActionList();
        GroundedSGAgentAction action;
        for (int i = 0; i < fireStates.size(); i++) {
            current = fireStates.get(i).getIntValForAttribute(Wildfire.ATT_INTENSITY);
            next = nextFireStates.get(i).getIntValForAttribute(Wildfire.ATT_INTENSITY);
           
            // compute the power of the agents' joint actions on this location
            location = new int[]{
            		fireStates.get(i).getIntValForAttribute(Wildfire.ATT_X),
            		fireStates.get(i).getIntValForAttribute(Wildfire.ATT_Y)
            };
            power = 0;
            for (int j = 0; j < actions.size(); j++) {
            	action = actions.get(j); 
                if (action.actionName().equals("x" + location[0]  + "y" + location[1])) {
                	agent = Wildfire.agentNum(action.actingAgent);
                    power += domain.agentTypes[agent];
                }
            }
            
            if (current == Wildfire.NUM_FIRE_STATES - 1) {
                if (next == Wildfire.NUM_FIRE_STATES - 1) {
                     // this is an absorbing state
                    totalProb *= 1.0;
                } else {
                    // this isn't possible
                    totalProb = 0.0;
                    break;
                }
            } else if (power == 0) {
                // no one is fighting this fire, so we expect it to increase
            	if (current == 0) {
                	double spread = probabilitySpread(state, i);
                    if (next == 0) {
                        // this is the inverse of the ignition probability
                        totalProb *= (1.0 - spread);
                    } else if (next == Math.min(2,  Wildfire.NUM_FIRE_STATES - 2)) {
                        // this is the ignition probability
                        totalProb *= spread;
                    } else {
                        // this isn't possible
                        totalProb = 0.0;
                        break;
                    }
                } else if (current == Wildfire.NUM_FIRE_STATES - 2) {
                    if (next == Wildfire.NUM_FIRE_STATES - 1) {
                        // this is the burnout probability
                        totalProb *= wildfireModel.burnoutProb;
                    } else if (next == Wildfire.NUM_FIRE_STATES - 2) {
                        // the fire didn't burn out
                        totalProb *= (1.0 - wildfireModel.burnoutProb);
                    } else {
                        // this isn't possible
                        totalProb = 0.0;
                        break;
                    }   
                } else if (current == next - 1) {
                    // the fire intensity should increase by 1
                    totalProb *= 1.0;
                } else {
                    // this isn't possible
                    totalProb = 0.0;
                    break;
                }
            } else {
                int expNext = Math.max(0, current - power);
                if (next == expNext) {
                    // this is what we expect
                    totalProb *= 1;
                } else {
                    // this isn't possible
                    totalProb = 0.0;
                    break;
                }
            } 
        }
        
        return totalProb;
    }
    
    /**
     * Calculates the probability of a fire spreading to a given location.
     * 
     * @param currentState The current state of fires in the agent's 
     *      neighborhood
     * @param position The position of the fire to calculation an ignition
     *      probability
     * 
     * @return The probability that the location in {@code position} starts
     *      on fire.
     */
    private double probabilitySpread(State state, int position) {
        // start with the initial, random spread probability for dealing with
        // locations outside the frontier
        double prob = WildfireParameters.RANDOM_SPREAD_PROB;
        
        // get the fire state variable
        List<ObjectInstance> fireStates = state.getObjectsOfClass(Wildfire.CLASS_FIRE);
        
        // get this fire's location
        int[] location = new int[]{
        		fireStates.get(position).getIntValForAttribute(Wildfire.ATT_X),
        		fireStates.get(position).getIntValForAttribute(Wildfire.ATT_Y)
        };
        
        int[] neighborLoc;
        int xDiff, yDiff, neighborFire;
        for (int i = 0; i < fireStates.size(); i++) {
        	if (i == position) continue;
        	
            // only consider locations on fire
        	neighborFire = fireStates.get(i).getIntValForAttribute(Wildfire.ATT_INTENSITY);
            if (neighborFire > 0 && neighborFire < Wildfire.NUM_FIRE_STATES - 1) {
                neighborLoc = new int[]{
                		fireStates.get(i).getIntValForAttribute(Wildfire.ATT_X),
                		fireStates.get(i).getIntValForAttribute(Wildfire.ATT_Y)
                };

                // which direction is this?
                xDiff = location[0] - neighborLoc[0];
                yDiff = location[1] - neighborLoc[1];
                
                // add the ignition probability
                if (xDiff == 0 && yDiff == 1) {
                    prob += wildfireModel.northIgnitionProb;
                } else if (xDiff == 0 && yDiff == -1) {
                    prob += wildfireModel.southIgnitionProb;
                } else if (xDiff == 1 && yDiff == 0) {
                    prob += wildfireModel.eastIgnitionProb;
                } else if (xDiff == -1 && yDiff == 0) {
                    prob += wildfireModel.westIgnitionProb;
                }
            }
        }
        
        return prob;
    }
	
	/**
     * Computes the probability of a transition in the internal state portion
     * of a the environment state.
     * 
     * @param state The current state of the environment
     * @param nextState The next state of the environment
     * 
     * @return The probability that the internal state portion of {@code state}
     *      changes to {@code nextState}
     */
    private double computeInternalTransitionProbability(State state,
    		JointAction ja, State nextState) {
    	List<ObjectInstance> selfAgentStates = state.getObjectsOfClass(Wildfire.CLASS_SELF_AGENT);
    	List<ObjectInstance> nextSelfAgentStates = nextState.getObjectsOfClass(Wildfire.CLASS_SELF_AGENT);
    	
    	List<ObjectInstance> agentStates = state.getObjectsOfClass(Wildfire.CLASS_AGENT);
    	List<ObjectInstance> nextAgentStates = nextState.getObjectsOfClass(Wildfire.CLASS_AGENT);
    	
        // check each agent
        int current, next;
        double prob = 1.0;
        
        // first check the self agent
        String agent;
        GroundedSGAgentAction action;
        
        if(selfAgentStates.get(0).getIntValForAttribute(Wildfire.ATT_AGENTNUM) == Wildfire.MASTERSTATE_AGENTNUM){
        	
        	// now check the other agents
	        for (int i = 0; i < agentStates.size(); i++) {
	        	// get the agent's action
	        	agent = Wildfire.agentName(agentStates.get(i).getIntValForAttribute(Wildfire.ATT_AGENTNUM));
	        	action = ja.action(agent);
	        	
		        current = agentStates.get(i).getIntValForAttribute(Wildfire.ATT_AVAILABLE);
	            next = nextAgentStates.get(i).getIntValForAttribute(Wildfire.ATT_AVAILABLE);
	            
	            if (Wildfire.NOOP.equals(action.actionName())) {
	            	if (current > 0 && current == next) {
		        		// the state doesn't change, as required
		        		prob *= 1.0;
		        	} else  if (current == 0 && next == 0) {
			            prob *= (1.0 - (1.0 / WildfireParameters.trueChargingTime));
			        } else if (current == 0 && next == Wildfire.NUM_AVAILABLE_STATES - 1) {
			            prob *= (1.0 / WildfireParameters.trueChargingTime);
			        } else {
			        	// this isn't allowed
			        	prob *= 0.0;
			        }
		        } else {
		        	if (current == 0 && next == 0) {
			            prob *= (1.0 - (1.0 / WildfireParameters.trueChargingTime));
			        } else if (current == 0 && next == Wildfire.NUM_AVAILABLE_STATES - 1) {
			            prob *= (1.0 / WildfireParameters.trueChargingTime);
			        } else if (current == next + 1) {
			            prob *= WildfireParameters.trueDischargeProb;
			        } else if (current == next) {
			            prob *= 1.0 - WildfireParameters.trueDischargeProb;
			        } else {
			        	// this isn't allowed
			        	prob *= 0.0;
			        }
		        }
		        
		        if (prob == 0.0) return prob;
	        }
        
        }else{
        	
        	if (selfAgentStates.get(0).getIntValForAttribute(Wildfire.ATT_AGENTNUM) > Wildfire.MASTERSTATE_AGENTNUM) {
	        	// get the agent's action
	        	agent = Wildfire.agentName(selfAgentStates.get(0).getIntValForAttribute(Wildfire.ATT_AGENTNUM));
	        	action = ja.action(agent);
	        	
	        	current = selfAgentStates.get(0).getIntValForAttribute(Wildfire.ATT_AVAILABLE);
		        next = nextSelfAgentStates.get(0).getIntValForAttribute(Wildfire.ATT_AVAILABLE);
		        
		        if (Wildfire.NOOP.equals(action.actionName())) {
		        	if (current > 0 && current == next) {
		        		// the state doesn't change, as required
		        		prob *= 1.0;
		        	} else  if (current == 0 && next == 0) {
			            prob *= (1.0 - (1.0 / WildfireParameters.chargingTime));
			        } else if (current == 0 && next == Wildfire.NUM_AVAILABLE_STATES - 1) {
			            prob *= (1.0 / WildfireParameters.chargingTime);
			        } else {
			        	// this isn't allowed
			        	prob *= 0.0;
			        }
		        } else {
		        	if (current == 0 && next == 0) {
			            prob *= (1.0 - (1.0 / WildfireParameters.chargingTime));
			        } else if (current == 0 && next == Wildfire.NUM_AVAILABLE_STATES - 1) {
			            prob *= (1.0 / WildfireParameters.chargingTime);
			        } else if (current == next + 1) {
			            prob *= WildfireParameters.dischargeProb;
			        } else if (current == next) {
			            prob *= 1.0 - WildfireParameters.dischargeProb;
			        } else {
			        	// this isn't allowed
			        	prob *= 0.0;
			        }
		        }
		        
		        if (prob == 0.0) return prob;
	        }
	        
	        // now check the other agents
	        for (int i = 0; i < agentStates.size(); i++) {
	        	// get the agent's action
	        	agent = Wildfire.agentName(agentStates.get(i).getIntValForAttribute(Wildfire.ATT_AGENTNUM));
	        	action = ja.action(agent);
	        	
		        current = agentStates.get(i).getIntValForAttribute(Wildfire.ATT_AVAILABLE);
	            next = nextAgentStates.get(i).getIntValForAttribute(Wildfire.ATT_AVAILABLE);
	            
	            if (Wildfire.NOOP.equals(action.actionName())) {
		        	if (current > 0 && current == next) {
		        		// the state doesn't change, as required
		        		prob *= 1.0;
		        	} else  if (current == 0 && next == 0) {
			            prob *= (1.0 - (1.0 / WildfireParameters.chargingTime));
			        } else if (current == 0 && next == Wildfire.NUM_AVAILABLE_STATES - 1) {
			            prob *= (1.0 / WildfireParameters.chargingTime);
			        } else {
			        	// this isn't allowed
			        	prob *= 0.0;
			        }
		        } else {
		        	if (current == 0 && next == 0) {
			            prob *= (1.0 - (1.0 / WildfireParameters.chargingTime));
			        } else if (current == 0 && next == Wildfire.NUM_AVAILABLE_STATES - 1) {
			            prob *= (1.0 / WildfireParameters.chargingTime);
			        } else if (current == next + 1) {
			            prob *= WildfireParameters.dischargeProb;
			        } else if (current == next) {
			            prob *= 1.0 - WildfireParameters.dischargeProb;
			        } else {
			        	// this isn't allowed
			        	prob *= 0.0;
			        }
		        }
		        
		        if (prob == 0.0) return prob;
	        }
	    }
        
        return prob;
    }
}
