package domains.wildfire;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import burlap.oomdp.auxiliary.DomainGenerator;
import burlap.oomdp.core.Attribute;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.ObjectClass;
import burlap.oomdp.core.objects.MutableObjectInstance;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.MutableState;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SimpleSGAgentAction;
import common.StateEnumerator;
import posg.ObservationFunction;
import posg.TabularBeliefState;

public class Wildfire implements DomainGenerator {
//	public static final int conf = 1;
	/** The number of fire states to use. */
	public static final int NUM_FIRE_STATES = 5;
	
	/** The number of suppressant levels to use. */
	public static final int NUM_AVAILABLE_STATES = 3;
	
	/** The intensityDiff for a NO_OBS observation. */
	public static final int NO_OBS = 1;
	
	/** The name of the noop action. */
	public static final String NOOP = "NOOP";
	
	/** The maximum fire fighting power of agents. */
	public static final int POWER = 3;
	
	/** The amount of noise to add to observations. */
	public static final double OBSERVATION_NOISE = 0.1;
	
	/** The penalty for trying to fight a non-existant fire. */
	public static final double NO_FIRE_PENALTY = -100.0;
	
	/** The range of an agent. */
	public static final int AGENT_RANGE = 1;
	
	/** The name of the attribute storing a x-coordinate. */
	public static final String ATT_X = "x";
	
	/** The name of the attribute storing a y-coordinate. */
	public static final String ATT_Y = "y";
	
	/** The name of the attribute storing a fire intensity. */
	public static final String ATT_INTENSITY = "intensity";
	
	/** The name of the attribute storing an agent's availability. */
	public static final String ATT_AVAILABLE= "available";
	
	/** The name of the attribute storing an agent's fire fighting power. */
	public static final String ATT_POWER= "power";
	
	/** The name of the attribute storing an agent's unique number (ID). */
	public static final String ATT_AGENTNUM= "agent_num";
	
	/** The name of the attribute storing an intensity difference in an observation. */
	public static final String ATT_OBS= "observation";
	
	/** The name of the fire ObjectClass. */
	public static final String CLASS_FIRE = "fire";
	
	/** The name of the self_agent ObjectClass. */
	public static final String CLASS_SELF_AGENT = "self_agent";
	
	/** The name of the agent ObjectClass. */
	public static final String CLASS_AGENT = "agent";
	
	/** The name of the observation ObjectClass. */
	public static final String CLASS_OBS= "obs";
	
	/** The value of ATT_AGENTNUM for the master state (that doesn't belong to any particular agent). */
	public static final int MASTERSTATE_AGENTNUM = -1;
	
	protected static List<Map<String, SGAgentType>> allAgentDefinitions = new ArrayList<Map<String, SGAgentType>>();

	private static boolean isSetAgentDefs = false;
	
//	public static StateEnumerator senum;
	
	@Override
	public Domain generateDomain() {
		WildfireDomain domain = new WildfireDomain();
		
		new WildfireParameters(1);
		
//		senum = new StateEnumerator(domain, new SimpleHashableStateFactory());
		
		// create the agent locations
		createAgents(domain);
		
		// create the fire locations
		createFires(domain);
		
		// build the neighborhoods
		buildNeighborhoods(domain);
		findNeighbors(domain);
		findVisibleFires(domain);
		
		// create the state and observation variables
		createStateObservationVariables(domain);
		
		// create the actions
		createActions(domain);
		
		ObservationFunction of = new WildfireObservations(domain);
		
		domain.setJointActionModel(new WildfireMechanics(domain));
		
		setAgentDefs(domain,WildfireParameters.numAgents);
		
//		domain.setStateEnumerator(new StateEnumerator(domain, new SimpleHashableStateFactory(), getAgentDefs(domain)));
		
		return domain;
	}
	
	public Domain generateDomain(int conf) {
		WildfireDomain domain = new WildfireDomain();
		
		new WildfireParameters(conf);
		
//		senum = new StateEnumerator(domain, new SimpleHashableStateFactory());
		
		// create the agent locations
		createAgents(domain);
		
		// create the fire locations
		createFires(domain);
		
		// build the neighborhoods
		buildNeighborhoods(domain);
		findNeighbors(domain);
		findVisibleFires(domain);
		
		// create the state and observation variables
		createStateObservationVariables(domain);
		
		// create the actions
		createActions(domain);
		
		ObservationFunction of = new WildfireObservations(domain);
		
		domain.setJointActionModel(new WildfireMechanics(domain));
		
		setAgentDefs(domain,WildfireParameters.numAgents);
		
//		domain.setStateEnumerator(new StateEnumerator(domain, new SimpleHashableStateFactory(), getAgentDefs(domain)));
		
		return domain;
	}
	
	public void setAgentDefs(WildfireDomain d, int numAgents){
		for(int currentAgentIndex=0;currentAgentIndex<numAgents;currentAgentIndex++){
			String aname1 = Wildfire.agentName(currentAgentIndex);
			SGAgentType atype1 = Wildfire.getAgentType((WildfireDomain) d,currentAgentIndex);
//			System.out.println("CurrentAgent: "+aname1+" Type: "+atype1.typeName);
			
			//find neighbors of subject agent
			int[] neighbors = ((WildfireDomain) d).neighbors[currentAgentIndex];
			int numNeighbors = neighbors.length;
			
//			System.out.println("Num of Neighbors: "+numNeighbors);
			
			//set agent definitions of agents in the local neighborhood of subject agent
			Map<String, SGAgentType> agentDefinitions = new HashMap<String, SGAgentType>();
			//add subject agent to agent definitions
			agentDefinitions.put(aname1, atype1);
			
			for(int a=0;a<numNeighbors;a++){
				String aname = Wildfire.agentName(neighbors[a]);
				SGAgentType atype = Wildfire.getAgentType((WildfireDomain) d,neighbors[a]);
//				System.out.println("Neighbor: "+aname+" Type: "+atype.typeName);
				agentDefinitions.put(aname, atype);
			}
			
			Wildfire.allAgentDefinitions.add(agentDefinitions);
		
		}
		
		
		Wildfire.isSetAgentDefs = true;
	}
	
	public static List<Map<String, SGAgentType>> getAllAgentDefs(WildfireDomain d){
		if(Wildfire.isSetAgentDefs ){
			return Wildfire.allAgentDefinitions;
		}else{
			throw new UnsupportedOperationException("Agent Definitions are not set!");
		}
	}
	
	
	
	/**
	 * Creates the set of agents in the {@link WildfireDomain}.
	 * 
	 * @param domain The {@link WildfireDomain} to create the agents in
	 */
	public void createAgents(WildfireDomain domain) {
		
		if (WildfireParameters.config == 1) {
			domain.agentLocations = new int[2][2];
			domain.agentTypes = new int[2];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 2;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 2;  // X location
			domain.agentLocations[1][1] = 2;  // Y location
			domain.agentTypes[1] = 1;
		}else if (WildfireParameters.config == 11) {
			domain.agentLocations = new int[2][2];
			domain.agentTypes = new int[2];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 2;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 2;  // X location
			domain.agentLocations[1][1] = 2;  // Y location
			domain.agentTypes[1] = 1;
		}else if (WildfireParameters.config == 12) {
			domain.agentLocations = new int[2][2];
			domain.agentTypes = new int[2];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 2;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 2;  // X location
			domain.agentLocations[1][1] = 2;  // Y location
			domain.agentTypes[1] = 2;
		}else if (WildfireParameters.config == 2) {
			domain.agentLocations = new int[3][2];
			domain.agentTypes = new int[3];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 2;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 2;  // X location
			domain.agentLocations[1][1] = 0;  // Y location
			domain.agentTypes[1] = 2;
			
			// assign the third agent
			domain.agentLocations[2][0] = 2;  // X location
			domain.agentLocations[2][1] = 2;  // Y location
			domain.agentTypes[2] = 3;
		}else if (WildfireParameters.config == 21) {
			domain.agentLocations = new int[3][2];
			domain.agentTypes = new int[3];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 2;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 2;  // X location
			domain.agentLocations[1][1] = 2;  // Y location
			domain.agentTypes[1] = 1;
			
			// assign the third agent
			domain.agentLocations[2][0] = 1;  // X location
			domain.agentLocations[2][1] = 2;  // Y location
			domain.agentTypes[2] = 1;
		}else if (WildfireParameters.config == 22) {
			domain.agentLocations = new int[3][2];
			domain.agentTypes = new int[3];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 2;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 2;  // X location
			domain.agentLocations[1][1] = 2;  // Y location
			domain.agentTypes[1] = 2;
			
			// assign the third agent
			domain.agentLocations[2][0] = 1;  // X location
			domain.agentLocations[2][1] = 2;  // Y location
			domain.agentTypes[2] = 3;
		}else if (WildfireParameters.config == 3) {
			domain.agentLocations = new int[5][2];
			domain.agentTypes = new int[5];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 3;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 4;  // X location
			domain.agentLocations[1][1] = 0;  // Y location
			domain.agentTypes[1] = 1;
			
			// assign the third agent
			domain.agentLocations[2][0] = 3;  // X location
			domain.agentLocations[2][1] = 2;  // Y location
			domain.agentTypes[2] = 2;
			
			// assign the fourth agent
			domain.agentLocations[3][0] = 2;  // X location
			domain.agentLocations[3][1] = 3;  // Y location
			domain.agentTypes[3] = 3;
			
			// assign the fifth agent
			domain.agentLocations[4][0] = 2;  // X location
			domain.agentLocations[4][1] = 1;  // Y location
			domain.agentTypes[4] = 3;
		}else if (WildfireParameters.config == 4) {
			domain.agentLocations = new int[5][2];
			domain.agentTypes = new int[5];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 3;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 4;  // X location
			domain.agentLocations[1][1] = 0;  // Y location
			domain.agentTypes[1] = 3;
			
			// assign the third agent
			domain.agentLocations[2][0] = 2;  // X location
			domain.agentLocations[2][1] = 1;  // Y location
			domain.agentTypes[2] = 2;
			
			// assign the fourth agent
			domain.agentLocations[3][0] = 2;  // X location
			domain.agentLocations[3][1] = 3;  // Y location
			domain.agentTypes[3] = 1;
			
			// assign the fifth agent
			domain.agentLocations[4][0] = 3;  // X location
			domain.agentLocations[4][1] = 2;  // Y location
			domain.agentTypes[4] = 1;
		}else if (WildfireParameters.config == 0) {
			domain.agentLocations = new int[2][2];
			domain.agentTypes = new int[2];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 1;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 2;  // X location
			domain.agentLocations[1][1] = 1;  // Y location
			domain.agentTypes[1] = 2;
		}else if (WildfireParameters.config == 5) {
			domain.agentLocations = new int[3][2];
			domain.agentTypes = new int[3];
			
			// assign the first agent
			domain.agentLocations[0][0] = 0;  // X location
			domain.agentLocations[0][1] = 1;  // Y location
			domain.agentTypes[0] = 1;
			
			// assign the second agent
			domain.agentLocations[1][0] = 2;  // X location
			domain.agentLocations[1][1] = 1;  // Y location
			domain.agentTypes[1] = 2;
			
			// assign the third agent
			domain.agentLocations[2][0] = 4;  // X location
			domain.agentLocations[2][1] = 1;  // Y location
			domain.agentTypes[2] = 3;
		}
	}
	
	
	private void createFires(WildfireDomain domain) {
		// build the lists of fire coordinates
		List<Integer> xList = new ArrayList<Integer>();
		List<Integer> yList = new ArrayList<Integer>();
		
		if(WildfireParameters.config == 1){
			//Manually setting fires for Config 1
			//F0 = (0,1) 	F2 = (2,1)		
			//F1 = (1,2) 	
			int[] fireX = {0,1,2};
			int[] fireY = {1,2,1};
			for (int i = 0; i < fireX.length; i++) {
				xList.add(fireX[i]);
				yList.add(fireY[i]);
			}
		}else if(WildfireParameters.config == 11){
			//Manually setting fires for Config 1
			//F0 = (0,1) 	F2 = (2,1)		
			//F1 = (1,1) 	
			int[] fireX = {0,1,2};
			int[] fireY = {1,1,1};
			for (int i = 0; i < fireX.length; i++) {
				xList.add(fireX[i]);
				yList.add(fireY[i]);
			}
		}else if(WildfireParameters.config == 12){
			//Manually setting fires for Config 1
			//F0 = (0,1) 	F2 = (2,1)		
			//F1 = (1,1) 	
			int[] fireX = {0,1,2};
			int[] fireY = {1,1,1};
			for (int i = 0; i < fireX.length; i++) {
				xList.add(fireX[i]);
				yList.add(fireY[i]);
			}
		}else if(WildfireParameters.config == 2){
			//Manually setting fires for Config 2
			//F0 = (0,1) 	F2 = (2,1) 		
			//F1 = (1,1) 	
			int[] fireX = {0,1,2};
			int[] fireY = {1,2,1};
			for (int i = 0; i < fireX.length; i++) {
				xList.add(fireX[i]);
				yList.add(fireY[i]);
			}
		}else if(WildfireParameters.config == 21){
			//Manually setting fires for Config 2
			//F0 = (0,1) 	F2 = (2,1) 		
			//F1 = (1,1) 	
			int[] fireX = {0,1,2};
			int[] fireY = {1,1,1};
			for (int i = 0; i < fireX.length; i++) {
				xList.add(fireX[i]);
				yList.add(fireY[i]);
			}
		}else if(WildfireParameters.config == 22){
			//Manually setting fires for Config 2
			//F0 = (0,1) 	F2 = (2,1) 		
			//F1 = (1,1) 	
			int[] fireX = {0,1,2};
			int[] fireY = {1,1,1};
			for (int i = 0; i < fireX.length; i++) {
				xList.add(fireX[i]);
				yList.add(fireY[i]);
			}
		}else if(WildfireParameters.config == 3){
			//Manually setting fires for Config 3
			//F0 = (1,3) 	F2 = (3,0) 		F3 = (0,0)
			//F1 = (2,2) 	
			int[] fireX = {1,2,3};
			int[] fireY = {3,2,0};
			for (int i = 0; i < fireX.length; i++) {
				xList.add(fireX[i]);
				yList.add(fireY[i]);
			}
		}else if(WildfireParameters.config == 4){
			//Manually setting fires for Config 4
			//F0 = (1,3) 	F2 = (4,1) 		F4 = (0,0)
			//F1 = (2,2) 	F3 = (2,0)
			int[] fireX = {1,2,4,2,0};
			int[] fireY = {3,2,1,0,0};
			for (int i = 0; i < fireX.length; i++) {
				xList.add(fireX[i]);
				yList.add(fireY[i]);
			}
		}else{
			boolean agentInLoc;
			for (int x = 0; x < WildfireParameters.width; x++) {
				for (int y = 0; y < WildfireParameters.height; y++) {
					// is there an agent here?
					agentInLoc = false;
					for (int i = 0; i < domain.agentLocations.length; i++) {
						if (domain.agentLocations[i][0] == x 
								&& domain.agentLocations[i][1] == y) {
							agentInLoc = true;
							break;
						}
					}
					
					if (!agentInLoc) {
						xList.add(x);
						yList.add(y);
					}
				}
			}
		}
		
		// save the locations in the domain
		
		domain.fireLocations = new int[xList.size()][2];
		for (int i = 0; i < domain.fireLocations.length; i++) {
			domain.fireLocations[i][0] = xList.get(i).intValue();
			domain.fireLocations[i][1] = yList.get(i).intValue();
		}
	}
	
	/**
     * Builds the neighborhoods of locations for each agent.
     * 
     * @param domain The {@link WildfireDomain} to save the neighborhoods in
     */
    private void buildNeighborhoods(WildfireDomain domain) {
    	// create the neighborhoods
        List<List<Integer[]>> neighborhoodLists = 
                new ArrayList<List<Integer[]>>();
                
        int xDiff, yDiff;
        for (int i = 0; i < WildfireParameters.numAgents; i++) {
            neighborhoodLists.add(new ArrayList<Integer[]>());
            
            for (int j = 0; j < domain.fireLocations.length; j++) {
            	xDiff = Math.abs(
            			domain.agentLocations[i][0] - domain.fireLocations[j][0]);
            	
//            	System.out.println("["+i+"] "+xDiff+" = (domain.agentLocations["+i+"][0] = "+domain.agentLocations[i][0]+
//            			") - (domain.fireLocations["+j+"][0] = "+domain.fireLocations[j][0]+")");
            	
            	yDiff = Math.abs(
            			domain.agentLocations[i][1] - domain.fireLocations[j][1]);
            	
//            	System.out.println("["+i+"] "+yDiff+" = (domain.agentLocations["+i+"][1] = "+domain.agentLocations[i][1]+
//            			") - (domain.fireLocations["+j+"][1] = "+domain.fireLocations[j][1]+")");
            	
            	if (xDiff <= AGENT_RANGE && yDiff <= AGENT_RANGE) {
            		neighborhoodLists.get(i).add(new Integer[]{
            				domain.fireLocations[j][0],
            				domain.fireLocations[j][1]});
//            		System.out.println("Is neighbor...! because ["+xDiff+" and "+yDiff+"] <= "+ AGENT_RANGE);
            	}
            }
//            for(Integer[] nList:neighborhoodLists.get(i)){
//            	System.out.println(i+" X: "+nList[0] + " Y: " +nList[1]);
//            }
            
        }
        
        
        
        // save the neighborhoods to the array in the WildfireDomain
        Integer[] coords;
        domain.neighborhoods = new int[WildfireParameters.numAgents][][];
        for (int i = 0; i < neighborhoodLists.size(); i++) {
        	domain.neighborhoods[i] = new int[neighborhoodLists.get(i).size()][2];
        	for (int j = 0; j < neighborhoodLists.get(i).size(); j++) {
        		coords = neighborhoodLists.get(i).get(j);
        		
        		domain.neighborhoods[i][j][0] = coords[0];
        		domain.neighborhoods[i][j][1] = coords[1];
        	}
        }
    }
    
    /**
     * Determines which agents are neighbors of one another, based on their
     * location neighborhoods.
     * 
     * @param domain The {@link WildfireDomain} to save the neighbor relationships in
	 */
    private void findNeighbors(WildfireDomain domain) {
    	List<List<Integer>> neighborLists = new ArrayList<List<Integer>>();
        for (int i = 0; i < WildfireParameters.numAgents; i++) {
        	neighborLists.add(new ArrayList<Integer>());
        }
        
        boolean overlap;
        for (int i = 0; i < WildfireParameters.numAgents - 1; i++) {
            for (int j = i + 1; j < WildfireParameters.numAgents; j++) {
                // look for overlap in the neighborhoods
                overlap = false;
                for (int iIndex = 0; iIndex < domain.neighborhoods[i].length; iIndex++) {
                    for (int jIndex = 0; jIndex < domain.neighborhoods[j].length; jIndex++) {
                        if (domain.neighborhoods[i][iIndex][0] == domain.neighborhoods[j][jIndex][0]
                        		&& domain.neighborhoods[i][iIndex][1] == domain.neighborhoods[j][jIndex][1]) {
                            overlap = true;
                            break;
                        }
                    }
                }
                
                if (overlap) {
                	neighborLists.get(i).add(j);
                	neighborLists.get(j).add(i);
                }
            }
        }
        
        // save the neighbors to the array
        domain.neighbors = new int[WildfireParameters.numAgents][];
        for (int i = 0; i < neighborLists.size(); i++) {
        	domain.neighbors[i] = new int[neighborLists.get(i).size()];
        	
        	for (int j = 0; j < neighborLists.get(i).size(); j++) {
        		domain.neighbors[i][j] = neighborLists.get(i).get(j);
        	}
        }
    
    }
    
    private void findVisibleFires(WildfireDomain domain) {
    	domain.visibleFireIndices = new int[WildfireParameters.numAgents][];
    	
    	// determine which fires each agent should have access to
    	List<Integer> visibleFireIndices = new ArrayList<Integer>();
    	for (int agent = 0; agent < WildfireParameters.numAgents; agent++) {
    		visibleFireIndices.clear();
    		
    		// save the fires that this agent can see
    		for (int fire = 0; fire < domain.fireLocations.length; fire++) {
    			if (visibleFireIndices.contains(fire)) continue;
    			
    			for (int i = 0; i < domain.neighborhoods[agent].length; i++) {
					if (domain.neighborhoods[agent][i][0] == domain.fireLocations[fire][0]
							&& domain.neighborhoods[agent][i][1] == domain.fireLocations[fire][1]) {
						visibleFireIndices.add(fire);
						break;
					}
    			}
    		}
    			
			//save the fires that the agent's neighbors can see
			int neighbor;
			for (int k = 0; k < domain.neighbors[agent].length; k++) {
				neighbor = domain.neighbors[agent][k];
				
				for (int fire = 0; fire < domain.fireLocations.length; fire++) {
					if (visibleFireIndices.contains(fire)) continue;
					
    				for (int i = 0; i < domain.neighborhoods[neighbor].length; i++) {
    					if (domain.neighborhoods[neighbor][i][0] == domain.fireLocations[fire][0]
    							&& domain.neighborhoods[neighbor][i][1] == domain.fireLocations[fire][1]) {
    						visibleFireIndices.add(fire);
    						break;
    					}
    				}
    			}
        	}
			
			// save the fires that this agent should have visible
			Collections.sort(visibleFireIndices);
			domain.visibleFireIndices[agent] = new int[visibleFireIndices.size()];
			for (int i = 0; i < visibleFireIndices.size(); i++) {
				domain.visibleFireIndices[agent][i] = visibleFireIndices.get(i).intValue();
			}
    	}
    }
	
    /**
     * Creates the state and observation variables in a {@link WildfireDomain}.
     * 
     * @param domain The {@link WildfireDomain} to save the variables in
     */
	private void createStateObservationVariables(WildfireDomain domain) {
		// create the attributes for the fire variables in the state
		Attribute xAtt = new Attribute(domain, ATT_X, Attribute.AttributeType.INT);
		xAtt.setLims(0, WildfireParameters.width - 1);
		
		Attribute yAtt = new Attribute(domain, ATT_Y, Attribute.AttributeType.INT);
		yAtt.setLims(0, WildfireParameters.height - 1);
		
		Attribute intensityAtt = new Attribute(domain, ATT_INTENSITY, Attribute.AttributeType.INT);
		intensityAtt.setLims(0, NUM_FIRE_STATES - 1);
		
		// create the ObjectClass for the fire variables in the state
		ObjectClass fireClass = new ObjectClass(domain, CLASS_FIRE);
		fireClass.addAttribute(xAtt);
		fireClass.addAttribute(yAtt);
		fireClass.addAttribute(intensityAtt);
		
		// create the attributes for the agent variables in the state
		Attribute availableAtt = new Attribute(domain, ATT_AVAILABLE, Attribute.AttributeType.INT);
		availableAtt.setLims(0, NUM_AVAILABLE_STATES - 1);
		
		Attribute powerAtt = new Attribute(domain, ATT_POWER, Attribute.AttributeType.INT);
		powerAtt.setLims(1, POWER);
		
		Attribute agentNumAtt = new Attribute(domain, ATT_AGENTNUM, Attribute.AttributeType.INT);
		agentNumAtt.setLims(0, WildfireParameters.numAgents - 1);
		
		// create the ObjectClass for the self agent variable in the state
		ObjectClass selfAgentClass = new ObjectClass(domain, CLASS_SELF_AGENT);
		selfAgentClass.addAttribute(xAtt);
		selfAgentClass.addAttribute(yAtt);
		selfAgentClass.addAttribute(availableAtt);
		selfAgentClass.addAttribute(powerAtt);
		selfAgentClass.addAttribute(agentNumAtt);
		
		// create the ObjectClass for the other agents' variables in the state
		ObjectClass agentClass = new ObjectClass(domain, CLASS_AGENT, true);
		agentClass.addAttribute(xAtt);
		agentClass.addAttribute(yAtt);
		agentClass.addAttribute(availableAtt);
		agentClass.addAttribute(powerAtt);
		agentClass.addAttribute(agentNumAtt);
		
		// create the attributes for the variables in the observations
		Attribute obsAtt = new Attribute(domain, ATT_OBS, Attribute.AttributeType.INT);
		obsAtt.setLims(2 - NUM_FIRE_STATES, NO_OBS);
		
		ObjectClass obsClass = new ObjectClass(domain, CLASS_OBS);
		obsClass.addAttribute(obsAtt);
	}
	
	/**
	 * Creates the set of actions in the {@link WildfireDomain}.
	 * 
	 * @param domain The {@link WildfireDomain} to save the actions in
	 */
	private void createActions(WildfireDomain domain) {
		// build a set of actions
		String actionName;
		SimpleSGAgentAction sa;
		for (int agent = 0; agent < domain.agentTypes.length; agent++) {
			// add one action for each location in the agent's neighborhood
			for (int i = 0; i < domain.neighborhoods[agent].length; i++) {
				actionName = "x" + domain.neighborhoods[agent][i][0]
						+ "y" + domain.neighborhoods[agent][i][1];
				sa = new SimpleSGAgentAction(domain, actionName);
				
				// also register this action with this agent
				domain.addSGAgentAction(sa, agentName(agent));
			}
			
			// also add the NOOP action
			sa = new SimpleSGAgentAction(domain, NOOP);
			
			// also register this action with this agent
			domain.addSGAgentAction(sa, agentName(agent));
		}
//		System.out.println(domain.getActionsPerAgent().get(agentName(0)).size());
	}

	
	/**
	 * Returns an empty state with the correct state variables for a particular agent.
	 * 
	 * @param d The {@link WildfireDomain}
	 * @param agent The agent we are creating the state for
	 * 
	 * @return An empty state for {@code agent}
	 */
	public static State getCleanState(WildfireDomain d, int agent){
		State s = new MutableState();
//		System.out.println(d.visibleFireIndices[agent].length + " "+d.neighbors[agent].length);
		addNObjects(d, s, CLASS_FIRE, d.visibleFireIndices[agent].length);
		addNObjects(d, s, CLASS_SELF_AGENT, 1);
		addNObjects(d, s, CLASS_AGENT, d.neighbors[agent].length);
		
		return s;
	}
	
	/**
	 * Returns an empty master state with the correct state variables.
	 * 
	 * @param d The {@link WildfireDomain}
	 * 
	 * @return An empty master state
	 */
	public static State getCleanMasterState(WildfireDomain d){
		State s = new MutableState();
		
//		int numFires = WildfireParameters.height * WildfireParameters.width - WildfireParameters.numAgents;
		int numFires = d.fireLocations.length;
		addNObjects(d, s, CLASS_FIRE, numFires);
		addNObjects(d, s, CLASS_SELF_AGENT, 1);
		addNObjects(d, s, CLASS_AGENT, WildfireParameters.numAgents);
		
		// set the self agent to not be an agent but instead signify the master state across all agents
		setSelfAgent(s, -1, -1, 0, NUM_AVAILABLE_STATES - 1, MASTERSTATE_AGENTNUM);
		
		return s;
	}
	
	public static List<State> enumerateStates(WildfireDomain d, State localWorld) {
		List<ObjectInstance> fires = localWorld.getObjectsOfClass(Wildfire.CLASS_FIRE);
        ObjectInstance agent = localWorld.getFirstObjectOfClass(Wildfire.CLASS_SELF_AGENT);
        List<ObjectInstance> otherAgents = localWorld.getObjectsOfClass(Wildfire.CLASS_AGENT);
        
		// first count the number of needed states
		int numStates = 1;
        int numFires = fires.size();
        int numAgents = otherAgents.size() + 1;
        for (int j = 0; j < numFires; j++) {
        	numStates *= Wildfire.NUM_FIRE_STATES;
        }
        for (int j = 0; j < numAgents; j++) {
        	numStates *= Wildfire.NUM_AVAILABLE_STATES;
        }
        
        int stateIndex, intensity, available;
        List<State> list = new ArrayList<State>();
        State state;
        for (int j = 0; j < numStates; j++) {
        	stateIndex = j;
        	state = Wildfire.getCleanState(d, 
        			agent.getIntValForAttribute(ATT_AGENTNUM));
        	
        	for (int k = 0; k < numFires; k++) {
        		intensity = stateIndex % Wildfire.NUM_FIRE_STATES;
        		stateIndex /= Wildfire.NUM_FIRE_STATES;
        		Wildfire.setFire(state, k, 
        				fires.get(k).getIntValForAttribute(Wildfire.ATT_X), 
        				fires.get(k).getIntValForAttribute(Wildfire.ATT_Y),
        				intensity);
        	}
        	
        	available = stateIndex % Wildfire.NUM_AVAILABLE_STATES;
        	stateIndex /= Wildfire.NUM_AVAILABLE_STATES;
        	Wildfire.setSelfAgent(state, 
        			agent.getIntValForAttribute(Wildfire.ATT_X),
        			agent.getIntValForAttribute(Wildfire.ATT_Y), 
        			agent.getIntValForAttribute(Wildfire.ATT_POWER),
        			available, 
        			agent.getIntValForAttribute(Wildfire.ATT_AGENTNUM));
        	
        	for (int k = 1; k < numAgents; k++) {
        		available = stateIndex % Wildfire.NUM_AVAILABLE_STATES;
            	stateIndex /= Wildfire.NUM_AVAILABLE_STATES;
            	Wildfire.setOtherAgent(state,
            			k - 1,
            			otherAgents.get(k - 1).getIntValForAttribute(Wildfire.ATT_X),
            			otherAgents.get(k - 1).getIntValForAttribute(Wildfire.ATT_Y), 
            			otherAgents.get(k - 1).getIntValForAttribute(Wildfire.ATT_POWER),
            			available, 
            			otherAgents.get(k - 1).getIntValForAttribute(Wildfire.ATT_AGENTNUM));
        	}
        	
        	list.add(state);
        }
        
        return list;
	}

	/**
	 * Creates a fully observable initial state for a particular agent.
	 * 
	 * @param d The {@link WildfireDomain}
	 * @param agent The agent to create the initial state for
	 * 
	 * @return A fully observable initial state for {@code agent} 
	 */
	public static State getInitialState(WildfireDomain d, int agent) {
		StateEnumerator senum = d.getStateEnumerator();
		
		State s = getCleanState(d, agent);
		
		// set the initial fire states
		// NOTE: we start with no fires
		int fire;
		for (int j = 0; j < d.visibleFireIndices[agent].length; j++) {
			fire = d.visibleFireIndices[agent][j];
			setFire(s, j, 
					d.fireLocations[fire][0],
					d.fireLocations[fire][1],
					NUM_FIRE_STATES - 2);
		}
		
		// set the self state
		// NOTE: we assume the agent is present to start
		setSelfAgent(s, 
				d.agentLocations[agent][0], 
				d.agentLocations[agent][1], 
				d.agentTypes[agent], 
				NUM_AVAILABLE_STATES - 1, 
				agent);

		// set the other agents' states
		// NOTE: we assume each neighbor is present to start
		int otherAgent;
		for (int i = 0; i < d.neighbors[agent].length; i++) {
			otherAgent = d.neighbors[agent][i];
			setOtherAgent(s, 
					i, 
					d.agentLocations[otherAgent][0], 
					d.agentLocations[otherAgent][1], 
					d.agentTypes[otherAgent], 
					NUM_AVAILABLE_STATES - 1,
					otherAgent);
		}
		senum.getEnumeratedID(s);
		

		return s;
	}
	
	public static State toggleAvailabilityForAgent(State curState, int agent){
		System.out.println("OLD STATE:\n"+curState.getCompleteStateDescription());
		int agentAvailability = curState.getObjectsOfClass(CLASS_AGENT).get(agent).getIntValForAttribute(ATT_AVAILABLE);
		System.out.println("agentAvailability: "+agentAvailability);
		int toggle  = (agentAvailability == 1) ? 0:1;
		System.out.println("toggle: "+toggle);
		State newState = new MutableState();
		newState = (MutableState) curState.copy();
		newState.getObjectsOfClass(CLASS_AGENT).get(agent).setValue(ATT_AVAILABLE, toggle);
		System.out.println("OLD STATE:\n"+curState.getCompleteStateDescription());
		System.out.println("NEW STATE:\n"+newState.getCompleteStateDescription());
		
		return newState;
		
	}
	
	/**
	 * Creates a partially observable next state for a particular agent.
	 * 
	 * @param d The {@link WildfireDomain}
	 * @param agent The agent to create the initial belief state for
	 * 
	 * @return An initial belief state for {@code agent}
	 */
	public static TabularBeliefState getInitialBeliefState(WildfireDomain d, int agent, StateEnumerator senum) {
		List<Integer> sIDList = new ArrayList<Integer>(); 
		// there are two possible states for each neighbor, each with equal probability
		int numAgents = d.neighbors[agent].length;
		int numAgentStates = 1; // the agent knows it is around to start
		for (int i = 0; i < numAgents; i++) {
			numAgentStates *= NUM_AVAILABLE_STATES;
		}
		
		State state;
		List<State> initialStates = new ArrayList<State>();
		int agentStateNum, available, neighborNum, fire;
		for (int i = 0; i < numAgentStates; i++) {
			state = getCleanState(d, agent);
			initialStates.add(state);
			
			// set the initial fire states
			// NOTE: we assume there are no fires to start
			for (int j = 0; j < d.visibleFireIndices[agent].length; j++) {
				fire = d.visibleFireIndices[agent][j];
				setFire(state, j, 
						d.fireLocations[fire][0],
						d.fireLocations[fire][1],
						NUM_FIRE_STATES - 2);
			}
			
			// set the self state
			// NOTE: we assume the agent is present to start
			setSelfAgent(state, 
					d.agentLocations[agent][0], 
					d.agentLocations[agent][1], 
					d.agentTypes[agent], 
					NUM_AVAILABLE_STATES - 1, 
					agent);
			
			// fill in the details for the other agents
			agentStateNum = i;
			for (int j = 0; j < numAgents; j++) {
				// figure out the availability
				available  = agentStateNum % NUM_AVAILABLE_STATES;
                agentStateNum /= NUM_AVAILABLE_STATES;
				
				// save the other agent's availability
                neighborNum = d.neighbors[agent][j];
				Wildfire.setOtherAgent(state,
						j,
						d.agentLocations[neighborNum][0],
						d.agentLocations[neighborNum][1],
						d.agentTypes[neighborNum],
						available,
						neighborNum);
			}
			sIDList.add(senum.getEnumeratedID(state));
//			System.out.println(senum.getEnumeratedID(state));
		}
//		System.out.println("States enumerated!!");

		
		// TODO create the belief state from the list of possible states
		// NOTE: each has equal initial probability
		
		TabularBeliefState initBeliefState = new TabularBeliefState(d, senum, agentName(agent));		
		initBeliefState.initializeBeliefsUniformly(sIDList);
		
		return initBeliefState;		
	}
	
//public static TabularBeliefState getBeliefState(WildfireDomain d, int agent) {
//		
////		StateEnumerator senum = new StateEnumerator(d, new SimpleHashableStateFactory(), Wildfire.getAllAgentDefs(d).get(agent));
//		StateEnumerator senum = d.getStateEnumerator();
//		List<Integer> sIDList = new ArrayList<Integer>(); 
//		// there are two possible states for each neighbor, each with equal probability
//		int numAgents = d.neighbors[agent].length;
//		int numAgentStates = 1; // the agent knows it is around to start
//		for (int i = 0; i < numAgents; i++) {
//			numAgentStates *= NUM_AVAILABLE_STATES;
//		}
//		
//		State state;
//		List<State> initialStates = new ArrayList<State>();
//		int agentStateNum, available, neighborNum, fire;
//		for (int i = 0; i < numAgentStates; i++) {
//			state = getCleanState(d, agent);
//			initialStates.add(state);
//			
//			// set the initial fire states
//			// NOTE: we assume there are no fires to start
//			for (int j = 0; j < d.visibleFireIndices[agent].length; j++) {
//				fire = d.visibleFireIndices[agent][j];
//				setFire(state, j, 
//						d.fireLocations[fire][0],
//						d.fireLocations[fire][1],
//						3);
//			}
//			
//			// set the self state
//			// NOTE: we assume the agent is present to start
//			setSelfAgent(state, 
//					d.agentLocations[agent][0], 
//					d.agentLocations[agent][1], 
//					d.agentTypes[agent], 
//					NUM_AVAILABLE_STATES - 1, 
//					agent);
//			
//			// fill in the details for the other agents
//			agentStateNum = i;
//			for (int j = 0; j < numAgents; j++) {
//				// figure out the availability
//				available  = agentStateNum % NUM_AVAILABLE_STATES;
//                agentStateNum /= NUM_AVAILABLE_STATES;
//				
//				// save the other agent's availability
//                neighborNum = d.neighbors[agent][j];
//				Wildfire.setOtherAgent(state,
//						j,
//						d.agentLocations[neighborNum][0],
//						d.agentLocations[neighborNum][1],
//						d.agentTypes[neighborNum],
//						available,
//						neighborNum);
//			}
//			sIDList.add(senum.getEnumeratedID(state));
//			
//			
////			System.out.println(senum.getEnumeratedID(state));
//		}
////		System.out.println("States enumerated!!");
//
//		
//		// TODO create the belief state from the list of possible states
//		// NOTE: each has equal initial probability
//		
//		d.setStateEnumerator(senum);		
//		TabularBeliefState initBeliefState = new TabularBeliefState(d, senum, agentName(agent));		
//		initBeliefState.initializeBeliefsUniformly(sIDList);
//		
////		JointAction targetJA = null;
////		for(JointAction ja: JointAction.getAllJointActions(senum.getStateForEnumerationId(0), getAllAgentDefs(d).get(agent))){
////			if((ja.action(Wildfire.agentName(0)).action.actionName.equals("x1y1")) && (ja.action(Wildfire.agentName(1)).action.actionName.equals("x1y1"))){
////				targetJA = ja;
////				System.out.println(" "+targetJA.actionName());
////			}
////		}
//		
////		TabularBeliefState nextB = new TabularBeliefState(d, senum, agentName(agent)); 
////		nextB = (TabularBeliefState) initBeliefState.getUpdatedBeliefState(d.getObservationFunction().getAllPossibleObservations().get(0),targetJA);
////		for(StateBelief sb: nextB.getStatesAndBeliefsWithNonZeroProbability()){
////			System.out.print(" "+sb.belief);
////		}
//		
//		return nextB;		
//	}

	
	
	public static State getInitialMasterState(WildfireDomain d) {
		State s = getCleanMasterState(d);
		
		// set the initial fire states
		// NOTE: we assume there are no fires to start
		for (int fire = 0; fire < d.fireLocations.length; fire++) {
			setFire(s, fire, 
					d.fireLocations[fire][0],
					d.fireLocations[fire][1],
					NUM_FIRE_STATES - 2);
		}
		
		// NOTE: the self state is set in getCleanMasterState
		
		// set the  agents' states
		// NOTE: we assume each agent is present to start
		for (int i = 0; i < WildfireParameters.numAgents; i++) {
			setOtherAgent(s, i, d.agentLocations[i][0], 
					d.agentLocations[i][1], 
					d.agentTypes[i], 
					NUM_AVAILABLE_STATES - 1,
					i);
		}

		return s;
	}
	
	
	/**
	 * Sets an agent's own attribute values
	 * 
	 * @param s the state in which the agent exists
	 * @param i indicates the ith agent object in the state whose values should be set
	 * @param x the x position of the agent
	 * @param y the y position of the agent
	 * @param power the fire fighting power of the agent
	 * @param available Whether the agent is currently available to fight fires
	 * @param agentNum The agent's unique number (ID)
	 */
	public static void setSelfAgent(State s, int x, int y, int power, int available, int agentNum) {
		ObjectInstance agent = s.getObjectsOfClass(CLASS_SELF_AGENT).get(0);
		agent.setValue(ATT_X, x);
		agent.setValue(ATT_Y, y);
		agent.setValue(ATT_POWER, power);
		agent.setValue(ATT_AVAILABLE, available);
		agent.setValue(ATT_AGENTNUM, agentNum);
	}
	
	/**
	 * Sets an other agent's attribute values
	 * 
	 * @param s the state in which the agent exists
	 * @param i indicates the ith agent object in the state whose values should be set
	 * @param x the x position of the agent
	 * @param y the y position of the agent
	 * @param power the fire fighting power of the agent
	 * @param available Whether the agent is currently available to fight fires
	 * @param agentNum The agent's unique number (ID)
	 */
	public static void setOtherAgent(State s, int i, int x, int y, int power, int available, int agentNum) {
		ObjectInstance agent = s.getObjectsOfClass(CLASS_AGENT).get(i);
		agent.setValue(ATT_X, x);
		agent.setValue(ATT_Y, y);
		agent.setValue(ATT_POWER, power);
		agent.setValue(ATT_AVAILABLE, available);
		agent.setValue(ATT_AGENTNUM, agentNum);
	}
	
	
	
	/**
	 * Sets a fire location objects attribute values
	 * @param s the state in which the fire might exist
	 * @param i indicates the ith location object whose values should be set
	 * @param x the x position of the location
	 * @param y the y position of the location
	 * @param intensity the intensity of the fire location
	 */
	public static void setFire(State s, int i, int x, int y, int intensity){
//		System.out.println("** "+i);
		ObjectInstance goal = s.getObjectsOfClass(CLASS_FIRE).get(i);
		goal.setValue(ATT_X, x);
		goal.setValue(ATT_Y, y);
		goal.setValue(ATT_INTENSITY, intensity);
	}
	
	/**
	 * AddsN objects of a specific object class to a state object
	 * @param d the domain of the object classes
	 * @param s the state to which the objects of the specified class should be added
	 * @param className the name of the object class for which to create object instances
	 * @param n the number of object instances to create
	 */
	protected static void addNObjects(Domain d, State s, String className, int n){
		for(int i = 0; i < n; i++){
			ObjectInstance o = new MutableObjectInstance(d.getObjectClass(className), className+i);
			s.addObject(o);
		}
	}
	
	/**
	 * Creates a new observation.
	 * 
	 * @param domain The {@link WildfireDomain}
	 * @param intensityChange The observed intensity difference
	 * 
	 * @return An observation for {@code intensityChange}
	 */
	public static State createObservation(WildfireDomain domain, int intensityChange) {
		State s = new MutableState();
		
		ObjectInstance o = new MutableObjectInstance(domain.getObjectClass(CLASS_OBS), CLASS_OBS);
		o.setValue(ATT_OBS, intensityChange);
		s.addObject(o);
//		System.out.println("Here.."+s.getCompleteStateDescription());
		return s;
	}
	
	/**
	 * Creates an individual agent's view on the current state of the environment based on the
	 * complete current master state.
	 * 
	 * @param d The {@link WildfireDomain}
	 * @param agent The agent to create a current state for
	 * @param masterState The current master environment state
	 * 
	 * @return {@code agent}'s view of {@code masterState}
	 */
	public static State createAgentStateFromMasterState(WildfireDomain d, int agent, State masterState) {
		State state = getCleanState(d, agent);
		
		// save the fires that this agent or its neighbors can see
		List<ObjectInstance> fires = masterState.getObjectsOfClass(CLASS_FIRE);
		ObjectInstance fire;
		for (int i = 0; i < d.visibleFireIndices[agent].length; i++) {
			fire = fires.get(d.visibleFireIndices[agent][i]);
			setFire(state, i, 
					fire.getIntValForAttribute(ATT_X),
					fire.getIntValForAttribute(ATT_Y),
					fire.getIntValForAttribute(ATT_INTENSITY));
		}
		
		// save the agent's internal state
		List<ObjectInstance> agents = masterState.getObjectsOfClass(CLASS_AGENT);
		setSelfAgent(state, 
				d.agentLocations[agent][0], 
				d.agentLocations[agent][1], 
				d.agentTypes[agent], 
				agents.get(agent).getIntValForAttribute(ATT_AVAILABLE), 
				agent);
		
		// save the other agents' states
		int otherAgent;
		for (int i = 0; i < d.neighbors[agent].length; i++) {
			otherAgent = d.neighbors[agent][i];
			setOtherAgent(state,
					i,
					d.agentLocations[otherAgent][0], 
					d.agentLocations[otherAgent][1], 
					d.agentTypes[otherAgent], 
					agents.get(otherAgent).getIntValForAttribute(ATT_AVAILABLE), 
					otherAgent);
		}
		
		return state;
	}
	
	public static State createOtherAgentStateFromLocalState(WildfireDomain d, int otherAgent, State agentState) {
		int agentNumber = agentState.getObjectsOfClass(CLASS_SELF_AGENT).get(0).getIntValForAttribute(ATT_AGENTNUM);
		
		if(agentNumber == otherAgent){
			return agentState;
		}
		
		// NOTE: we use our own id here because we want to use our number of fires and neighbors, in case we have more or less than otherAgent
		// This is because we are making a mock state for otherAgent in our local world view
		State state = getCleanState(d, agentNumber);
		
		// copy the fires
		List<ObjectInstance> fires = agentState.getObjectsOfClass(CLASS_FIRE);
		ObjectInstance fire;
//		System.out.println(fires.size());
		
		for (int i = 0; i < fires.size(); i++) {
			fire = fires.get(i);
			setFire(state, i, 
					fire.getIntValForAttribute(ATT_X),
					fire.getIntValForAttribute(ATT_Y),
					fire.getIntValForAttribute(ATT_INTENSITY));
		}
		
		// find the otherAgent's internal state
		List<ObjectInstance> agents = agentState.getObjectsOfClass(CLASS_AGENT);
		for (ObjectInstance agent : agents) {
			if (agent.getIntValForAttribute(ATT_AGENTNUM) == otherAgent) {
				setSelfAgent(state, 
						agent.getIntValForAttribute(ATT_X),
						agent.getIntValForAttribute(ATT_Y),
						agent.getIntValForAttribute(ATT_POWER),
						agent.getIntValForAttribute(ATT_AVAILABLE),
						agent.getIntValForAttribute(ATT_AGENTNUM));
				break;
			}
		}
		
		// save the original agent's internal state
		ObjectInstance originalInternalState = agentState.getObjectsOfClass(CLASS_SELF_AGENT).get(0);
		int index = 0;
		setOtherAgent(state,
				index++,
				originalInternalState.getIntValForAttribute(ATT_X),
				originalInternalState.getIntValForAttribute(ATT_Y),
				originalInternalState.getIntValForAttribute(ATT_POWER),
				originalInternalState.getIntValForAttribute(ATT_AVAILABLE),
				originalInternalState.getIntValForAttribute(ATT_AGENTNUM));
				
		
		// save the other agents' states
		for (ObjectInstance agent : agents) {
			if (agent.getIntValForAttribute(ATT_AGENTNUM) != otherAgent) {
				setOtherAgent(state, 
						index++,
						agent.getIntValForAttribute(ATT_X),
						agent.getIntValForAttribute(ATT_Y),
						agent.getIntValForAttribute(ATT_POWER),
						agent.getIntValForAttribute(ATT_AVAILABLE),
						agent.getIntValForAttribute(ATT_AGENTNUM));
			}
		}
		
		return state;
	}
	
	/**
	 * Converts an agent number to an agent name.
	 * 
	 * @param agent The agent number
	 * 
	 * @return The agent name
	 */
	public static String agentName(int agent) {
		String aname = "agent" + agent;
//		System.out.println("Ag Name: "+aname+" Len: "+aname.length());
		return aname;
	}
	
	/**
	 * Converts an agent name to an agent number.
	 * 
	 * @param agent The agent name
	 * 
	 * @return The agent number
	 */
	public static int agentNum(String agentName) {
//		System.out.println("agentName: "+agentName+" Len: "+agentName.length());
		int aID = Integer.parseInt(agentName.substring(5,6));// agentName.length()));
//		System.out.println(agentName+"--"+aID);
		return aID;
	}
	
	/**
	 * Creates and returns an {@link burlap.oomdp.stochasticgames.SGAgentType} for a particular
	 * agent in the environment
	 *
	 * @param domain the domain object of the grid game.
	 * @param agent The index of the agent in the environment
	 * 
	 * @return An {@link burlap.oomdp.stochasticgames.SGAgentType} for {@code agent}
	 */
	public static SGAgentType getAgentType(WildfireDomain domain, int agent){
//		System.out.println(agentName(agent));
		SGAgentType at = new SGAgentType(Wildfire.CLASS_AGENT,// + agent, 
				domain.getObjectClass(CLASS_AGENT), 
				domain.getAgentActions(agentName(agent)));
//		System.out.println("type name "+at.typeName+" actions size "+domain.getAgentActions(agentName(agent)).toString());
		return at;
	}
	
	public class WildfireObservations extends ObservationFunction{
		
		public WildfireObservations(WildfireDomain domain) {
			super(domain);
		}

		@Override
		public boolean canEnumerateObservations() {
			return true;
		}

		@Override
		public List<State> getAllPossibleObservations() {
			List<State> ret = new ArrayList<State>();
			
			for (int i = NO_OBS; i >= 2 - NUM_FIRE_STATES; i--) {
//				System.out.println("ObsIndex: "+i);
				ret.add(Wildfire.createObservation((WildfireDomain) domain, i));
			}
			
			return ret;
		}

		@Override 
		public double getObservationProbability(State observation, State state, State nextState, JointAction ja) {
			// get the agent from the state
			List<ObjectInstance> selfAgentStates = state.getObjectsOfClass(CLASS_SELF_AGENT);
			int agentNum = selfAgentStates.get(0).getIntValForAttribute(ATT_AGENTNUM);
//			System.out.println("HERE--");
			String agentName = agentName(agentNum);
//			System.out.println("HERE-- "+agentName+" Num: "+agentNum);
			
			// get this agent's action in the joint set
			GroundedSGAgentAction action = ja.action(agentName);
			
			// get the intensity difference from the observation
			List<ObjectInstance> observationValues = observation.getObjectsOfClass(CLASS_OBS);
			int obsDiff = observationValues.get(0).getIntValForAttribute(ATT_OBS);
			
			// NOOBS are only made by NOOPS
	        if (NO_OBS == obsDiff) {
	            return NOOP.equals(action.actionName()) ? 1.0 : 0.0;
	        }
	        
	        // NOOPs only produce NOOBS
	        if (NOOP.equals(action.actionName())) {
	            return NO_OBS == obsDiff ? 1.0 : 0.0;
	        } 
	        
	        // figure out which position the agent acted upon
	        List<ObjectInstance> fires = state.getObjectsOfClass(CLASS_FIRE);
	        int x, y, fire = -1;
	        for (int i = 0; i < fires.size(); i++) {
	            x = fires.get(i).getIntValForAttribute(ATT_X);
	            y = fires.get(i).getIntValForAttribute(ATT_Y);
	            if (action.actionName().equals("x" + x + "y" + y)) {
	                fire = i;
	                break;
	            }
	        }
	        
	        List<ObjectInstance> nextFires = nextState.getObjectsOfClass(CLASS_FIRE);
	        int fireIntensity = fires.get(fire).getIntValForAttribute(ATT_INTENSITY);
	        int nextFireIntensity = nextFires.get(fire).getIntValForAttribute(ATT_INTENSITY);
	        int diff = nextFireIntensity - fireIntensity;
	        return (diff == obsDiff) 
	                ? 1.0 - OBSERVATION_NOISE
	                // cannot observe NFS - 1, and did not observe the correct obs
	                : OBSERVATION_NOISE / (NUM_FIRE_STATES - 2); 
		}

		@Override
		public List<ObservationProbability> getObservationProbabilities(
				State state, State nextState, JointAction ja) {
			return getObservationProbabilitiesByEnumeration(state, nextState, ja);
		}

		@Override
		public State sampleObservation(State state, State nextState, JointAction ja) {
			return sampleObservationByEnumeration(state, nextState, ja);
		}
		
	}

	public static class WildfireJointRewardFunction implements JointReward {
		
		@Override
		public Map<String, Double> reward(State s, JointAction ja, State sp) {
			Map <String, Double> rewards = new HashMap<String, Double>();
			
			// count the number of locations not on fire
			int count = 0;
			List<ObjectInstance> fireStates = s.getObjectsOfClass(Wildfire.CLASS_FIRE);
			for (ObjectInstance fire : fireStates) {
				if (fire.getIntValForAttribute(ATT_INTENSITY) == 0) count++;
			}
			
			GroundedSGAgentAction action;
			int agentNumber, available = 0;
			List<ObjectInstance> agentStates;
			for (String agent : ja.getAgentNames()) {
				agentNumber = agentNum(agent);
				double reward = count;
				
				// was this a valid action
				action = ja.action(agent);
				if (!NOOP.equals(action.actionName())) {
					// get the agent's availability
					agentStates = s.getObjectsOfClass(Wildfire.CLASS_SELF_AGENT);
					if (agentStates.get(0).getIntValForAttribute(ATT_AGENTNUM) == agentNumber) {
						available = agentStates.get(0).getIntValForAttribute(ATT_AVAILABLE);
					} else {
						agentStates = s.getObjectsOfClass(Wildfire.CLASS_AGENT);
						for (ObjectInstance agentState : agentStates) {
							if (agentState.getIntValForAttribute(ATT_AGENTNUM) == agentNumber) {
								available = agentState.getIntValForAttribute(ATT_AVAILABLE);
								break;
							}
						}
					}
					
					// did the agent try to act when it is unavailable?
                    if (available == 0) {
                            reward += NO_FIRE_PENALTY;
                    } else {
                    	for (ObjectInstance fire : fireStates) {
                    		if ((0 == fire.getIntValForAttribute(ATT_INTENSITY)
                    				|| NUM_FIRE_STATES - 1 == fire.getIntValForAttribute(ATT_INTENSITY))
                                    && action.actionName().equals("x" + fire.getIntValForAttribute(ATT_X)
                                    + "y" + fire.getIntValForAttribute(ATT_Y))) {
                    			reward += NO_FIRE_PENALTY;
                    		}
                    	}
                    } 
				}
				
//				System.out.println(agent + " - Rew: "+ reward);
				rewards.put(agent, reward);
			}
			
			return rewards;
		}
	}
}
