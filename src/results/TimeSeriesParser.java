package results;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import domains.wildfire.Wildfire;
import domains.wildfire.WildfireDomain;
import domains.wildfire.WildfireParameters;

public class TimeSeriesParser {
	private static final String POSTHOC = "I-PBVI PostHoc";
	private static final String PREDICTIVE = "I-PBVI Predictive";
	private static final String NOOP = "NOOP";
	private static final String RANDOM = "RANDOM";
	private static final String HEURISTIC = "HEURISTIC";
	private static final String INDIVIDUAL_FIRE = "IndividualFire";
	private static final String COLLABORATION = "Collaboration";
	private static final String SHARED_FIRE_ALONE = "SharedFireAlone";
	private static final String NOOP_RECHARGE = "NOOP_Recharge";
	private static final String NOOP_NO_FIRE = "NOOP_NoFire";
	private static final String NOOP_FIRE = "NOOP_Fire";
	private static final double GAMMA = 0.9;
	
	public static void main(String[] args) {
		//example command line argument
		/*
		 * order of command line arguments: conf experiment
		 * 
		 * [mkran@erebus oasys-git]$  java -cp "bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar" results/TimeSeriesParser 1 5 
		 * 
		 */
		
		TimeSeriesParser parser = new TimeSeriesParser();
		parser.run(args);
	}
	
	private void run(String[] args) {

		int conf = 0;
		int experiment = 0;
		boolean estimateTF = false;
		
		if(args.length == 3){
			conf = Integer.parseInt(args[0]);
			experiment = Integer.parseInt(args[1]);
			estimateTF = Boolean.parseBoolean(args[2]);
		}
		

		// grab the directory
		Wildfire wf = new Wildfire();
		WildfireDomain d = (WildfireDomain) wf.generateDomain(conf,estimateTF);

		int numFires = d.fireLocations.length;
		
		String outFileHead = "output/Simulations/Config_"+WildfireParameters.config+"_Results/Experiment_"+experiment+"/";
//		String outFileHead = "output/Muthu/Config_"+WildfireParameters.config+"_Results/Experiment_"+experiment+"/";
		String resultHead = outFileHead + "Dumps";
		
		String path = resultHead;
		File directory = new File(path);
					
		// parse the rewards
		parseRewards(directory);
		
		// parse the fires
		parseFireIntensities(directory);
		
		// parse per fires
		parsePerFireIntensities(directory, numFires);
		
		// parse the actions
		parseActions(directory, d);
		
		parsePaperResults(directory);
	}
	
	private void parseRewards(File directory) {
		// get the results from the different solutions
		List<Double> posthoc = parseRewards(directory, 
				new IPBVIPostHocRewardsFilter(), 
				POSTHOC);
		List<Double> noop = parseRewards(directory, 
				new NOOPRewardsFilter(), 
				NOOP);
		List<Double> random = parseRewards(directory, 
				new RANDOMRewardsFilter(), 
				RANDOM);
		List<Double> heuristic = parseRewards(directory, 
				new HEURISTICRewardsFilter(), 
				HEURISTIC);
		List<Double> predictive = parseRewards(directory, 
				new PREDICTIVERewardsFilter(), 
				PREDICTIVE);
		
		// save the rewards to file
		List<List<Double>> rewards = new ArrayList<List<Double>>();
		rewards.add(posthoc);
		rewards.add(noop);
		rewards.add(random);
		rewards.add(heuristic);
		rewards.add(predictive);
		
		List<String> solutions = Arrays.asList(new String[]{POSTHOC, NOOP, RANDOM, HEURISTIC, PREDICTIVE});
		logFile(rewards, solutions, directory.getAbsolutePath() + "/RewardsPerStep.csv");
		
		// now create the cumulative, undiscounted rewards
		List<List<Double>> undiscounted = new ArrayList<List<Double>>();
		double sum;
		for (List<Double> solutionRewards : rewards) {
			List<Double> undiscountedResults = new ArrayList<Double>();
			undiscounted.add(undiscountedResults);
			
			sum = 0.0;
			for (int i = 0; i < solutionRewards.size(); i++) {
				sum += solutionRewards.get(i);
				undiscountedResults.add(sum);
			}
		}
		logFile(undiscounted, solutions, directory.getAbsolutePath() + "/UndiscountedCumulativeRewards.csv");
				
		// now create the discounted rewards
		List<List<Double>> discounted = new ArrayList<List<Double>>();
		double cumulativeGamma;
		for (List<Double> solutionRewards : rewards) {
			List<Double> discountedResults = new ArrayList<Double>();
			discounted.add(discountedResults);
			
			sum = 0.0;
			cumulativeGamma = 1;
			for (int i = 0; i < solutionRewards.size(); i++) {
				sum += cumulativeGamma * solutionRewards.get(i);
				discountedResults.add(sum);
				cumulativeGamma *= GAMMA;
			}
		}
		logFile(discounted, solutions, directory.getAbsolutePath() + "/DiscountedCumulativeRewards.csv");
		
	}
	
	private void parseFireIntensities(File directory) {
		// get the results from the different solutions
		List<List<List<Double>>> posthoc = parseFireIntensities(directory, 
				new IPBVIPostHocFireFilter(), 
				POSTHOC);
		List<List<List<Double>>> noop = parseFireIntensities(directory, 
				new NOOPFireFilter(), 
				NOOP);
		List<List<List<Double>>> random = parseFireIntensities(directory, 
				new RANDOMFireFilter(), 
				RANDOM);
		List<List<List<Double>>> heuristic = parseFireIntensities(directory, 
				new HEURISTICFireFilter(), 
				HEURISTIC);
		List<List<List<Double>>> predictive = parseFireIntensities(directory, 
				new PREDICTIVEFireFilter(), 
				PREDICTIVE);
		
		// save the fire intensities to file
		List<List<Double>> intensities = new ArrayList<List<Double>>();
		intensities.add(processFireIntensities(posthoc));
		intensities.add(processFireIntensities(noop));
		intensities.add(processFireIntensities(random));
		intensities.add(processFireIntensities(heuristic));
		intensities.add(processFireIntensities(predictive));
		
		List<String> solutions = Arrays.asList(new String[]{POSTHOC, NOOP, RANDOM, HEURISTIC,PREDICTIVE});
		logFile(intensities, solutions, directory.getAbsolutePath() + "/FireIntensitiesPerStep.csv");		
	}
	
	private void parsePerFireIntensities(File directory, int numFires) {
		// get the results from the different solutions
		String[] solns = new String[numFires+5];
		for(int i=0;i<numFires;i++){
			solns[i] = "F"+i;
		}
		solns[numFires] = NOOP;
		solns[numFires+1] = POSTHOC;
		solns[numFires+2] = RANDOM;
		solns[numFires+3] = HEURISTIC;
		solns[numFires+4] = PREDICTIVE;
		
//		System.out.println(numFires);
		List<List<Double>> perFire = new ArrayList<List<Double>>();  
		for(int i=0;i<numFires;i++){
//			System.out.println("adding "+solns[i]);
			perFire.add(parsePerFireIntensities(directory, 
					new IPBVIPostHocFireFilter(), 
					solns[i], 
					Integer.toString(i)));
		}
		
		// get the results from the different solutions
		List<Double> posthoc = processFireIntensities(
				parseFireIntensities(directory, 
						new IPBVIPostHocFireFilter(), 
						POSTHOC));
		List<Double> noop = processFireIntensities(
				parseFireIntensities(directory, 
						new NOOPFireFilter(), 
						NOOP));
		List<Double> random = processFireIntensities(
				parseFireIntensities(directory, 
					new RANDOMFireFilter(), 
					RANDOM));
		List<Double> heuristic = processFireIntensities(
				parseFireIntensities(directory, 
					new HEURISTICFireFilter(), 
					HEURISTIC));
		List<Double> predictive = processFireIntensities(
				parseFireIntensities(directory, 
					new PREDICTIVEFireFilter(), 
					PREDICTIVE));
				
		// save the intensities to file
		List<List<Double>> intensities = new ArrayList<List<Double>>();
		for(int i=0;i<numFires;i++){
			intensities.add(perFire.get(i));
		}
		
		intensities.add(noop);
		intensities.add(posthoc);
		intensities.add(random);
		intensities.add(heuristic);
		intensities.add(predictive);

		List<String> solutions = Arrays.asList(solns);
		logFile(intensities, solutions, directory.getAbsolutePath() + "/AllFireIntensitiesPerStep.csv");		
	}
	
	private void parseActions(File directory, WildfireDomain domain) {
		// get the actions from the different solutions
		List<List<List<String>>> posthoc = parseActions(directory, 
				new IPBVIPostHocActionsFilter(), 
				POSTHOC);
		List<List<List<String>>> noop = parseActions(directory, 
				new NOOPActionsFilter(), 
				NOOP);
		List<List<List<String>>> random = parseActions(directory, 
				new RANDOMActionsFilter(), 
				RANDOM);
		List<List<List<String>>> heuristic = parseActions(directory, 
				new HEURISTICActionsFilter(), 
				HEURISTIC);
		List<List<List<String>>> predictive = parseActions(directory, 
				new PREDICTIVEActionsFilter(), 
				PREDICTIVE);
		
		// get the fire intensities from the different solutions
		List<List<List<Double>>> posthocIntensities = parseFireIntensities(directory,
				new IPBVIPostHocFireFilter(), 
				POSTHOC);
		List<List<List<Double>>> noopIntensities = parseFireIntensities(directory,
				new NOOPFireFilter(), 
				NOOP);
		List<List<List<Double>>> randomIntensities = parseFireIntensities(directory,
				new RANDOMFireFilter(), 
				RANDOM);
		List<List<List<Double>>> heuristicIntensities = parseFireIntensities(directory,
				new HEURISTICFireFilter(), 
				HEURISTIC);
		List<List<List<Double>>> predictiveIntensities = parseFireIntensities(directory,
				new PREDICTIVEFireFilter(), 
				PREDICTIVE);
		
		// get the suppressants from the different solutions
		List<List<List<Double>>> posthocSuppressants = parseSuppressants(directory,
				new IPBVIPostHocSuppressantsFilter(), 
				POSTHOC);
		List<List<List<Double>>> noopSuppressants = parseSuppressants(directory,
				new NOOPSuppressantsFilter(), 
				NOOP);
		List<List<List<Double>>> randomSuppressants = parseSuppressants(directory,
				new RANDOMSuppressantsFilter(), 
				RANDOM);
		List<List<List<Double>>> heuristicSuppressants = parseSuppressants(directory,
				new HEURISTICSuppressantsFilter(), 
				HEURISTIC);
		List<List<List<Double>>> predictiveSuppressants = parseSuppressants(directory,
				new PREDICTIVESuppressantsFilter(), 
				PREDICTIVE);
		
		// count the actions
		Map<String, List<Integer>> posthocCounts = countActions(posthoc, 
				posthocIntensities, posthocSuppressants, domain);
		Map<String, List<Integer>> noopCounts = countActions(noop, 
				noopIntensities, noopSuppressants, domain);
		Map<String, List<Integer>> randomCounts = countActions(random, 
				randomIntensities, randomSuppressants, domain);
		Map<String, List<Integer>> heuristicCounts = countActions(heuristic, 
				heuristicIntensities, heuristicSuppressants, domain);
		Map<String, List<Integer>> predictiveCounts = countActions(predictive, 
				predictiveIntensities, predictiveSuppressants, domain);
		
		List<Map<String, List<Integer>>> actions = new ArrayList<Map<String, List<Integer>>>();
		actions.add(posthocCounts);
		actions.add(noopCounts);
		actions.add(randomCounts);
		actions.add(heuristicCounts);
		actions.add(predictiveCounts);
		
		List<String> solutions = Arrays.asList(new String[]{POSTHOC, NOOP, RANDOM, HEURISTIC, PREDICTIVE});
		logActionCountsFile(actions, solutions, directory.getAbsolutePath() + "/Actions.csv");		
	}
	
	private void parsePaperResults(File directory) {
		// get the fire intensities from the different solutions
		List<List<List<Double>>> posthocIntensities = parseFireIntensities(directory,
				new IPBVIPostHocFireFilter(), 
				POSTHOC);
		List<List<List<Double>>> noopIntensities = parseFireIntensities(directory,
				new NOOPFireFilter(), 
				NOOP);
		List<List<List<Double>>> randomIntensities = parseFireIntensities(directory,
				new RANDOMFireFilter(), 
				RANDOM);
		List<List<List<Double>>> heuristicIntensities = parseFireIntensities(directory,
				new HEURISTICFireFilter(), 
				HEURISTIC);
		List<List<List<Double>>> predictiveIntensities = parseFireIntensities(directory,
				new PREDICTIVEFireFilter(), 
				PREDICTIVE);
		
		// calculate the average intensities
		List<Double> posthocAvgIntensities= calculateAverageIntensities(posthocIntensities);
		List<Double> noopAvgIntensities= calculateAverageIntensities(noopIntensities);
		List<Double> randomAvgIntensities= calculateAverageIntensities(randomIntensities);
		List<Double> heuristicAvgIntensities= calculateAverageIntensities(heuristicIntensities);
		List<Double> predictiveAvgIntensities= calculateAverageIntensities(predictiveIntensities);
		
		List<List<Double>> avgIntensities = new ArrayList<List<Double>>();
		avgIntensities.add(posthocAvgIntensities);
		avgIntensities.add(noopAvgIntensities);
		avgIntensities.add(randomAvgIntensities);
		avgIntensities.add(heuristicAvgIntensities);
		avgIntensities.add(predictiveAvgIntensities);
		
		// get the rewards from the different solutions
		List<List<List<Double>>> posthocRewards = parseAllRewards(directory,
				new IPBVIPostHocRewardsFilter(), 
				POSTHOC);
		List<List<List<Double>>> noopRewards = parseAllRewards(directory,
				new NOOPRewardsFilter(), 
				NOOP);
		List<List<List<Double>>> randomRewards = parseAllRewards(directory,
				new RANDOMRewardsFilter(), 
				RANDOM);
		List<List<List<Double>>> heuristicRewards = parseAllRewards(directory,
				new HEURISTICRewardsFilter(), 
				HEURISTIC);
		List<List<List<Double>>> predictiveRewards = parseAllRewards(directory,
				new PREDICTIVERewardsFilter(), 
				PREDICTIVE);
		
		// calculate the discounted rewards
		List<Double> posthocDiscounted = calculateDiscountedRewards(posthocRewards);
		List<Double> noopDiscounted = calculateDiscountedRewards(noopRewards);
		List<Double> randomDiscounted = calculateDiscountedRewards(randomRewards);
		List<Double> heuristicDiscounted = calculateDiscountedRewards(heuristicRewards);
		List<Double> predictiveDiscounted = calculateDiscountedRewards(predictiveRewards);
		
		List<List<Double>> discountedRewards = new ArrayList<List<Double>>();
		discountedRewards.add(posthocDiscounted);
		discountedRewards.add(noopDiscounted);
		discountedRewards.add(randomDiscounted);
		discountedRewards.add(heuristicDiscounted);
		discountedRewards.add(predictiveDiscounted);
		
		// log the results
		List<String> solutions = Arrays.asList(new String[]{POSTHOC, NOOP, RANDOM, HEURISTIC,PREDICTIVE});
		logPaperResults(avgIntensities, discountedRewards, solutions, directory.getAbsolutePath() + "/PaperResults.csv");
				
	}
	
	private List<Double> calculateAverageIntensities(List<List<List<Double>>> intensities) {
		List<Double> averages = new ArrayList<Double>();
		double sum;
		for (List<List<Double>> fires : intensities) {
			for (int i = 0; i < fires.size(); i++) {
				for (int j = 0; j < fires.get(i).size(); j++) {
					averages.add(fires.get(i).get(j));
				}
			}
		}
		
		return averages;
	}
	
	private List<Double> calculateDiscountedRewards(List<List<List<Double>>> rewards) {
		List<Double> discounted = new ArrayList<Double>();
		double cumulative, cumulativeGamma;
		for (List<List<Double>> agents : rewards) {
			cumulative = 0.0;
			for (int i = 0; i < agents.size(); i++) {
				cumulativeGamma = 1;
				for (int j = 0; j < agents.get(i).size(); j++) {
					cumulative += cumulativeGamma * agents.get(i).get(j);
					cumulativeGamma *= GAMMA;
				}
			}
			discounted.add(cumulative);
		}
		
		return discounted;
	}
	
	private double calculateMean(List<Double> list) {
		double sum = 0.0;
		for (Double value : list) {
			sum += value.doubleValue();
		}
		
		return sum / list.size();
	}
	
	private double calculateSE(List<Double> list, double mean) {
		// calculate the standard deviation
		double sd = calculateStandardDeviation(list, mean);
		return sd / Math.sqrt(list.size());
	}
	
	private double calculateStandardDeviation(List<Double> list, double mean) {
		// now compute the variance
        double sum = 0.0;
        double val = 0.0;
        for (int i = 0; i < list.size(); i++) {
            val = list.get(i).doubleValue() - mean;
            sum += val * val;
        }

        return Math.sqrt(sum / (list.size() - 1));
	}
	
	private List<Double> parseRewards(File directory, FilenameFilter filter, String solution) {
		File[] files = directory.listFiles(filter);
		if (files.length > 1) {
			System.err.println("There should only be one " + solution + " rewards file found");
			System.exit(-1);
		}
		
		List<Double> rewards = new ArrayList<Double>();
		
		String line = "";
		try {
			// now grab all of the rewards
			List<List<Double>> groupResults = new ArrayList<List<Double>>();
			List<List<Double>> runResults = new ArrayList<List<Double>>();
			List<Double> groupRewards = null;
			List<Double> agentRewards;
			BufferedReader reader = new BufferedReader(new FileReader(files[0]));
			String[] split;
			double sum;
			while (reader.ready()) {
				line = reader.readLine();
				if (line.startsWith("Trial")) { 
					// get ready for another trial
					for (List<Double> list : runResults) {
						list.clear();
					}
					runResults.clear();
				} else if (line.startsWith("Total")){
					// add a place to store the group results for this trial
					groupRewards = new ArrayList<Double>();
					groupResults.add(groupRewards);
					
					// aggregate the results of this trial
					for (int i = 0; i < runResults.get(0).size(); i++) {
						sum = 0;
						for (List<Double> list : runResults) {
							sum += list.get(i);
						}	
						groupRewards.add(sum);
					}
				} else {
					// parse the line
					split = line.split(",");
					
					// is this an agent's results?
					if (split.length > 1) {
						agentRewards = new ArrayList<Double>();
						runResults.add(agentRewards);
						
						// save each reward value
						for (int i = 1; i < split.length; i++) {
							agentRewards.add(Double.valueOf(split[i]));
						}
					}
				}
			}
			reader.close();
			
			// average the rewards
			for (int i = 0; i < groupResults.get(0).size(); i++) {
				sum = 0;
				for (List<Double> list : groupResults) {
					sum += list.get(i);
				}
				rewards.add(sum / groupResults.size());
			}
		} catch (Exception ex) {
			System.err.println("Problem trying to read file: " + files[0].getPath() + "\nline: " + line);
			ex.printStackTrace();
			System.exit(-1);
		}
		
		return rewards;
	}
	
	private List<List<List<Double>>> parseAllRewards(File directory, FilenameFilter filter, String solution) {
		File[] files = directory.listFiles(filter);
		if (files.length > 1) {
			System.err.println("There should only be one " + solution + " rewards file found");
			System.exit(-1);
		}
		
		List<List<List<Double>>> rewards = new ArrayList<List<List<Double>>>();
		
		String line = "";
		try {
			// now grab all of the intensities
			BufferedReader reader = new BufferedReader(new FileReader(files[0]));
			String[] split;
			List<List<Double>> listOfAgents = null;
			List<Double> list;
			while (reader.ready()) {
				line = reader.readLine();
				if (line.startsWith("Trial")) { 
					// get ready for another trial
					listOfAgents = new ArrayList<List<Double>>();
					rewards.add(listOfAgents);
				} else if (line.startsWith("Total")){
					// do nothing
				} else {
					// parse the line
					split = line.split(",");
					
					// is this an agent's actions?
					if (split.length > 1) {
						list = new ArrayList<Double>();
						
						for (int i = 1; i < split.length; i++) {
							list.add(Double.valueOf(split[i]));
						}
						
						listOfAgents.add(list);
					}
				}
			}
			reader.close();
		} catch (Exception ex) {
			System.err.println("Problem trying to read file: " + files[0].getPath() + "\nline: " + line);
			ex.printStackTrace();
			System.exit(-1);
		}
		
		return rewards;
	}
	
	private List<List<List<Double>>> parseFireIntensities(File directory, FilenameFilter filter, String solution) {
		File[] files = directory.listFiles(filter);
		if (files.length > 1) {
			System.err.println("There should only be one " + solution + " fires file found");
			System.exit(-1);
		}
		
		List<List<List<Double>>> intensities = new ArrayList<List<List<Double>>>();
		
		String line = "";
		try {
			// now grab all of the intensities
			BufferedReader reader = new BufferedReader(new FileReader(files[0]));
			String[] split;
			List<List<Double>> listOfFires = null;
			List<Double> list;
			while (reader.ready()) {
				line = reader.readLine();
				if (line.startsWith("Trial")) { 
					// get ready for another trial
					listOfFires = new ArrayList<List<Double>>();
					intensities.add(listOfFires);
				} else if (line.startsWith("Total")){
					// do nothing
				} else {
					// parse the line
					split = line.split(",");
					
					// is this an agent's actions?
					if (split.length > 1) {
						list = new ArrayList<Double>();
						
						for (int i = 1; i < split.length; i++) {
							list.add(Double.valueOf(split[i]));
						}
						
						listOfFires.add(list);
					}
				}
			}
			reader.close();
		} catch (Exception ex) {
			System.err.println("Problem trying to read file: " + files[0].getPath() + "\nline: " + line);
			ex.printStackTrace();
			System.exit(-1);
		}
		
		return intensities;
	}
	
	private List<Double> parsePerFireIntensities(File directory, FilenameFilter filter, 
			String solution, String fireIndex) {
		File[] files = directory.listFiles(filter);
		if (files.length > 1) {
			System.err.println("There should only be one " + solution + " fires file found");
			System.exit(-1);
		}
		
		List<Double> intensities = new ArrayList<Double>();
		
		String line = "";
		try {
			// now grab all of the intensities
			List<List<Double>> totalResults = new ArrayList<List<Double>>();
			List<List<Double>> runResults = new ArrayList<List<Double>>();
			List<Double> totalIntensities = null;
			List<Double> fireIntensities;
			BufferedReader reader = new BufferedReader(new FileReader(files[0]));
			String[] split;
			double sum;
			while (reader.ready()) {
				line = reader.readLine();
//				System.out.println("["+fireIndex+"] Reading line: "+line);
				if (line.startsWith("Trial")) { 
					// get ready for another trial
					for (List<Double> list : runResults) {
						list.clear();
					}
					runResults.clear();
				} else if (line.startsWith(fireIndex)){
					// parse the line
					split = line.split(",");
					
					// is this an agent's results?
					if (split.length > 1) {
						fireIntensities = new ArrayList<Double>();
						runResults.add(fireIntensities);
						
						// save each reward value
						for (int i = 1; i < split.length; i++) {
							fireIntensities.add(Double.valueOf(split[i]));
						}
					}
					
					// add a place to store the group results for this trial
					totalIntensities = new ArrayList<Double>();
					totalResults.add(totalIntensities);
					
					// aggregate the results of this trial
					for (int i = 0; i < runResults.get(0).size(); i++) {
						sum = 0;
						for (List<Double> list : runResults) {
							sum += list.get(i);
						}	
						totalIntensities.add(sum);
					}
				}
//				} else {
//					// parse the line
//					split = line.split(",");
//					
//					// is this an agent's results?
//					if (split.length > 1) {
//						fireIntensities = new ArrayList<Double>();
//						runResults.add(fireIntensities);
//						
//						// save each reward value
//						for (int i = 1; i < split.length; i++) {
//							fireIntensities.add(Double.valueOf(split[i]));
//						}
//					}
//				}
			}
			reader.close();
			
			// average the rewards
			for (int i = 0; i < totalResults.get(0).size(); i++) {
				sum = 0;
				for (List<Double> list : totalResults) {
					sum += list.get(i);
				}
				intensities.add(sum / (totalResults.size() * runResults.size()));
			}
		} catch (Exception ex) {
			System.err.println("Problem trying to read file: " + files[0].getPath() + "\nline: " + line);
			ex.printStackTrace();
			System.exit(-1);
		}
		
		return intensities;
	}
	
	private List<List<List<Double>>> parseSuppressants(File directory, FilenameFilter filter, String solution) {
		File[] files = directory.listFiles(filter);
		if (files.length > 1) {
			System.err.println("There should only be one " + solution + " suppressants file found");
			System.exit(-1);
		}
		
		List<List<List<Double>>> suppressants = new ArrayList<List<List<Double>>>();
		
		String line = "";
		try {
			// now grab all of the suppressants
			BufferedReader reader = new BufferedReader(new FileReader(files[0]));
			String[] split;
			List<List<Double>> listOfAgents = null;
			List<Double> list;
			while (reader.ready()) {
				line = reader.readLine();
				if (line.startsWith("Trial")) { 
					// get ready for another trial
					listOfAgents = new ArrayList<List<Double>>();
					suppressants.add(listOfAgents);
				} else if (line.startsWith("Total")){
					// do nothing
				} else {
					// parse the line
					split = line.split(",");
					
					// is this an agent's actions?
					if (split.length > 1) {
						list = new ArrayList<Double>();
						
						for (int i = 1; i < split.length; i++) {
							list.add(Double.valueOf(split[i]));
						}
						
						listOfAgents.add(list);
					}
				}
			}
			reader.close();
		} catch (Exception ex) {
			System.err.println("Problem trying to read file: " + files[0].getPath() + "\nline: " + line);
			ex.printStackTrace();
			System.exit(-1);
		}
		
		return suppressants;
	}
	
	private List<List<List<String>>> parseActions(File directory, FilenameFilter filter, String solution) {
		File[] files = directory.listFiles(filter);
		if (files.length > 1) {
			System.err.println("There should only be one " + solution + " actions file found");
			System.exit(-1);
		}
		
		List<List<List<String>>> actions = new ArrayList<List<List<String>>>();
		
		String line = "";
		try {
			// now grab all of the actions
			BufferedReader reader = new BufferedReader(new FileReader(files[0]));
			String[] split;
			List<List<String>> listOfAgents = null;
			List<String> list;
			while (reader.ready()) {
				line = reader.readLine();
				if (line.startsWith("Trial")) { 
					// get ready for another trial
					listOfAgents = new ArrayList<List<String>>();
					actions.add(listOfAgents);
				} else if (line.startsWith("Total")){
					// do nothing
				} else {
					// parse the line
					split = line.split(",");
					
					// is this an agent's actions?
					if (split.length > 1) {
						list = new ArrayList<String>();
						
						for (int i = 1; i < split.length; i++) {
							list.add(split[i]);
						}
						
						listOfAgents.add(list);
					}
				}
			}
			reader.close();
		} catch (Exception ex) {
			System.err.println("Problem trying to read file: " + files[0].getPath() + "\nline: " + line);
			ex.printStackTrace();
			System.exit(-1);
		}
		
		return actions;
	}
	
	private List<Double> processFireIntensities(List<List<List<Double>>> allIntensities) {
		// compute the total intensities per step
		List<List<Double>> totalIntensities = new ArrayList<List<Double>>();
		List<Double> runList;
		double sum;
		for (List<List<Double>> run : allIntensities) {
			runList = new ArrayList<Double>();
			totalIntensities.add(runList);
			
			for (int i = 0; i < run.get(0).size(); i++) {
				sum = 0.0;
				for (List<Double> fire : run) {
					sum += fire.get(i);
				}
				runList.add(sum);
			}
		}
			
		// average the rewards
		List<Double> intensities = new ArrayList<Double>();
		for (int i = 0; i < totalIntensities.get(0).size(); i++) {
			sum = 0;
			for (List<Double> list : totalIntensities) {
				sum += list.get(i);
			}
			intensities.add(sum / (totalIntensities.size() * allIntensities.get(0).size()));
		}
		
		return intensities;
	}
	
	private Map<String, List<Integer>> countActions(
			List<List<List<String>>> actions,
			List<List<List<Double>>> intensities,
			List<List<List<Double>>> suppressants,
			WildfireDomain domain) {
		// build the list of possible actions
		List<String> actionNames = new ArrayList<String>();
		for (List<List<String>> run : actions) {
			for (List<String> agent : run) {
				for (String action : agent) {
					if (!actionNames.contains(action)) {
						actionNames.add(action);
					}
				}
			}
		}
		
		// count the actions per agent/run
		Map<String, List<Integer>> ret = new HashMap<String, List<Integer>>();
		Map<String, Integer> counts = new HashMap<String, Integer>();
		List<Integer> list;
		Integer count;
		for (List<List<String>> run : actions) {
			counts.clear();
			for (String actionName : actionNames) {
				counts.put(actionName, Integer.valueOf(0));
			}
			
			for (List<String> agent : run) {
				for (String action : agent) {
					count = counts.get(action);
					counts.put(action, Integer.valueOf(count.intValue() + 1));
				}
			}
			
			for (String key : counts.keySet()) {
				list = ret.get(key);
				if (list == null) {
					list = new ArrayList<Integer>();
					ret.put(key, list);
				}
				list.add(counts.get(key));
			}
		}
		
		// add in the counts per type
		
		// map each fire action to a fire number
		Map<String, Integer> fireMap = new HashMap<String, Integer>();
		for (int i = 0; i < domain.fireLocations.length; i++) {
			fireMap.put(
					"x" + domain.fireLocations[i][0] 
							+ "y" + domain.fireLocations[i][1], 
					i);
		}
		
		// count the number of agents per fire
		int num;
		int[] numAgentsPerFire = new int[domain.fireLocations.length];
		for (int i = 0; i < domain.fireLocations.length; i++) {
			num = 0;
			for (int j = 0; j < domain.neighborhoods.length; j++) {
				for (int k = 0; k < domain.neighborhoods[j].length; k++) {
					if (domain.fireLocations[i][0] == domain.neighborhoods[j][k][0]
							&& domain.fireLocations[i][1] == domain.neighborhoods[j][k][1]) {
						num++;
						break;
					}
				}
			}
			numAgentsPerFire[i] = num;
		}
		
		// split fires between individual and shared
		List<String> individualFires = new ArrayList<String>();
		List<String> sharedFires = new ArrayList<String>();
		for (int i = 0; i < numAgentsPerFire.length; i++) {
			if (numAgentsPerFire[i] == 1) {
				individualFires.add("x" + domain.fireLocations[i][0] 
							+ "y" + domain.fireLocations[i][1]);
			} else if (numAgentsPerFire[i] > 1) {
				sharedFires.add("x" + domain.fireLocations[i][0] 
							+ "y" + domain.fireLocations[i][1]);
			}
		}
		
		int individualFire, collaboration, sharedAlone, noopRecharge, noopNoFire, noopFire;
		String action;
		double fireIntensity, suppressant;
		int fire;
		boolean foundFire;
		for (int run = 0; run < actions.size(); run++) {
			individualFire = 0;
			collaboration = 0;
			sharedAlone = 0;
			noopRecharge = 0;
			noopNoFire = 0;
			noopFire = 0;
			
			for (int step = 0; step < actions.get(run).get(0).size(); step++) {
				for (int agent = 0; agent < actions.get(run).size(); agent++) {
					action = actions.get(run).get(agent).get(step);
					if (Wildfire.NOOP.equals(action)) {
						// was this agent charging?
						suppressant = suppressants.get(run).get(agent).get(step);
						if (suppressant == 0.0) {
							noopRecharge++;
						} else {
							// was there a fire?
							foundFire = false;
							for (int location = 0; location < domain.neighborhoods[agent].length; location++) {
								fire = fireMap.get("x" + domain.neighborhoods[agent][location][0]
										+ "y" + domain.neighborhoods[agent][location][1]);
								fireIntensity = intensities.get(run).get(fire).get(step);
								
								if (fireIntensity > 0.0 && fireIntensity < Wildfire.NUM_FIRE_STATES - 1) {
									foundFire = true;
									break;
								} 
							}
							
							if (foundFire) {
								noopFire++;
							} else {
								noopNoFire++;
							}
						}
					} else {
						// was this an individual fire
						if (individualFires.contains(action)) {
							individualFire++;
						} else {
							// how many agents fought this fire?
							num = 0;
							for (int otherAgent = 0; otherAgent < actions.get(run).size(); otherAgent++) {
								if (otherAgent == agent) continue;
								
								if (action.equals(actions.get(run).get(otherAgent).get(step))) {
									num++;
								}
							}
							
							if (num > 0) {
								collaboration++;
							} else {
								sharedAlone++;
							}
						}
					}
				}
			}
			
			list = ret.get(INDIVIDUAL_FIRE);
			if (list == null) {
				list = new ArrayList<Integer>();
				ret.put(INDIVIDUAL_FIRE, list);
			}
			list.add(individualFire);
			
			list = ret.get(COLLABORATION);
			if (list == null) {
				list = new ArrayList<Integer>();
				ret.put(COLLABORATION, list);
			}
			list.add(collaboration);
			
			list = ret.get(SHARED_FIRE_ALONE);
			if (list == null) {
				list = new ArrayList<Integer>();
				ret.put(SHARED_FIRE_ALONE, list);
			}
			list.add(sharedAlone);
			
			list = ret.get(NOOP_RECHARGE);
			if (list == null) {
				list = new ArrayList<Integer>();
				ret.put(NOOP_RECHARGE, list);
			}
			list.add(noopRecharge);
			
			list = ret.get(NOOP_NO_FIRE);
			if (list == null) {
				list = new ArrayList<Integer>();
				ret.put(NOOP_NO_FIRE, list);
			}
			list.add(noopNoFire);
			
			list = ret.get(NOOP_FIRE);
			if (list == null) {
				list = new ArrayList<Integer>();
				ret.put(NOOP_FIRE, list);
			}
			list.add(noopFire);
		}
		
		return ret;
	}
	
	private void logFile(List<List<Double>> results, List<String> solutions, String fileName) {
		try {
			// create the header
			StringBuffer sb = new StringBuffer();
			sb.append("Step");
			for (int i = 0; i < results.get(0).size(); i++) {
				sb.append(",").append(i + 1);
			}
			sb.append("\n");
			
			// create the results strings
			List<Double> list;
			for (int i = 0; i < results.size(); i++) {
				list = results.get(i);
				
				sb.append(solutions.get(i));
				for (int j = 0; j < list.size(); j++) {
					sb.append(",").append(list.get(j));
				}
				sb.append("\n");
			}
			
			// log the results
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
			writer.write(sb.toString());
			writer.close();
		} catch (Exception ex) {
			System.err.println("Problem writing results file: " + fileName);
			ex.printStackTrace();
			System.exit(-1);
		}
	}
	
	private void logActionCountsFile(List<Map<String, List<Integer>>> actionCounts, List<String> solutions, String fileName) {
		try {
			// create the header
			StringBuffer sb = new StringBuffer();
			sb.append("Solution,Action,AverageCount\n");
			
			List<String> actionNames = new ArrayList<String>();
			for (Map<String, List<Integer>> map : actionCounts) {
				for (String key : map.keySet()) {
					if (!actionNames.contains(key)) {
						actionNames.add(key);
					}
				}
			}
			
			// create the results strings
			Map<String, List<Integer>> map;
			List<Integer> counts;
			double mean;
			for (int i = 0; i < actionCounts.size(); i++) {
				map = actionCounts.get(i);
				for (String actionName : actionNames) {
					sb.append(solutions.get(i));
					sb.append(",").append(actionName);
					
					counts = map.get(actionName);
					if (counts == null) {
						sb.append(",").append(0);
					} else {
						mean = 0.0;
						for (Integer count : counts) {
							mean += count.doubleValue();
						}
						mean /= counts.size();
						
						sb.append(",").append(mean);
					}
					sb.append("\n");
				}
			}
			
			// log the results
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
			writer.write(sb.toString());
			writer.close();
		} catch (Exception ex) {
			System.err.println("Problem writing results file: " + fileName);
			ex.printStackTrace();
			System.exit(-1);
		}
	}
	
	private void logPaperResults(List<List<Double>> avgIntensities, 
			List<List<Double>> discountedRewards, 
			List<String> solutions, String fileName) {
		try {
			// create the header
			StringBuffer sb = new StringBuffer();
			sb.append("Measure,Solution,Mean,SE\n");
			
			// first log the fire intensities
			double mean;
			for (int i = 0; i < solutions.size(); i++) {
				sb.append(solutions.get(i));
				sb.append(",").append("FireIntensity");
				
				mean = calculateMean(avgIntensities.get(i));
				sb.append(",").append(mean);
				sb.append(",").append(calculateSE(avgIntensities.get(i), mean));
				sb.append("\n");
			}
			
			// second, log the discounted rewards
			for (int i = 0; i < solutions.size(); i++) {
				sb.append(solutions.get(i));
				sb.append(",").append("DiscRewards");
				
				mean = calculateMean(discountedRewards.get(i));
				sb.append(",").append(mean);
				sb.append(",").append(calculateSE(discountedRewards.get(i), mean));
				sb.append("\n");
			}
			
			// log the results
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
			writer.write(sb.toString());
			writer.close();
		} catch (Exception ex) {
			System.err.println("Problem writing results file: " + fileName);
			ex.printStackTrace();
			System.exit(-1);
		}
	}
	
	private class IPBVIPostHocFireFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "Fire_Dump.*";
            return name.matches(match);
        }
    }
    
    private class NOOPFireFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "NOOP_BASELINE_Fire_Dump.*";
            return name.matches(match);
        }
    }
    
    private class RANDOMFireFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "RANDOM_BASELINE_Fire_Dump.*";
            return name.matches(match);
        }
    }
    
    private class HEURISTICFireFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "HEURISTIC_BASELINE_Fire_Dump.*";
            return name.matches(match);
        }
    }
    
    private class PREDICTIVEFireFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "PREDICTIVE_Fire_Dump.*";
            return name.matches(match);
        }
    }
    
    private class IPBVIPostHocRewardsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "Reward_Dump.*";
            return name.matches(match);
        }
    }
    
    private class NOOPRewardsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "NOOP_BASELINE_Reward_Dump.*";
            return name.matches(match);
        }
    }
    
    private class RANDOMRewardsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "RANDOM_BASELINE_Reward_Dump.*";
            return name.matches(match);
        }
    }
    
    private class HEURISTICRewardsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "HEURISTIC_BASELINE_Reward_Dump.*";
            return name.matches(match);
        }
    }
    
    private class PREDICTIVERewardsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "PREDICTIVE_Reward_Dump.*";
            return name.matches(match);
        }
    }
    
    private class IPBVIPostHocSuppressantsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "Suppressant_Dump.*";
            return name.matches(match);
        }
    }
    
    private class NOOPSuppressantsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "NOOP_BASELINE_Suppressant_Dump.*";
            return name.matches(match);
        }
    }
    
    private class RANDOMSuppressantsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "RANDOM_BASELINE_Suppressant_Dump.*";
            return name.matches(match);
        }
    }
    
    private class HEURISTICSuppressantsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "HEURISTIC_BASELINE_Suppressant_Dump.*";
            return name.matches(match);
        }
    }
    
    private class PREDICTIVESuppressantsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "PREDICTIVE_Suppressant_Dump.*";
            return name.matches(match);
        }
    }
    
    private class IPBVIPostHocActionsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "JointAction_Dump.*";
            return name.matches(match);
        }
    }
    
    private class NOOPActionsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "NOOP_BASELINE_JointAction_Dump.*";
            return name.matches(match);
        }
    }
    
    private class RANDOMActionsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "RANDOM_BASELINE_JointAction_Dump.*";
            return name.matches(match);
        }
    }
    
    private class HEURISTICActionsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "HEURISTIC_BASELINE_JointAction_Dump.*";
            return name.matches(match);
        }
    }
    
    private class PREDICTIVEActionsFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            String match = "PREDICTIVE_JointAction_Dump.*";
            return name.matches(match);
        }
    }
}