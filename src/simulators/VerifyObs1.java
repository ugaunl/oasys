package simulators;

import iPOMDPLiteSolver.iPBVI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nestedMDPSolver.NestedVI;
import posg.ObservationFunction;
import posg.POSGDomain;
import posg.TabularBeliefState;
import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.stochasticgames.agents.SetStrategySGAgent.SetStrategyAgentFactory;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.TransitionProbability;
import burlap.oomdp.core.states.State;
import burlap.oomdp.statehashing.HashableStateFactory;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;
import common.POWorld;
import common.StateEnumerator;
import common.pomdp.BeliefMAMDPPolicyAgent;
import common.pomdp.EnumerableBeliefState.StateBelief;
import domains.wildfire.Wildfire;
import domains.wildfire.WildfireDomain;
import domains.wildfire.WildfireParameters;
import domains.wildfire.Wildfire.WildfireJointRewardFunction;

public class VerifyObs1 {

	public static void main(String[] args) {
		boolean cogSimulator = true;
		int maxLevel = 1;
		int experiment = 2;
		
		String outFileHead = "output/Simulations/Config_"+WildfireParameters.config+"_Results/Experiment_"+experiment+"/";
		
		Map<Integer,Policy> neighborPolicies = new HashMap<Integer,Policy>();
		Map<String,ArrayList<SGAgentAction>> agentActions = new HashMap<String,ArrayList<SGAgentAction>>();
		
		Wildfire wf = new Wildfire();
		WildfireDomain d = (WildfireDomain) wf.generateDomain();
		
		//3. Get initial state
		State ms = Wildfire.getInitialMasterState((WildfireDomain) d);
		
		
		JointReward jr = new WildfireJointRewardFunction();
		TerminalFunction tf = new NullTermination();
		ObservationFunction of = ((WildfireDomain) d).getObservationFunction();
		int numPlayers = WildfireParameters.numAgents;
		
		Map<Integer, String> agIDtoName = new HashMap<Integer, String>();
		List<Map<String, SGAgentType>> allAgentDefinitions = new ArrayList<Map<String, SGAgentType>>();
		StateEnumerator senums[] = new StateEnumerator[numPlayers];
		
		
		//1. Get agent definitions
		allAgentDefinitions = Wildfire.getAllAgentDefs((WildfireDomain) d);
		
		//2. Get agent id-name mapping
		for(int n=0;n<numPlayers;n++){
			agIDtoName.put(n, Wildfire.agentName(n));
		}
		
		Map<String, State> agentStateMap = new HashMap<String,State>();;
		Map<String, TabularBeliefState> srcBSVector = new HashMap<String, TabularBeliefState>();
		
		POWorld pow = new POWorld(d,jr,tf,of,allAgentDefinitions,ms);
		
		for(int i=0;i<numPlayers;i++){
			int agentID = i;
			String agentName = agIDtoName.get(agentID);
			
			Map<String, SGAgentType> agentDefinitions = allAgentDefinitions.get(agentID);
			
			State si = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, agentID, ms);
			agentStateMap.put(agentName, si);
			
			List<JointAction> allJAs = JointAction.getAllJointActions(si, agentDefinitions);
			
			HashableStateFactory hf = new SimpleHashableStateFactory(true,false);
			((WildfireDomain) d).setStateEnumerator(new StateEnumerator(d, hf, agentDefinitions));
			
			senums[i] = new StateEnumerator(d, hf, agentDefinitions);
            senums[i].findReachableStatesAndEnumerate(si);
			
			TabularBeliefState tbs = new TabularBeliefState(d, senums[i], agentName);
			tbs = Wildfire.getInitialBeliefState((WildfireDomain) d, agentID, senums[i]);
			srcBSVector.put(agentName, tbs);
			
			int agentType = ((WildfireDomain) d).agentTypes[agentID]; 

			String filename1 = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_iPBVI_alpha_actions_level"+maxLevel+"_agent"+agentID+"-"+agentType+".txt";
					
			String filename2 = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_iPBVI_alpha_vectors_level"+maxLevel+"_agent"+agentID+"-"+agentType+".txt";
			
			List<SGAgentAction> alphaVectorActions = iPBVI.readActions((POSGDomain) d,filename1);
			
			List<double[]> alphaVectors = iPBVI.readAlphas(filename2);
			
			iPBVI pVI = new iPBVI(d, jr, tf, of, agentDefinitions,agIDtoName,allJAs, 
							alphaVectors, alphaVectorActions, agentID, agentName, senums[i]);
			Policy pPBVI = new GreedyQPolicy(pVI);
			
			BeliefMAMDPPolicyAgent pVIagent = new BeliefMAMDPPolicyAgent((POSGDomain) d, agentName,pPBVI);
			pVIagent.joinWorld(pow, pow.getAllAgentDefinitions().get(agentID).get(agentName));
		}
			
		
		//For Subject Agent's cognitive simulator:
//		if(cogSimulator){
//			System.out.println("cognitive simulation to verify Observation 1 is starting..");
//			//Works for 2-agents
//			int[] obsIndex = {1,1,1,1,1,2,2,2,2,2};
//			int numStep = 10;
//			int subjectAgentID = 0;
//			String subjectAgName = Wildfire.agentName(subjectAgentID);
//			int otherAgentIndex = 1;
//			TabularBeliefState curB = Wildfire.getInitialBeliefState((WildfireDomain) d, subjectAgentID, senum);
//			State curMS = Wildfire.getInitialMasterState((WildfireDomain) d);
//			System.out.println("created initial states from domain..");
//			
////			BeliefMAMDPPolicyAgent PBVIagent = new BeliefMAMDPPolicyAgent((POSGDomain) d, subjectAgName, otherAgentsFinalPolicyMap.get(subjectAgentID),pPBVI);
//			System.out.println("initialized pbvi agent..");
//			
//			Policy oagPolicy = otherAgentsPolicyMap.get(subjectAgentID).get(otherAgentIndex);
//			SetStrategyAgentFactory agentFactory = new SetStrategyAgentFactory(d, oagPolicy);
//			SGAgent MDPagent = agentFactory.generateAgent();
//			System.out.println("initialized mdp agent..");
//			
//			POWorld pow = new POWorld((POSGDomain) d, jr, tf, of, agentDefinitions, curMS);
////			PBVIagent.joinWorld(pow, pow.getAgentDefinitions().get(agIDtoName.get(subjectAgentID)));
//			MDPagent.joinWorld(pow, pow.getAgentDefinitions().get(agIDtoName.get(otherAgentIndex)));
//			System.out.println("added "+pow.getRegisteredAgents().size()+" agents to the POWorld to simulate..");
//			
//			System.out.println("verifying observation 1..starting..");
////			verifyObs1(pow, (WildfireDomain) d, curB, curMS, oagPolicy, pPBVI, 
////					obsIndex, numStep, subjectAgName, subjectAgentID, true);
//			
//			System.out.println("cognitive simulation to verify Observation 1 is done!!");
//		}
	}
	
	public static void verifyObs1(POWorld pow, WildfireDomain d, ObservationFunction of, TabularBeliefState curB, 
			State masterState, Policy subjectAgentPolicy,
			int[] obsIndex,int numStep,String subjectAgentName,int subjectAgentIndex, 
			Map<Integer, String> agIDtoName, Map<Integer,Policy> otherAgentsPolicyMap, 
			Map<String,ArrayList<SGAgentAction>> agentActions, boolean debug){
		
		List<State> obs = of.getAllPossibleObservations();

		List<SGAgentAction> sgActions = ((WildfireDomain) d).getActionsPerAgent().get(subjectAgentName);
		List<GroundedSGAgentAction> groundedActions = SGtoGroundedSG(sgActions, subjectAgentName);
		
		System.out.println("finished initializing stuff..\nstarting simulation ["+numStep+" iterations]..");
		
		double[] tao = new double[numStep];
		for(int i=0; i< numStep; i++){
			System.out.println("Iteration "+i+"..");
			
			JointAction ja = new JointAction();
			for(int agID=0;agID<pow.getRegisteredAgents().size();agID++){
				if(agID==subjectAgentIndex){
					ja.addAction(pow.getRegisteredAgents().get(agID).getAction(curB));
					System.out.println("["+i+"] Got agent "+agID+"'s action from its policy..");
				}else{
					State agState = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, agID, masterState);
					ja.addAction(pow.getRegisteredAgents().get(agID).getAction(agState));
					System.out.println("["+i+"] Got agent "+agID+"'s action from its policy..");//for state ["+se.getEnumeratedID(agState)+"]");
				}
			}
			
			GroundedSGAgentAction a = ja.action(subjectAgentName);
			 
			State nextMasterState = d.getJointActionModel().performJointAction(masterState, ja);
			System.out.println("["+i+"] performing joint action.."+ja.actionName()+"..");
			
			State ob = obs.get(obsIndex[i]);
			
			if(debug){
				System.out.println("["+i+"] JointAction : "+ja.actionName());
			}
			
			State curState = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, subjectAgentIndex, masterState);
			State ns = d.getJointActionModel().performJointAction(curState, ja);
			
			System.out.println("["+i+"] created mental next state for subject agent by performing ja from its curState..");
			System.out.print("["+i+"] ");
			
			tao[i] = get_T_A_O(pow,d,ns,ob,groundedActions.indexOf(a),curB,agIDtoName,otherAgentsPolicyMap,agentActions,subjectAgentName);
			
			System.out.println("["+i+"] T_A_O ["+i+"] = "+ tao[i]);
			
			TabularBeliefState nextB = new TabularBeliefState((WildfireDomain) d,subjectAgentName);
			nextB = (TabularBeliefState) curB.getUpdatedBeliefState(ob,ja);
			System.out.println("["+i+"] updated belief of subject agent..");
			
			curB = nextB;
			masterState = nextMasterState;
			System.out.println("["+i+"] restart with new belief and masterState..");
		}
		
		System.out.println("finished all iterations...\n Listing TAOs..");
		
		for(int n=0;n<numStep;n++){
			System.out.println("["+n+"] "+tao[n]);
		}
		
	}
	
	public static double get_T_A_O(POWorld pow, WildfireDomain domain, State ns, 
			State observation, int gaIndex, TabularBeliefState curB, 
			Map<Integer, String> agIDtoName, Map<Integer,Policy> otherAgentsPolicyMap, 
			Map<String,ArrayList<SGAgentAction>> agentActions, String subjectAgentName) {
		
		ObservationFunction of = pow.getOF();
//		StateEnumerator se = pow.getStateEnumerator();
		List<StateBelief> stateBeliefs = curB.getStatesAndBeliefsWithNonZeroProbability();
		System.out.println("|CurBeliefStates| (non-zero): "+curB.getStatesAndBeliefsWithNonZeroProbability().size());
		
		if(stateBeliefs!=null && agIDtoName!=null && otherAgentsPolicyMap!=null){
			int numAgents = pow.getRegisteredAgents().size();
			List<JointAction> allJAs = JointAction.getAllJointActions(ns, pow.getAgentDefinitions());
			SGAgentAction ga = agentActions.get(subjectAgentName).get(gaIndex);
			List<JointAction> jaListforGA = getGAJAMap(allJAs, agentActions, subjectAgentName).get(ga);
			double sum = 0.;
			//sum over current states
			for(StateBelief sb: stateBeliefs){
				State s = sb.s;
				//sum over others actions
				for(JointAction ja: jaListforGA){
					//prod over others policies
					double cumOthersJAProb = 1.;
					for(int i=0; i<numAgents; i++){
						if(otherAgentsPolicyMap.containsKey(i)){
							String actingAgent = agIDtoName.get(i);
							Double d = otherAgentsPolicyMap.get(i).getProbOfAction(s, ja.action(actingAgent));
							cumOthersJAProb *= d;
						}
					}
					
					//System.out.println("cumOthersJAProb: "+cumOthersJAProb);
					
					//find prob of next state given, s, ns, ja
					List<TransitionProbability> tps = domain.getJointActionModel().transitionProbsFor(s, ja);
					double tProb = 0.;
					for(TransitionProbability tp: tps){
						if(tp.s.equals(ns)){
							tProb = tp.p;
							//System.out.println("tProb: "+tProb);
						}
					}
					if(tProb>0){
						sum += cumOthersJAProb * tProb * of.getObservationProbability(observation, s, ns, ja) * curB.belief(s);
					}
				}	
			}
			return sum;
		}else{
			if(stateBeliefs==null){
				System.out.println("No states found ..");
			}
			if(agIDtoName==null){
				System.out.println("agIDtoName mapping not defined ..");
			}
			if(otherAgentsPolicyMap==null){
				System.out.println("otherAgentsPolicyMap not defined ..");
			}
			return 0.;
		}
		
	}
	
	public static Map<SGAgentAction,List<JointAction>> getGAJAMap(List<JointAction> allJAs, 
			Map<String,ArrayList<SGAgentAction>> agentActions, String agentName){
		
		Map<SGAgentAction,List<JointAction>> gaJAMap = new HashMap<SGAgentAction,List<JointAction>>();
		List<SGAgentAction> allAgActions = agentActions.get(agentName);
		for(SGAgentAction ga:allAgActions){
			gaJAMap.put(ga, getJAListforGA(ga,allJAs,agentName));
		}
		
		return gaJAMap;
	}
	
	public static List<JointAction> getJAListforGA(SGAgentAction ga, List<JointAction> allJAs, String agentName) {
		List<JointAction> jaListforGA = new ArrayList<JointAction>();
		for(JointAction ja: allJAs){
			if(ja.action(agentName).action.equals(ga)){
				jaListforGA.add(ja);
			}
		}
		return jaListforGA;
	}
	
	public static List<GroundedSGAgentAction> SGtoGroundedSG(
			List<SGAgentAction> allsgActions, String actingAgent) {
		List<GroundedSGAgentAction> allAgActions = new ArrayList<GroundedSGAgentAction>();
		for(int i=0;i<allsgActions.size();i++){
			allAgActions.add(allsgActions.get(i).getAssociatedGroundedAction(actingAgent));
		}
		return allAgActions;
	}

}
