package simulators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nestedMDPSolver.NestedVI;
import common.StateEnumerator;
import posg.ObservationFunction;
import posg.POSGDomain;
import burlap.behavior.policy.Policy;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.statehashing.HashableState;
import burlap.oomdp.statehashing.HashableStateFactory;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.SGDomain;
import domains.wildfire.Wildfire;
import domains.wildfire.WildfireDomain;
import domains.wildfire.WildfireParameters;
import domains.wildfire.Wildfire.WildfireJointRewardFunction;

public class Testing {

	public Testing() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		
		int currentAgentIndex = 0;
		State ms = null;
		WildfireDomain d = null;
		int numPlayers = 0;
		JointReward jr = null;
		TerminalFunction tf = null;
//		StateEnumerator senum = null;
		List<Map<String, SGAgentType>> allAgentDefinitions = new ArrayList<Map<String, SGAgentType>>();
		Map<Integer, String> agIDtoName = new HashMap<Integer, String>();
		Map<Integer,Policy> agFinalPolicyMap = new HashMap<Integer,Policy>();
		
		System.out.println("Generating the domain..");
		Wildfire wf = new Wildfire();
		d = (WildfireDomain) wf.generateDomain();
		jr = new WildfireJointRewardFunction();
		tf = new NullTermination();
		numPlayers = WildfireParameters.numAgents;
		
		
		//1. Get agent definitions
		System.out.println("Getting all agent definitions..");
		allAgentDefinitions = Wildfire.getAllAgentDefs((WildfireDomain) d);
		
		//2. Get agent id-name mapping
		System.out.println("Getting agent id-name mappings..");
		for(int n=0;n<numPlayers;n++){
			agIDtoName.put(n, Wildfire.agentName(n));
		}
		
		//3. Get initial state
		ms = Wildfire.getInitialMasterState((WildfireDomain) d);
		
		//1. find neighbors of subject agent
		int[] neighbors = ((WildfireDomain) d).neighbors[currentAgentIndex];
		int numNeighbors = neighbors.length;
		
		List<Integer> agentsInWorld = new ArrayList<Integer>();
		agentsInWorld.add(currentAgentIndex);
		for(int i=0;i<numNeighbors;i++){
			agentsInWorld.add(neighbors[i]);
		}
		int numAgentsInSubjectAgentWorld = agentsInWorld.size();
		System.out.println("Number of agents in "+currentAgentIndex+"'s world: "+ numAgentsInSubjectAgentWorld);
		
		//2. set agent definitions of agents in the local neighborhood of subject agent
		System.out.println("Getting agent definitions for all agents in subject agent's world..");
		Map<String, SGAgentType> agentDefinitions = allAgentDefinitions.get(currentAgentIndex);
		
		//3. create state of subject agent from master state
		System.out.println("Getting subject agent's initial state..");
		State si = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, currentAgentIndex, ms);
		
		HashSet<HashableState> states = new HashSet<HashableState>();
		HashableStateFactory hf = new SimpleHashableStateFactory(true,false);
		
		//4. Setting state enumerator for subject agent's world..
		System.out.println("Setting state enumerator for subject agent's world..");
		((WildfireDomain) d).setStateEnumerator(new StateEnumerator(d, hf, agentDefinitions));
		
		System.out.println("Getting state enumerator..");
		StateEnumerator senum = ((POSGDomain) d).getStateEnumerator();
		
		//Testing States....
		
		//1. Get initial state
		System.out.println("Master State:\n"+ms.getCompleteStateDescription());
		
		System.out.println("Agent 0 State si:\n"+si.getCompleteStateDescription());
		
		State s_i = Wildfire.createOtherAgentStateFromLocalState(d,1, si);
		System.out.println("Agent 1 State s_i from si:\n"+s_i.getCompleteStateDescription());
		
		State s_i_i = Wildfire.createOtherAgentStateFromLocalState(d,1, s_i);
		System.out.println("Agent 1 State s_i_i from si_i:\n"+s_i_i.getCompleteStateDescription());
		
		State sj = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, 1, ms);
		System.out.println("Agent 1 State:\n"+sj.getCompleteStateDescription());
		
		State s_j = Wildfire.createOtherAgentStateFromLocalState(d,0, sj);
		System.out.println("Agent 0 State s_j from sj:\n"+s_j.getCompleteStateDescription());
		
		//2. Finding all reachable states from a agent state: si..
		System.out.print("Finding all reachable states from a agent state: si");
		senum.findReachableStatesAndEnumerate(si);
		int nS = senum.numStatesEnumerated();
		for(int i=0;i<nS;i++){
			if(i%1000 == 0){
				System.out.print(".");
			}
			State st = senum.getStateForEnumerationId(i);
			HashableState hashedST = hf.hashState(st);
			states.add(hashedST);
		}
		
		System.out.print("Number of states after reachability: "+ states.size()+" == "+senum.numStatesEnumerated());
		
//		//Testing Nested MDP Solver for an agent from its initial state:
//		int maxIterations = 2;
//		int maxLevel = 0;
//		State s = si; //and sj should both work;
//		
//		int currentAgentIndex = 0;
//		Policy ag0Policy = NestedVI.runVI(s,d,senum,allAgentDefinitions.get(currentAgentIndex),agIDtoName,jr,tf,hf,
//				0.9,0.5,maxIterations,maxLevel,numPlayers,currentAgentIndex,false);
//		
//		currentAgentIndex = 1;
//		Policy ag1Policy = NestedVI.runVI(
//				Wildfire.createOtherAgentStateFromLocalState(
//						d, 
//						currentAgentIndex, s),
//				d, senum,allAgentDefinitions.get(currentAgentIndex),agIDtoName,jr,tf,hf,
//				0.9,0.5,maxIterations,maxLevel,numPlayers,currentAgentIndex,false);
//		
//		Policy agFinalPolicy = ag0Policy;
//		for(State state: states){
//			System.out.println("["+senum.getEnumeratedID(state)+"] "+ printActionProbs(agFinalPolicy.getActionDistributionForState(state)));
//		}

	}
	
	public static String printActionProbs(List<ActionProb> aps){
		String str = "";
		for(ActionProb ap:aps){
			str += " "+Double.toString(ap.pSelection);
		}
		return str;
	}
	

}
