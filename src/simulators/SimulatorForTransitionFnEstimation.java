package simulators;

import iPOMDPLiteSolver.iPBVI;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.Random;

import common.POWorld;
import common.StateEnumerator;
import common.pomdp.BeliefMAMDPPolicyAgent;
import common.pomdp.EnumerableBeliefState.StateBelief;
import posg.ObservationFunction;
import posg.POSGDomain;
import posg.TabularBeliefState;
import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.agents.RandomSGAgent;
import burlap.debugtools.MyTimer;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.statehashing.HashableStateFactory;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.WorldObserver;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SimpleSGAgentAction;
import datastructures.Triple;
import domains.wildfire.Wildfire;
import domains.wildfire.WildfireDomain;
import domains.wildfire.WildfireParameters;
import domains.wildfire.Wildfire.WildfireJointRewardFunction;

public class SimulatorForTransitionFnEstimation {

	public static void main(String[] args) {
		
		//example command line argument
		/*
		 * order of command line arguments: maxStages maxtrials conf experiment tempVar
		 * 
		 * [mkran@erebus oasys-git]$  java -cp "bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar" simulators/PBVIPosthocSimulator 100 30 1 5 15 > output/Simulations/Config_1_Results/Experiment_5/Simulation_2-WF_config1_experiment5_100steps_30trials.txt
		 * 
		 */
		
		//TODO Set max trials and max stages here
		
//		int conf = 2;
//		int k = 500;
//		int maxhorizons = 100;
//		int maxtrials = 30;
//		boolean estimateTF=false;
		
		int conf = 0;
		int k = 0;
		int maxhorizons = 0;
		int maxtrials = 0;
		boolean estimateTF=false;
		
		if(args.length == 5){
			k = Integer.parseInt(args[0]);
			maxhorizons = Integer.parseInt(args[1]);
			maxtrials = Integer.parseInt(args[2]);
			conf = Integer.parseInt(args[3]);
			estimateTF = Boolean.parseBoolean(args[4]);
		}
		
		int totalCount = k*maxhorizons*maxtrials;
		
		
		Wildfire wf = new Wildfire();
		WildfireDomain d = (WildfireDomain) wf.generateDomain(conf,estimateTF);
		
		String outFileHead = "output/FinalTransitionFns/Config_"+WildfireParameters.config+"/";
		String filename = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_estimatedTransitionFn_"+k+"_"+maxhorizons+"_"+maxtrials+".txt";
		
		//3. Get initial state
		State ms = Wildfire.getInitialMasterState((WildfireDomain) d);
		
		JointReward jr = new WildfireJointRewardFunction();
		TerminalFunction tf = new NullTermination();
		ObservationFunction of = ((WildfireDomain) d).getObservationFunction();
		int numPlayers = WildfireParameters.numAgents;
		
		Map<Integer, String> agIDtoName = new HashMap<Integer, String>();
		List<Map<String, SGAgentType>> allAgentDefinitions = new ArrayList<Map<String, SGAgentType>>();
		
		//1. Get agent definitions
		allAgentDefinitions = Wildfire.getAllAgentDefs((WildfireDomain) d);
		
		//2. Get agent id-name mapping
		for(int n=0;n<numPlayers;n++){
			agIDtoName.put(n, Wildfire.agentName(n));
		}
		
		Map<String, State> agentStateMap = new HashMap<String,State>();;
		Map<String, TabularBeliefState> srcBSVector = new HashMap<String, TabularBeliefState>();
		Map<String, List<State>> agentParticles = new HashMap<String, List<State>>();
		Map<String,ArrayList<SGAgentAction>> agentActions = new HashMap<String,ArrayList<SGAgentAction>>();
		
		POWorld pow = new POWorld(d,jr,tf,of,allAgentDefinitions,ms);
	
		// CountVector = Mapping from a XAX triple to a count
		// LocalCountVectorPerAgent = Mapping from NeighborID to CountVector 
		// GlobalCountVectorPerAgent = Mapping from Agent ID to LocalCountVectorPerAgent
		//Map<Integer,Map<Integer,Map<Triple<Integer, SGAgentAction, Integer>, Integer>>> globalCountVectorPerAgent = new HashMap<Integer,Map<Integer,Map<Triple<Integer, SGAgentAction, Integer>, Integer>>>();
		List<Map<Integer,Map<Integer,Map<Triple<Integer, SGAgentAction, Integer>, Integer>>>> globalCountVectorPerAgentPerTrial = new ArrayList<Map<Integer,Map<Integer,Map<Triple<Integer, SGAgentAction, Integer>, Integer>>>>();
		StateEnumerator senums[] = new StateEnumerator[numPlayers];
		
		for(int i=0;i<numPlayers;i++){
			int agentID = i;
			String agentName = agIDtoName.get(agentID);
			
			Map<String, SGAgentType> agentDefinitions = allAgentDefinitions.get(agentID);
			
			State si = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, agentID, ms);
			agentStateMap.put(agentName, si);
			
//			List<JointAction> allJAs = JointAction.getAllJointActions(si, agentDefinitions);
			
			HashableStateFactory hf = new SimpleHashableStateFactory(true,false);
			((WildfireDomain) d).setStateEnumerator(new StateEnumerator(d, hf, agentDefinitions));
			
			senums[i] = new StateEnumerator(d, hf, agentDefinitions);
            senums[i].findReachableStatesAndEnumerate(si);
			
            //2. Initialize count vectors for each neighbor of the subject agent
			for(Map.Entry<String, SGAgentType> e: allAgentDefinitions.get(agentID).entrySet()){
				agentActions.put(e.getKey(), (ArrayList<SGAgentAction>) e.getValue().actions);
			}
			
			SGAgent a = new RandomSGAgent();
			SGAgentType at = Wildfire.getAgentType(d, i);
			a.joinWorld(pow, at);
			
		}
		
		
		
//		printGCV(globalCountVectorPerAgent,agentStateMap,agentActions,agIDtoName);
		//For each agent
		int trial = 0;
		MyTimer trialtimer = new MyTimer();
		
		System.out.println("Running Trials: ");
		System.out.print("Trial :");
		while(trial<maxtrials){
			
			System.out.print(" "+ trial);
			trialtimer.start();
			Map<Integer,Map<Integer,Map<Triple<Integer, SGAgentAction, Integer>, Integer>>> globalCountVectorPerAgent = new HashMap<Integer,Map<Integer,Map<Triple<Integer, SGAgentAction, Integer>, Integer>>>();
			for(int i=0;i<numPlayers;i++){
				int agentID = i;
				String agentName = agIDtoName.get(agentID);
				TabularBeliefState tbs = new TabularBeliefState(d, senums[i], agentName);
				tbs = Wildfire.getInitialBeliefState((WildfireDomain) d, agentID, senums[i]);
				
				//Create a random non-uniform initial belief state
				List<StateBelief> sbs = tbs.getStatesAndBeliefsWithNonZeroProbability();
				double[] initDist = generateRandomDistribution(sbs.size());
	//			printArrayVector(initDist);
				TabularBeliefState initBelief = setBelief(tbs,sbs,initDist);
				srcBSVector.put(agentName, initBelief);
				
				//Create k particles for each agent
				List<State> particles = new ArrayList<State>();
				for(int pID = 0; pID < k; pID++){
					particles.add(initBelief.sampleStateFromBelief());
				}
				agentParticles.put(agentName, particles);
				
				
				Map<Integer,Map<Triple<Integer, SGAgentAction, Integer>, Integer>> localCountVectorPerAgent = new HashMap<Integer,Map<Triple<Integer,SGAgentAction,Integer>,Integer>>();
				int oaSize = agentStateMap.get(agentName).getObjectsOfClass("agent").size();
				for(int oaID=0;oaID<oaSize;oaID++){
					int agID = agentStateMap.get(agentName).getObjectsOfClass("agent").get(oaID).getIntValForAttribute("agent_num");
					List<SGAgentAction> agActions = agentActions.get(agIDtoName.get(agID));
					localCountVectorPerAgent.put(agID,initCountVector(Wildfire.NUM_AVAILABLE_STATES,agActions));
				}
				
				globalCountVectorPerAgent.put(i, localCountVectorPerAgent);
			
			}
			
			Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Integer>>> cv = 
					new HashMap<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Integer>>>();
			
			cv = pow.runGame(maxhorizons, ms, agentParticles, agentActions, srcBSVector, globalCountVectorPerAgent);

			
			globalCountVectorPerAgentPerTrial.add(cv);
			
			trial++;
			trialtimer.stop();
		}
		System.out.println();
		
		
//		printGCV(globalCountVectorPerAgentPerTrial,agentStateMap,agentActions,agIDtoName,maxtrials);
		
		Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> globalCountVectorPerAgentCumulative= 
				new HashMap<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>>();
		
		globalCountVectorPerAgentCumulative = addCountVectors(globalCountVectorPerAgentPerTrial, agentStateMap, agentActions, agIDtoName, maxtrials, totalCount);
		
		System.out.println("\nFinished running "+maxtrials+" trials..Cumulative Results:");
//		printGCVDouble(globalCountVectorPerAgentCumulative,agentStateMap,agentActions,agIDtoName);	
		
		Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> estimatedAgentTransitionFunction= 
				new HashMap<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>>();
		
		estimatedAgentTransitionFunction = getTransitionFnFromCountVector(globalCountVectorPerAgentCumulative, agentStateMap, agentActions, agIDtoName,filename);
		
//		System.out.println("-------------Estimated Agent Transition Function--------------");
//		printTransitionFn(estimatedAgentTransitionFunction,agentStateMap,agentActions,agIDtoName);
		
		Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> parsedAgentTransitionFunction= 
				new HashMap<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>>();
		
		parsedAgentTransitionFunction = readTransitionFnFromFile(d,filename);
		
		System.out.println("-------------Parsed Agent Transition Function--------------");
		printTransitionFn(parsedAgentTransitionFunction,agentStateMap,agentActions,agIDtoName);
		
		
		
		
		System.out.println("---------------------------------------------------");
		System.out.println("[TIMING RESULTS] Performed total "+maxtrials+" trials..in "+trialtimer.getTotalTime()+
				"s \n[TIMING RESULTS] Avg time/trial = "+trialtimer.getAvgTime());
		System.out.println("---------------------------------------------------");
		
	}
	
	
	public static Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> readTransitionFnFromFile(WildfireDomain d,
			String filename) {
		
		ArrayList<String> list = new ArrayList<String>();
		
		try {
			Scanner s = new Scanner(new File(filename));
			
			while (s.hasNext()){
			    list.add(s.next());
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
		//Line 0 = agIDtoName
		String[] agentNames = list.get(0).split(",");
		int numAgents = agentNames.length;
		
		Map<Integer, String> agIDtoName = new HashMap<Integer, String>();
		
		for(int i=0;i<numAgents;i++){
			agIDtoName.put(i,agentNames[i]);
		}
		
		//Line 1 = oaSizes
		String[] oaSizeStr = list.get(1).split(",");
		int[] oaSizes = new int[agIDtoName.size()];
		for(int i=0;i<agIDtoName.size();i++){
			oaSizes[i]= Integer.parseInt(oaSizeStr[i]);
		}
		
		//Line 2 = oaIDs
		String[] oaIDStrs = list.get(2).split(":");
		List<List<Integer>> oaIDs = new ArrayList<List<Integer>>();
		for(int k=0;k<oaIDStrs.length;k++){
			String[] oaIDStr = oaIDStrs[k].split(",");
			List<Integer> oaIDtemp = new ArrayList<Integer>();
			for(int j=0;j<oaIDStr.length;j++){
				int temp = Integer.parseInt(oaIDStr[j]);
				oaIDtemp.add(temp);
			}
			oaIDs.add(oaIDtemp);
		}
		
		
		//Line 3 = agentActions
		String[] agentActionStrs = list.get(3).split(":");
		Map<String, ArrayList<SGAgentAction>> agentActions = new HashMap<String, ArrayList<SGAgentAction>>();
		for(int k=0;k<agentActionStrs.length;k++){
			ArrayList<SGAgentAction> agActions = new ArrayList<SGAgentAction>();
			String[] agentActionStr = agentActionStrs[k].split(",");
			for(int a=0;a<agentActionStr.length;a++){
				SGAgentAction tempAct = new SimpleSGAgentAction(((WildfireDomain) d),agentActionStr[a]);
				agActions.add(tempAct);
			}
			agentActions.put(agIDtoName.get(k),agActions);
		}
		
		Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> estimatedAgentTransitionFunction= 
				new HashMap<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>>();
		
		int line=4;
		for(int i=0;i<agIDtoName.size();i++){
			System.out.println("Agent "+i+" ");
			Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>> localProbabilityPerAgent= 
					new HashMap<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>();
			
			int oaSize = oaSizes[i];
			for(int j=0;j<oaSize;j++){
				int oaID = oaIDs.get(i).get(j);
				System.out.println("Neighbor "+oaID+" ");
				List<SGAgentAction> agActions = agentActions.get(agIDtoName.get(oaID));
				
				Map<Triple<Integer, SGAgentAction, Integer>, Double> prob = initCountVectorDouble(Wildfire.NUM_AVAILABLE_STATES,agActions);
				
				for(SGAgentAction a: agActions){
					for(int x=0;x<Wildfire.NUM_AVAILABLE_STATES;x++){
						for(int xp=0;xp<Wildfire.NUM_AVAILABLE_STATES;xp++){
							Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
							double temp = Double.parseDouble(list.get(line));
							prob.put(trip, temp);
							line++;
						}
					}
				}
				localProbabilityPerAgent.put(oaID, prob);
			}
			estimatedAgentTransitionFunction.put(i, localProbabilityPerAgent);
		}
		
		return estimatedAgentTransitionFunction;
	}
	
	private static Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> readTransitionFnFromFile(
			Map<String, State> agentStateMap,
			Map<String, ArrayList<SGAgentAction>> agentActions,
			Map<Integer, String> agIDtoName,
			String filename) {
		
		ArrayList<String> list = new ArrayList<String>();
		
		try {
			Scanner s = new Scanner(new File(filename));
			
			while (s.hasNext()){
			    list.add(s.next());
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> estimatedAgentTransitionFunction= 
				new HashMap<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>>();
		
		int line=0;
		for(int i=0;i<agIDtoName.size();i++){
			System.out.println("Agent "+i+" ");
			Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>> localProbabilityPerAgent= 
					new HashMap<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>();
			
			int oaSize = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").size();
			for(int j=0;j<oaSize;j++){
				int oaID = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").get(j).getIntValForAttribute("agent_num");
				System.out.println("Neighbor "+oaID+" ");
				List<SGAgentAction> agActions = agentActions.get(agIDtoName.get(oaID));
				
				Map<Triple<Integer, SGAgentAction, Integer>, Double> prob = initCountVectorDouble(Wildfire.NUM_AVAILABLE_STATES,agActions);
				
				for(SGAgentAction a: agActions){
					for(int x=0;x<Wildfire.NUM_AVAILABLE_STATES;x++){
						for(int xp=0;xp<Wildfire.NUM_AVAILABLE_STATES;xp++){
							Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
							double temp = Double.parseDouble(list.get(line));
							prob.put(trip, temp);
							line++;
						}
					}
				}
				localProbabilityPerAgent.put(oaID, prob);
			}
			estimatedAgentTransitionFunction.put(i, localProbabilityPerAgent);
		}
		
		return estimatedAgentTransitionFunction;
	}
	
	private static Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> getTransitionFnFromCountVector(
			Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> globalCountVectorPerAgentCumulative,
			Map<String, State> agentStateMap,
			Map<String, ArrayList<SGAgentAction>> agentActions,
			Map<Integer, String> agIDtoName,
			String filename) {
		
		Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> estimatedAgentTransitionFunction= 
				new HashMap<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>>();
		
		String content="";
		//agentIDtoName: agent0,agent1
		//agentActions: Map
		//oaSize: 1,1
		//oaID: bla,bla
		
		for(int i=0;i<agIDtoName.size();i++){
			if(i!=agIDtoName.size()-1){
				content+=agIDtoName.get(i)+",";
			}else{
				content+=agIDtoName.get(i);
			}
		}
		content+="\n";
		for(int i=0;i<agIDtoName.size();i++){
			int oaSize = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").size();
			if(i!=agIDtoName.size()-1){
				content+=Integer.toString(oaSize)+",";
			}else{
				content+=Integer.toString(oaSize);
			}
		}
		content+="\n";
		for(int i=0;i<agIDtoName.size();i++){
			int oaSize = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").size();
			for(int j=0;j<oaSize;j++){
				int oaID = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").get(j).getIntValForAttribute("agent_num");
				if(j!=oaSize-1){
					content+=Integer.toString(oaID)+",";
				}else{
					content+=Integer.toString(oaID);
				}
			}
			if(i!=agIDtoName.size()-1){
				content+=":";
			}else{
				content+="\n";
			}
		}
		for(int i=0;i<agIDtoName.size();i++){
			List<SGAgentAction> agActions = agentActions.get(agIDtoName.get(i));
			int aCnt=0;
			for(SGAgentAction a: agActions){
				if(aCnt!=agActions.size()-1){
					content+=a.actionName+",";
				}else{
					content+=a.actionName;
				}
				aCnt++;
			}
			if(i!=agIDtoName.size()-1){
				content+=":";
			}else{
				content+="\n";
			}
		}
		
		for(int i=0;i<agIDtoName.size();i++){
			System.out.println("Agent "+i+" ");
			Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>> localProbabilityPerAgent= 
					new HashMap<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>();
			
			int oaSize = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").size();
			for(int j=0;j<oaSize;j++){
				int oaID = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").get(j).getIntValForAttribute("agent_num");
				System.out.println("Neighbor "+oaID+" ");
				List<SGAgentAction> agActions = agentActions.get(agIDtoName.get(oaID));
				Map<Triple<Integer, SGAgentAction, Integer>, Double> prob = initCountVectorDouble(Wildfire.NUM_AVAILABLE_STATES,agActions);
				
				for(SGAgentAction a: agActions){
					for(int x=0;x<Wildfire.NUM_AVAILABLE_STATES;x++){
						double sum = 0.;
						for(int xp=0;xp<Wildfire.NUM_AVAILABLE_STATES;xp++){
							Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
							sum += globalCountVectorPerAgentCumulative.get(i).get(oaID).get(trip);
						}
						for(int xp=0;xp<Wildfire.NUM_AVAILABLE_STATES;xp++){
							Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
							if(sum>0){
								double temp = globalCountVectorPerAgentCumulative.get(i).get(oaID).get(trip)/sum;
								prob.put(trip, temp);
								content += Double.toString(temp) +"\n"; 
							}else{
								double temp = 1.0/(Wildfire.NUM_AVAILABLE_STATES*1.0);
								prob.put(trip, temp);
								content += Double.toString(temp) +"\n";
							}
						}
					}
				}
				localProbabilityPerAgent.put(oaID, prob);
			}
			estimatedAgentTransitionFunction.put(i, localProbabilityPerAgent);
		}
		
		writeOutputToFile(content, filename);
		
		return estimatedAgentTransitionFunction;
	}

	private static Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> addCountVectors(
			List<Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Integer>>>> globalCountVectorPerAgentPerTrial,
			Map<String, State> agentStateMap, 
			Map<String,ArrayList<SGAgentAction>> agentActions,
			Map<Integer, String> agIDtoName,int maxtrials, int totalCount) {
		
		Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> globalCountVectorPerAgentCumulative= 
				new HashMap<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>>();
		
		for(int i=0;i<agIDtoName.size();i++){
//			System.out.println("Agent "+i+" ");
			Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>> localCountVectorPerAgentCumulative= 
					new HashMap<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>();
			
			int oaSize = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").size();
			for(int j=0;j<oaSize;j++){
				int oaID = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").get(j).getIntValForAttribute("agent_num");
//				System.out.println("Neighbor "+oaID+" ");
				List<SGAgentAction> agActions = agentActions.get(agIDtoName.get(oaID));
//				for(SGAgentAction agAction:agActions){
//					System.out.print(" "+ agAction.actionName);
//				}
//				System.out.println();
				
				
				Map<Triple<Integer, SGAgentAction, Integer>, Double> sumCV = initCountVectorDouble(Wildfire.NUM_AVAILABLE_STATES,agActions);
				
				for(int x=0;x<Wildfire.NUM_AVAILABLE_STATES;x++){
					for(SGAgentAction a: agActions){
						for(int xp=0;xp<Wildfire.NUM_AVAILABLE_STATES;xp++){
							Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
							double sum = 0.;
							for(int trial=0; trial < maxtrials; trial++){
								sum += globalCountVectorPerAgentPerTrial.get(trial).get(i).get(oaID).get(trip);
							}
							sumCV.put(trip, sum/totalCount);
						}
					}
				}
				localCountVectorPerAgentCumulative.put(oaID, sumCV);
			}
			globalCountVectorPerAgentCumulative.put(i, localCountVectorPerAgentCumulative);
		}
		
		return globalCountVectorPerAgentCumulative;
		
	}
	
	public static void printTransitionFn(
			Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> estimatedAgentTransitionFunction, 
			Map<String, State> agentStateMap, 
			Map<String,ArrayList<SGAgentAction>> agentActions,
			Map<Integer, String> agIDtoName) {
		
		for(int i=0;i<agIDtoName.size();i++){
			System.out.println("Agent "+i+" ");
			
			int oaSize = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").size();
			for(int oaID=0;oaID<oaSize;oaID++){
				int agID = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").get(oaID).getIntValForAttribute("agent_num");
				System.out.println("Neighbor "+agID+" ");
				List<SGAgentAction> agActions = agentActions.get(agIDtoName.get(agID));
				for(SGAgentAction agAction:agActions){
					System.out.print(" "+ agAction.actionName);
				}
				System.out.println();
				printTransitionProbs(estimatedAgentTransitionFunction.get(i).get(agID),
						Wildfire.NUM_AVAILABLE_STATES,agActions);
			}
			
		}
	}
	
	public static void printGCVDouble(
			Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Double>>> globalCountVectorPerAgent, 
			Map<String, State> agentStateMap, 
			Map<String,ArrayList<SGAgentAction>> agentActions,
			Map<Integer, String> agIDtoName) {
		
		for(int i=0;i<agIDtoName.size();i++){
			System.out.println("Agent "+i+" ");
			
			int oaSize = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").size();
			for(int oaID=0;oaID<oaSize;oaID++){
				int agID = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").get(oaID).getIntValForAttribute("agent_num");
				System.out.println("Neighbor "+agID+" ");
				List<SGAgentAction> agActions = agentActions.get(agIDtoName.get(agID));
				for(SGAgentAction agAction:agActions){
					System.out.print(" "+ agAction.actionName);
				}
				System.out.println();
				printCountVectorDouble(globalCountVectorPerAgent.get(i).get(agID),
						Wildfire.NUM_AVAILABLE_STATES,agActions);
			}
			
		}
	}

	// CountVector = Mapping from a XAX triple to a count
	// LocalCountVectorPerAgent = Mapping from NeighborID to CountVector 
	// GlobalCountVectorPerAgent = Mapping from Agent ID to LocalCountVectorPerAgent
	public static void printGCV(
			Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Integer>>> globalCountVectorPerAgent, 
			Map<String, State> agentStateMap, 
			Map<String,ArrayList<SGAgentAction>> agentActions,
			Map<Integer, String> agIDtoName) {
		
		for(int i=0;i<agIDtoName.size();i++){
			System.out.println("Agent "+i+" ");
			
			int oaSize = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").size();
			for(int oaID=0;oaID<oaSize;oaID++){
				int agID = agentStateMap.get(agIDtoName.get(i)).getObjectsOfClass("agent").get(oaID).getIntValForAttribute("agent_num");
				System.out.println("Neighbor "+agID+" ");
				List<SGAgentAction> agActions = agentActions.get(agIDtoName.get(agID));
				for(SGAgentAction agAction:agActions){
					System.out.print(" "+ agAction.actionName);
				}
				System.out.println();
				printCountVector(globalCountVectorPerAgent.get(i).get(agID),
						Wildfire.NUM_AVAILABLE_STATES,agActions);
			}
			
		}
	}
	
	public static void printGCV(
			List<Map<Integer, Map<Integer, Map<Triple<Integer, SGAgentAction, Integer>, Integer>>>> globalCountVectorPerAgentPerTrial, 
			Map<String, State> agentStateMap, 
			Map<String,ArrayList<SGAgentAction>> agentActions,
			Map<Integer, String> agIDtoName, 
			int maxtrials) {
		
		for(int trial=0; trial<maxtrials; trial++){
			System.out.println("Per Trial "+trial+" \n---------------");
			printGCV(globalCountVectorPerAgentPerTrial.get(trial), agentStateMap, agentActions, agIDtoName);
		}
		
	}
	
	private static void printTransitionProbs(Map<Triple<Integer, SGAgentAction, Integer>, Double> countVector,
			int numAvailableStates, List<SGAgentAction> agActions) {
		
		for(SGAgentAction a: agActions){ 
			System.out.println(a.actionName+"\n-------------------");
			for(int x=0;x<numAvailableStates;x++){
				double sum=0.;
				for(int xp=0;xp<numAvailableStates;xp++){
					Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
					System.out.print(" "+countVector.get(trip));
					sum+=countVector.get(trip);
				}
				System.out.print(" Total: "+sum+"\n");
			}
			System.out.println("-------------------");
		}
		
		
	}


	private static void printCountVectorDouble(Map<Triple<Integer, SGAgentAction, Integer>, Double> countVector,
			int numAvailableStates, List<SGAgentAction> agActions) {
		
		double sum=0.;
		for(int x=0;x<numAvailableStates;x++){
			for(SGAgentAction a: agActions){
				for(int xp=0;xp<numAvailableStates;xp++){
					Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
					System.out.print(" "+countVector.get(trip));
					sum+=countVector.get(trip);
				}
			}
		}
		System.out.println(" \nTotal: "+sum);
		
	}
	
	private static void printCountVector(Map<Triple<Integer, SGAgentAction, Integer>, Integer> countVector,
			int numAvailableStates, List<SGAgentAction> agActions) {
		
		for(int x=0;x<numAvailableStates;x++){
			for(SGAgentAction a: agActions){
				for(int xp=0;xp<numAvailableStates;xp++){
					Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
					System.out.print(" "+countVector.get(trip));
				}
			}
		}
		System.out.println();
		
	}
	
	private static Map<Triple<Integer, SGAgentAction, Integer>, Double> initCountVectorDouble(
			int numAvailableStates, List<SGAgentAction> agActions) {
		
		Map<Triple<Integer,SGAgentAction,Integer>,Double> localCountVector = new HashMap<Triple<Integer,SGAgentAction,Integer>,Double>();
		for(int x=0;x<numAvailableStates;x++){
			for(SGAgentAction a: agActions){
				for(int xp=0;xp<numAvailableStates;xp++){
					Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
					localCountVector.put(trip, 0.);
				}
			}
		}
		return localCountVector;
	}

	private static Map<Triple<Integer, SGAgentAction, Integer>, Integer> initCountVector(
			int numAvailableStates, List<SGAgentAction> agActions) {
		
		Map<Triple<Integer,SGAgentAction,Integer>,Integer> localCountVector = new HashMap<Triple<Integer,SGAgentAction,Integer>,Integer>();
		for(int x=0;x<numAvailableStates;x++){
			for(SGAgentAction a: agActions){
				for(int xp=0;xp<numAvailableStates;xp++){
					Triple<Integer,SGAgentAction,Integer> trip = new Triple<Integer, SGAgentAction, Integer>(x,a,xp);
					localCountVector.put(trip, 0);
				}
			}
		}
		return localCountVector;
	}

	public static TabularBeliefState setBelief(TabularBeliefState tbs, List<StateBelief> sbs, double[] initDist) {
		for(int i=0;i<sbs.size();i++){
			tbs.setBelief(sbs.get(i).s, initDist[i]);
		}
		return tbs;
	}
	
	public static Map<SGAgentAction,List<JointAction>> getGAJAMap(List<JointAction> allJAs, String agentName, Map<String, ArrayList<SGAgentAction>> agentActions){
		Map<SGAgentAction,List<JointAction>> gaJAMap = new HashMap<SGAgentAction,List<JointAction>>();
		List<SGAgentAction> allAgActions = agentActions.get(agentName);
		for(SGAgentAction ga:allAgActions){
			gaJAMap.put(ga, getJAListforGA(ga,allJAs,agentName));
		}
		
		return gaJAMap;
	}
	
	public static List<JointAction> getJAListforGA(SGAgentAction ga, List<JointAction> allJAs, String agentName) {
		List<JointAction> jaListforGA = new ArrayList<JointAction>();
		for(JointAction ja: allJAs){
			if(ja.action(agentName).action.equals(ga)){
				jaListforGA.add(ja);
			}
		}
		return jaListforGA;
	}

	public static double[] generateRandomDistribution(int numElements){
		//Get n random numbers, calculate their sum and normalize the sum to 1 by dividing each number with the sum.
		int[] nums = new int[numElements];
		Random randomGenerator = new Random();
		
		double sum=0.;
        for (int i = 0; i < numElements; ++i){
            nums[i] = randomGenerator.nextInt(100);
            sum += nums[i];
        }
        
        double[] dist = new double[numElements];
        double distSum = 0;
        for (int i = 0; i < numElements-1; ++i){
            dist[i] = (nums[i]*1.0)/sum;
            distSum += dist[i];
        }
        dist[numElements-1] = 1-distSum;
        
//        double dSum = 0;
//        for (int i = 0; i < numElements; ++i){
//            dSum += dist[i];
//        }
//        
//        System.out.println(dSum);
//        printArrayVector(dist);
		
        return dist;
	}
	
	public static void printArrayVector(double [] b){
		for(int i = 0; i < b.length; i++){
			System.out.print(" "+b[i]);
		}
		System.out.println();
	}
	
	public static void printBeliefVector(TabularBeliefState bs){
		double [] b = bs.getBeliefVector();
		System.out.print("b["+b.length+"]");
		for(int i = 0; i < b.length; i++){
			System.out.print(" "+b[i]);
			if(i%30==0)
				System.out.println();
		}
		System.out.println();
	}
	
	static String saveSuppressantResultsToFile(GameAnalysis read, int trial) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		int suppressantLevel[][] = new int[numAgents][maxStages+1];
		int totalSuppressantLevel[] = new int[maxStages+1];
		
		String suppressantHead = "\nTrial "+trial+"\n";
		String suppressantStr = "";
		String totals = "Total,";
		
		int sumSuppressant = 0;
		for(int i=0; i<=maxStages; i++){
			sumSuppressant=0;
			for (int agID=0; agID<numAgents; agID++){
				//suppressants
				suppressantLevel[agID][i] = read.getState(i).getObjectsOfClass("agent").get(agID).getIntValForAttribute("available");
				sumSuppressant += suppressantLevel[agID][i];
			}
			totalSuppressantLevel[i] = sumSuppressant;
			
		}
		
		for(int agID=0; agID<numAgents; agID++){
			suppressantStr += agID+",";
			for (int i=0; i<=maxStages; i++){
				suppressantStr += Integer.toString(suppressantLevel[agID][i])+",";
			}
			suppressantStr += "\n";
		}
		
		suppressantStr += totals;
		for (int i=0; i<=maxStages; i++){
			suppressantStr += Integer.toString(totalSuppressantLevel[i])+",";
		}
		
		String content = suppressantHead + suppressantStr + "\n" ;
		
		return content;
	}
	
	static String saveRewardResultsToFile(GameAnalysis read, int trial) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		List<Map<String, Double>> rMapList = read.jointRewards;
		double rewards[][] = new double[numAgents][maxStages+1];
		double totalRewards[] = new double[maxStages+1];
		
		String rewardHead = "\nTrial "+trial+"\n";
		String rewardStr = "";
		String totals = "Total,";
		
		double sumReward = 0;
		int ag=0;
		for(int i=0; i<=maxStages; i++){
			//Rewards
			sumReward=0;
			ag=0;
			if(i!=maxStages){
				for(Entry<String, Double> rEntry: rMapList.get(i).entrySet()){
					rewards[ag][i] = rEntry.getValue();
					sumReward+=rewards[ag][i];
					ag++;
				}
				totalRewards[i] = sumReward;
			}
		}
		
		for(int agID=0; agID<numAgents; agID++){
			rewardStr += agID+",";
			for (int i=0; i<=maxStages; i++){
				if(i!=maxStages){rewardStr += Double.toString(rewards[agID][i])+",";}
			}
			rewardStr += "\n";
		}
		
		rewardStr += totals;
		for (int i=0; i<=maxStages; i++){
			if(i!=maxStages){rewardStr += Double.toString(totalRewards[i])+",";}
		}
		
		String content = rewardHead + rewardStr + "\n" ;
		
		return content;
		//writeOutputToFile(content, resultsFile);
		
		
	}
	
	static String saveActionResultsToFile(GameAnalysis read, int trial) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		List<JointAction> allJAsList = read.jointActions;
		String actions[][] = new String[numAgents][maxStages];
		
		String actionHead = "\nTrial "+trial+"\n";
		String actionStr = "";
		
		for(int i=0; i<=maxStages; i++){
			for (int agID=0; agID<numAgents; agID++){
				//actions
				String agName = "agent"+agID;
				if(i!=maxStages){
					actions[agID][i] = allJAsList.get(i).action(agName).actionName();
				}
			}
		}
		
		for(int agID=0; agID<numAgents; agID++){
			actionStr += agID+",";
			for (int i=0; i<=maxStages; i++){
				if(i!=maxStages){actionStr += actions[agID][i]+",";} 
			}
			actionStr += "\n";
		}
		
		String content = actionHead + actionStr + "\n";
		
		return content;
//		writeOutputToFile(content, resultsFile);
		
		
	}
	
	static String saveFireResultsToFile(GameAnalysis read, int trial) {
		int maxStages = read.maxTimeStep();
		int numFires = read.getState(0).getObjectsOfClass("fire").size();
		
		int fireIntensity[][] = new int[numFires][maxStages+1];
		int totalFireIntensity[] = new int[maxStages+1];
		
		String fireHead = "\nTrial "+trial+"\n";
		String fireStr = "";
		String totals = "Total,";
		int sumFire = 0;
		
		
		for(int i=0; i<=maxStages; i++){
			//Fires
			sumFire=0;
			for (int f=0; f<numFires; f++){
				fireIntensity[f][i] = Integer.parseInt(read.getState(i).getAllObjects().get(f).getStringValForAttribute("intensity"));
				sumFire += fireIntensity[f][i];
			}
			totalFireIntensity[i] = sumFire;
		}
		
		for(int f=0; f<numFires; f++){
			fireStr += f+",";
			for (int i=0; i<=maxStages; i++){
				fireStr += Integer.toString(fireIntensity[f][i])+",";
			}
			fireStr += "\n";
		}
		
		fireStr += totals;
		for (int i=0; i<=maxStages; i++){
			fireStr += Integer.toString(totalFireIntensity[i])+",";
		}
		
		String content = fireHead + fireStr + "\n" ;
		
		
		return content;
		
		
	}
	
	
	
	static void saveAllResultsToFile(GameAnalysis read,	String resultsFile) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		int numFires = read.getState(0).getObjectsOfClass("fire").size();
		
		List<JointAction> allJAsList = read.jointActions;
		String actions[][] = new String[numAgents][maxStages];
		double rewards[][] = new double[numAgents][maxStages+1];
		List<Map<String, Double>> rMapList = read.jointRewards;
		
		double totalRewards[] = new double[maxStages+1];
		int suppressantLevel[][] = new int[numAgents][maxStages+1];
		int totalSuppressantLevel[] = new int[maxStages+1];
		int fireIntensity[][] = new int[numFires][maxStages+1];
		int totalFireIntensity[] = new int[maxStages+1];
		
		String fireHead = "\nFire Intensities\n";
		String fireStr = "";
		String rewardHead = "\nJoint Rewards\n";
		String rewardStr = "";
		String actionHead = "\nJoint Actions\n";
		String actionStr = "";
		String suppressantHead = "\nSuppressant Level\n";
		String suppressantStr = "";
		String totals = "Total,";
		
		double sumReward = 0;
		int sumSuppressant = 0;
		int sumFire = 0;
		int ag=0;
		
		
		for(int i=0; i<=maxStages; i++){
			sumSuppressant=0;
			for (int agID=0; agID<numAgents; agID++){
				//actions
				String agName = "agent"+agID;
				if(i!=maxStages){
					actions[agID][i] = allJAsList.get(i).action(agName).actionName();
				}
				
				//suppressants
				suppressantLevel[agID][i] = read.getState(i).getObjectsOfClass("agent").get(agID).getIntValForAttribute("available");
				sumSuppressant += suppressantLevel[agID][i];
			}
			totalSuppressantLevel[i] = sumSuppressant;
			
			//Rewards
			sumReward=0;
			ag=0;
			if(i!=maxStages){
				for(Entry<String, Double> rEntry: rMapList.get(i).entrySet()){
					rewards[ag][i] = rEntry.getValue();
					sumReward+=rewards[ag][i];
					ag++;
				}
				totalRewards[i] = sumReward;
			}
			//Fires
			sumFire=0;
			for (int f=0; f<numFires; f++){
				fireIntensity[f][i] = Integer.parseInt(read.getState(i).getAllObjects().get(f).getStringValForAttribute("intensity"));
				sumFire += fireIntensity[f][i];
			}
			totalFireIntensity[i] = sumFire;
		}
		
		for(int f=0; f<numFires; f++){
			fireStr += f+",";
			for (int i=0; i<=maxStages; i++){
				fireStr += Integer.toString(fireIntensity[f][i])+",";
			}
			fireStr += "\n";
		}
		
		for(int agID=0; agID<numAgents; agID++){
			rewardStr += agID+",";
			actionStr += agID+",";
			suppressantStr += agID+",";
			for (int i=0; i<=maxStages; i++){
				if(i!=maxStages){rewardStr += Double.toString(rewards[agID][i])+",";}
				if(i!=maxStages){actionStr += actions[agID][i]+",";} 
				suppressantStr += Integer.toString(suppressantLevel[agID][i])+",";
			}
			rewardStr += "\n";
			actionStr += "\n";
			suppressantStr += "\n";
		}
		
		fireStr += totals;
		rewardStr += totals;
		suppressantStr += totals;
		for (int i=0; i<=maxStages; i++){
			fireStr += Integer.toString(totalFireIntensity[i])+",";
			if(i!=maxStages){rewardStr += Double.toString(totalRewards[i])+",";}
			suppressantStr += Integer.toString(totalSuppressantLevel[i])+",";
		}
		
		String content = fireHead + fireStr + "\n" +
				rewardHead + rewardStr + "\n" +
				suppressantHead + suppressantStr + "\n" +
				actionHead + actionStr + "\n";
		
		
		writeOutputToFile(content, resultsFile);
		
		
	}

	@SuppressWarnings("unused")
	private static void printAgentSuppressantStats(GameAnalysis read) {
		
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		int suppressantLevel[][] = new int[numAgents][maxStages+1];
		int totalSuppressantLevel[] = new int[maxStages+1];
		for(int i=0; i<=maxStages; i++){
			int sum=0;
			for (int agID=0; agID<numAgents; agID++){
				suppressantLevel[agID][i] = read.getState(i).getObjectsOfClass("agent").get(agID).getIntValForAttribute("available");
				sum += suppressantLevel[agID][i];
			}
			totalSuppressantLevel[i] = sum;
		}
		
		for (int agID=0; agID<numAgents; agID++){
			System.out.println("[Agent "+agID+"]\t"+Arrays.toString(suppressantLevel[agID]));
		}
		System.out.println("[Total]\t\t"+Arrays.toString(totalSuppressantLevel));
		
	}

	@SuppressWarnings("unused")
	private static void printAgentRewardStats(GameAnalysis read) {
		List<Map<String, Double>> rMapList = read.jointRewards;
		
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		double rewards[][] = new double[numAgents][maxStages+1];
		double totalRewards[] = new double[maxStages+1];
		
		for(int i=0;i<rMapList.size();i++){
			int agID=0;
			double sum=0;
			for(Entry<String, Double> rEntry: rMapList.get(i).entrySet()){
				rewards[agID][i] = rEntry.getValue();
				sum+=rewards[agID][i];
				agID++;
			}
			totalRewards[i] = sum;
		}
			
		for (int agID=0; agID<numAgents; agID++){
			System.out.println("[Agent "+agID+"]\t"+Arrays.toString(rewards[agID]));
		}
		System.out.println("[Total]\t\t"+Arrays.toString(totalRewards));
	}
	
	@SuppressWarnings("unused")
	private static double[] getTotalRewardForTrial(GameAnalysis read){
		List<Map<String, Double>> rMapList = read.jointRewards;
		
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		double rewards[][] = new double[numAgents][maxStages+1];
		double totalRewards[] = new double[maxStages+1];
		
		for(int i=0;i<rMapList.size();i++){
			int agID=0;
			double sum=0;
			for(Entry<String, Double> rEntry: rMapList.get(i).entrySet()){
				rewards[agID][i] = rEntry.getValue();
				sum+=rewards[agID][i];
				agID++;
			}
			totalRewards[i] = sum;
		}
		return totalRewards;
	}

	@SuppressWarnings("unused")
	private static void printAgentActionStats(GameAnalysis read) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		List<JointAction> allJAsList = read.jointActions;
		
		String actions[][] = new String[numAgents][maxStages];
		
		for(int i=0;i<allJAsList.size();i++){
			for (int agID=0; agID<numAgents; agID++){
				String agName = "agent"+agID;
				actions[agID][i] = allJAsList.get(i).action(agName).actionName();
			}
		}
		for (int agID=0; agID<numAgents; agID++){
			System.out.println("[Agent "+agID+"]\t"+Arrays.toString(actions[agID]));
		}
		
	}
	
	

	public static void writeOutputToFile(String content, String filename){
		boolean writeToFile = true;
		if(writeToFile){
			try {
				File file = new File(filename);
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				
				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.close();
				
//				System.out.println("Done writing serialized output to file..");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@SuppressWarnings("unused")
	private static void printFireStats(int numFires, GameAnalysis read) {
		int maxStages = read.maxTimeStep();
		int fireIntensity[][] = new int[numFires][maxStages+1];
		int totalFireIntensity[] = new int[maxStages+1];
		for(int i=0; i<=maxStages; i++){
			int sum=0;
			for (int f=0; f<numFires; f++){
				fireIntensity[f][i] = Integer.parseInt(read.getState(i).getAllObjects().get(f).getStringValForAttribute("intensity"));
				sum += fireIntensity[f][i];
			}
			totalFireIntensity[i] = sum;
		}
		
		for (int f=0; f<numFires; f++){
			System.out.println("[Fire "+f+"]\t"+Arrays.toString(fireIntensity[f]));
		}
		System.out.println("[Total]\t\t"+Arrays.toString(totalFireIntensity));
		
	}

	static String readFile(String path, Charset encoding) 
			  throws IOException 
			{
			  byte[] encoded = Files.readAllBytes(Paths.get(path));
			  return new String(encoded, encoding);
			}

}
