package simulators;

import iPOMDPLiteSolver.iPBVI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import common.POWorld;
import common.PolicyFromFile;
import common.StateEnumerator;
import posg.ObservationFunction;
import posg.POSGDomain;
import burlap.behavior.policy.Policy;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.agents.RandomSGAgent;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.MutableState;
import burlap.oomdp.core.states.State;
import burlap.oomdp.statehashing.HashableState;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.World;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;
import domains.wildfire.Wildfire;
import domains.wildfire.WildfireDomain;
import domains.wildfire.WildfireParameters;
import domains.wildfire.Wildfire.WildfireJointRewardFunction;

public class RandomSimulator {
	protected static List<SGAgentAction> alphaVectorActions = new ArrayList<SGAgentAction>();
	protected static List<double[]> alphaVectors = new ArrayList<double[]>();
	protected static Map<Integer, String> agIDtoName = new HashMap<Integer,String>();

	public static void main(String[] args) {
		
		
		
		
		
		
		int maxIterations = 2;
		int numHorizons = 4;
		int otherAgentIndex = 1;
		int subjectAgentIndex = 0;
		int maxLevel = 0;
		boolean finite = true;
		
		Wildfire wf = new Wildfire();
		WildfireDomain d = (WildfireDomain) wf.generateDomain();
		
		JointReward jr = new WildfireJointRewardFunction();
		TerminalFunction tf = new NullTermination();
		ObservationFunction of = ((WildfireDomain) d).getObservationFunction();
//		State ms = Wildfire.getInitialMasterState(d);
		int numPlayers = WildfireParameters.numAgents;
		
		//1. Get initial state
		State ms = Wildfire.getInitialMasterState((WildfireDomain) d);
		System.out.println("MS---------"+ms.getCompleteStateDescription());
		
		State si = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, 0, ms);
		System.out.println("SI---------"+si.getCompleteStateDescription());
		
		State sj = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, 1, ms);
		System.out.println("SJ---------"+sj.getCompleteStateDescription());
		
		
			
		
//		System.out.println("Getting agent definitions..");
//		Map<String, SGAgentType> agentDefinitions = Wildfire.getAgentDefs((WildfireDomain) d);
//		
//		World w = new POWorld(d, jr, tf, of, agentDefinitions, ms);
//		for( int i = 0;i < numPlayers; i++){
//			SGAgent sga = new RandomSGAgent();
//			SGAgentType at = Wildfire.getAgentType(d, i);
//			sga.joinWorld(w, at);
//		}
//		
//		int numFires = WildfireParameters.height * WildfireParameters.width - WildfireParameters.numAgents;
//		
//		GameAnalysis ga = w.runGame(maxIterations);
//
//		String serialized = ga.serialize();
//		System.out.println(serialized);
//		GameAnalysis read = GameAnalysis.parseGame(d, serialized);
//		System.out.println(read.maxTimeStep()+"\n------------");
//		
//		int fireIntensity[][] = new int[numFires][maxIterations+1];
//		int totalFireIntensity[] = new int[maxIterations+1];
//		for(int i=0; i<=maxIterations; i++){
//			int sum=0;
//			for (int f=0; f<numFires; f++){
//				fireIntensity[f][i] = Integer.parseInt(read.getState(i).getAllObjects().get(f).getStringValForAttribute("intensity"));
//				sum += fireIntensity[f][i];
//			}
//			totalFireIntensity[i] = sum;
//		}
//		
//		for (int f=0; f<numFires; f++){
//			System.out.println("["+f+"]\t"+Arrays.toString(fireIntensity[f]));
//		}
//		System.out.println("[Total]\t"+Arrays.toString(totalFireIntensity));
//		
		
		
		
		
//		Wildfire wf = new Wildfire();
//		WildfireDomain d = (WildfireDomain) wf.generateDomain();
//		
//		JointReward jr = new WildfireJointRewardFunction();
//		TerminalFunction tf = new NullTermination();
//		ObservationFunction of = ((WildfireDomain) d).getObservationFunction();
//		State ms = Wildfire.getInitialMasterState(d);
//		int numPlayers = WildfireParameters.numAgents;
//		System.out.println(ms.getCompleteStateDescription());
////		alphaVectorActions = iPBVI.readActions(d,"output/alpha_actions.txt");
////		alphaVectors = iPBVI.readAlphas("output/alpha_vectors_agent0.txt");
//		
//		System.out.println("Getting agent definitions..");
//		Map<String, SGAgentType> agentDefinitions = Wildfire.getAgentDefs((WildfireDomain) d);
//		
//		
//		
//		//2. Get agent id-name mapping
//		List<StateEnumerator> senums = new ArrayList<StateEnumerator>();
//		
//		System.out.println("Getting agent id-name mappings..");
//		for(int n=0;n<numPlayers;n++){
//			agIDtoName.put(n, Wildfire.agentName(n));
//			
//			System.out.println(Wildfire.getInitialState(d, n).getCompleteStateDescription());
//			
////			StateEnumerator senum = ((POSGDomain) d).getStateEnumerator();
////			senums.add(senum);
//			
//		}
//		
//		
//		//4. Caching all enumerated states..
//		SimpleHashableStateFactory hf = new SimpleHashableStateFactory();
//		System.out.print("Caching all enumerated states..");
//		State s = null;
//		for(int n=0;n<numPlayers;n++){
//			s=Wildfire.getInitialState(d, n);
//			//finding all reachable states from a dummy state
//			if(finite){
//				senums.get(n).findReachableStatesAndEnumerate(s,maxIterations); //remove horizons later
//			}else{
//				senums.get(n).findReachableStatesAndEnumerate(s);
//			}
//			System.out.println("[Done] in "+senums.get(n).numStatesEnumerated()+" unique states found.");
//		}
//		
//		
//		
//
//	
//		int maxMDPLevel = 0;
//		String aname = "x1y0";
//		String actingAgent = agIDtoName.get(otherAgentIndex);
//		String filename = "output/Latest/2-WF_nmdp_policy_level"+maxMDPLevel+"_agent"+otherAgentIndex+".txt";
//		Map<String,Integer> actionNametoID = iPBVI.readMDPActionNameToID(filename);
//		Policy agFinalPolicy = new PolicyFromFile(d, actingAgent, senums.get(otherAgentIndex), filename);
//		Map<Integer, Policy> otherAgentsFinalPolicy = new HashMap<Integer, Policy>();
//		otherAgentsFinalPolicy.put(otherAgentIndex,agFinalPolicy);
//		
//		
//		State subject = Wildfire.getInitialState((WildfireDomain) d,subjectAgentIndex);
//		int ssID = senums.get(subjectAgentIndex).getEnumeratedID(subject);
//		State other = senums.get(otherAgentIndex).getStateForEnumerationId(ssID);
//		
//		System.out.println(otherAgentsFinalPolicy.size());
//		System.out.println(otherAgentsFinalPolicy.get(otherAgentIndex).getAction(other));
//		
//		
//		
//		
////		System.out.println(agFinalPolicy.getAction(s));
////		System.out.println(agFinalPolicy.getActionDistributionForState(s).get(actionNametoID.get(aname)).pSelection);
////		Map<State,GroundedSGAgentAction> sGA = iPBVI.readBestMDPActions(d,agIDtoName.get(currentAgentIndex),senum,filename);
////		for(int s1=0;s1<20;s1++){
////			System.out.println(s1+" "+sGA.get(senum.getStateForEnumerationId(s1)));
////		}
////		System.out.println(agFinalPolicy.getProbOfAction(s, sGA.get(s)));
////		System.out.println("[Done] "+senum.numStatesEnumerated()+" unique states found.");
////		
////		String filename = "output/TEST_2-WF_nmdp_policy_level"+maxLevel+"_agent"+currentAgentIndex+"_iter"+maxIterations+".txt";
////		
////		
//		
////		
////		Map<String,Integer> actionNametoID = iPBVI.readMDPActionNameToID(filename);
////		
////		Map<State,double[]> sD = iPBVI.readMDPActionDistribution(d,agIDtoName.get(currentAgentIndex),senum,filename);
//////		for(int s1=0;s1<senum.numStatesEnumerated();s1++){
//////			System.out.println(s1+" "+Arrays.toString(sD.get(senum.getStateForEnumerationId(s1))));
//////		}
////		
////		String aname = "x1y0";
////		System.out.println(sD.get(senum.getStateForEnumerationId(111))[actionNametoID.get(aname)]);
////		
////		Map<State,List<ActionProb>> MDPActionDistributionForState = iPBVI.readMDPStateToActionDistribution(d,agIDtoName.get(currentAgentIndex),senum,filename);
////		System.out.println(MDPActionDistributionForState.get(senum.getStateForEnumerationId(111)).get(actionNametoID.get(aname)).pSelection );
		

		
		
	}

	

}
