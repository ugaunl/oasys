package simulators;

import iPOMDPLiteSolver.iPBVI;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import common.POWorld;
import common.StateEnumerator;
import common.pomdp.BeliefMAMDPPolicyAgent;
import posg.ObservationFunction;
import posg.POSGDomain;
import posg.TabularBeliefState;
import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.debugtools.MyTimer;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.statehashing.HashableStateFactory;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;
import domains.wildfire.Wildfire;
import domains.wildfire.WildfireDomain;
import domains.wildfire.WildfireParameters;
import domains.wildfire.Wildfire.WildfireJointRewardFunction;

public class PBVISimulator {

	public static void main(String[] args) {
		
		//example command line argument
		/*
		 * order of command line arguments: maxStages maxtrials conf experiment tempVar
		 * 
		 * [mkran@erebus oasys-git]$  java -cp "bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar" simulators/PBVIPosthocSimulator 100 30 1 5 15 > output/Simulations/Config_1_Results/Experiment_5/Simulation_2-WF_config1_experiment5_100steps_30trials.txt
		 * 
		 */
		
		//TODO Set max trials and max stages here
		
		//estimateTF = false for posthoc, estimateTF = true for predictive;
		
		int maxLevel = 1;
		
		int maxStages = 0;
		int maxtrials = 0;
		int conf = 0;
		int experiment = 0;
		int tempVar = 0;
		boolean estimateTF = false;
		
		if(args.length == 6){
			maxStages = Integer.parseInt(args[0]);
			maxtrials = Integer.parseInt(args[1]);
			conf = Integer.parseInt(args[2]);
			experiment = Integer.parseInt(args[3]);
			tempVar = Integer.parseInt(args[4]);
			estimateTF = Boolean.parseBoolean(args[5]);
		}
		
		MyTimer timer = new MyTimer();
		timer.start();
		Wildfire wf = new Wildfire();
		WildfireDomain d = (WildfireDomain) wf.generateDomain(conf,estimateTF);
		
		String outFileHead = "output/Simulations/Config_"+WildfireParameters.config+"_Results/Experiment_"+experiment+"/";
		String resultHead = outFileHead + "Dumps/";
		String tempHead = resultHead + "Temp/";
		
		if(estimateTF==true){
			resultHead = resultHead + "PREDICTIVE_";
			tempHead = tempHead + "PREDICTIVE_";
		}
		
		String suppressantResultsFile = "Suppressant_Dump"+"_maxStages"+maxStages+"_maxTrials"+maxtrials+".csv";
		String rewardResultsFile = "Reward_Dump"+"_maxStages"+maxStages+"_maxTrials"+maxtrials+".csv";
		String actionResultsFile = "JointAction_Dump"+"_maxStages"+maxStages+"_maxTrials"+maxtrials+".csv";
		String fireResultsFile = "Fire_Dump"+"_maxStages"+maxStages+"_maxTrials"+maxtrials+".csv";
		
		//3. Get initial state
		State ms = Wildfire.getInitialMasterState((WildfireDomain) d);
		
		
		JointReward jr = new WildfireJointRewardFunction();
		TerminalFunction tf = new NullTermination();
		ObservationFunction of = ((WildfireDomain) d).getObservationFunction();
		int numPlayers = WildfireParameters.numAgents;
		
		Map<Integer, String> agIDtoName = new HashMap<Integer, String>();
		List<Map<String, SGAgentType>> allAgentDefinitions = new ArrayList<Map<String, SGAgentType>>();
		StateEnumerator senums[] = new StateEnumerator[numPlayers];
		
		
		//1. Get agent definitions
		allAgentDefinitions = Wildfire.getAllAgentDefs((WildfireDomain) d);
		
		//2. Get agent id-name mapping
		for(int n=0;n<numPlayers;n++){
			agIDtoName.put(n, Wildfire.agentName(n));
		}
		
		Map<String, State> agentStateMap = new HashMap<String,State>();;
		Map<String, TabularBeliefState> srcBSVector = new HashMap<String, TabularBeliefState>();
		
		POWorld pow = new POWorld(d,jr,tf,of,allAgentDefinitions,ms);
	
		for(int i=0;i<numPlayers;i++){
			int agentID = i;
			String agentName = agIDtoName.get(agentID);
			
			Map<String, SGAgentType> agentDefinitions = allAgentDefinitions.get(agentID);
			
			State si = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, agentID, ms);
			agentStateMap.put(agentName, si);
			
			List<JointAction> allJAs = JointAction.getAllJointActions(si, agentDefinitions);
			
			HashableStateFactory hf = new SimpleHashableStateFactory(true,false);
			((WildfireDomain) d).setStateEnumerator(new StateEnumerator(d, hf, agentDefinitions));
			
			senums[i] = new StateEnumerator(d, hf, agentDefinitions);
            senums[i].findReachableStatesAndEnumerate(si);
//            System.out.println(senums[i].numStatesEnumerated());
			
			TabularBeliefState tbs = new TabularBeliefState(d, senums[i], agentName);
			tbs = Wildfire.getInitialBeliefState((WildfireDomain) d, agentID, senums[i]);
			srcBSVector.put(agentName, tbs);
			
			int agentType = ((WildfireDomain) d).agentTypes[agentID]; 

			String filename1 = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_iPBVI_alpha_actions_level"+maxLevel+"_agent"+agentID+"-"+agentType+".txt";
					
			String filename2 = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_iPBVI_alpha_vectors_level"+maxLevel+"_agent"+agentID+"-"+agentType+".txt";
			
			List<SGAgentAction> alphaVectorActions = iPBVI.readActions((POSGDomain) d,filename1);
			
			List<double[]> alphaVectors = iPBVI.readAlphas(filename2);
			
//			for(int i1=0;i1<alphaVectors.size();i1++){
//				System.out.print(" "+ alphaVectors.get(i1).length);
//			}
//			System.out.println();
			
			iPBVI pVI = new iPBVI(d, jr, tf, of, agentDefinitions,agIDtoName,allJAs, 
							alphaVectors, alphaVectorActions, agentID, agentName, senums[i]);
			Policy pPBVI = new GreedyQPolicy(pVI);
			
			BeliefMAMDPPolicyAgent pVIagent = new BeliefMAMDPPolicyAgent((POSGDomain) d, agentName,pPBVI);
			pVIagent.joinWorld(pow, pow.getAllAgentDefinitions().get(agentID).get(agentName));
		}
		
		System.out.println("Running Trials: ");
		int trial = 1;
		
		
		MyTimer trialtimer = new MyTimer();
		String suppressantContent = "";
		String rewardContent = "";
		String actionContent = "";
		String fireContent = "";
		while(trial<=maxtrials){
			trialtimer.start();
			GameAnalysis ga = pow.runGame(maxStages, ms, agentStateMap, srcBSVector);
			
			//Dump suppressant results to file
			suppressantContent += saveSuppressantResultsToFile(ga,trial);
			//Dump rewards results to file
			rewardContent += saveRewardResultsToFile(ga,trial);
			//Dump actions results to file
			actionContent += saveActionResultsToFile(ga,trial);
			//Dump fires results to file
			fireContent += saveFireResultsToFile(ga,trial);
			
			if(trial%tempVar==0) {
				writeOutputToFile(suppressantContent, tempHead+"AT-"+trial+"-"+suppressantResultsFile);
				writeOutputToFile(rewardContent, tempHead+"AT-"+trial+"-"+rewardResultsFile);
				writeOutputToFile(actionContent, tempHead+"AT-"+trial+"-"+actionResultsFile);
				writeOutputToFile(fireContent, tempHead+"AT-"+trial+"-"+fireResultsFile);
				
				System.out.println();
			}
			System.out.print(trial+" ");
			
			trial++;
			trialtimer.stop();
		}
	
		writeOutputToFile(suppressantContent, resultHead+suppressantResultsFile);
		writeOutputToFile(rewardContent, resultHead+rewardResultsFile);
		writeOutputToFile(actionContent, resultHead+actionResultsFile);
		writeOutputToFile(fireContent, resultHead+fireResultsFile);
		System.out.println("\nFinished running "+maxtrials+" trials..All results saved to file!");
		timer.stop();
		System.out.println("---------------------------------------------------");
		System.out.println("[TIMING RESULTS] Performed total "+maxtrials+" trials..in "+trialtimer.getTotalTime()+
				"s \n[TIMING RESULTS] Avg time/trial = "+trialtimer.getAvgTime());
		System.out.println("---------------------------------------------------");
		
	}
	
	static String saveSuppressantResultsToFile(GameAnalysis read, int trial) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		int suppressantLevel[][] = new int[numAgents][maxStages+1];
		int totalSuppressantLevel[] = new int[maxStages+1];
		
		String suppressantHead = "\nTrial "+trial+"\n";
		String suppressantStr = "";
		String totals = "Total,";
		
		int sumSuppressant = 0;
		for(int i=0; i<=maxStages; i++){
			sumSuppressant=0;
			for (int agID=0; agID<numAgents; agID++){
				//suppressants
				suppressantLevel[agID][i] = read.getState(i).getObjectsOfClass("agent").get(agID).getIntValForAttribute("available");
				sumSuppressant += suppressantLevel[agID][i];
			}
			totalSuppressantLevel[i] = sumSuppressant;
			
		}
		
		for(int agID=0; agID<numAgents; agID++){
			suppressantStr += agID+",";
			for (int i=0; i<=maxStages; i++){
				suppressantStr += Integer.toString(suppressantLevel[agID][i])+",";
			}
			suppressantStr += "\n";
		}
		
		suppressantStr += totals;
		for (int i=0; i<=maxStages; i++){
			suppressantStr += Integer.toString(totalSuppressantLevel[i])+",";
		}
		
		String content = suppressantHead + suppressantStr + "\n" ;
		
		return content;
	}
	
	static String saveRewardResultsToFile(GameAnalysis read, int trial) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		List<Map<String, Double>> rMapList = read.jointRewards;
		double rewards[][] = new double[numAgents][maxStages+1];
		double totalRewards[] = new double[maxStages+1];
		
		String rewardHead = "\nTrial "+trial+"\n";
		String rewardStr = "";
		String totals = "Total,";
		
		double sumReward = 0;
		int ag=0;
		for(int i=0; i<=maxStages; i++){
			//Rewards
			sumReward=0;
			ag=0;
			if(i!=maxStages){
				for(Entry<String, Double> rEntry: rMapList.get(i).entrySet()){
					rewards[ag][i] = rEntry.getValue();
					sumReward+=rewards[ag][i];
					ag++;
				}
				totalRewards[i] = sumReward;
			}
		}
		
		for(int agID=0; agID<numAgents; agID++){
			rewardStr += agID+",";
			for (int i=0; i<=maxStages; i++){
				if(i!=maxStages){rewardStr += Double.toString(rewards[agID][i])+",";}
			}
			rewardStr += "\n";
		}
		
		rewardStr += totals;
		for (int i=0; i<=maxStages; i++){
			if(i!=maxStages){rewardStr += Double.toString(totalRewards[i])+",";}
		}
		
		String content = rewardHead + rewardStr + "\n" ;
		
		return content;
		//writeOutputToFile(content, resultsFile);
		
		
	}
	
	static String saveActionResultsToFile(GameAnalysis read, int trial) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		List<JointAction> allJAsList = read.jointActions;
		String actions[][] = new String[numAgents][maxStages];
		
		String actionHead = "\nTrial "+trial+"\n";
		String actionStr = "";
		
		for(int i=0; i<=maxStages; i++){
			for (int agID=0; agID<numAgents; agID++){
				//actions
				String agName = "agent"+agID;
				if(i!=maxStages){
					actions[agID][i] = allJAsList.get(i).action(agName).actionName();
				}
			}
		}
		
		for(int agID=0; agID<numAgents; agID++){
			actionStr += agID+",";
			for (int i=0; i<=maxStages; i++){
				if(i!=maxStages){actionStr += actions[agID][i]+",";} 
			}
			actionStr += "\n";
		}
		
		String content = actionHead + actionStr + "\n";
		
		return content;
//		writeOutputToFile(content, resultsFile);
		
		
	}
	
	static String saveFireResultsToFile(GameAnalysis read, int trial) {
		int maxStages = read.maxTimeStep();
		int numFires = read.getState(0).getObjectsOfClass(Wildfire.CLASS_FIRE).size();
		
		int fireIntensity[][] = new int[numFires][maxStages+1];
		int totalFireIntensity[] = new int[maxStages+1];
		
		String fireHead = "\nTrial "+trial+"\n";
		String fireStr = "";
		String totals = "Total,";
		int sumFire = 0;
		
		
		for(int i=0; i<=maxStages; i++){
			//Fires
			sumFire=0;
			for (int f=0; f<numFires; f++){
				fireIntensity[f][i] = Integer.parseInt(read.getState(i).getObjectsOfClass(Wildfire.CLASS_FIRE).get(f).getStringValForAttribute(Wildfire.ATT_INTENSITY));
				sumFire += fireIntensity[f][i];
			}
			totalFireIntensity[i] = sumFire;
		}
		
		for(int f=0; f<numFires; f++){
			fireStr += f+",";
			for (int i=0; i<=maxStages; i++){
				fireStr += Integer.toString(fireIntensity[f][i])+",";
			}
			fireStr += "\n";
		}
		
		fireStr += totals;
		for (int i=0; i<=maxStages; i++){
			fireStr += Integer.toString(totalFireIntensity[i])+",";
		}
		
		String content = fireHead + fireStr + "\n" ;
		
		
		return content;
		
		
	}
	
	
	
	static void saveAllResultsToFile(GameAnalysis read,	String resultsFile) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		int numFires = read.getState(0).getObjectsOfClass("fire").size();
		
		List<JointAction> allJAsList = read.jointActions;
		String actions[][] = new String[numAgents][maxStages];
		double rewards[][] = new double[numAgents][maxStages+1];
		List<Map<String, Double>> rMapList = read.jointRewards;
		
		double totalRewards[] = new double[maxStages+1];
		int suppressantLevel[][] = new int[numAgents][maxStages+1];
		int totalSuppressantLevel[] = new int[maxStages+1];
		int fireIntensity[][] = new int[numFires][maxStages+1];
		int totalFireIntensity[] = new int[maxStages+1];
		
		String fireHead = "\nFire Intensities\n";
		String fireStr = "";
		String rewardHead = "\nJoint Rewards\n";
		String rewardStr = "";
		String actionHead = "\nJoint Actions\n";
		String actionStr = "";
		String suppressantHead = "\nSuppressant Level\n";
		String suppressantStr = "";
		String totals = "Total,";
		
		double sumReward = 0;
		int sumSuppressant = 0;
		int sumFire = 0;
		int ag=0;
		
		
		for(int i=0; i<=maxStages; i++){
			sumSuppressant=0;
			for (int agID=0; agID<numAgents; agID++){
				//actions
				String agName = "agent"+agID;
				if(i!=maxStages){
					actions[agID][i] = allJAsList.get(i).action(agName).actionName();
				}
				
				//suppressants
				suppressantLevel[agID][i] = read.getState(i).getObjectsOfClass("agent").get(agID).getIntValForAttribute("available");
				sumSuppressant += suppressantLevel[agID][i];
			}
			totalSuppressantLevel[i] = sumSuppressant;
			
			//Rewards
			sumReward=0;
			ag=0;
			if(i!=maxStages){
				for(Entry<String, Double> rEntry: rMapList.get(i).entrySet()){
					rewards[ag][i] = rEntry.getValue();
					sumReward+=rewards[ag][i];
					ag++;
				}
				totalRewards[i] = sumReward;
			}
			//Fires
			sumFire=0;
			for (int f=0; f<numFires; f++){
				fireIntensity[f][i] = Integer.parseInt(read.getState(i).getObjectsOfClass(Wildfire.CLASS_FIRE).get(f).getStringValForAttribute(Wildfire.ATT_INTENSITY));
				sumFire += fireIntensity[f][i];
			}
			totalFireIntensity[i] = sumFire;
		}
		
		for(int f=0; f<numFires; f++){
			fireStr += f+",";
			for (int i=0; i<=maxStages; i++){
				fireStr += Integer.toString(fireIntensity[f][i])+",";
			}
			fireStr += "\n";
		}
		
		for(int agID=0; agID<numAgents; agID++){
			rewardStr += agID+",";
			actionStr += agID+",";
			suppressantStr += agID+",";
			for (int i=0; i<=maxStages; i++){
				if(i!=maxStages){rewardStr += Double.toString(rewards[agID][i])+",";}
				if(i!=maxStages){actionStr += actions[agID][i]+",";} 
				suppressantStr += Integer.toString(suppressantLevel[agID][i])+",";
			}
			rewardStr += "\n";
			actionStr += "\n";
			suppressantStr += "\n";
		}
		
		fireStr += totals;
		rewardStr += totals;
		suppressantStr += totals;
		for (int i=0; i<=maxStages; i++){
			fireStr += Integer.toString(totalFireIntensity[i])+",";
			if(i!=maxStages){rewardStr += Double.toString(totalRewards[i])+",";}
			suppressantStr += Integer.toString(totalSuppressantLevel[i])+",";
		}
		
		String content = fireHead + fireStr + "\n" +
				rewardHead + rewardStr + "\n" +
				suppressantHead + suppressantStr + "\n" +
				actionHead + actionStr + "\n";
		
		
		writeOutputToFile(content, resultsFile);
		
		
	}

	@SuppressWarnings("unused")
	private static void printAgentSuppressantStats(GameAnalysis read) {
		
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		int suppressantLevel[][] = new int[numAgents][maxStages+1];
		int totalSuppressantLevel[] = new int[maxStages+1];
		for(int i=0; i<=maxStages; i++){
			int sum=0;
			for (int agID=0; agID<numAgents; agID++){
				suppressantLevel[agID][i] = read.getState(i).getObjectsOfClass("agent").get(agID).getIntValForAttribute("available");
				sum += suppressantLevel[agID][i];
			}
			totalSuppressantLevel[i] = sum;
		}
		
		for (int agID=0; agID<numAgents; agID++){
			System.out.println("[Agent "+agID+"]\t"+Arrays.toString(suppressantLevel[agID]));
		}
		System.out.println("[Total]\t\t"+Arrays.toString(totalSuppressantLevel));
		
	}

	@SuppressWarnings("unused")
	private static void printAgentRewardStats(GameAnalysis read) {
		List<Map<String, Double>> rMapList = read.jointRewards;
		
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		double rewards[][] = new double[numAgents][maxStages+1];
		double totalRewards[] = new double[maxStages+1];
		
		for(int i=0;i<rMapList.size();i++){
			int agID=0;
			double sum=0;
			for(Entry<String, Double> rEntry: rMapList.get(i).entrySet()){
				rewards[agID][i] = rEntry.getValue();
				sum+=rewards[agID][i];
				agID++;
			}
			totalRewards[i] = sum;
		}
			
		for (int agID=0; agID<numAgents; agID++){
			System.out.println("[Agent "+agID+"]\t"+Arrays.toString(rewards[agID]));
		}
		System.out.println("[Total]\t\t"+Arrays.toString(totalRewards));
	}
	
	@SuppressWarnings("unused")
	private static double[] getTotalRewardForTrial(GameAnalysis read){
		List<Map<String, Double>> rMapList = read.jointRewards;
		
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		double rewards[][] = new double[numAgents][maxStages+1];
		double totalRewards[] = new double[maxStages+1];
		
		for(int i=0;i<rMapList.size();i++){
			int agID=0;
			double sum=0;
			for(Entry<String, Double> rEntry: rMapList.get(i).entrySet()){
				rewards[agID][i] = rEntry.getValue();
				sum+=rewards[agID][i];
				agID++;
			}
			totalRewards[i] = sum;
		}
		return totalRewards;
	}

	@SuppressWarnings("unused")
	private static void printAgentActionStats(GameAnalysis read) {
		int maxStages = read.maxTimeStep();
		int numAgents = read.getState(0).getObjectsOfClass("agent").size();
		
		List<JointAction> allJAsList = read.jointActions;
		
		String actions[][] = new String[numAgents][maxStages];
		
		for(int i=0;i<allJAsList.size();i++){
			for (int agID=0; agID<numAgents; agID++){
				String agName = "agent"+agID;
				actions[agID][i] = allJAsList.get(i).action(agName).actionName();
			}
		}
		for (int agID=0; agID<numAgents; agID++){
			System.out.println("[Agent "+agID+"]\t"+Arrays.toString(actions[agID]));
		}
		
	}

	public static void writeOutputToFile(String content, String filename){
		boolean writeToFile = true;
		if(writeToFile){
			try {
				File file = new File(filename);
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				
				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.close();
				
//				System.out.println("Done writing serialized output to file..");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@SuppressWarnings("unused")
	private static void printFireStats(int numFires, GameAnalysis read) {
		int maxStages = read.maxTimeStep();
		int fireIntensity[][] = new int[numFires][maxStages+1];
		int totalFireIntensity[] = new int[maxStages+1];
		for(int i=0; i<=maxStages; i++){
			int sum=0;
			for (int f=0; f<numFires; f++){
				fireIntensity[f][i] = Integer.parseInt(read.getState(i).getObjectsOfClass(Wildfire.CLASS_FIRE).get(f).getStringValForAttribute(Wildfire.ATT_INTENSITY));
				sum += fireIntensity[f][i];
			}
			totalFireIntensity[i] = sum;
		}
		
		for (int f=0; f<numFires; f++){
			System.out.println("[Fire "+f+"]\t"+Arrays.toString(fireIntensity[f]));
		}
		System.out.println("[Total]\t\t"+Arrays.toString(totalFireIntensity));
		
	}

	static String readFile(String path, Charset encoding) 
			  throws IOException 
			{
			  byte[] encoded = Files.readAllBytes(Paths.get(path));
			  return new String(encoded, encoding);
			}

}
