package simulators;

/**
 * The {@link NoopBaselineForWildfire} class simulates the 
 * spread of fires if no agents were trying to fight it.
 * 
 * @author Muthu Chandrasekaran
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.agents.RandomSGAgent;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointActionModel;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.World;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SimpleSGAgentAction;
import common.POWorld;
import domains.wildfire.Wildfire;
import domains.wildfire.WildfireDomain;
import domains.wildfire.WildfireParameters;
import domains.wildfire.Wildfire.WildfireJointRewardFunction;
import posg.ObservationFunction;

public class HeuristicBaselineForWildfire {

	public static void main(String[] args) {
		
		//TODO Set max trials and max stages here
		int maxStages = 0;
		int maxtrials = 0;
		int conf = 0;
		int experiment = 0;
		//baseline=1 for heuristic, 0 for random
		int baseline = 1;
		boolean estimateTF = false;
		
		if(args.length == 5){
			maxStages = Integer.parseInt(args[0]);
			maxtrials = Integer.parseInt(args[1]);
			conf = Integer.parseInt(args[2]);
			experiment = Integer.parseInt(args[3]);
			estimateTF = Boolean.parseBoolean(args[4]);
		}
		
		Wildfire wf = new Wildfire();
		WildfireDomain d = (WildfireDomain) wf.generateDomain(conf,estimateTF);
		
		String outFileHead = "output/Simulations/Config_"+WildfireParameters.config+"_Results/Experiment_"+experiment+"/";
		String resultHead = outFileHead + "Dumps";
		String randHead = resultHead + "/HEURISTIC_BASELINE_";
		
		String suppressantResultsFile = "Suppressant_Dump"+"_maxStages"+maxStages+"_maxTrials"+maxtrials+".csv";
		String rewardResultsFile = "Reward_Dump"+"_maxStages"+maxStages+"_maxTrials"+maxtrials+".csv";
		String actionResultsFile = "JointAction_Dump"+"_maxStages"+maxStages+"_maxTrials"+maxtrials+".csv";
		String fireResultsFile = "Fire_Dump"+"_maxStages"+maxStages+"_maxTrials"+maxtrials+".csv";
		
		JointReward jr = new WildfireJointRewardFunction();
		TerminalFunction tf = new NullTermination();
		ObservationFunction of = ((WildfireDomain) d).getObservationFunction();
		
		State ms = Wildfire.getInitialMasterState(d);
		
		//1. Get agent definitions
//		System.out.println("Getting all agent definitions..");
		List<Map<String, SGAgentType>> allAgentDefinitions = Wildfire.getAllAgentDefs((WildfireDomain) d);
		
		
		POWorld w = new POWorld(d, jr, tf, of, allAgentDefinitions, ms);
		for( int i = 0;i < WildfireParameters.numAgents; i++){
			SGAgent a = new RandomSGAgent();
			SGAgentType at = Wildfire.getAgentType(d, i);
			a.joinWorld(w, at);
		}
		
			
//		System.out.println("Running Trials: ");
		int trial = 1;
		
		String suppressantContent = "";
		String rewardContent = "";
		String actionContent = "";
		String fireContent = "";
		while(trial<=maxtrials){
			GameAnalysis ga = w.runGame(maxStages,ms,baseline);
			
			//Dump suppressant results to file
			suppressantContent += PBVISimulator.saveSuppressantResultsToFile(ga,trial);
			//Dump rewards results to file
			rewardContent += PBVISimulator.saveRewardResultsToFile(ga,trial);
			//Dump actions results to file
			actionContent += PBVISimulator.saveActionResultsToFile(ga,trial);
			//Dump fires results to file
			fireContent += PBVISimulator.saveFireResultsToFile(ga,trial);
			
			trial++;
		}
		
		PBVISimulator.writeOutputToFile(suppressantContent, randHead+suppressantResultsFile);
		PBVISimulator.writeOutputToFile(rewardContent, randHead+rewardResultsFile);
		PBVISimulator.writeOutputToFile(actionContent, randHead+actionResultsFile);
		PBVISimulator.writeOutputToFile(fireContent, randHead+fireResultsFile);
		System.out.println("\nFinished running "+maxtrials+" HEURISTIC trials..All results saved to file!");
		
		
		
	}

}
