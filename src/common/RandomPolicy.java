package common;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import burlap.behavior.policy.Policy;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.behavior.policy.Policy.PolicyUndefinedException;
import burlap.debugtools.RandomFactory;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;

public class RandomPolicy extends Policy {
	protected SGDomain d;
	protected List<GroundedSGAgentAction> gaList;
	protected Random rand = RandomFactory.getMapped(0);
	
	public RandomPolicy(SGDomain d, List<GroundedSGAgentAction> allAgActions) {
		this.d = d;
		this.gaList = allAgActions;
	}

	@Override
	public AbstractGroundedAction getAction(State s) {
		System.out.println("Random Policy for sampling action is not to be used..");
		return null;
		
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {
		if(this.gaList.size() == 0){
			throw new PolicyUndefinedException();
		}
		double p = 1./gaList.size();
		List<ActionProb> aps = new ArrayList<ActionProb>(gaList.size());
		for(GroundedSGAgentAction ga : gaList){
			ActionProb ap = new ActionProb(ga, p);
			aps.add(ap);
		}
		return aps;
	}

	@Override
	public boolean isStochastic() {
		return true;
	}

	@Override
	public boolean isDefinedFor(State s) {
		return this.gaList.size() > 0;
	}

}
