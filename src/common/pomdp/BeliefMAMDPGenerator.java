package common.pomdp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import posg.POSGDomain;
import posg.TabularBeliefState;
import common.StateEnumerator;
import burlap.behavior.policy.Policy;
import burlap.oomdp.auxiliary.DomainGenerator;
import burlap.oomdp.core.Attribute;
import burlap.oomdp.core.Attribute.AttributeType;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.ObjectClass;
import burlap.oomdp.core.TransitionProbability;
import burlap.oomdp.core.objects.MutableObjectInstance;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.MutableState;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointActionModel;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;



public class BeliefMAMDPGenerator implements DomainGenerator {

	public static final String					CLASSBELIEF = "belief";
	public static final String					ATTBELIEF = "belief";
	
	
	protected POSGDomain							posgdomain;
	protected String 								actingAgent;
	protected Map<Integer, Policy> 					otherAgentsPolicyMap;
//	protected List<JointAction>						allJAs;
//	protected JointActionModel 						jaModel;
	
	
	public BeliefMAMDPGenerator(POSGDomain posgdomain, String actingAgent, Map<Integer, Policy>	otherAgentsPolicyMap){
		this.posgdomain = posgdomain;
		this.actingAgent = actingAgent;
		this.otherAgentsPolicyMap = otherAgentsPolicyMap;
//		this.jaModel = posgdomain.getJointActionModel();
//		this.allJAs = allJAs;
	}
	
	public BeliefMAMDPGenerator(POSGDomain posgdomain, String actingAgent){
		this.posgdomain = posgdomain;
		this.actingAgent = actingAgent;
		
	}
	
	
	@Override
	public Domain generateDomain() {
		
		SGDomain domain = new SGDomain();
		
		//int nStates = podomain.getStateEnumerator().numStatesEnumerated();
		
		Attribute batt = new Attribute(domain, ATTBELIEF, AttributeType.DOUBLEARRAY);
		batt.setLims(0., 1.);
		
		ObjectClass beliefClass = new ObjectClass(domain, CLASSBELIEF);
		beliefClass.addAttribute(batt);
		
		for(SGAgentAction mdpAction : this.posgdomain.getActionsPerAgent().get(actingAgent)){
			new BeliefAction(mdpAction, domain);
		}
		
		domain.setJointActionModel(new BeliefJAModel(posgdomain, actingAgent));
		
		return domain;
	}
	
	public static State getBeliefMDPState(SGDomain beliefDomain, BeliefState bs){
		return getBeliefMDPState(beliefDomain, ((TabularBeliefState) bs).getBeliefVector());
	}
	
	public static State getBeliefMDPState(SGDomain beliefDomain, double [] beliefStateVector){
		State s = new MutableState();
		ObjectInstance bOb = new MutableObjectInstance(beliefDomain.getObjectClass(CLASSBELIEF), CLASSBELIEF+"0");
		bOb.setValue(ATTBELIEF, beliefStateVector);
		s.addObject(bOb);
		return s;
	}
	
	public class BeliefAction extends SGAgentAction{
		
		protected SGAgentAction mdpAction;
		
		public BeliefAction(SGAgentAction mdpAction, SGDomain domain){
			super(domain,mdpAction.actionName);
			this.mdpAction = mdpAction;
		}
		
		@Override
		public boolean applicableInState(State s, GroundedSGAgentAction gsa) {
			return true;
		}

		@Override
		public boolean isParameterized() {
			return false;
		}

		@Override
		public GroundedSGAgentAction getAssociatedGroundedAction(
				String actingAgent) {
			return mdpAction.getAssociatedGroundedAction(actingAgent);
		}

		@Override
		public List<GroundedSGAgentAction> getAllApplicableGroundedActions(
				State s, String actingAgent) {
			List<GroundedSGAgentAction> mdpGAs = this.mdpAction.getAllApplicableGroundedActions(s, actingAgent) ;
			List<GroundedSGAgentAction> beliefGAs = new ArrayList<GroundedSGAgentAction>(mdpGAs.size());
			for(GroundedSGAgentAction mga : mdpGAs){
				beliefGAs.add(mga);
			}
			return beliefGAs;
		}
		
		
	}
	
	public class BeliefJAModel extends JointActionModel{
		
		protected POSGDomain posgdomain;
		protected String actingAgent;

		public BeliefJAModel(POSGDomain posgdomain, String actingAgent){
			this.posgdomain = posgdomain;
			this.actingAgent = actingAgent;
		}
		
		@Override
		protected State actionHelper(State s, JointAction ja) {
			
			TabularBeliefState bs = new TabularBeliefState(this.posgdomain, this.actingAgent);
			ObjectInstance bObject = s.getFirstObjectOfClass(CLASSBELIEF);
			double [] bVector = bObject.getDoubleArrayValForAttribute(ATTBELIEF);
			bs.setBeliefVector(bVector);
			
//			GroundedSGAgentAction mdpGA = new SimpleGroundedSGAgentAction(BeliefMAMDPGenerator.this.actingAgent,this.mdpAction);
			
			//sample a current state
			State mdpS = bs.sampleStateFromBelief();
			//sample a next state
//			JointAction ja = allJAs.get(getRandomJAction(allJAs.size()));
			State mdpSP = this.performJointAction(mdpS, ja);
			//sample an observation
			State sampledObs = this.posgdomain.getObservationFunction().sampleObservation(mdpS, mdpSP, ja);
			//get next belief state
			TabularBeliefState nbs = (TabularBeliefState) bs.getUpdatedBeliefState(sampledObs, ja);
			//set the returned state object's belief vector to this
			bObject.setValue(ATTBELIEF, nbs.getBeliefVector());
			
			return s;
		}

		@Override
		public List<TransitionProbability> transitionProbsFor(State s, JointAction ja) {
			//belief state
			TabularBeliefState bs = new TabularBeliefState(this.posgdomain, this.actingAgent);
			ObjectInstance bObject = s.getFirstObjectOfClass(CLASSBELIEF);
			double [] bVector = bObject.getDoubleArrayValForAttribute(ATTBELIEF);
			bs.setBeliefVector(bVector);
			
			List<State> observations = this.posgdomain.getObservationFunction().getAllPossibleObservations();
			List<TransitionProbability> tps = new ArrayList<TransitionProbability>(observations.size());
			for(State observation : observations){
				double p = bs.getProbObservation(observation, ja);
				if(p > 0){
					TabularBeliefState nbs = (TabularBeliefState) bs.getUpdatedBeliefState(observation, ja);;
					State ns = new MutableState();
					ObjectInstance nbObject = new MutableObjectInstance(ja.action(this.actingAgent).action.domain.getObjectClass(CLASSBELIEF), CLASSBELIEF+"0");
					nbObject.setValue(ATTBELIEF, nbs.getBeliefVector());
					ns.addObject(nbObject);
					
					TransitionProbability tp = new TransitionProbability(ns, p);
					tps.add(tp);
				}
			}
			
			List<TransitionProbability> collapsed = this.collapseTransitionProbabilityDuplicates(tps);
			
			return collapsed;
		}

		
		protected List<TransitionProbability> collapseTransitionProbabilityDuplicates(List<TransitionProbability> tps){
			List<TransitionProbability> collapsed = new ArrayList<TransitionProbability>(tps.size());
			for(TransitionProbability tp : tps){
				TransitionProbability stored = this.matchingStateTP(collapsed, tp.s);
				if(stored == null){
					collapsed.add(tp);
				}
				else{
					stored.p += tp.p;
				}
			}
			return collapsed;
		}
		
		protected TransitionProbability matchingStateTP(List<TransitionProbability> tps, State s){
			
			for(TransitionProbability tp : tps){
				if(tp.s.equals(s)){
					return tp;
				}
			}
			
			return null;
			
		}

		
		
	}
	
	
	
	public static class BeliefRF implements JointReward{

		protected POSGDomain			posgdomain;
		protected JointReward			mdpRF;
		protected StateEnumerator		stateEnumerator;
		protected BeliefMAMDPGenerator 	bGen;
		
		
		public BeliefRF(POSGDomain posgdomain, BeliefMAMDPGenerator bGen, JointReward mdpRF){
			this.posgdomain = posgdomain;
			this.bGen = bGen;
			this.mdpRF = mdpRF;
			this.stateEnumerator = this.posgdomain.getStateEnumerator();
		}
		
		
		
		public double reward(State s, JointAction ja, State sp, String actingAgent) {
//			BeliefMAMDPGenerator bGen = new BeliefMAMDPGenerator(posgdomain, actingAgent);
			BeliefJAModel jaModel = bGen.new BeliefJAModel(this.posgdomain,actingAgent);
			
			double [] belief = s.getFirstObjectOfClass(CLASSBELIEF).getDoubleArrayValForAttribute(ATTBELIEF);
			double sum = 0.;
			for(int i = 0; i < belief.length; i++){
				if(belief[i] > 0.){
					State mdpS = this.stateEnumerator.getStateForEnumerationId(i);
					//cumulative product of probabilities over others' actions from their policy
					double cumOthersJAProb = 1.;
					for (int k=0;k<ja.getAgentNames().size();k++) {
						if(bGen.otherAgentsPolicyMap.containsKey(k)){
							cumOthersJAProb *= bGen.otherAgentsPolicyMap.get(k).getProbOfAction(mdpS, ja.action(ja.getAgentNames().get(k)));
						}
					}
					List<TransitionProbability> tps = jaModel.transitionProbsFor(mdpS, ja);
					double sumTransR = 0.;
					for(TransitionProbability tp : tps){
						double r = this.mdpRF.reward(mdpS, ja, tp.s).get(actingAgent);
						double wr = tp.p*r;
						sumTransR += wr;
					}
					System.out.println("I should be here!");
					sum += cumOthersJAProb * belief[i] * sumTransR;
				}
			}
			
			return sum;
		}

		@Override
		public Map<String, Double> reward(State s, JointAction ja, State sp) {
			Map <String, Double> rewards = new HashMap<String, Double>();
			for (String agent : ja.getAgentNames()) {
				rewards.put(agent, this.reward(s, ja, sp, agent));
			}
			return rewards;
		}
		
//		@Override
//		public double reward(State s, GroundedAction a, State sprime) {
//			
//			GroundedAction mdpGA = new GroundedAction(this.podomain.getAction(a.actionName()), a.params);
//			
//			if(this.srcRFIsSAOnly){
//				return this.saOnlyReward(s, mdpGA);
//			}
//			
//			return this.sasReward(s, mdpGA);
//			
//		}
//		
//		protected double saOnlyReward(State s, GroundedAction a){
//			double [] belief = s.getFirstObjectOfClass(CLASSBELIEF).getDoubleArrayValue(ATTBELIEF);
//			
//			double sum = 0.;
//			for(int i = 0; i < belief.length; i++){
//				if(belief[i] > 0.){
//					State mdpS = this.stateEnumerator.getStateForEnumertionId(i);
//					double r = this.mdpRF.reward(mdpS, a, null);
//					sum += belief[i]*r;
//				}
//			}
//			
//			return sum;
//		}
//		
//		protected double sasReward(State s, GroundedAction a){
//			
//			double [] belief = s.getFirstObjectOfClass(CLASSBELIEF).getDoubleArrayValue(ATTBELIEF);
//			
//			double sum = 0.;
//			for(int i = 0; i < belief.length; i++){
//				if(belief[i] > 0.){
//					State mdpS = this.stateEnumerator.getStateForEnumertionId(i);
//					List<TransitionProbability> tps = a.action.getTransitions(mdpS, a.params);
//					double sumTransR = 0.;
//					for(TransitionProbability tp : tps){
//						double r = this.mdpRF.reward(mdpS, a, tp.s);
//						double wr = r*tp.p;
//						sumTransR += wr;
//					}
//					sum += belief[i]*sumTransR;
//				}
//			}
//			
//			return sum;
//		}

		
		
		
		
	}

}
