package common.pomdp;

import java.util.Map;

import burlap.behavior.policy.Policy;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import posg.POSGDomain;
import posg.TabularBeliefState;


public class BeliefMAMDPPolicyAgent extends SGAgent {
	protected POSGDomain							domain;
	protected String 								actingAgent;
	protected Map<Integer, Policy> 					otherAgentsPolicyMap;
	protected Policy								policy;
	protected SGDomain								beliefDomain;
	
	public BeliefMAMDPPolicyAgent(POSGDomain domain, String actingAgent, Map<Integer, Policy> otherAgentsPolicyMap, Policy policy){
		this.domain = domain;
		this.policy = policy;
		this.actingAgent = actingAgent;
		this.otherAgentsPolicyMap = otherAgentsPolicyMap;
		
		BeliefMAMDPGenerator bdgen = new BeliefMAMDPGenerator(this.domain, this.actingAgent, this.otherAgentsPolicyMap);
		this.beliefDomain = (SGDomain)bdgen.generateDomain();
	}
	
	public BeliefMAMDPPolicyAgent(POSGDomain domain, String actingAgent, Policy policy){
		this.domain = domain;
		this.policy = policy;
		this.actingAgent = actingAgent;
		
		
		BeliefMAMDPGenerator bdgen = new BeliefMAMDPGenerator(this.domain, this.actingAgent);
		this.beliefDomain = (SGDomain)bdgen.generateDomain();
	}
	
	
	@Override
	public GroundedSGAgentAction getAction(State curBelief) {
		if(!(curBelief instanceof TabularBeliefState)) {
			throw new RuntimeException("iPBVI cannot return the "
					+ "Q-values for the given state, because the given "
					+ "state is not a EnumerableBeliefState instance. "
					+ "It is a " + curBelief.getClass().getName()
					+ " call getAction(BeliefState curBelief) instead..");
		}
		//convert to belief state
		State s = BeliefMAMDPGenerator.getBeliefMDPState(this.beliefDomain, (BeliefState) curBelief);
		GroundedSGAgentAction ga = (GroundedSGAgentAction) this.policy.getAction(s);
		//System.out.println(s.getFirstObjectOfClass(BeliefMDPGenerator.CLASSBELIEF).getStringValForAttribute(BeliefMDPGenerator.ATTBELIEF) + ": " + ga.toString());
		return ga;
	}


	@Override
	public void gameStarting() {
		
	}



	@Override
	public void observeOutcome(State s, JointAction jointAction,
			Map<String, Double> jointReward, State sprime, boolean isTerminal) {
		// TODO Auto-generated method stub
		
	}
	
	public void observeOutcome(State curMS, Map<String,TabularBeliefState> srcBSVector, Map<String, State> agentStateMap, JointAction jointAction,
			Map<String, Double> jointReward, Map<String, State> agentObsMap, State MSprime, Map<String,TabularBeliefState> newBSVector, Map<String, State> agentSPMap, boolean isTerminal) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void gameTerminated() {
		// TODO Auto-generated method stub
		
	}
}
