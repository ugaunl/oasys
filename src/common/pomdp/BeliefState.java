package common.pomdp;

import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;

public interface BeliefState extends State {

	double belief(State s);
	State sampleStateFromBelief();
	BeliefState getUpdatedBeliefState(State observation, JointAction ga);
	
}
