package common.pomdp;

import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;


public abstract class BeliefAgent extends SGAgent{
	protected BeliefState curBelief;
	
	public abstract GroundedSGAgentAction getAction(BeliefState curBelief);
}
