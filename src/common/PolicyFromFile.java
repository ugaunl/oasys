package common;

import iPOMDPLiteSolver.iPBVI;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import posg.POSGDomain;
import burlap.behavior.policy.Policy;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.behavior.policy.Policy.PolicyUndefinedException;
import burlap.debugtools.RandomFactory;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;

public class PolicyFromFile extends Policy {
	protected SGDomain d;
	protected String filename;
	protected String actingAgent;
	protected StateEnumerator senum;
	protected Map<State,GroundedSGAgentAction> stateToActionMap;
	protected Map<State,List<ActionProb>> stateToActionDistribution;
	protected Map<String,Integer> actionNametoID;
	
	public PolicyFromFile(SGDomain d, String actingAgent, StateEnumerator senum, String filename) {
		this.d = d;
		this.actingAgent = actingAgent;
		this.senum = senum; 
		this.stateToActionMap = iPBVI.readBestMDPActions((POSGDomain) d,actingAgent,senum,filename);
		this.stateToActionDistribution = iPBVI.readMDPStateToActionDistribution((POSGDomain) d,actingAgent,senum,filename);
//		System.out.println("Policy is now loaded successfully from file..");
	}

	@Override
	public AbstractGroundedAction getAction(State s) {
			return this.stateToActionMap.get(getEquivalentStateInMap(s));
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {
		return this.stateToActionDistribution.get(getEquivalentStateInMap(s));
	}
	
	public double getProbOfAction(State s, AbstractGroundedAction ga){
		List <ActionProb> probs = this.getActionDistributionForState(s);
//		System.out.println(senum.getEnumeratedID(s)+"-"+ga.actionName());
		if(probs == null || probs.size() == 0){
			System.out.println("--"+probs.size()+"--"+probs.get(0));
			throw new PolicyUndefinedException();
		}
		for(ActionProb ap : probs){
			if(ap.ga.equals(ga)){
				return ap.pSelection;
			}
		}
		return 0.;
	}
	
	private State getEquivalentStateInMap(State s){
		if(stateToActionMap.containsKey(s)){
			return s;
		}
		for(State state : stateToActionMap.keySet()) {
			if(s.equals(state))
				return state;
		}
		return null;
	}
	
	
	@Override
	public boolean isStochastic() {
		return true;
	}

	@Override
	public boolean isDefinedFor(State s) {
		return true;
	}

}
