package common;

import java.util.List;
import java.util.Map;

import burlap.datastructures.HashedAggregator;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.objects.MutableObjectInstance;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.SGStateGenerator;


/**
 * A stochastic games state generator that always returns the same base state, which is specified via the constructor. The
 * provided source state does *not* need to worry about the object name of OO-MDP objects corresponding to agent states.
 * This generator will automatically reassign the relevant OO-MDP object names to the names of each agent by querying the agent type
 * and agent name in the list of agents provides to the {@link #generateState(List)} method. This reassignment is done
 * each time the {@link #generateState(List)} method is called on a copy of the originally provided state.
 * @author James MacGlashan
 *
 */
public class SGAgentStateMapGenerator {

	/**
	 * The source state that will be copied and returned by the {@link #generateState(List)} method.
	 */
	protected Map<Integer,State> srcAgentStateMap;
	protected State srcMasterState;
	
	
	/**
	 * Initializes.
	 * @param srcState The source state that will be copied and returned by the {@link #generateState(List)} method.
	 */
	public SGAgentStateMapGenerator(State srcMasterState, Map<Integer,State> srcAgentStateMap){
		this.srcAgentStateMap = srcAgentStateMap;
		this.srcMasterState = srcMasterState;
	}
	
	
	
	/**
	 * Creates an object instance belonging to the object class specified in the agent's {@link SGAgentType} data member.
	 * The returned object instance will have the name of the agent.
	 * @param a the agent for which to create an OO-MDP state object instance
	 * @return an object instance for this agent.
	 */
	protected ObjectInstance getAgentObjectInstance(SGAgent a){
		return new MutableObjectInstance(a.getAgentType().oclass, a.getAgentName());
	}
	
	
	public State generateState(List<SGAgent> agents ) {
		
		
		State s = this.srcMasterState.copy();
		HashedAggregator<String> counts = new HashedAggregator<String>();
		
		for(SGAgent a : agents){
			
			String agentClassName = a.getAgentType().oclass.name;
			int index = (int) counts.v(agentClassName);
			List<ObjectInstance> possibleAgentObjects = s.getObjectsOfClass(agentClassName);
			if(possibleAgentObjects.size() <= index){
				throw new RuntimeException("Error: State used by MasterStateGeneratorSG does not have enough oo-mdp objects for agents defined by class: " + agentClassName);
			}
			ObjectInstance agentObject = possibleAgentObjects.get(index);
			s.renameObject(agentObject, a.getAgentName());
			
			counts.add(agentClassName, 1.);
			
			
		}
		
		return s;
		
	}

}

