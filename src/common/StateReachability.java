package common;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import posg.POSGDomain;
import burlap.debugtools.DPrint;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.TransitionProbability;
import burlap.oomdp.core.states.State;
import burlap.oomdp.statehashing.HashableState;
import burlap.oomdp.statehashing.HashableStateFactory;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointActionModel;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.World;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;

public class StateReachability {
	/**
	 * The debugID used for making calls to {@link burlap.debugtools.DPrint}.
	 */
	public static int			debugID = 22222;
	
	
	/**
	 * Returns the list of {@link burlap.oomdp.core.states.State} objects that are reachable from a source state.
	 * @param from the source state
	 * @param inDomain the domain of the state
	 * @param usingHashFactory the state hashing factory to use for indexing states and testing equality.
	 * @return the list of {@link burlap.oomdp.core.states.State} objects that are reachable from a source state.
	 */
	public static List <State> getReachableStates(State from, POSGDomain inDomain, 
			Map<String, SGAgentType> agentDefinitions, HashableStateFactory usingHashFactory){
		
		return getReachableStates(from, inDomain, agentDefinitions, usingHashFactory, new NullTermination());
	}
	
	
	/**
	 * Returns the list of {@link burlap.oomdp.core.states.State} objects that are reachable from a source state.
	 * @param from the source state
	 * @param inDomain the domain of the state
	 * @param usingHashFactory the state hashing factory to use for indexing states and testing equality.
	 * @param tf a terminal function that prevents expansion from terminal states.
	 * @return the list of {@link burlap.oomdp.core.states.State} objects that are reachable from a source state.
	 */
	public static List <State> getReachableStates(State from, POSGDomain inDomain, 
			Map<String, SGAgentType> agentDefinitions, HashableStateFactory usingHashFactory, TerminalFunction tf){
		
		Set <HashableState> hashedStates = getReachableHashedStates(from, inDomain, agentDefinitions, usingHashFactory, tf);
		List <State> states = new ArrayList<State>(hashedStates.size());
		for(HashableState sh : hashedStates){
			states.add(sh.s);
		}
		
		return states;
	}
	
	
	/**
	 * Returns the set of {@link burlap.oomdp.core.states.State} objects that are reachable from a source state.
	 * @param from the source state
	 * @param inDomain the domain of the state Map<String, SGAgentType>
	 * @param agentDefinitions for the agents in the domain 
	 * @param usingHashFactory the state hashing factory to use for indexing states and testing equality.
	 * @return the set of {@link burlap.oomdp.core.states.State} objects that are reachable from a source state.
	 */
	public static Set<HashableState> getReachableHashedStates(State from, POSGDomain inDomain, 
			Map<String, SGAgentType> agentDefinitions, HashableStateFactory usingHashFactory) {
		
		return getReachableHashedStates(from, inDomain, agentDefinitions, usingHashFactory, new NullTermination());
	}
	
	public static Set<HashableState> getReachableHashedStates(State from, POSGDomain inDomain, 
			Map<String, SGAgentType> agentDefinitions, HashableStateFactory usingHashFactory, int horizon) {
		
		return getReachableHashedStates(from, inDomain, agentDefinitions, usingHashFactory, new NullTermination(), horizon);
	}

	/**
	 * Returns the set of {@link burlap.oomdp.core.states.State} objects that are reachable from a source state.
	 * @param from the source state
	 * @param inDomain the domain of the state
	 * @param inDomain the domain of the state Map<String, SGAgentType>
	 * @param usingHashFactory the state hashing factory to use for indexing states and testing equality.
	 * @param tf a terminal function that prevents expansion from terminal states.
	 * @return the set of {@link burlap.oomdp.core.states.State} objects that are reachable from a source state.
	 */

	public static Set<HashableState> getReachableHashedStates(State from, POSGDomain inDomain, 
			Map<String, SGAgentType> agentDefinitions, HashableStateFactory usingHashFactory, TerminalFunction tf) {
		
		Set<HashableState> hashedStates = new HashSet<HashableState>();
		HashableState shi = usingHashFactory.hashState(from);
		JointActionModel jaModel = inDomain.getJointActionModel(); 
		List<JointAction> jas = JointAction.getAllJointActions(from, agentDefinitions);
		int nGenerated = 0;
		
		LinkedList <HashableState> openList = new LinkedList<HashableState>();
		openList.offer(shi);
		hashedStates.add(shi);
		while(openList.size() > 0){
			HashableState sh = openList.poll();
			
			if(tf.isTerminal(sh.s)){
				continue; //don't expand
			}
			
			for(JointAction ja : jas){
				List<TransitionProbability> tps = jaModel.transitionProbsFor(sh.s, ja);
				for(TransitionProbability tp : tps){
					HashableState nsh = usingHashFactory.hashState(tp.s);
					nGenerated++;
					if(!hashedStates.contains(nsh)){
						openList.offer(nsh);
						hashedStates.add(nsh);
					}
				}
				
			}
			
		}
		
//		DPrint.cl(debugID, "Num generated: " + nGenerated + "; num unique: " + hashedStates.size());
		
		return hashedStates;
	}
	
	public static Set<HashableState> getReachableHashedStates(State from, POSGDomain inDomain, 
			Map<String, SGAgentType> agentDefinitions, HashableStateFactory usingHashFactory, TerminalFunction tf, int horizon) {
		
		Set<HashableState> hashedStates = new HashSet<HashableState>();
		HashableState shi = usingHashFactory.hashState(from);
		JointActionModel jaModel = inDomain.getJointActionModel(); 
		List<JointAction> jas = JointAction.getAllJointActions(from, agentDefinitions);
		int nGenerated = 0;
		
//		LinkedList <HashableState> openList = new LinkedList<HashableState>();
//		openList.offer(shi);
		
		datastructures.QueueLinkedList<HashableState> openList = new datastructures.QueueLinkedList<HashableState>();
		openList.add(shi);
		hashedStates.add(shi);
		
		int h=0;
		while(openList.size() > 0){
			HashableState sh = openList.poll();
			h++;
			if(tf.isTerminal(sh.s)){
				continue; //don't expand
			}
			
			for(JointAction ja : jas){
				List<TransitionProbability> tps = jaModel.transitionProbsFor(sh.s, ja);
				for(TransitionProbability tp : tps){
					HashableState nsh = usingHashFactory.hashState(tp.s);
					nGenerated++;
					if(!hashedStates.contains(nsh) && (h <= horizon)){
						openList.add(nsh);
						hashedStates.add(nsh);
					}
				}
				
			}
			
		}
		
//		DPrint.cl(debugID, "[FINITE] "+horizon+"-steps; Num generated: " + nGenerated + "; num unique: " + hashedStates.size());
		
		return hashedStates;
	}




}
