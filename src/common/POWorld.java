package common;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import common.pomdp.BeliefMAMDPPolicyAgent;
import domains.wildfire.Wildfire;
import domains.wildfire.WildfireDomain;
import posg.ObservationFunction;
import posg.POSGDomain;
import posg.TabularBeliefState;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.JointPolicy;
import burlap.datastructures.HashedAggregator;
import burlap.debugtools.DPrint;
import burlap.oomdp.auxiliary.StateAbstraction;
import burlap.oomdp.auxiliary.common.NullAbstraction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointActionModel;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.World;
import burlap.oomdp.stochasticgames.WorldObserver;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SimpleGroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SimpleSGAgentAction;


/**
 * This class provides a means to have agents play against each other and synchronize all of their actions and observations.
 * Any number of agents can join a World instance and they will be told when a game is starting, when a game ends, when
 * they need to provide an action, and what happened to all agents after every agent made their action selection. The world
 * may also make use of an optional {@link burlap.oomdp.auxiliary.StateAbstraction} object so that agents are provided an 
 * abstract and simpler representation of the world. A game can be run until a terminal state is hit, or for a specific
 * number of stages, the latter of which is useful for repeated games.
 * @author James MacGlashan
 *
 */
public class POWorld extends World{

	protected POSGDomain						domain;
	protected Map<String, State> 				agentStateMap;//initial agent-masterState map
	protected State								masterState;//initial master state
	protected List <SGAgent>					agents;
	protected Map<SGAgentType, List<SGAgent>>	agentsByType;
	protected HashedAggregator<String>			agentCumulativeReward;
	protected List<Map<String, SGAgentType>> 	allAgentDefinitions;
	
	protected JointActionModel 					worldModel;
	protected JointReward						jointRewardModel;
	protected TerminalFunction					tf;
	protected ObservationFunction 				of;
	protected Map<String, TabularBeliefState>	curBeliefStateVector;//current agent-beliefstate map
//	protected SGAgentStateMapGenerator			initialAgentStateMapGenerator;
	
	protected StateAbstraction					abstractionForAgents;
	
	protected JointAction						lastJointAction;
	
	protected List<WorldObserver>				worldObservers;
	protected GameAnalysis						currentGameRecord;
	
	protected boolean							isRecordingGame = false;
	
	protected int								debugId;
	protected StateEnumerator 					senum;
	
	/**
	 * Initializes the world.
	 * @param domain the SGDomain the world will use
	 * @param jr the joint reward function
	 * @param tf the terminal function
	 * @param initialState the initial state of the world every time a new game starts
	 */
	public POWorld(POSGDomain domain, JointReward jr, TerminalFunction tf, ObservationFunction of, List<Map<String, SGAgentType>> allAgentDefinitions, State masterState) {
		super(domain, jr, tf, masterState);
		this.init(domain, domain.getJointActionModel(), jr, tf, of, allAgentDefinitions, new NullAbstraction());
	}
	
	protected void init(POSGDomain domain, JointActionModel jam, JointReward jr, TerminalFunction tf, ObservationFunction of, List<Map<String, SGAgentType>> allAgentDefinitions, StateAbstraction abstractionForAgents){
		this.domain = domain;
		this.worldModel = jam;
		this.jointRewardModel = jr;
		this.tf = tf;
		this.of = of;
		this.allAgentDefinitions = allAgentDefinitions;
		
		this.abstractionForAgents = abstractionForAgents;
		
		agents = new ArrayList<SGAgent>();
		agentsByType = new HashMap<SGAgentType, List<SGAgent>>();
		
		agentCumulativeReward = new HashedAggregator<String>();
		
		worldObservers = new ArrayList<WorldObserver>();
		
		debugId = 284673923;
	}
	
	public POWorld(POSGDomain domain, JointReward jr, TerminalFunction tf, ObservationFunction of, List<Map<String, SGAgentType>> allAgentDefinitions, State masterState, Map<String,State> agentStateMap, Map<String, TabularBeliefState> curBeliefStateVector){
		super(domain, jr, tf, masterState);
		this.init(domain, domain.getJointActionModel(), jr, tf, of, allAgentDefinitions, new NullAbstraction());
		this.masterState = masterState;
		this.agentStateMap = agentStateMap;
		this.curBeliefStateVector = curBeliefStateVector;
	}
	
	//DO NOT USE
	public POWorld(POSGDomain domain, JointReward jr, TerminalFunction tf, ObservationFunction of, Map<String, SGAgentType> agentDefinitions, StateEnumerator senum, State masterState, Map<String,State> agentStateMap, Map<String, TabularBeliefState> curBeliefStateVector){
		super(domain, jr, tf, masterState);
		this.init(domain, domain.getJointActionModel(), jr, tf, of, agentDefinitions, senum);
		this.masterState = masterState;
		this.agentStateMap = agentStateMap;
		this.curBeliefStateVector = curBeliefStateVector;
	}
	
	//DO NOT USE
	public POWorld(POSGDomain domain, JointReward jr, TerminalFunction tf, ObservationFunction of, Map<String, SGAgentType> agentDefinitions, StateEnumerator senum, State masterState) {
		super(domain, jr, tf, masterState);
		this.init(domain, domain.getJointActionModel(), jr, tf, of, agentDefinitions, senum);
	}
	
	

		/**
	 * Initializes the world.
	 * @param domain the SGDomain the world will use
	 * @param jr the joint reward function
	 * @param tf the terminal function
	 * @param sg a state generator for generating initial states of a game
	 */
//	public POWorld(POSGDomain domain, JointReward jr, TerminalFunction tf, ObservationFunction of, SGAgentStateMapGenerator sg){
//		this.init(domain, domain.getJointActionModel(), jr, tf, of, sg, new NullAbstraction());
//	}
	public ObservationFunction getOF(){
		return of;
	}
	
	//DO NOT USE
	public StateEnumerator getStateEnumerator(){
		if(senum==null) System.out.println("State Enumerator is not set yet! Please use this instead:"+
						"public POWorld(POSGDomain domain, JointReward jr, TerminalFunction tf, ObservationFunction of, Map<String, SGAgentType> agentDefinitions, StateEnumerator senum, State masterState)");
		return senum;
	}

	
	//DO NOT USE
		protected void init(POSGDomain domain, JointActionModel jam, JointReward jr, TerminalFunction tf, ObservationFunction of, Map<String, SGAgentType> agentDefinitions, StateEnumerator senum){
			this.domain = domain;
			this.worldModel = jam;
			this.jointRewardModel = jr;
			this.tf = tf;
			this.of = of;
			this.agentDefinitions = agentDefinitions;
			
			this.senum = senum;
			
			agents = new ArrayList<SGAgent>();
			agentsByType = new HashMap<SGAgentType, List<SGAgent>>();
			
			agentCumulativeReward = new HashedAggregator<String>();
			
			worldObservers = new ArrayList<WorldObserver>();
			
			debugId = 284673923;
		}
		
		
	
	
	/**
	 * This class will report execution information as games are played using the {@link burlap.debugtools.DPrint} class. If the user
	 * wishes to suppress these messages, they can retrieve this code and suppress DPrint from printing messages that correspond to this code.
	 * @return the debug code used with {@link burlap.debugtools.DPrint}.
	 */
	public int getDebugId(){
		return debugId;
	}
	
	/**
	 * Sets the debug code that is use for printing with {@link burlap.debugtools.DPrint}.
	 * @param id the debug code to use when printing messages
	 */
	public void setDebugId(int id){
		debugId = id;
	}
	
	
	/**
	 * Returns the cumulative reward that the agent with name aname has received across all interactions in this world.
	 * @param aname the name of the agent
	 * @return the cumulative reward the agent has received in this world.
	 */
	public double getCumulativeRewardForAgent(String aname){
		return agentCumulativeReward.v(aname);
	}
	
	
	/**
	 * Registers an agent to be a participant in this world.
	 * @param a the agent to be registered in this world
	 * @param at the agent type the agent will be playing as
	 * @return the unique name that will identify this agent in this world.
	 */
	protected String registerAgent(SGAgent a, SGAgentType at){
		//don't register the same agent multiple times
		if(this.agentInstanceExists(a)){
			return a.getAgentName();
		}
		
		String agentName = this.getNewWorldNameForAgentAndIndex(a, at);
//		System.out.println("This agent shouldn't exist!! - "+ agentName);
		
		return agentName;
		
	}
	
	
	/**
	 * Returns the current world state
	 * @return the current world state
	 */
	public State getCurrentWorldState(){
		return this.masterState;
	}
	
	public Map<String,TabularBeliefState> getCurrentBeliefStateVector(){
		return this.curBeliefStateVector;
	}
	
	/**
	 * Causes the world to set the current state to a state generated by the provided {@link SGStateGenerator} object.
	 */
//	public void generateNewCurrentState(){
//		this.masterState = initialAgentStateMapGenerator.generateState(agents);
//	}
	
	/**
	 * Returns the last joint action taken in this world; null if none have been taken yet.
	 * @return the last joint action taken in this world; null if none have been taken yet
	 */
	public JointAction getLastJointAction(){
		return this.lastJointAction;
	}
	
	/**
	 * Adds a world observer to this world
	 * @param ob the observer to add
	 */
	public void addWorldObserver(WorldObserver ob){
		this.worldObservers.add(ob);
	}
	
	/**
	 * Removes the specified world observer from this world
	 * @param ob the world observer to remove
	 */
	public void removeWorldObserver(WorldObserver ob){
		this.worldObservers.remove(ob);
	}
	
	/**
	 * Clears all world observers from this world.
	 */
	public void clearAllWorldObserver(){
		this.worldObservers.clear();
	}
	
	

	
	/**
	 * Runs a game until a terminal state is hit for maxStages have occurred
	 * @param maxStages the maximum number of stages to play in the game before its forced to end.
	 */
	public GameAnalysis runGame(int maxStages, State srcMasterState, Map<String,State> agentStateMap ,Map<String,TabularBeliefState> srcBSVector){
		
		this.masterState = srcMasterState;
		this.curBeliefStateVector = srcBSVector;
		this.agentStateMap = agentStateMap;
		
		for(SGAgent a : agents){
			a.gameStarting();
		}
		
//		masterState = initialAgentStateMapGenerator.generateState(agents);
		this.currentGameRecord = new GameAnalysis(masterState);
		this.isRecordingGame = true;
		int t = 0;

		for(WorldObserver wob : this.worldObservers){
			wob.gameStarting(this.masterState);
		}
		
		while(t < maxStages){
			this.runStage(this.masterState, this.agentStateMap ,this.curBeliefStateVector);
			t++;
		}
		
		for(SGAgent a : agents){
			a.gameTerminated();
		}

		for(WorldObserver wob : this.worldObservers){
			wob.gameEnding(this.masterState);
		}
		
//		DPrint.cl(debugId, masterState.getCompleteStateDescription());
		
		this.isRecordingGame = false;
		
		return this.currentGameRecord;
		
	}
	
	/**
	 * Runs a game until a terminal state is hit for maxStages have occurred
	 * @param maxStages the maximum number of stages to play in the game before its forced to end.
	 */
	public GameAnalysis runGame(int maxStages, State srcMasterState, SGAgentAction sga){
		
		this.masterState = srcMasterState;
		
		for(SGAgent a : agents){
			a.gameStarting();
		}
		
//		masterState = initialAgentStateMapGenerator.generateState(agents);
		this.currentGameRecord = new GameAnalysis(masterState);
		this.isRecordingGame = true;
		int t = 0;

		for(WorldObserver wob : this.worldObservers){
			wob.gameStarting(this.masterState);
		}
		
		while(t < maxStages){
			this.runStage(this.masterState, sga);
			t++;
		}
		
		for(SGAgent a : agents){
			a.gameTerminated();
		}

		for(WorldObserver wob : this.worldObservers){
			wob.gameEnding(this.masterState);
		}
		
//		DPrint.cl(debugId, masterState.getCompleteStateDescription());
		
		this.isRecordingGame = false;
		
		return this.currentGameRecord;
		
	}
	
	/**
	 * Runs a game until a terminal state is hit for maxStages have occurred
	 * @param maxStages the maximum number of stages to play in the game before its forced to end.
	 */
	public GameAnalysis runGame(int maxStages, State srcMasterState, int baseline){
		
		this.masterState = srcMasterState;
		
		for(SGAgent a : agents){
			a.gameStarting();
		}
		
//		masterState = initialAgentStateMapGenerator.generateState(agents);
		this.currentGameRecord = new GameAnalysis(masterState);
		this.isRecordingGame = true;
		int t = 0;

		for(WorldObserver wob : this.worldObservers){
			wob.gameStarting(this.masterState);
		}
		
		while(t < maxStages){
			this.runStage(this.masterState, baseline);
			t++;
		}
		
		for(SGAgent a : agents){
			a.gameTerminated();
		}

		for(WorldObserver wob : this.worldObservers){
			wob.gameEnding(this.masterState);
		}
		
//		DPrint.cl(debugId, masterState.getCompleteStateDescription());
		
		this.isRecordingGame = false;
		
		return this.currentGameRecord;
		
	}
	
	
	/**
	 * Runs a single stage of this game.
	 */
	public void runStage(State srcMasterState, Map<String,State> agentStateMap ,Map<String,TabularBeliefState> srcBSVector){
		this.masterState = srcMasterState;
		this.curBeliefStateVector = srcBSVector;
		this.agentStateMap = agentStateMap;
		Map<String,TabularBeliefState> newBSVector = new HashMap<String,TabularBeliefState>();
		
		if(tf.isTerminal(srcMasterState)){
			return ; //cannot continue this game
		}
		
		JointAction ja = new JointAction();
		State abstractedCurrent = abstractionForAgents.abstraction(srcMasterState);
		for(SGAgent a : agents){
//			State abstractedAgentCurrent = abstractionForAgents.abstraction(this.agentStateMap.get(a.getAgentName()));
			ja.addAction(a.getAction(this.curBeliefStateVector.get(a.getAgentName())));
		}
		this.lastJointAction = ja;
		
		
//		DPrint.cl(debugId, ja.toString());
		
		
		//now that we have the joint action, perform it
		State nextMasterState = worldModel.performJointAction(srcMasterState, ja);
		State abstractedPrime = this.abstractionForAgents.abstraction(nextMasterState);
		
		Map<String, Double> jointReward = jointRewardModel.reward(srcMasterState, ja, nextMasterState);
		
//		DPrint.cl(debugId, jointReward.toString());
		
		//index reward
		for(String aname : jointReward.keySet()){
			double r = jointReward.get(aname);
			agentCumulativeReward.add(aname, r);
		}
		
		//sample agent observation states from true current, next states and joint action
		Map<String, State> agentObsMap = new HashMap<String, State>();
		
		//get agent states from true next state and Use this sampled observation to update each agent's belief given the true next state
		Map<String, State> agentSPMap = new HashMap<String, State>();
		
		int agID = 0;
		for(SGAgent a : agents){
			//true next state for agent
			agentSPMap.put(a.getAgentName(),Wildfire.createAgentStateFromMasterState((WildfireDomain) domain, agID, nextMasterState));
			//true sampled obs for agent
			State sampledObs = of.sampleObservation(agentStateMap.get(a.getAgentName()), agentSPMap.get(a.getAgentName()), this.lastJointAction);
			agentObsMap.put(a.getAgentName(), sampledObs);
			newBSVector.put(a.getAgentName(), (TabularBeliefState) srcBSVector.get(a.getAgentName()).getUpdatedBeliefState(sampledObs, agentSPMap.get(a.getAgentName()),  this.lastJointAction));
			agID++;
		}
		
		//tell all the agents about it
		for(SGAgent a : agents){
			((BeliefMAMDPPolicyAgent) a).observeOutcome(abstractedCurrent, srcBSVector, agentStateMap, ja, jointReward, agentObsMap, abstractedPrime, newBSVector, agentSPMap, tf.isTerminal(nextMasterState));
		}
		
		//tell observers
		for(WorldObserver o : this.worldObservers){
			o.observe(srcMasterState, ja, jointReward, nextMasterState);
		}
		
		//update the state
		this.masterState = nextMasterState;
		this.curBeliefStateVector = newBSVector;
		this.agentStateMap = agentSPMap;
		
		//record events
		if(this.isRecordingGame){
			this.currentGameRecord.recordTransitionTo(this.lastJointAction, this.masterState, jointReward);
		}
		
	}
	
	
	
	/**
	 * Runs a single stage of this game.
	 */
	public void runStage(State srcMasterState, int baseline){
		this.masterState = srcMasterState;
		
		if(tf.isTerminal(srcMasterState)){
			return ; //cannot continue this game
		}
		
		JointAction ja = new JointAction();
		
		State abstractedCurrent = abstractionForAgents.abstraction(srcMasterState);
		int agID = 0;
		for(SGAgent a : agents){
			if(baseline == 0){
				//true next state for agent
				State agState = Wildfire.createAgentStateFromMasterState((WildfireDomain) domain, agID, abstractedCurrent);
				int agentAvailability = abstractedCurrent.getObjectsOfClass("agent").get(agID).getIntValForAttribute("available");
				if(agentAvailability == 0){
					SGAgentAction noopAction = new SimpleSGAgentAction((WildfireDomain) domain, Wildfire.NOOP);
					ja.addAction(noopAction.getAssociatedGroundedAction(a.getAgentName()));
				}else{
					ja.addAction(a.getAction(agState));
				}
			}else if(baseline==1){
				//true next state for agent
				State agState = Wildfire.createAgentStateFromMasterState((WildfireDomain) domain, agID, abstractedCurrent);
				int agentAvailability = abstractedCurrent.getObjectsOfClass("agent").get(agID).getIntValForAttribute("available");
				GroundedSGAgentAction ga = null;
				if(agentAvailability == 0){
					SGAgentAction noopAction = new SimpleSGAgentAction((WildfireDomain) domain, Wildfire.NOOP);
					ga = noopAction.getAssociatedGroundedAction(a.getAgentName());
				}else{
					ga = a.getAction(agState);
					while(!checkValidAction(ga,agState)){
						ga = a.getAction(agState);
					}
				}
				ja.addAction(ga);
			}
			agID++;
		}
		this.lastJointAction = ja;
		
		
//		DPrint.cl(debugId, ja.toString());
		
		
		//now that we have the joint action, perform it
		State nextMasterState = worldModel.performJointAction(srcMasterState, ja);
		State abstractedPrime = this.abstractionForAgents.abstraction(nextMasterState);
		
		Map<String, Double> jointReward = jointRewardModel.reward(srcMasterState, ja, nextMasterState);
		
//		DPrint.cl(debugId, jointReward.toString());
		
		//index reward
		for(String aname : jointReward.keySet()){
			double r = jointReward.get(aname);
			agentCumulativeReward.add(aname, r);
		}
		
		
		//tell all the agents about it
		for(SGAgent a : agents){
			a.observeOutcome(abstractedCurrent, ja, jointReward, abstractedPrime,  tf.isTerminal(nextMasterState));
		}
		
		//tell observers
		for(WorldObserver o : this.worldObservers){
			o.observe(srcMasterState, ja, jointReward, nextMasterState);
		}
		
		//update the state
		this.masterState = nextMasterState;
		
		//record events
		if(this.isRecordingGame){
			this.currentGameRecord.recordTransitionTo(this.lastJointAction, this.masterState, jointReward);
		}
		
	}
	
	private boolean checkValidAction(GroundedSGAgentAction ga, State agState) {
		boolean validAction = true;
		String actionStr = ga.actionName();
		if(Wildfire.NOOP.equals(actionStr)){
			return true;
		}
		
		int numFires = agState.getObjectsOfClass(Wildfire.CLASS_FIRE).size();
		int fireIntensity;
		ObjectInstance fire;
		for (int f=0; f<numFires; f++){
			fire = agState.getObjectsOfClass(Wildfire.CLASS_FIRE).get(f);
			fireIntensity = fire.getIntValForAttribute(Wildfire.ATT_INTENSITY);
			if(fireIntensity == 0 || fireIntensity == Wildfire.NUM_FIRE_STATES - 1){
				int xFire = fire.getIntValForAttribute(Wildfire.ATT_X);
				int yFire = fire.getIntValForAttribute(Wildfire.ATT_Y);
				if(actionStr.equals("x" + xFire + "y" + yFire)){
					return false;
				}
			}
		}
		return validAction;
	}

	/**
	 * Runs a single stage of this game.
	 */
	public void runStage(State srcMasterState, SGAgentAction sga){
		this.masterState = srcMasterState;
		
		if(tf.isTerminal(srcMasterState)){
			return ; //cannot continue this game
		}
		
//		List<SGAgentAction> acts = this.domain.getAgentActions();
//		GroundedSGAgentAction noopAction;
//		for(SGAgentAction act:acts){
//			if(act.actionName.equals("NOOP")){
//				noopAction = act.getAssociatedGroundedAction(a.getAgentName());
//			}
//		}
		
		JointAction ja = new JointAction();
		
		State abstractedCurrent = abstractionForAgents.abstraction(srcMasterState);
		for(SGAgent a : agents){
			ja.addAction(sga.getAssociatedGroundedAction(a.getAgentName()));
		}
		this.lastJointAction = ja;
		
		
//		DPrint.cl(debugId, ja.toString());
		
		
		//now that we have the joint action, perform it
		State nextMasterState = worldModel.performJointAction(srcMasterState, ja);
		State abstractedPrime = this.abstractionForAgents.abstraction(nextMasterState);
		
		Map<String, Double> jointReward = jointRewardModel.reward(srcMasterState, ja, nextMasterState);
		
//		DPrint.cl(debugId, jointReward.toString());
		
		//index reward
		for(String aname : jointReward.keySet()){
			double r = jointReward.get(aname);
			agentCumulativeReward.add(aname, r);
		}
		
		
		//tell all the agents about it
		for(SGAgent a : agents){
			a.observeOutcome(abstractedCurrent, ja, jointReward, abstractedPrime,  tf.isTerminal(nextMasterState));
		}
		
		//tell observers
		for(WorldObserver o : this.worldObservers){
			o.observe(srcMasterState, ja, jointReward, nextMasterState);
		}
		
		//update the state
		this.masterState = nextMasterState;
		
		//record events
		if(this.isRecordingGame){
			this.currentGameRecord.recordTransitionTo(this.lastJointAction, this.masterState, jointReward);
		}
		
	}
	
	
	
	
	/**
	 * Runs a single stage following a joint policy for the current world state
	 * @param jp the joint policy to follow
	 */
	protected void rolloutOneStageOfJointPolicy(JointPolicy jp){
		
		if(tf.isTerminal(masterState)){
			return ; //cannot continue this game
		}
		
		this.lastJointAction = (JointAction)jp.getAction(this.masterState);
		
		DPrint.cl(debugId, this.lastJointAction.toString());
		
		
		//now that we have the joint action, perform it
		State sp = worldModel.performJointAction(masterState, this.lastJointAction);
		Map<String, Double> jointReward = jointRewardModel.reward(masterState, this.lastJointAction, sp);
		
		DPrint.cl(debugId, jointReward.toString());
		
		//index reward
		for(String aname : jointReward.keySet()){
			double r = jointReward.get(aname);
			agentCumulativeReward.add(aname, r);
		}
		
		
		//tell observers
		for(WorldObserver o : this.worldObservers){
			o.observe(masterState, this.lastJointAction, jointReward, sp);
		}
		
		//update the state
		masterState = sp;
		
		//record events
		if(this.isRecordingGame){
			this.currentGameRecord.recordTransitionTo(this.lastJointAction, this.masterState, jointReward);
		}
		
	}
	
	/**
	 * Returns the {@link JointActionModel} used in this world.
	 * @return the {@link JointActionModel} used in this world.
	 */
	public JointActionModel getActionModel(){
		return worldModel;
	}
	
	
	/**
	 * Returns the {@link JointReward} function used in this world.
	 * @return the {@link JointReward} function used in this world.
	 */
	public JointReward getRewardModel(){
		return jointRewardModel;
	}
	
	/**
	 * Returns the {@link burlap.oomdp.core.TerminalFunction} used in this world.
	 * @return the {@link burlap.oomdp.core.TerminalFunction} used in this world.
	 */
	public TerminalFunction getTF(){
		return tf;
	}
	
	
	/**
	 * Returns the list of agents participating in this world.
	 * @return the list of agents participating in this world.
	 */
	public List <SGAgent> getRegisteredAgents(){
		return new ArrayList<SGAgent>(agents);
	}
	
	
	/**
	 * Returns the agent definitions for the agents registered in this world.
	 * @return the agent definitions for the agents registered in this world.
	 */
	public List<Map<String, SGAgentType>> getAllAgentDefinitions(){
		return this.allAgentDefinitions;
	}
	
	public Map<String, SGAgentType> getAgentDefinitions(int agentID){
		return this.allAgentDefinitions.get(agentID);
	}
	
	
	/**
	 * Returns the player index for the agent with the given name.
	 * @param aname the name of the agent
	 * @return the player index of the agent with the given name.
	 */
	public int getPlayerNumberForAgent(String aname){
		for(int i = 0; i < agents.size(); i++){
			SGAgent a = agents.get(i);
			if(a.getAgentName().equals(aname)){
				return i;
			}
		}
		
		return -1;
	}
	
	
	/**
	 * Returns a unique agent name for the given agent object and agent type for that agent.
	 * @param a the agent for which a unique name is to be returned
	 * @param type the agent type of the agent
	 * @return a unique name for the agent
	 */
	protected String getNewWorldNameForAgentAndIndex(SGAgent a, SGAgentType type){
	
		
		List <SGAgent> aots = agentsByType.get(type);
		if(aots == null){
			aots = new ArrayList<SGAgent>();
			agentsByType.put(type, aots);
		}
		
		String name = type.typeName + aots.size();
		agents.add(a);
		aots.add(a);
		
		Map<String,SGAgentType> agDef = new HashMap<String,SGAgentType>();
		agDef.put(name, type);
		
		this.allAgentDefinitions.add(agDef);
		return name;
	}
	
	
	/**
	 * Returns whether the reference for the given agent already exists in the registered agents
	 * @param a the agent reference to check for
	 * @return true if that agent reference is already registered; false otherwise
	 */
	protected boolean agentInstanceExists(SGAgent a){
		for(SGAgent A : agents){
			if(A == a){
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Runs a game until a terminal state is hit for maxStages have occurred
	 * @param maxStages the maximum number of stages to play in the game before its forced to end.
	 */
	public GameAnalysis runGame(int maxStages, State srcMasterState){
		
		this.masterState = srcMasterState;
		
		for(SGAgent a : agents){
			a.gameStarting();
		}
		
//		masterState = initialAgentStateMapGenerator.generateState(agents);
		this.currentGameRecord = new GameAnalysis(masterState);
		this.isRecordingGame = true;
		int t = 0;

		for(WorldObserver wob : this.worldObservers){
			wob.gameStarting(this.masterState);
		}
		
		while(t < maxStages){
			this.runStage(this.masterState);
			t++;
		}
		
		for(SGAgent a : agents){
			a.gameTerminated();
		}

		for(WorldObserver wob : this.worldObservers){
			wob.gameEnding(this.masterState);
		}
		
//		DPrint.cl(debugId, masterState.getCompleteStateDescription());
		
		this.isRecordingGame = false;
		
		return this.currentGameRecord;
		
	}
	
	/**
	 * Runs a single stage of this game.
	 */
	public void runStage(State srcMasterState){
		this.masterState = srcMasterState;
		
		if(tf.isTerminal(srcMasterState)){
			return ; //cannot continue this game
		}
		
		JointAction ja = new JointAction();
		
		State abstractedCurrent = abstractionForAgents.abstraction(srcMasterState);
		int agID = 0;
		for(SGAgent a : agents){
			//true next state for agent
			State agState = Wildfire.createAgentStateFromMasterState((WildfireDomain) domain, agID, abstractedCurrent);
			int agentAvailability = abstractedCurrent.getObjectsOfClass("agent").get(agID).getIntValForAttribute("available");
			if(agentAvailability != 0){
				ja.addAction(a.getAction(agState));
			}else{
				SGAgentAction noopAction = new SimpleSGAgentAction((WildfireDomain) domain, Wildfire.NOOP);
				ja.addAction(noopAction.getAssociatedGroundedAction(a.getAgentName()));
			}
			agID++;
		}
		this.lastJointAction = ja;
		
		
//		DPrint.cl(debugId, ja.toString());
		
		
		//now that we have the joint action, perform it
		State nextMasterState = worldModel.performJointAction(srcMasterState, ja);
		State abstractedPrime = this.abstractionForAgents.abstraction(nextMasterState);
		
		Map<String, Double> jointReward = jointRewardModel.reward(srcMasterState, ja, nextMasterState);
		
//		DPrint.cl(debugId, jointReward.toString());
		
		//index reward
		for(String aname : jointReward.keySet()){
			double r = jointReward.get(aname);
			agentCumulativeReward.add(aname, r);
		}
		
		
		//tell all the agents about it
		for(SGAgent a : agents){
			a.observeOutcome(abstractedCurrent, ja, jointReward, abstractedPrime,  tf.isTerminal(nextMasterState));
		}
		
		//tell observers
		for(WorldObserver o : this.worldObservers){
			o.observe(srcMasterState, ja, jointReward, nextMasterState);
		}
		
		//update the state
		this.masterState = nextMasterState;
		
		//record events
		if(this.isRecordingGame){
			this.currentGameRecord.recordTransitionTo(this.lastJointAction, this.masterState, jointReward);
		}
		
	}

}
