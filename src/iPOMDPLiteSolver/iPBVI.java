package iPOMDPLiteSolver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import posg.*;
import common.*;
import common.pomdp.EnumerableBeliefState.StateBelief;
import common.pomdp.BeliefMAMDPGenerator;
import nestedMDPSolver.NestedVI;
import libpomdp.common.Utils;
import domains.wildfire.*;
import domains.wildfire.Wildfire.*;
import burlap.behavior.policy.*;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.behavior.singleagent.MDPSolver;
import burlap.behavior.singleagent.planning.Planner;
import burlap.behavior.valuefunction.QFunction;
import burlap.behavior.valuefunction.QValue;
import burlap.debugtools.*;
import burlap.oomdp.auxiliary.common.NullTermination;
import burlap.oomdp.core.*;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.*;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SimpleSGAgentAction;
import burlap.oomdp.statehashing.*;

public class iPBVI extends MDPSolver implements QFunction, Planner {
	//Domain Enum
	public static final String WF = "wf";
	public static final String GG = "gg";
	public static final String CN = "cn";
	public enum dom {WF,GG,CN};
	
	//Flags
	@SuppressWarnings("unused")
	private boolean finite = true;
	protected static boolean debug = true;
	protected static boolean debug1 = false;
	protected static boolean verbosemode = true;
	protected static boolean testingmode = false;
	protected static boolean debugmode = false;
	protected boolean planningStarted = false;
	protected boolean beliefExpansion = true;
	
	//Parameters
	protected int horizons;
	protected double maxDelta;
	protected int maxIterations;
	protected double gamma;
	protected int maxLevel;
	protected int maxMDPLevel;
	
	//Domain-specific data types
	protected POSGDomain domain;
	protected JointReward jr;
	protected TerminalFunction tf;
	protected ObservationFunction of;
	protected static HashableStateFactory hashingFactory;
	protected JointActionModel jaModel;
	
	//Agent-specific data types
	protected int currentAgentIndex;
	protected String currentAgentName;
	protected static Map<String, SGAgentType> agentDefinitions = new HashMap<String, SGAgentType>();
	protected static Map<Integer, Policy> otherAgentsPolicyMap;
	protected static Map<Integer, String> agIDtoName = new HashMap<Integer,String>();
	protected static List<Integer> agentsInWorld;
	
	//Planner-specific data types
	protected static List<State> states = new ArrayList<State>();
	protected static Map<String,ArrayList<SGAgentAction>> agentActions = new HashMap<String,ArrayList<SGAgentAction>>();
	protected List<JointAction> allJAs = new ArrayList<JointAction>();
	protected List<State> observations = new ArrayList<State>();
	protected List<ArrayList<Double>> belief_points = new ArrayList<ArrayList<Double>>();
	protected ArrayList<TabularBeliefState> belief_states = new ArrayList<TabularBeliefState>();
	protected List<State> initialStateList = new ArrayList<State>();
	protected List<double[]> alphaVectors = new ArrayList<double[]>();
	protected List<SGAgentAction> alphaVectorActions = new ArrayList<SGAgentAction>();
	
	protected int numStates;
	protected int numObservations;
	protected int numAgents;
	protected int numJAs;
	protected int numBeliefPoints;
	protected int numCurAgentActions;
	
	protected Random rand = RandomFactory.getMapped(0);

	protected double [][] vectorSetReward = null;
	protected double [][][][] vectorSetActionOb = null;
	protected double [][][] vectorSetActionBelief = null;
	protected double [][] vectorSetJAReward = null;
	protected double [][][][] vectorSetJActionOb = null;
	protected double [][][] vectorSetJActionBelief = null;
	protected TabularBeliefState initialBeliefState;
	
	protected static int debugCode = 123456789;
	private StateEnumerator senum;
	private static Map<JointAction, Integer> allJAsMap = new HashMap<JointAction, Integer>();
	protected static String outFileHead;
	
	

	public iPBVI(POSGDomain d, JointReward jr, TerminalFunction tf, ObservationFunction of,
			double gamma, Map<String, SGAgentType> agentDefinitions,Map<Integer,String> agIDtoName,
			List<JointAction> allJAs, TabularBeliefState tbs, Map<Integer, Policy> otherAgentsPolicyMap, 
			int horizons, int maxIterations, int currentAgentIndex, String currentAgentName,int maxLevel, List<Integer> agentsInWorld) {
		
		initPBVI(d,jr,tf,of,gamma,agentDefinitions,agIDtoName,allJAs,tbs,
				otherAgentsPolicyMap,currentAgentIndex,currentAgentName,
				maxIterations,horizons,maxLevel,agentsInWorld);
		
	}
	
	//used during simulations
	public iPBVI(POSGDomain d, JointReward jr, TerminalFunction tf, ObservationFunction of,
			 Map<String, SGAgentType> agentDefinitions, Map<Integer,String> agIDtoName,List<JointAction> allJAs, 
			 List<double[]> alphaVectors, List<SGAgentAction> alphaVectorActions,
			 int currentAgentIndex, String currentAgentName) {
		
		initPBVI(d,jr,tf,of,agentDefinitions,agIDtoName,allJAs,currentAgentName);
		this.currentAgentIndex = currentAgentIndex;
		this.alphaVectorActions = alphaVectorActions;
		this.alphaVectors = alphaVectors;
	}
	
	//used during simulations
	public iPBVI(POSGDomain d, JointReward jr, TerminalFunction tf, ObservationFunction of,
			 Map<String, SGAgentType> agentDefinitions, Map<Integer,String> agIDtoName,List<JointAction> allJAs, 
			 List<double[]> alphaVectors, List<SGAgentAction> alphaVectorActions,
			 int currentAgentIndex, String currentAgentName, StateEnumerator senum) {
		
		initPBVI(d,jr,tf,of,agentDefinitions,agIDtoName,allJAs,currentAgentName);
		this.senum = senum;
		this.currentAgentIndex = currentAgentIndex;
		this.alphaVectorActions = alphaVectorActions;
		this.alphaVectors = alphaVectors;
		
//		for(int i1=0;i1<this.alphaVectors.size();i1++){
//			System.out.print(" "+ this.alphaVectors.get(i1).length);
//		}
//		System.out.println();
	}
	
	//used during simulations
	public void initPBVI(POSGDomain d, JointReward jr, TerminalFunction tf, ObservationFunction of, 
			Map<String, SGAgentType> agentDefs, Map<Integer,String> agIDtoN, List<JointAction> allJAs, 
			String currentAgentName) {
		
		this.domain = d;
		this.jr = jr;
		this.tf = tf;
		this.of = of;
		this.setAgentDefinitions(agentDefs);
		this.allJAs = allJAs;
		this.jaModel = d.getJointActionModel();
		this.currentAgentName = currentAgentName;
		agIDtoName = agIDtoN;
		senum = d.getStateEnumerator();
		agentDefinitions = agentDefs;
		
		
		//Caching Actions per Agent
		for(Map.Entry<String, SGAgentType> e: agentDefinitions.entrySet()){
			agentActions.put(e.getKey(), (ArrayList<SGAgentAction>) e.getValue().actions);
		}
		
		//Caching Observations
		this.observations = this.of.getAllPossibleObservations();
		
		this.numStates = states.size(); 
		this.numJAs = this.allJAs.size();
		this.numAgents = agentDefinitions.size();
		this.numObservations = this.observations.size();
		this.numCurAgentActions = agentActions.get(currentAgentName).size();
		
		if(testingmode){
			System.out.println("1. " + this.numStates);
			System.out.println("2. " + this.numJAs);
			System.out.println("3. " + this.numAgents);
			System.out.println("4. " + this.numObservations);
			System.out.println("5. " + this.numCurAgentActions);
		}
	}
	
	

	public void initPBVI(POSGDomain d, JointReward jr,
			TerminalFunction tf, ObservationFunction of, 
			double gamma, 
			Map<String, SGAgentType> agentDefs, Map<Integer,String> agIDtoN, List<JointAction> allJAs,
			TabularBeliefState tbs, Map<Integer, Policy> otherAgPolicyMap, 
			int currentAgentIndex, String currentAgentName, 
			int maxIterations, int horizons, int maxLevel, List<Integer> agInWorld) {
		
		this.domain = d;
		this.jr = jr;
		this.tf = tf;
		this.of = of;
		this.gamma = gamma;
		this.setAgentDefinitions(agentDefs);
		this.allJAs = allJAs;
		this.initialBeliefState = tbs;
		this.maxIterations = maxIterations;
		this.horizons = horizons;
		this.maxLevel = maxLevel;	
		this.jaModel = d.getJointActionModel();
		this.currentAgentIndex = currentAgentIndex;
		this.currentAgentName = currentAgentName;
		otherAgentsPolicyMap = otherAgPolicyMap;
		agIDtoName = agIDtoN;
		agentsInWorld = agInWorld;
		this.senum = d.getStateEnumerator();
		
		
		
		//Caching Actions per Agent
		System.out.println("[Subject Agent "+this.currentAgentIndex+"] Caching Actions per Agent..");
		for(Map.Entry<String, SGAgentType> e: agentDefinitions.entrySet()){
			agentActions.put(e.getKey(), (ArrayList<SGAgentAction>) e.getValue().actions);
		}
		
		//Caching Observations
		System.out.print("[Subject Agent "+this.currentAgentIndex+"] Caching Observations..");
		this.observations = this.of.getAllPossibleObservations();
		System.out.println(".[Done] Found "+this.observations.size()+" observations!");
		
		this.numStates = states.size(); 
		this.numJAs = this.allJAs.size();
		this.numAgents = agentDefinitions.size();
		this.numObservations = this.observations.size();
		this.numCurAgentActions = agentActions.get(currentAgentName).size();
		
		//System.out.println("States with non zero prob: "+ tbs.getStatesAndBeliefsWithNonZeroProbability().size());
		
		if(testingmode){
			System.out.println("1. " + this.numStates);
			System.out.println("2. " + this.numJAs);
			System.out.println("3. " + this.numAgents);
			System.out.println("4. " + this.numObservations);
			System.out.println("5. " + this.numCurAgentActions);
		}
		
		// Caching the transition, reward and observation matrices!
		this.vectorSetJAReward = new double[this.numStates][this.numJAs];
		this.vectorSetReward = new double[this.numStates][this.numCurAgentActions];
		
		if(!testingmode){
			// sum over all ja (fixing ga for subject agent) * cumulative prob of others actions..");
			System.out.print("[Subject Agent "+this.currentAgentIndex+"] Caching R(s,ga).. " + this.numStates);
		
			for(int stateIndex = 0; stateIndex < this.numStates; ++stateIndex) {
				if(stateIndex%1000 == 0){
					System.out.print(".");
				}
				
				State s = states.get(stateIndex);
				
				for(JointAction ja:this.allJAs) {
					double sumRew = 0.;
					List<TransitionProbability> tprobs = this.jaModel.transitionProbsFor(s, ja);
					for(TransitionProbability tp:tprobs){
						sumRew += this.jr.reward(s, ja, tp.s).get(this.currentAgentName)*tp.p;// eq. 8 from the paper
					}
					
					this.vectorSetJAReward[stateIndex][allJAsMap.get(ja)] = sumRew;
					//System.out.println(this.currentAgentName+" sID: "+stateIndex+" jaID: "+jaIndex+" Rew: "+this.vectorSetJAReward[stateIndex][jaIndex]);
					
				}
			
				// get rew for each ground action (sum over JAList for that JA)
				for(int gaIndex = 0; gaIndex<this.numCurAgentActions; gaIndex++){
					this.vectorSetReward[stateIndex][gaIndex] = getVSRew(stateIndex, gaIndex, this.currentAgentName);
				}
			}
		}
		System.out.println(".[Done]");
		
	}
	
	@SuppressWarnings("unused")
	private double getVSRew(int stateIndex, int gaIndex, String agentName) {
		State s = states.get(stateIndex);
		SGAgentAction ga = agentActions.get(agentName).get(gaIndex);
		List<JointAction> jaListforGA = getGAJAMap(allJAs, agentName).get(ga);
		if(false){
			System.out.println(("Num Players: "+Integer.toString(this.numAgents)+" CurrentAgent: "+agentName));
			System.out.println(("Got GAJAMap: "+Integer.toString(getGAJAMap(allJAs, agentName).size())+" Got jaListforGA: "+Integer.toString(getGAJAMap(allJAs, agentName).get(ga).size())));
			NestedVI.printList2(jaListforGA);
		}
		
		double sumQ = 0.;
		
		for(JointAction ja: jaListforGA){
			double cumOthersJAProb = 1.;
			int jaIndex = 9999999;
			for(int jaId = 0; jaId<this.allJAs.size();jaId++){
				if(this.allJAs.get(jaId).equals(ja)){
					jaIndex = jaId;
				}
			}
			for(int i:agentsInWorld){
				if(otherAgentsPolicyMap.containsKey(i)){
					String actingAgent = agIDtoName.get(i);
					Double d = otherAgentsPolicyMap.get(i).getProbOfAction(s, ja.action(actingAgent));
					cumOthersJAProb *= d;
				}
			}
			if (jaIndex != 9999999){
				sumQ += cumOthersJAProb * this.vectorSetJAReward[stateIndex][jaIndex];
			}else{
				System.out.println("Fucked my friend!! Inside getVSRew()");
			}
		}
		return sumQ;
	}
	
	
	public static Map<SGAgentAction,List<JointAction>> getGAJAMap(List<JointAction> allJAs, String agentName){
		Map<SGAgentAction,List<JointAction>> gaJAMap = new HashMap<SGAgentAction,List<JointAction>>();
		List<SGAgentAction> allAgActions = agentActions.get(agentName);
		for(SGAgentAction ga:allAgActions){
			gaJAMap.put(ga, getJAListforGA(ga,allJAs,agentName));
		}
		
		return gaJAMap;
	}
	
	public static List<JointAction> getJAListforGA(SGAgentAction ga, List<JointAction> allJAs, String agentName) {
		List<JointAction> jaListforGA = new ArrayList<JointAction>();
		for(JointAction ja: allJAs){
			if(ja.action(agentName).action.equals(ga)){
				jaListforGA.add(ja);
			}
		}
		return jaListforGA;
	}
		
	
	@Override
	public double value(State s) {
		System.out.println("You shouldn't reach this... something is wrong!");
		return 0.;
	}
	
	

	@Override
	public List<QValue> getQs(State s) {
		List<SGAgentAction> acts = this.domain.getActionsPerAgent().get(this.currentAgentName);
		List<QValue> result = new ArrayList<QValue>(acts.size());
		TabularBeliefState bs = new TabularBeliefState(this.domain, this.senum, this.currentAgentName);
		bs.setBeliefVector(s.getFirstObjectOfClass(BeliefMAMDPGenerator.CLASSBELIEF).getDoubleArrayValForAttribute(BeliefMAMDPGenerator.ATTBELIEF));
//		printBeliefVector(bs);
//		if(bs.getBeliefVector().length != this.alphaVectors.get(0).length)
//			System.out.println("GetQs] Belief Vector Size: "+bs.getBeliefVector().length + " "+this.senum.numStatesEnumerated());
		
		for(SGAgentAction act : acts){
			QValue Qsa = this.getQ(bs,act.getAssociatedGroundedAction(currentAgentName));
			result.add(Qsa);
		}
		return result;
	}

	@Override
	public QValue getQ(State s, AbstractGroundedAction a) {
		TabularBeliefState bs = new TabularBeliefState(this.domain, this.senum, this.currentAgentName);
		bs.setBeliefVector(s.getFirstObjectOfClass(BeliefMAMDPGenerator.CLASSBELIEF).getDoubleArrayValForAttribute(BeliefMAMDPGenerator.ATTBELIEF));
//		if(bs.getBeliefVector().length != this.alphaVectors.get(0).length)
//			System.out.println("GetQ] Belief Vector Size: "+bs.getBeliefVector().length + " "+this.senum.numStatesEnumerated());
		QValue Qsa = new QValue(s, a, this.qForBelief(bs, a));
		return Qsa;
	}
	
	private double qForBelief(TabularBeliefState bs, AbstractGroundedAction a) {
//		if(bs.getBeliefVector().length != this.alphaVectors.get(0).length)
//			System.out.println("qForBelief] Belief Vector Size: "+bs.getBeliefVector().length + " "+this.senum.numStatesEnumerated());
		double qv = Double.NEGATIVE_INFINITY;
		for(int i =0;i<this.alphaVectorActions.size();i++){
			if(a.actionName().equals(this.alphaVectorActions.get(i).actionName)){
				double tempQValue=vectorProduct(bs.getBeliefVector(),this.alphaVectors.get(i));
				if(qv < tempQValue){
					qv = tempQValue;
				}
			}
		}
		
		return qv;
	}

	/**
	 * vector product of two vector of doubles
	 * @param vec1
	 * @param vec2
	 * @return
	 */
	private double vectorProduct(double[] vec1, double[] vec2) {
		double sum = 0.0;
		if(vec1.length < vec2.length){
			System.out.flush();
			System.err.println("PBVI: vectorProduct: vectors not of same size");
			System.err.println("vec1: " + vec1.length + " vec2: " + vec2.length);
			System.err.flush();
			System.exit(-1);
		}
		for(int i = 0; i<vec2.length;i++){
			sum+=vec1[i]*vec2[i];
		}
		return sum;
	}

	public void resetPlannerResults() {
		this.belief_points.clear();
		this.belief_states.clear();
		this.alphaVectors.clear();
		this.alphaVectorActions.clear();
	}

	@Override
	public GreedyQPolicy planFromState(State initialBeliefState){
		if(!(initialBeliefState instanceof TabularBeliefState)) {
			throw new RuntimeException("NestedPBVI cannot return the "
					+ "Q-values for the given state, because the given "
					+ "state is not a EnumerableBeliefState instance. "
					+ "It is a " + initialBeliefState.getClass().getName());
		}
		System.out.println("iPBVI-Lite planning begins ...");
		planFromBeliefState(initialBeliefState);
		System.out.println("iPBVI-Lite planning ends ...");
		
		GreedyQPolicy ipbviPol = new GreedyQPolicy(this);
		if(debug1){System.out.println("belief_points.size(): "+belief_points.size());
			System.out.println("alphaVectors.size(): "+this.alphaVectors.size());
			System.out.println("alphaVectorActions.size(): "+this.alphaVectorActions.size());
			System.out.println("Printing AlphaVectors..");
			printListArrayVectors(this.alphaVectors);
		}
		
		//printAgentPolicyForState(belief_states, ipbviPol);
		
		return ipbviPol;
		
	}
	
		
	private void printListArrayVectors(List<double[]> ListVectors) {
		for(int a=0; a<ListVectors.size(); a++){
			System.out.print("["+a+"]");
			printArrayVector(ListVectors.get(a));
		}
	}
	
	private void printListListVectors(List<ArrayList<Double>> ListVectors) {
		for(int a=0; a<ListVectors.size(); a++){
			System.out.print("["+a+"]");
			printListVector(ListVectors.get(a));
		}
	}

	@SuppressWarnings("unused")
	private void printAgentPolicyForState(
			ArrayList<TabularBeliefState> belief_states, GreedyQPolicy ipbviPol) {
		int i=0;
		for(TabularBeliefState bs:belief_states){
			System.out.print("Belief State ["+i+"]  ");
//			printBeliefVector(bs);
			for (int a=0; a<ipbviPol.getActionDistributionForState(bs).size(); a++){
				System.out.print(" "+ipbviPol.getActionDistributionForState(bs).get(a).pSelection);
			}
			System.out.println();
			i++;
		}
		
	}
	
	
	@SuppressWarnings("unused")
	private int debugStats(int stat){
		if(debug) {
			printBeliefStats(stat);
			if(stat!=0)
				printAlphaStats(stat);
			stat++;
		}
		return stat;
	}

	public void planFromBeliefState(State initialBeliefState) {
		if(!(initialBeliefState instanceof TabularBeliefState)) {
			throw new RuntimeException("NestedPBVI cannot return the "
					+ "Q-values for the given state, because the given "
					+ "state is not a EnumerableBeliefState instance. "
					+ "It is a " + initialBeliefState.getClass().getName());
		}
		
		//printBeliefVector((TabularBeliefState) initialBeliefState);
		
		ArrayList<Double> inputBeliefState = convertToJavaList(((TabularBeliefState) initialBeliefState).getBeliefVector());
		
		//Create initial list of belief states/points (it will contain only 1 element)
		this.belief_points.add(inputBeliefState);
		this.belief_states.add((TabularBeliefState) initialBeliefState);
		
		
		boolean insertVector = true;

		//Generate 8 to 14 belief points by recursively calling SSRA to start iPBVI planning 
		int numb = 0;
		while(numb < 8){
			List<TabularBeliefState> tbst = simulateSSRA(domain, this.belief_states);
			for(int i=0;i<tbst.size();i++){
				if(!this.belief_states.contains(tbst.get(i))){
					this.belief_states.add(tbst.get(i));
				}
			}
			numb = this.belief_states.size();
		}
		
		List<ArrayList<Double>> addBPs = new ArrayList<ArrayList<Double>>();
		
		for(int bs = 0;bs<this.belief_states.size();bs++){
			addBPs.add(convertToJavaList(((TabularBeliefState) this.belief_states.get(bs)).getBeliefVector()));
        }
        
		boolean insertVec = false;
		
		//doubling belief points adding them one by one to maintain order
		for(ArrayList<Double> tempBP : addBPs){
			insertVec = true;
			for(List<Double> beliefVector:this.belief_points){
				if(tempBP.equals(beliefVector)){
					insertVec = false;
					break;
				}
			}
			if(insertVec){
				this.belief_points.add(tempBP);
			}
		}
		
		this.numBeliefPoints  = this.belief_points.size();
		
		for(int k = 0; k < this.numBeliefPoints; ++k) {
			this.alphaVectors.add(null);
			this.alphaVectorActions.add(null);
		}
		System.out.println("[Subject Agent "+this.currentAgentIndex+"] [Initial Stats] |Belief Points| = "+this.belief_points.size()+" |Alpha Vectors| = "+this.alphaVectors.size());
		
		int backupCount = -1;
		MyTimer bTimer = new MyTimer();
		MyTimer viTimer = new MyTimer();
		
		for (int mainCount = 0; mainCount< this.maxIterations; mainCount++){
			viTimer.start();
			backupCount++;
			this.numBeliefPoints = this.belief_points.size();
			
			
			if(mainCount != 0) {
				vectorSetJActionOb = new double[this.numJAs][this.numObservations][this.alphaVectors.size()][this.numStates];
				vectorSetActionOb = new double[this.numCurAgentActions][this.numObservations][this.alphaVectors.size()][this.numStates];
			}
			vectorSetJActionBelief = new double[this.numJAs][this.numBeliefPoints][this.numStates];
			vectorSetActionBelief = new double[this.numCurAgentActions][this.numBeliefPoints][this.numStates];
			
			//System.out.println("Sanity check..1");
			/*System.out.println(this.numStates+" "+this.senum.numStatesEnumerated()+" "+
			 * this.allJAs.size()+" "+this.observations.size()+" "+this.numObservations+" "+
			 * this.alphaVectors.size()+" "+this.numCurAgentActions+" "+this.numBeliefPoints);
			 */
			
			for(int stateIndex = 0; stateIndex < this.numStates; ++stateIndex) {
				State s = states.get(stateIndex);
				for(JointAction ja:this.allJAs) {
					
					if(mainCount == 0) continue;
					
					for(int obIndex = 0;obIndex<observations.size();obIndex++) {
						for(int rvIndex = 0; rvIndex < this.alphaVectors.size(); ++rvIndex) {
							
							List<TransitionProbability> tps = this.jaModel.transitionProbsFor(s, ja);
							double nextStateSum = 0.0;
							for(TransitionProbability tp : tps) {
								int sPrimeIndex = this.senum.getEnumeratedID(tp.s);
								nextStateSum += tp.p * of.getObservationProbability(observations.get(obIndex), s, tp.s, ja) *
										this.alphaVectors.get(rvIndex)[sPrimeIndex];// eq 9
							}
							//System.out.println("JA Index :"+allJAsMap.get(ja)+" Obs Index :"+obIndex +obsList.get(obIndex).getCompleteStateDescription());
							vectorSetJActionOb[allJAsMap.get(ja)][obIndex][rvIndex][stateIndex] = 
									this.gamma * nextStateSum;// equation 9 outside of the summation
						}
					}
				}
				
				// get vectorSetJActionOb for each ground action (sum over JAList for that JA)
				if(mainCount != 0){
					for(int observationIndex = 0; observationIndex < this.numObservations; ++observationIndex) {
						for(int rvIndex = 0; rvIndex < this.alphaVectors.size(); ++rvIndex) {
							for(int gaIndex = 0; gaIndex<this.numCurAgentActions; gaIndex++){

								this.vectorSetActionOb[gaIndex][observationIndex][rvIndex][stateIndex] 
										= getVSAOb(gaIndex,observationIndex,rvIndex,stateIndex,this.currentAgentName);

							}
						}
					}
				}
			}
			
			for(int gaIndex = 0; gaIndex < this.numCurAgentActions; ++gaIndex) {
				for(int beliefIndex = 0; beliefIndex < this.numBeliefPoints; ++beliefIndex) {

					double[] sum = new double[this.numStates];

					for(int j = 0; j < this.numStates; ++j) {
						sum[j] = 0;
					}

					if(mainCount == 0) continue;

					double[] productArray = null;
					if(mainCount != 0) productArray = new double[this.alphaVectors.size()];

					for(int observationIndex = 0; observationIndex < this.numObservations; ++observationIndex) {
						for(int rvIndex = 0; rvIndex < this.alphaVectors.size(); ++rvIndex) {
							double acc = 0.0;
							
							// the vector multiplication per return vector and belief point vector
							for(int stateIndex = 0; stateIndex < this.numStates; ++stateIndex) {
								acc += vectorSetActionOb[gaIndex][observationIndex][rvIndex][stateIndex] * 
										belief_points.get(beliefIndex).get(stateIndex);
							}

							productArray[rvIndex] = acc;
						}

						double max_value = Double.NEGATIVE_INFINITY;
						int max_index = -1;
						
						// maximization over return vectors in eq 9
						for(int j = 0; j < this.alphaVectors.size(); ++j) {
							double test = productArray[j];
							if(test > max_value) {
								max_value = test;
								max_index = j;
							}
						}

						//System.out.println("ObsIndex: " + observationIndex + ", Max Val: " + max_value + ", Max Ind: " + max_index);

						// summation over observations in eq 9 this sum is for a particular action vector, notice no ob index!!
						for(int j = 0; j < this.numStates; ++j) {
							sum[j] += vectorSetActionOb[gaIndex][observationIndex][max_index][j]; // this is sum per obs. for a particular action and belief point!
						}
					}



					// notice observations have been summed out from the previous step as per eq 9
					for(int j = 0; j < this.numStates; ++j) {
						vectorSetActionBelief[gaIndex][beliefIndex][j] = vectorSetReward[j][gaIndex] + sum[j];
					}
				}
			}
			
			//Find best action for each belief point according to eq 10 
			for(int beliefIndex = 0; beliefIndex < this.numBeliefPoints; ++beliefIndex) {
				//	System.out.println("   Belief Index: " + beliefIndex);
				double[] productArray = new double[this.numCurAgentActions];
				for(int gaIndex = 0; gaIndex < this.numCurAgentActions; ++gaIndex) {
					//System.out.println("      Action Index: " + actionIndex);
					double acc = 0.0;
					for(int j = 0; j < this.numStates; ++j) {
						//System.out.println("j: " + j + ", BP[beliefIndex][j]: " + belief_points.get(beliefIndex).get(j));
						acc += vectorSetActionBelief[gaIndex][beliefIndex][j] * belief_points.get(beliefIndex).get(j);
					}
					
					productArray[gaIndex] = acc;
				}

				double max_value = Double.NEGATIVE_INFINITY;
				int max_index = -1;

				for(int j = 0; j < this.numCurAgentActions; ++j) {
					//System.out.println(" @@@@@   gaIndex: "+j+" -- " +  productArray[j] );
					double test = productArray[j];
					if(test > max_value) {
						max_value = test;
						max_index = j;
					}
				}
				
				//System.out.println("Setting Alphas: [" + beliefIndex+"] \nvectorSetActionBelief["+max_index+"][" + beliefIndex+"] :");
				//printArrayVector(vectorSetActionBelief[max_index][beliefIndex]);
				//System.out.println("Setting Action: "+agentActions.get(this.currentAgentName).get(max_index).actionName);
				
				double[] temp = new double[vectorSetActionBelief[max_index][beliefIndex].length];
				for (int kkk = 0; kkk < vectorSetActionBelief[max_index][beliefIndex].length; kkk++){
					temp[kkk] = vectorSetActionBelief[max_index][beliefIndex][kkk];
				}
				this.alphaVectors.set(beliefIndex, temp);
				this.alphaVectorActions.set(beliefIndex,agentActions.get(this.currentAgentName).get(max_index));
			}
			
			//stat = debugStats(stat);
			
			if(backupCount>=this.horizons && this.beliefExpansion){
				// expansion of belief points and addition to alpha vectors and actions!
				// the belief space will double after this step
				// list of new belief points to add
				bTimer.start();
				List<ArrayList<Double>> addBeliefPoints = new ArrayList<ArrayList<Double>>();
				
				//expand belief states
				//System.out.println("No of Belief states before: "+this.belief_states.size());
				List<TabularBeliefState> tbst = simulateSSRA(domain, this.belief_states);
	            
				for(int i=0;i<tbst.size();i++){
					if(!this.belief_states.contains(tbst.get(i))){
						this.belief_states.add(tbst.get(i));
					}
				}
	            //System.out.println("No of Belief states after: "+this.belief_states.size());
				
				//doubling belief points adding them one by one to maintain order
	            for(int bs = 0;bs<this.belief_states.size();bs++){
	            	addBeliefPoints.add(convertToJavaList(((TabularBeliefState) this.belief_states.get(bs)).getBeliefVector()));
	            }
	            
	            //doubling belief points adding them one by one to maintain order
				for(ArrayList<Double> tempBP : addBeliefPoints){
					insertVector = true;
					for(List<Double> beliefVector:this.belief_points){
						if(tempBP.equals(beliefVector)){
							insertVector = false;
							break;
						}
					}
					if(insertVector){
						// adding belief point as it is not the same as any preexisting point
						this.belief_points.add(tempBP);
						
						//printListVector(tempBP);
						
						// finding the best alpha vector in the set present since all other will not do
						int actionIndexToAdd = getBestActionIndex(tempBP);
						//System.out.println("actionIndexToAdd: "+actionIndexToAdd);
						
						double[] temp = new double[this.alphaVectors.get(actionIndexToAdd).length];
						for (int kkk = 0; kkk < this.alphaVectors.get(actionIndexToAdd).length; kkk++){
							temp[kkk] = this.alphaVectors.get(actionIndexToAdd)[kkk];
						}
						this.alphaVectors.add(temp);
						
						SGAgentAction temp_a = new SimpleSGAgentAction(this.alphaVectorActions.get(actionIndexToAdd).domain,this.alphaVectorActions.get(actionIndexToAdd).actionName);
						this.alphaVectorActions.add(temp_a);
					}
				}
	            
	            
				//stat = debugStats(stat);
				backupCount = 0;
				bTimer.stop();
				System.out.println("[Subject Agent "+this.currentAgentIndex+"] [Iteration " + mainCount+"] Finished belief update..in "+bTimer.getTime()+"s ");
				System.out.println("[Subject Agent "+this.currentAgentIndex+"] [Iteration " + mainCount+"] [Updated Stats] |Belief Points| = "+this.belief_states.size()+" == "+this.belief_points.size()+" |Alpha Vectors| = "+this.alphaVectors.size());
				
			}
			viTimer.stop();
			int agentType = ((WildfireDomain) domain).agentTypes[this.currentAgentIndex]; 
			//TODO filename 1
			String filename1 = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_iPBVI_alpha_actions_level"+this.maxLevel+"_agent"+this.currentAgentIndex+"-"+agentType+".txt";
			writeActions(this.domain,this.alphaVectorActions,filename1);
			
			//TODO filename 2
			String filename2 = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_iPBVI_alpha_vectors_level"+this.maxLevel+"_agent"+this.currentAgentIndex+"-"+agentType+".txt";
			writeAlphas(this.alphaVectors,filename2);
			//stat = debugStats(stat);
			System.out.println("[Subject Agent "+this.currentAgentIndex+"] [Iteration " + mainCount+"] Finished VI backup: "+(backupCount+1)+" in "+viTimer.getTime()+"s..");
		}
		System.out.println("---------------------------------------------------");
		System.out.println("[Subject Agent "+this.currentAgentIndex+"] [TIMING RESULTS] Performed total "+this.maxIterations+" backups..in "+viTimer.getTotalTime()+
				"s \n[Subject Agent "+this.currentAgentIndex+"] [TIMING RESULTS] Avg time/backup = "+viTimer.getAvgTime()+
				"s \n[Subject Agent "+this.currentAgentIndex+"] [TIMING RESULTS] Total time for "+((this.maxIterations-1)/this.horizons)+" belief expansion steps = "+bTimer.getTotalTime()+
				"s \n[Subject Agent "+this.currentAgentIndex+"] [TIMING RESULTS] Avg time/belief expansion step = "+bTimer.getAvgTime());
		System.out.println("---------------------------------------------------");
		
		int agentType = ((WildfireDomain) domain).agentTypes[this.currentAgentIndex]; 
		//TODO filename 1
		String filename1 = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_iPBVI_alpha_actions_level"+this.maxLevel+"_agent"+this.currentAgentIndex+"-"+agentType+".txt";
		writeActions(this.domain,this.alphaVectorActions,filename1);
		
		//TODO filename 2
		String filename2 = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_iPBVI_alpha_vectors_level"+this.maxLevel+"_agent"+this.currentAgentIndex+"-"+agentType+".txt";
		writeAlphas(this.alphaVectors,filename2);
		
		//stat = debugStats(stat);
	}
	

	

	private void printAlphaStats(int stat) {
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^ ALPHA STAT "+stat+" ^^^^^^^^^^^^^^^^^^^^^^^^^^");
		System.out.println("Alpha (vector/actions) size: "+ this.alphaVectors.size() +"/"+this.alphaVectorActions.size());
		System.out.println("Printing AlphaVectors..");
		if(this.alphaVectors.get(0)!=null) {
			printListArrayVectors(this.alphaVectors);
		}else{
			System.out.println("NULL");
		}
		System.out.println("Printing AlphaVectors Actions..");
		if(this.alphaVectorActions.get(0)!=null){
			printListVector(this.alphaVectorActions);
		}else{
			System.out.println("NULL");
		}
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^ ALPHA STAT "+stat+" ^^^^^^^^^^^^^^^^^^^^^^^^^^");
	}

	private void printListVector(List<SGAgentAction> alphaVectorActions) {
		for(SGAgentAction sga:alphaVectorActions){
			System.out.print(" "+sga.actionName);
		}	
		System.out.println();
	}

	private void printBeliefStats(int stat) {
		System.out.println("************************** BELIEF STAT "+stat+" **************************");
		System.out.println("Belief (point/state) size: "+this.belief_points.size() +"/"+this.belief_states.size());
		System.out.println("Printing Belief Points..");
		printListListVectors(this.belief_points);
		System.out.println("Printing Belief States..");
		printListBeliefVector(this.belief_states);
		System.out.println("************************** BELIEF STAT "+stat+" **************************");
	}

	@SuppressWarnings("unused")
	private static void printMap(Map<State, Integer> allObsMap) {
		System.out.println("Printing map entries..");
		for(Entry<State, Integer> e:allObsMap.entrySet()){
			System.out.print(e.getKey().getCompleteStateDescription()+" *** "+e.getValue());
		}
		System.out.println();
	}

	public static int getRandomJAction(int numJA) {
        return (Utils.gen.nextInt(Integer.MAX_VALUE) % numJA);
    }
	
	//SSRA expansion method from PBVI paper
	public List<TabularBeliefState> simulateSSRA(POSGDomain d, ArrayList<TabularBeliefState> tbsList){
		
		List<TabularBeliefState> nextBeliefStateList = new ArrayList<TabularBeliefState>();
		
		for(TabularBeliefState curB: tbsList){
//			System.out.println("CurB has "+curB.getStatesAndBeliefsWithNonZeroProbability().size()+" non-zero belief states");
			State sampledCurState = curB.sampleStateFromBelief();//sampleState(curB);
//			System.out.println(sampledCurState.getCompleteStateDescription());
			JointAction ja = this.allJAs.get(getRandomJAction(this.allJAs.size()));
			State sampledNxtState = this.jaModel.performJointAction(sampledCurState, ja);
//			System.out.println(sampledNxtState.getCompleteStateDescription());
			State sampledObs = this.of.sampleObservation(sampledCurState, sampledNxtState, ja);
			TabularBeliefState updatedBeliefState = (TabularBeliefState) curB.getUpdatedBeliefState(sampledObs,ja);
//			TabularBeliefState updatedBeliefState = (TabularBeliefState) curB.getUpdatedBeliefState(sampledObs,sampledNxtState,ja);
//			System.out.println("NewB has "+updatedBeliefState.getStatesAndBeliefsWithNonZeroProbability().size()+" non-zero belief states");
//			if(!nextBeliefStateList.contains(updatedBeliefState)){
				nextBeliefStateList.add(updatedBeliefState);
//			}
		}
		
		return nextBeliefStateList;	
	}
	
	public State sampleState(TabularBeliefState curB) {
		double [] bCopy = new double[curB.numStates()];
		double rand = Math.random();
		double sum = 0.;
		State retState = null;
		for(int i = 0; i < curB.numStates(); i++){
			sum += curB.belief(i);
			bCopy[i] = sum;
		}
		if(bCopy[curB.numStates()-1] != 1){
			System.out.println("Fucked inside SampleState in BeliefSetExpansion" + bCopy[curB.numStates()-1]);
			return null;
		}else{
			for(int i = 0; i < curB.numStates(); i++){
				if(rand <= bCopy[i]){
					retState = curB.stateForId(i);
					break;
				}
			}
		}
		return retState;
		
	}

	private double getVSAOb(int gaIndex, int observationIndex, int rvIndex,	int stateIndex, String agentName) {
		//System.out.println(observationIndex+" "+rvIndex+" "+gaIndex+" "+stateIndex+" "+this.currentAgentName+" top");
		
		State s = states.get(stateIndex);
		SGAgentAction ga = agentActions.get(agentName).get(gaIndex);
		List<JointAction> jaListforGA = getGAJAMap(allJAs, agentName).get(ga);
		
		double sumQ = 0.;
		
		for(JointAction ja: jaListforGA){
			double cumOthersJAProb = 1.;
			int jaIndex = 9999999;
			for(int jaId = 0; jaId<this.allJAs.size();jaId++){
				if(this.allJAs.get(jaId).equals(ja)){
					jaIndex = jaId;
				}
			}
			for(int i:agentsInWorld){
				if(otherAgentsPolicyMap.containsKey(i)){
					String actingAgent = agIDtoName.get(i);
					cumOthersJAProb *= otherAgentsPolicyMap.get(i).getProbOfAction(s, ja.action(actingAgent));
				}
			}
			if (jaIndex != 9999999){
				sumQ += cumOthersJAProb * this.vectorSetJActionOb[jaIndex][observationIndex][rvIndex][stateIndex];
			}else{
				System.out.println("Fucked my friend!! Inside getVSRew()");
			}
		}
		//if(sumQ > 0) System.out.println(observationIndex+" "+rvIndex+" "+gaIndex+" "+stateIndex+" "+this.currentAgentName+" "+sumQ);
		return sumQ;
	}
	
	/**
	 * returns the index of the alpha vector and action that fits best
	 * @param input_belief_point
	 * @return
	 */
	public Integer getBestActionIndex(List<Double> input_belief_point) {
		int maxIndex = -1;
		double maxSum = Double.NEGATIVE_INFINITY;
		
		//System.out.println("blaaaaaaaaaahhhhhhhhh-- "+this.alphaVectors.size());
		for(int i = 0; i < this.alphaVectors.size(); ++i) {
			double tempSum=0.0;
			for (int j=0; j < this.alphaVectors.get(i).length;++j){
				tempSum+=this.alphaVectors.get(i)[j]*input_belief_point.get(j);
				}
			if(tempSum > maxSum){
				maxSum = tempSum;
				maxIndex = i;
			}
			
		}
		
		return maxIndex;
	}
	
	
	public static List<State> getStatesWithAgentAvailability(int targetAvailability, int agentID ){
		List<State> swaa = new ArrayList<State>();
		for(State s:states){
			ObjectInstance otherAgentObj = s.getObjectsOfClass("agent").get(agentID);
			int availability = otherAgentObj.getIntValForAttribute("availabile");
			if(availability == targetAvailability){
				swaa.add(s);
			}
		}
		return swaa;
	}
	
	
	public static void verifyObs1(POWorld pow, WildfireDomain d, TabularBeliefState curB, 
			State masterState, Policy otherAgentPolicy, Policy subjectAgentPolicy,
			int[] obsIndex,int numStep,String subjectAgentName,int subjectAgentIndex, boolean debug){
		
		ObservationFunction of = pow.getOF();
		List<State> obs = of.getAllPossibleObservations();

		List<SGAgentAction> sgActions = ((WildfireDomain) d).getActionsPerAgent().get(subjectAgentName);
		List<GroundedSGAgentAction> groundedActions = NestedVI.SGtoGroundedSG(sgActions, subjectAgentName);
		
		System.out.println("finished initializing stuff..\nstarting simulation ["+numStep+" iterations]..");
		
		double[] tao = new double[numStep];
		for(int i=0; i< numStep; i++){
			System.out.println("Iteration "+i+"..");
			
			JointAction ja = new JointAction();
			for(int agID=0;agID<pow.getRegisteredAgents().size();agID++){
				if(agID==subjectAgentIndex){
					ja.addAction(pow.getRegisteredAgents().get(agID).getAction(curB));
					System.out.println("["+i+"] Got agent "+agID+"'s action from its policy..");
				}else{
					State agState = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, agID, masterState);
					ja.addAction(pow.getRegisteredAgents().get(agID).getAction(agState));
					System.out.println("["+i+"] Got agent "+agID+"'s action from its policy..");//for state ["+se.getEnumeratedID(agState)+"]");
				}
			}
			
			GroundedSGAgentAction a = ja.action(subjectAgentName);
			 
			State nextMasterState = d.getJointActionModel().performJointAction(masterState, ja);
			System.out.println("["+i+"] performing joint action.."+ja.actionName()+"..");
			
			State ob = obs.get(obsIndex[i]);
			
			if(debug){
				System.out.println("["+i+"] JointAction : "+ja.actionName());
			}
			
			State curState = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, subjectAgentIndex, masterState);
			State ns = d.getJointActionModel().performJointAction(curState, ja);
			
			System.out.println("["+i+"] created mental next state for subject agent by performing ja from its curState..");
			System.out.print("["+i+"] ");
			
			tao[i] = get_T_A_O(pow,d,ns,ob,groundedActions.indexOf(a),curB,subjectAgentName);
			
			System.out.println("["+i+"] T_A_O ["+i+"] = "+ tao[i]);
			
			TabularBeliefState nextB = new TabularBeliefState((WildfireDomain) d,subjectAgentName);
			nextB = (TabularBeliefState) curB.getUpdatedBeliefState(ob,ja);
			System.out.println("["+i+"] updated belief of subject agent..");
			
			curB = nextB;
			masterState = nextMasterState;
			System.out.println("["+i+"] restart with new belief and masterState..");
		}
		
		System.out.println("finished all iterations...\n Listing TAOs..");
		
		for(int n=0;n<numStep;n++){
			System.out.println("["+n+"] "+tao[n]);
		}
		
	}
	
	public static double get_T_A_O(POWorld pow, WildfireDomain domain, State ns, 
			State observation, int gaIndex, TabularBeliefState curB, String subjectAgentName) {
		
		ObservationFunction of = pow.getOF();
//		StateEnumerator se = pow.getStateEnumerator();
		List<StateBelief> stateBeliefs = curB.getStatesAndBeliefsWithNonZeroProbability();
		System.out.println("|CurBeliefStates| (non-zero): "+curB.getStatesAndBeliefsWithNonZeroProbability().size());
		
		if(stateBeliefs!=null && agIDtoName!=null && otherAgentsPolicyMap!=null){
			int numAgents = pow.getRegisteredAgents().size();
			List<JointAction> allJAs = JointAction.getAllJointActions(ns, pow.getAgentDefinitions());
			SGAgentAction ga = agentActions.get(subjectAgentName).get(gaIndex);
			List<JointAction> jaListforGA = getGAJAMap(allJAs, subjectAgentName).get(ga);
			double sum = 0.;
			//sum over current states
			for(StateBelief sb: stateBeliefs){
				State s = sb.s;
				//sum over others actions
				for(JointAction ja: jaListforGA){
					//prod over others policies
					double cumOthersJAProb = 1.;
					for(int i=0; i<numAgents; i++){
						if(otherAgentsPolicyMap.containsKey(i)){
							String actingAgent = agIDtoName.get(i);
							Double d = otherAgentsPolicyMap.get(i).getProbOfAction(s, ja.action(actingAgent));
							cumOthersJAProb *= d;
						}
					}
					
					//System.out.println("cumOthersJAProb: "+cumOthersJAProb);
					
					//find prob of next state given, s, ns, ja
					List<TransitionProbability> tps = domain.getJointActionModel().transitionProbsFor(s, ja);
					double tProb = 0.;
					for(TransitionProbability tp: tps){
						if(tp.s.equals(ns)){
							tProb = tp.p;
							//System.out.println("tProb: "+tProb);
						}
					}
					if(tProb>0){
						sum += cumOthersJAProb * tProb * of.getObservationProbability(observation, s, ns, ja) * curB.belief(s);
					}
				}	
			}
			return sum;
		}else{
			if(stateBeliefs==null){
				System.out.println("No states found ..");
			}
			if(agIDtoName==null){
				System.out.println("agIDtoName mapping not defined ..");
			}
			if(otherAgentsPolicyMap==null){
				System.out.println("otherAgentsPolicyMap not defined ..");
			}
			return 0.;
		}
		
	}
	
	public void writeActions(POSGDomain d, List<SGAgentAction> alphaVectorActions, String filename){
		boolean writeToFile = true;
		if(writeToFile){
			//Write alphavector actions
			try {
				File file = new File(filename);
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				
				String content = "";
				for(SGAgentAction act: alphaVectorActions){
					content = content + "\n" + act.actionName;
				}
				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.close();
				
				System.out.println("Done writing Alpha Vector Actions..");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public static void writeAlphas(List<double[]> alphaVectors, String filename) {
		int numVectors = alphaVectors.size();
		int numStates = alphaVectors.get(0).length;
		
		String content = "";
		for(int i=0;i<numVectors;i++){
			content = content+ Double.toString(alphaVectors.get(i)[0]);
			for(int j=0;j<numStates;j++){
				if(j==0) continue;
				content = content + "," + Double.toString(alphaVectors.get(i)[j]);
			}

			content = content +"\n";
		}

		boolean writeToFile = true;
		if(writeToFile){
			//Write alphavector actions
			try {
				File file = new File(filename);
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.close();
				
				System.out.println("Done writing Alpha Vectors..");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public static List<SGAgentAction> readActions(POSGDomain d, String filename){
		ArrayList<String> list = new ArrayList<String>();
		
		try {
			Scanner s = new Scanner(new File(filename));
			
			while (s.hasNext()){
			    list.add(s.next());
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		List<SGAgentAction> alphaVectorActs = new ArrayList<SGAgentAction>();
		for(int i=0;i<list.size();i++){
			SGAgentAction a = new SimpleSGAgentAction(d,list.get(i));
			alphaVectorActs.add(a);
		}
		
//		for(SGAgentAction act: alphaVectorActs){
//			System.out.println(act.actionName);
//		}
		
		return alphaVectorActs;
	}
	
	public static Map<State,GroundedSGAgentAction> readBestMDPActions(POSGDomain domain, 
			String agName, StateEnumerator senum, String filename) {
		
		Map<State,GroundedSGAgentAction> bestMDPActionForState = new HashMap<State,GroundedSGAgentAction>();
		
		try {
			Scanner s = new Scanner(new File(filename));
			
			while (s.hasNext()){
				String[] sa = s.next().split(":");
				if (sa[0].startsWith("x")) continue;
				try {
					if(Integer.parseInt(sa[0])<senum.numStatesEnumerated()){
						State ss = senum.getStateForEnumerationId(Integer.parseInt(sa[0]));
						SGAgentAction sga = new SimpleSGAgentAction(domain, sa[1]);  
						bestMDPActionForState.put(ss,sga.getAssociatedGroundedAction(agName));
					}
				} catch (Exception ex) {
					String str = "";
					for (String sastr : sa) {
						str += sastr;
					}
					System.err.println(str);
					ex.printStackTrace();
					System.exit(-1);
				}
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return bestMDPActionForState;
	}
	
	public static Map<String,Integer> readMDPActionNameToID(String filename) {
				
		Map<String,Integer> actionNameToID = new HashMap<String,Integer>();
		try {
			Scanner s = new Scanner(new File(filename));
			String actionNames = s.nextLine();
			String[] actionNamesArray = actionNames.split(",");
			for(int i=0;i<actionNamesArray.length;i++){
				actionNameToID.put(actionNamesArray[i], i);
			}
			
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return actionNameToID;
	}
	
	public static Map<State,double[]> readMDPActionDistribution(POSGDomain domain, 
			String agName, StateEnumerator senum, String filename) {
		
		Map<State,double[]> MDPActionDistributionForState = new HashMap<State,double[]>();
		
		try {
			Scanner s = new Scanner(new File(filename));
			s.nextLine();
			while (s.hasNext()){
				String[] sa = s.next().split(":");
				if(Integer.parseInt(sa[0])<senum.numStatesEnumerated()){
					State ss = senum.getStateForEnumerationId(Integer.parseInt(sa[0]));
					String[] aDistArr = sa[2].split(",");
					double[] d = new double[aDistArr.length];
					for(int i=0;i<aDistArr.length;i++){
						if(aDistArr[i]!=null){
							d[i] = Double.parseDouble(aDistArr[i]);
						}
					}
					MDPActionDistributionForState.put(ss,d);
				}
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return MDPActionDistributionForState;
	}
	
	public static Map<State,List<ActionProb>> readMDPStateToActionDistribution(POSGDomain domain, 
			String agName, StateEnumerator senum, String filename) {
		
		Map<State,List<ActionProb>> MDPActionDistributionForState = new HashMap<State,List<ActionProb>>();
		
		try {
			Scanner s = new Scanner(new File(filename));
			String actionNames = s.nextLine();
			String[] actionNamesArray = actionNames.split(",");
			
			while (s.hasNext()){
				String[] sa = s.next().split(":");
				if (sa[0].startsWith("x")) continue;
				if(Integer.parseInt(sa[0])<senum.numStatesEnumerated()){
					State ss = senum.getStateForEnumerationId(Integer.parseInt(sa[0]));
					String[] aDistArr = sa[2].split(",");
					List<ActionProb> apList = new ArrayList<ActionProb>();
	
					for(int i=0;i<aDistArr.length;i++){
						if(aDistArr[i]!=null){
							SGAgentAction sga = new SimpleSGAgentAction(domain, actionNamesArray[i]);  
							ActionProb ap = new ActionProb(sga.getAssociatedGroundedAction(agName), Double.parseDouble(aDistArr[i]));
							apList.add(ap);
						}
					}
					MDPActionDistributionForState.put(ss,apList);
				}
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return MDPActionDistributionForState;
	}
	
	public static List<double[]> readAlphas(String filename) {
		List<double[]> alphaVectors = new ArrayList<double[]>();
		
		List<String> avString = new ArrayList<String>();
		
		try {
			Scanner s = new Scanner(new File(filename));
			
			while (s.hasNext()){
				avString.add(s.next());
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		int numAlphas = avString.size();
		int numStates = avString.get(0).split(",").length;
		
		for(int i=0;i<numAlphas;i++){
			double[] d = new double[numStates];
			String[] dStr = avString.get(i).split(",");
			for(int j=0;j<numStates;j++){
				d[j] = Double.parseDouble(dStr[j]);
			}
			alphaVectors.add(d);
		}
		
//		for(int i=0;i<alphaVectors.size();i++){
//			for(int j=0;j<alphaVectors.get(i).length;j++){
//				System.out.print(alphaVectors.get(i)[j] +" ");
//			}
//			System.out.println();
//		}
		
		return alphaVectors;
		
	}
	
	

	// TODO Main Method 
	public static void main(String[] args) {
		
		//example command line argument
		/*
		 * [mkran@erebus oasys-git]$ java -cp "bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar" iPOMDPLiteSolver/iPBVI 0.9 0.1 24 6 50 1 5 > output/Simulations/Config_1_Results/Experiment_5/RUN_2-WF_config1_iPBVI_experiment5_6h-24iters.txt
		 */
		
		dom dom = iPBVI.dom.WF;
		
		int maxLevel = 1;
		int maxMDPLevel = maxLevel-1;
		double gamma = 0;
		double maxDelta = 0;
		int maxIterations = 0;
		int horizons = 0;
		int maxMDPIter = 0;
		int conf = 0;
		int experiment = 0;
		
		if(args.length == 7){
			gamma = Double.parseDouble(args[0]);
			maxDelta = Double.parseDouble(args[1]);
			maxIterations = Integer.parseInt(args[2]);
			horizons = Integer.parseInt(args[3]);
			maxMDPIter = Integer.parseInt(args[4]);
			conf = Integer.parseInt(args[5]);
			experiment = Integer.parseInt(args[6]);
		}
		
		//Testing arguments
//		System.out.print(gamma + " " + maxDelta + " " + maxIterations + " " + horizons + " " + maxMDPIter + " " + conf + " " + experiment);
		
		
		boolean finite = false;
		boolean readFromFile = true;
		
		State ms = null;
		SGDomain d = null;
		int numPlayers = 0;
		JointReward jr = null;
		TerminalFunction tf = null;
		ObservationFunction of = null;
		Map<Integer, String> agIDtoName = new HashMap<Integer, String>();
		List<Map<String, SGAgentType>> allAgentDefinitions = new ArrayList<Map<String, SGAgentType>>();
//		Map<Integer,Map<Integer,Policy>> otherAgentsFinalPolicyMap = new HashMap<Integer,Map<Integer,Policy>>();
		
		MyTimer timer = new MyTimer();
		MyTimer nmdpPerAgTimer = new MyTimer();
		MyTimer ipbviPerAgTimer = new MyTimer();
		MyTimer ipbviPlannerTimer = new MyTimer();
		MyTimer ipbviInitTimer = new MyTimer();
		MyTimer senumTimer = new MyTimer();
		

		//Now solve other agents' Nested MDP for lower levels: 
			
		timer.start();
		System.out.println("Generating the domain..");
		if(dom.equals(iPBVI.dom.WF)){
			
			System.out.println("Generating the domain..");
			Wildfire wf = new Wildfire();
			d = (WildfireDomain) wf.generateDomain(conf);
			jr = new WildfireJointRewardFunction();
			tf = new NullTermination();
			of = ((WildfireDomain) d).getObservationFunction();
			numPlayers = WildfireParameters.numAgents;
			outFileHead = "output/Simulations/Config_"+WildfireParameters.config+"_Results/Experiment_"+experiment+"/";
			
			
			//1. Get agent definitions
			System.out.println("Getting all agent definitions..");
			allAgentDefinitions = Wildfire.getAllAgentDefs((WildfireDomain) d);
			
			//2. Get agent id-name mapping
			System.out.println("Getting agent id-name mappings..");
			for(int n=0;n<numPlayers;n++){
				agIDtoName.put(n, Wildfire.agentName(n));
			}
			
			//3. Get initial state
			ms = Wildfire.getInitialMasterState((WildfireDomain) d);
			
			for(int currentAgentIndex = 0; currentAgentIndex < numPlayers; currentAgentIndex++){
//				resetPlannerResults();
				System.out.println("---------------------------------------------------");
				System.out.println("                  Subject agent: "+currentAgentIndex);
				System.out.println("---------------------------------------------------");
				
				nmdpPerAgTimer.start();
				
				//1. find neighbors of subject agent
				int[] neighbors = ((WildfireDomain) d).neighbors[currentAgentIndex];
				int numNeighbors = neighbors.length;
				
				List<Integer> agentsInWorld = new ArrayList<Integer>();
				agentsInWorld.add(currentAgentIndex);
				for(int i=0;i<numNeighbors;i++){
					agentsInWorld.add(neighbors[i]);
				}
				int numAgentsInSubjectAgentWorld = agentsInWorld.size();
				System.out.println("[Subject Agent "+currentAgentIndex+"] Number of agents in world: "+ numAgentsInSubjectAgentWorld);
				
				//2. set agent definitions of agents in the local neighborhood of subject agent
				System.out.println("[Subject Agent "+currentAgentIndex+"] Defining all agents in world..");
				Map<String, SGAgentType> agentDefinitions = allAgentDefinitions.get(currentAgentIndex);
				
				//3. create state of subject agent from master state
				System.out.println("[Subject Agent "+currentAgentIndex+"] Getting initial state..");
				State si = Wildfire.createAgentStateFromMasterState((WildfireDomain) d, currentAgentIndex, ms);
				
				//4. Setting state enumerator for subject agent's world..
				System.out.println("[Subject Agent "+currentAgentIndex+"] Setting state enumerator for world..");
				HashableStateFactory hf = new SimpleHashableStateFactory(true,false);
				((WildfireDomain) d).setStateEnumerator(new StateEnumerator(d, hf, agentDefinitions));
				
				System.out.println("[Subject Agent "+currentAgentIndex+"] Getting state enumerator for world..");
				StateEnumerator senum = ((POSGDomain) d).getStateEnumerator();
				
				//5. Caching all enumerated states..
				senumTimer.start();
				Set<HashableState> hstates = new HashSet<HashableState>();
				states = new ArrayList<State>();
				
				System.out.print("[Subject Agent "+currentAgentIndex+"] Enumerating all reachable states..");
				if(finite){
					senum.findReachableStatesAndEnumerate(si,maxIterations); //remove horizons later
				}else{
					senum.findReachableStatesAndEnumerate(si);
				}
				int nS = senum.numStatesEnumerated();
				for(int i=0;i<nS;i++){
					if(i%1000 == 0){
						System.out.print(".");
					}
					State st = senum.getStateForEnumerationId(i);
					states.add(st);
					HashableState hashedST = hf.hashState(st);
					hstates.add(hashedST);
				}
				senumTimer.stop();
				System.out.println("..[Done] in "+senumTimer.getTime()+"s; ");
				System.out.println("[Subject Agent "+currentAgentIndex+"] Unique states found: "+hstates.size());
				
				Map<Integer,Policy> neighborPolicies = new HashMap<Integer,Policy>();
				
				for(int neighborAgent: neighbors){
					int neighborType = ((WildfireDomain) d).agentTypes[neighborAgent]; 
					System.out.println("---------------------------------------------------");
					System.out.println("[Subject Agent "+currentAgentIndex+"] [Neighbor "+neighborAgent+"] Getting neighbor's local state from world..");

					if(readFromFile){
						String filename = outFileHead + WildfireParameters.numAgents+"-WF_config"+WildfireParameters.config+"_firestates"+Wildfire.NUM_FIRE_STATES+"_nmdp_policy_level"+maxMDPLevel+"_agent"+currentAgentIndex+"_neighbor"+neighborAgent+"-"+neighborType+"_iter"+maxMDPIter+".txt";
						System.out.println("[Subject Agent "+currentAgentIndex+"] [Neighbor "+neighborAgent+"] Reading neighbor's policy from "+filename); 
						String actingAgent = agIDtoName.get(neighborAgent);
						Policy nPol = new PolicyFromFile(d, actingAgent, senum, filename);
						neighborPolicies.put(neighborAgent,nPol);
						System.out.println("[Subject Agent "+currentAgentIndex+"] [Neighbor "+neighborAgent+"] Finished loading neighbor's policy; Maps "+hstates.size()+" states to a distribution over "+nPol.getActionDistributionForState(si).size()+" actions!");
					}
					else{
						State s_i = Wildfire.createOtherAgentStateFromLocalState((WildfireDomain) d,neighborAgent, si);
						System.out.println("Planning from neighbor agent "+neighborAgent+"'s local state.."); 
						System.out.println("Number of enumerated states: "+senum.numStatesEnumerated());
						Policy nPol = NestedVI.runVI(s_i,d,senum,agentDefinitions,agIDtoName,jr,tf,hf,
								gamma,maxDelta,maxIterations,maxMDPLevel,agentsInWorld,neighborAgent,finite);
						neighborPolicies.put(neighborAgent,nPol);
					}
				}
				
				nmdpPerAgTimer.stop();
				System.out.println("---------------------------------------------------");
				
				//Next solve subject agents' iPBVI for maxLevel: 
				ipbviPerAgTimer.start();
				System.out.println("[Subject Agent "+currentAgentIndex+"] Initializing iPBVI-Lite planner..");
				System.out.println("---------------------------------------------------");
				ipbviInitTimer.start();
				
				//4. Get Joint Actions and cache it 
				System.out.println("[Subject Agent "+currentAgentIndex+"] Getting joint actions..");
				List<JointAction> allJAs = JointAction.getAllJointActions(si, agentDefinitions);
				for(int jaIndex=0;jaIndex<allJAs.size();jaIndex++){
					allJAsMap.put(allJAs.get(jaIndex),jaIndex); 
				}
				
				//1. Get initial belief state
				System.out.println("[Subject Agent "+currentAgentIndex+"] Getting initial belief state..");
				TabularBeliefState tbs = Wildfire.getInitialBeliefState((WildfireDomain) d, currentAgentIndex, senum);
					
				String currentAgentName = Wildfire.agentName(currentAgentIndex);
				
				//3. Initialize planner
				iPBVI pVI = new iPBVI((POSGDomain)d,jr,tf,of,gamma,
						agentDefinitions,agIDtoName,allJAs,tbs,neighborPolicies,horizons,maxIterations,
						currentAgentIndex,currentAgentName,maxLevel,agentsInWorld);
			
				ipbviInitTimer.stop();
				System.out.println("[Subject Agent "+currentAgentIndex+"] Finished initializing planner in "+ipbviInitTimer.getTime()+"s..");	
				System.out.println("---------------------------------------------------");
				
				System.out.println("[Subject Agent "+currentAgentIndex+"] iPBVI-Lite planning begins..");
				ipbviPlannerTimer.start();
				
				pVI.planFromBeliefState(tbs);
				
				ipbviPlannerTimer.stop();
				System.out.println("[Subject Agent "+currentAgentIndex+"] iPBVI-Lite planning ends in "+ipbviPlannerTimer.getTime()+"s..");
				
				ipbviPerAgTimer.stop();
				System.out.println("---------------------------------------------------");
				System.out.println("Computed agent "+currentAgentIndex+"'s level-"+maxLevel+" iPBVI policy in "+ipbviPerAgTimer.getTime()+"s");
				System.out.println("---------------------------------------------------");
				
				
//				otherAgentsFinalPolicyMap.put(currentAgentIndex,neighborPolicies);	
			}
			
			timer.stop();
			System.out.println("Total Time: "+timer.getTime()+"s..");
		
	
			//For Subject Agent's cognitive simulator:
//			if(cogSimulator){
//				System.out.println("cognitive simulation to verify Observation 1 is starting..");
//				//Works for 2-agents
//				int[] obsIndex = {1,1,1,1,1,2,2,2,2,2};
//				int numStep = 10;
//				int subjectAgentID = 0;
//				String subjectAgName = Wildfire.agentName(subjectAgentID);
//				int otherAgentIndex = 1;
//				TabularBeliefState curB = Wildfire.getInitialBeliefState((WildfireDomain) d, subjectAgentID);
//				State curMS = Wildfire.getInitialMasterState((WildfireDomain) d);
//				System.out.println("created initial states from domain..");
//				
////				BeliefMAMDPPolicyAgent PBVIagent = new BeliefMAMDPPolicyAgent((POSGDomain) d, subjectAgName, otherAgentsFinalPolicyMap.get(subjectAgentID),pPBVI);
//				System.out.println("initialized pbvi agent..");
//				
//				Policy oagPolicy = otherAgentsFinalPolicyMap.get(subjectAgentID).get(otherAgentIndex);
//				SetStrategyAgentFactory agentFactory = new SetStrategyAgentFactory(d, oagPolicy);
//				SGAgent MDPagent = agentFactory.generateAgent();
//				System.out.println("initialized mdp agent..");
//				
//				POWorld pow = new POWorld((POSGDomain) d, jr, tf, of, agentDefinitions, curMS);
////				PBVIagent.joinWorld(pow, pow.getAgentDefinitions().get(agIDtoName.get(subjectAgentID)));
//				MDPagent.joinWorld(pow, pow.getAgentDefinitions().get(agIDtoName.get(otherAgentIndex)));
//				System.out.println("added "+pow.getRegisteredAgents().size()+" agents to the POWorld to simulate..");
//				
//				System.out.println("verifying observation 1..starting..");
////				verifyObs1(pow, (WildfireDomain) d, curB, curMS, oagPolicy, pPBVI, 
////						obsIndex, numStep, subjectAgName, subjectAgentID, true);
//				
//				System.out.println("cognitive simulation to verify Observation 1 is done!!");
//			}
		}
	}
			

	public void printArrayVector(double [] b){
		for(int i = 0; i < b.length; i++){
			System.out.print(" "+b[i]);
		}
		System.out.println();
	}
	
	public void printListVector(ArrayList<Double> b){
		for(int i = 0; i < b.size(); i++){
			System.out.print(" "+b.get(i));
		}
		System.out.println();
	}
	
	public void printListBeliefVector(Set<HashableState> bsList){
		for(HashableState bs:bsList){
			printBeliefVector((TabularBeliefState) bs.s);
		}
	}
	
	public void printListBeliefVector(ArrayList<TabularBeliefState> bsList){
		for(int i = 0; i < bsList.size(); i++){
			printBeliefVector(bsList.get(i));
		}
	}
	
	public static void printBeliefVector(TabularBeliefState bs){
		double [] b = bs.getBeliefVector();
		System.out.print("b["+b.length+"]");
		for(int i = 0; i < b.length; i++){
			System.out.print(" "+b[i]);
		}
		System.out.println();
	}
	
	public static void printAgentPolicyForState(int sID, StateEnumerator senum, Policy agPolicy) {
		System.out.print("State ["+sID+"]  ");
		State s = senum.getStateForEnumerationId(sID);
		for (int a=0; a<agPolicy.getActionDistributionForState(s).size(); a++){
			System.out.print(" "+agPolicy.getActionDistributionForState(s).get(a).pSelection);
		}
		System.out.println();
	}

	@SuppressWarnings("unused")
	private static void printMap2(Map<JointAction, Integer> allJAsMap) {
		System.out.println("Printing map entries..");
		for(Entry<JointAction, Integer> e:allJAsMap.entrySet()){
			System.out.println(e.getKey().actionName()+" - "+e.getValue());
		}
		System.out.println();
		
	}

	public class JointActionTransitions{
		public JointAction ja;
		public List<TransitionProbability> tps;
		public List<Map<String, Double>> jrs;
		
		/**
		 * Generates the transition information for the given state and joint aciton
		 * @param s the state in which the joint action is applied
		 * @param ja the joint action applied to the given state
		 */
		public JointActionTransitions(State s, JointAction ja){
			this.ja = ja;
			this.tps = jaModel.transitionProbsFor(s, ja);
			this.jrs = new ArrayList<Map<String,Double>>(this.tps.size());
			for(TransitionProbability tp : this.tps){
				Map<String, Double> jrr = jr.reward(s, ja, tp.s);
				this.jrs.add(jrr);
			}
		}	
	}
	
	public void setAgentDefinitions(Map<String, SGAgentType> agentDefs){
		
		if(this.planningStarted){
			throw new RuntimeException("Cannot reset the agent definitions after planning has already started.");
		}
		
		if(agentDefs == null){
			System.out.println("[FYI] Agent Definitions: Null");
			return;
		}
		
		if(agentDefinitions == agentDefs){
			return ;
		}
		
		agentDefinitions = agentDefs;
	}
	
	@SuppressWarnings("unused")
	private void normalizeVector(List<Double> inputVector){
		double sum=0.0;
		for(Double d:inputVector){
			sum+=d;
		}
		if(sum==0.0){
			System.err.println("PBVI: normalizeVector : normalize sum of beliefs was zero");
			for(int i=0;i< inputVector.size();i++){
				inputVector.set(i,1.0/inputVector.size());
			}
		}
		else{
			for(int i=0;i< inputVector.size();i++){
				inputVector.set(i,inputVector.get(i)/sum);
			}
		}

	}

	@SuppressWarnings("unused")
	private boolean vectorsEqual(List<Double> v1, List<Double> v2){
		if(v1.size()==v2.size()){
			for(int i =0; i<v1.size();i++){
				if(v1.get(i)!=v2.get(i)){
					return false;
				}
			}
			return true;
		}
		return false;
	}

	private ArrayList<Double> convertToJavaList(double[] inputList){
		ArrayList<Double> outputList = new ArrayList<Double>();
		for(double d : inputList){
			outputList.add(d);
		}
		return outputList;
	}

	@Override
	public void resetSolver() {
		
	}

}
