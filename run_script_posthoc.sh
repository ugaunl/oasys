#!/bin/bash

declare -a confArray
confArray=("2")

declare -a exptArray
exptArray=("30")

declare -a agentArray
agentArray=("3")

declare -a estimateTFArray
estimateTFArray=("false")

NUMEXPTS=${#confArray[@]}

for ((i=0;i<$NUMEXPTS;i++)); do

CONF=${confArray[i]}
EXPT=${exptArray[i]}
NUMAGENTS=${agentArray[i]}
ESTIMATETF=${estimateTFArray[i]}

GAMMA="0.9"
MAXDELTA="0.1"
MAXMDPITERS="50"
MAXITERATIONS="24"
HORIZONS="6"
MAXSTAGES="100"
MAXTRIALS="30"
TEMPVAR="2"

CLSPTH="bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar"
DIRSIM="output/Simulations"
DIRCONF="$DIRSIM/Config_"$CONF"_Results"
DIREXPT="$DIRCONF/Experiment_$EXPT"
DIRDUMPS="$DIREXPT/Dumps"
DIRTEMP="$DIRDUMPS/Temp"

clear
echo "Running $NUMAGENTS-agent Wildfire Domain - Experiment $EXPT, Config $CONF"

echo "Creating required directories.."
if [ ! -d "$DIRSIM" ]; then
  mkdir $DIRSIM
fi
if [ ! -d "$DIRCONF" ]; then
  mkdir $DIRCONF
fi
if [ ! -d "$DIREXPT" ]; then
  mkdir $DIREXPT
fi
if [ ! -d "$DIRDUMPS" ]; then
  mkdir $DIRDUMPS
fi
if [ ! -d "$DIRTEMP" ]; then
  mkdir $DIRTEMP
fi

echo "Starting nestedMDP planning..with params: $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT $ESTIMATETF"
java -cp $CLSPTH nestedMDPSolver/NestedVI $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT $ESTIMATETF > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_NestedMDP_experiment"$EXPT"_"$MAXMDPITERS"iters_estimateTF-"$ESTIMATETF".txt

echo "Starting iPBVI planning..with params: $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT $ESTIMATETF"
java -cp $CLSPTH iPOMDPLiteSolver/iPBVI $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT $ESTIMATETF > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_iPBVI_experiment"$EXPT"_"$HORIZONS"h-"$MAXITERATIONS"iters_estimateTF-"$ESTIMATETF".txt

echo "Starting iPBVI simulations..with params: $MAXSTAGES $MAXTRIALS $CONF $EXPT $TEMPVAR $ESTIMATETF"
java -cp $CLSPTH simulators/PBVISimulator $MAXSTAGES $MAXTRIALS $CONF $EXPT $TEMPVAR $ESTIMATETF > $DIREXPT/Simulation_"$NUMAGENTS"-WF_config"$CONF"_experiment"$EXPT"_"$MAXSTAGES"steps_"$MAXTRIALS"trials_estimateTF-"$ESTIMATETF".txt

done