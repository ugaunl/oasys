#!/bin/bash

declare -a confArray
confArray=("1")

declare -a exptArray
exptArray=("1")

declare -a agentArray
agentArray=("2")

#declare -a estimateTFArray
#estimateTFArray=("true")

NUMEXPTS=${#confArray[@]}

for ((i=0;i<$NUMEXPTS;i++)); do

CONF=${confArray[i]}
EXPT=${exptArray[i]}
NUMAGENTS=${agentArray[i]}
#ESTIMATETF=${estimateTFArray[i]}

GAMMA="0.9"
MAXDELTA="0.1"
MAXMDPITERS="2"
MAXITERATIONS="2"
HORIZONS="2"
MAXSTAGES="2"
MAXTRIALS="2"
TEMPVAR="2"

CLSPTH="bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar"
DIRSIM="output/Simulations"
DIRCONF="$DIRSIM/Config_"$CONF"_Results"
DIREXPT="$DIRCONF/Experiment_$EXPT"
DIRDUMPS="$DIREXPT/Dumps"
DIRTEMP="$DIRDUMPS/Temp"

clear
echo "Running $NUMAGENTS-agent Wildfire Domain - Experiment $EXPT, Config $CONF"

echo "Creating required directories.."
if [ ! -d "$DIRSIM" ]; then
  mkdir $DIRSIM
fi
if [ ! -d "$DIRCONF" ]; then
  mkdir $DIRCONF
fi
if [ ! -d "$DIREXPT" ]; then
  mkdir $DIREXPT
fi
if [ ! -d "$DIRDUMPS" ]; then
  mkdir $DIRDUMPS
fi
if [ ! -d "$DIRTEMP" ]; then
  mkdir $DIRTEMP
fi

ESTIMATETF="true"
echo "Estimating Transition Function.." 
echo "Starting nestedMDP-Predictive planning..with params: $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT $ESTIMATETF"
java -cp $CLSPTH nestedMDPSolver/NestedVI $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT $ESTIMATETF > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_NestedMDP_experiment"$EXPT"_"$MAXMDPITERS"iters_estimateTF-"$ESTIMATETF".txt

echo "Starting iPBVI-Predictive planning..with params: $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT $ESTIMATETF"
java -cp $CLSPTH iPOMDPLiteSolver/iPBVI $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT $ESTIMATETF > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_iPBVI_experiment"$EXPT"_"$HORIZONS"h-"$MAXITERATIONS"iters_estimateTF-"$ESTIMATETF".txt

echo "Starting iPBVI-Predictive simulations..with params: $MAXSTAGES $MAXTRIALS $CONF $EXPT $TEMPVAR $ESTIMATETF"
java -cp $CLSPTH simulators/PBVISimulator $MAXSTAGES $MAXTRIALS $CONF $EXPT $TEMPVAR $ESTIMATETF > $DIREXPT/Simulation_"$NUMAGENTS"-WF_config"$CONF"_experiment"$EXPT"_"$MAXSTAGES"steps_"$MAXTRIALS"trials_estimateTF-"$ESTIMATETF".txt


ESTIMATETF="false"
echo "Using Presumed Transition Function.." 
echo "Starting nestedMDP-Posthoc planning..with params: $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT $ESTIMATETF"
java -cp $CLSPTH nestedMDPSolver/NestedVI $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT $ESTIMATETF > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_NestedMDP_experiment"$EXPT"_"$MAXMDPITERS"iters_estimateTF-"$ESTIMATETF".txt

echo "Starting iPBVI-Posthoc planning..with params: $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT $ESTIMATETF"
java -cp $CLSPTH iPOMDPLiteSolver/iPBVI $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT $ESTIMATETF > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_iPBVI_experiment"$EXPT"_"$HORIZONS"h-"$MAXITERATIONS"iters_estimateTF-"$ESTIMATETF".txt

echo "Starting iPBVI-Posthoc simulations..with params: $MAXSTAGES $MAXTRIALS $CONF $EXPT $TEMPVAR $ESTIMATETF"
java -cp $CLSPTH simulators/PBVISimulator $MAXSTAGES $MAXTRIALS $CONF $EXPT $TEMPVAR $ESTIMATETF > $DIREXPT/Simulation_"$NUMAGENTS"-WF_config"$CONF"_experiment"$EXPT"_"$MAXSTAGES"steps_"$MAXTRIALS"trials_estimateTF-"$ESTIMATETF".txt

echo "Starting NOOP simulations..with params: $MAXSTAGES $MAXTRIALS $CONF $EXPT $ESTIMATETF"
java -cp $CLSPTH simulators/NoopBaselineForWildfire $MAXSTAGES $MAXTRIALS $CONF $EXPT $ESTIMATETF

echo "Starting Random simulations..with params: $MAXSTAGES $MAXTRIALS $CONF $EXPT $ESTIMATETF"
java -cp $CLSPTH simulators/RandomBaselineForWildfire $MAXSTAGES $MAXTRIALS $CONF $EXPT $ESTIMATETF

echo "Starting Heuristic simulations..with params: $MAXSTAGES $MAXTRIALS $CONF $EXPT $ESTIMATETF"
java -cp $CLSPTH simulators/HeuristicBaselineForWildfire $MAXSTAGES $MAXTRIALS $CONF $EXPT $ESTIMATETF

ESTIMATETF="true"
echo "Accumulating all simulation results.."
java -cp $CLSPTH results/TimeSeriesParser $CONF $EXPT $ESTIMATETF

zip -r $DIRCONF/$NUMAGENTS-WF_Expt"$EXPT"_Config"$CONF".zip $DIREXPT

FILE2ATTACH="$DIRCONF/$NUMAGENTS-WF_Expt"$EXPT"_Config"$CONF".zip"

from="mkran@uga.edu"
to="mkran1985@gmail.com"
subject="$NUMAGENTS-WF_Expt"$EXPT"_Config"$CONF""
body="Please find the results attached..Muthu"
declare -a attachments
attachments=( "$FILE2ATTACH" )
 
declare -a attargs
for att in "${attachments[@]}"; do
  attargs+=( "-a"  "$att" )  
done
 
mail -s "$subject" -r "$from" "${attargs[@]}" "$to" <<< "$body"

done

