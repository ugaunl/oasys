#!/bin/bash

EXPT="3"
CONF="1"
NUMAGENTS="2"

GAMMA="0.9"
MAXDELTA="0.1"
MAXMDPITERS="50"
MAXITERATIONS="24"
HORIZONS="6"
MAXSTAGES="100"
MAXTRIALS="30"
TEMPVAR="5"

CLSPTH="bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar"
DIRSIM="output/Simulations"
DIRCONF="$DIRSIM/Config_"$CONF"_Results"
DIREXPT="$DIRCONF/Experiment_$EXPT"
DIRDUMPS="$DIREXPT/Dumps"
DIRTEMP="$DIRDUMPS/Temp"

clear
echo "Running $NUMAGENTS-agent Wildfire Domain - Experiment $EXPT, Config $CONF"

echo "Creating required directories.."
if [ ! -d "$DIRSIM" ]; then
  mkdir $DIRSIM
fi
if [ ! -d "$DIRCONF" ]; then
  mkdir $DIRCONF
fi
if [ ! -d "$DIREXPT" ]; then
  mkdir $DIREXPT
fi
if [ ! -d "$DIRDUMPS" ]; then
  mkdir $DIRDUMPS
fi
if [ ! -d "$DIRTEMP" ]; then
  mkdir $DIRTEMP
fi

#echo "Starting nestedMDP planning..with params: $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT"
#java -cp $CLSPTH nestedMDPSolver/NestedVI $GAMMA $MAXDELTA $MAXMDPITERS $CONF $EXPT > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_NestedMDP_experiment"$EXPT"_"$MAXMDPITERS"iters.txt

#echo "Starting iPBVI planning..with params: $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT"
#java -cp $CLSPTH iPOMDPLiteSolver/iPBVI $GAMMA $MAXDELTA $MAXITERATIONS $HORIZONS $MAXMDPITERS $CONF $EXPT > $DIREXPT/RUN_"$NUMAGENTS"-WF_config"$CONF"_iPBVI_experiment"$EXPT"_"$HORIZONS"h-"$MAXITERATIONS"iters.txt

#echo "Starting simulations..with params: $MAXSTAGES $MAXTRIALS $CONF $EXPT $TEMPVAR"
#java -cp $CLSPTH simulators/PBVIPosthocSimulator $MAXSTAGES $MAXTRIALS $CONF $EXPT $TEMPVAR > $DIREXPT/Simulation_"$NUMAGENTS"-WF_config"$CONF"_experiment"$EXPT"_"$MAXSTAGES"steps_"$MAXTRIALS"trials.txt

echo "Starting NOOP simulations..with params: $MAXSTAGES $MAXTRIALS $CONF $EXPT"
java -cp $CLSPTH simulators/NoopBaselineForWildfire $MAXSTAGES $MAXTRIALS $CONF $EXPT

echo "Accumulating simulation results.."
java -cp $CLSPTH results/TimeSeriesParser $CONF $EXPT

zip -r $DIRCONF/$NUMAGENTS-WF_Expt"$EXPT"_Config"$CONF".zip $DIREXPT

FILE2ATTACH="$DIRCONF/$NUMAGENTS-WF_Expt"$EXPT"_Config"$CONF".zip"

from="mkran@uga.edu"
to="mkran1985@gmail.com"
subject="$NUMAGENTS-WF_Expt"$EXPT"_Config"$CONF""
body="Please find the results attached..Muthu"
declare -a attachments
attachments=( "$FILE2ATTACH" )
 
declare -a attargs
for att in "${attachments[@]}"; do
  attargs+=( "-a"  "$att" )  
done
 
mail -s "$subject" -r "$from" "${attargs[@]}" "$to" <<< "$body"



