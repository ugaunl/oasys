#!/bin/bash

declare -a confArray
confArray=("1" "11" "12" "2" "21" "22")

declare -a agentArray
agentArray=("2" "2" "2" "3" "3" "3")

#declare -a estimateTFArray
#estimateTFArray=("true")

NUMEXPTS=${#confArray[@]}

for ((i=0;i<$NUMEXPTS;i++)); do

CONF=${confArray[i]}
NUMAGENTS=${agentArray[i]}
#ESTIMATETF=${estimateTFArray[i]}

K="500"
MAXSTAGES="100"
MAXTRIALS="30"
ESTIMATETF="false"

CLSPTH="bin/:lib/burlap.jar:lib/libpomdp.jar:external/*.jar"
DIRTF="output/FinalTransitionFns"
DIRCONF="$DIRTF/Config_"$CONF""

clear
echo "Estimating Transition Fn for $NUMAGENTS-agent Wildfire Domain - Experiment $EXPT, Config $CONF"

echo "Creating required directories.."
if [ ! -d "$DIRTF" ]; then
  mkdir $DIRTF
fi
if [ ! -d "$DIRCONF" ]; then
  mkdir $DIRCONF
fi

echo "Starting TF Estimation..with params: $K $MAXSTAGES $MAXTRIALS $CONF $ESTIMATETF"
java -cp $CLSPTH simulators/SimulatorForTransitionFnEstimation $K $MAXSTAGES $MAXTRIALS $CONF $ESTIMATETF > $DIRCONF/RUN_"$NUMAGENTS"-WF_config"$CONF"_estimatedTransitionFn_"$K"_"$MAXSTAGES"_"$MAXTRIALS"_pretty_print.txt

done

